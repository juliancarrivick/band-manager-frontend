#!/bin/bash

container_name="orchestrate-postgres"
POSTGRES_DB="${POSTGRES_DB:=postgres}"
POSTGRES_USER="${POSTGRES_USER:=postgres}"
POSTGRES_PASSWORD="${POSTGRES_PASSWORD:=postgres}"
database_port="5432"
postgres_version="16.4"
volume_name="orchestrate-postgres-data"

# Set in devcontainer.json
if [[ -n "$IS_DEVCONTAINER" ]]; then
    echo "Running in Devcontainer, not continuing"
    exit 0
fi

# Check if Podman is available
if command -v podman &> /dev/null; then
    container_runtime="podman"
# Check if Docker is available
elif command -v docker &> /dev/null; then
    container_runtime="docker"
else
    echo "Error: Neither Podman nor Docker is installed on the system."
    exit 1
fi

if "$container_runtime" ps -a --format '{{.Names}}' | grep -q "$container_name"; then
    if "$container_runtime" ps --format '{{.Names}}' | grep -q "$container_name"; then
        echo "Container '$container_name' is already running."
        exit 0
    fi

    "$container_runtime" start "$container_name"
    if [ "$?" -eq 0 ]; then
        echo "Container '$container_name' has been started."
        exit 0
    fi

    echo "Error: Failed to start container '$container_name'."
    exit 1
fi

# Check if the volume exists, and if not, create it
if ! "$container_runtime" volume inspect "$volume_name" &> /dev/null; then
    "$container_runtime" volume create "$volume_name"
fi

# Run PostgreSQL container
"$container_runtime" run -d \
    --name "$container_name" \
    -e POSTGRES_DB="$POSTGRES_DB" \
    -e POSTGRES_USER="$POSTGRES_USER" \
    -e POSTGRES_PASSWORD="$POSTGRES_PASSWORD" \
    -p "$database_port:5432" \
    -v "$volume_name:/var/lib/postgresql/data" \
    "docker.io/postgres:$postgres_version"

if [ "$?" -eq 0 ]; then
    echo "PostgreSQL container '$container_name' is now running in the background."
else
    echo "Error: Failed to start PostgreSQL container."
    exit 1
fi
