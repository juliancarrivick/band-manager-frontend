## Checklist

-   [ ] Bumped version in `OrchestrateApi.csproj`
-   [ ] Bumped version in `package.json` & `npm install` to bump `package-lock.json`
-   [ ] Updated change date in `LICENSE`
-   [ ] Updates to `releases.ts`
    -   [ ] Release description contains all relevant notes
    -   [ ] Release date is correct
    -   [ ] New development version added
-   [ ] Has Stripe been updated? Check pinned SDK API version & version in use on Stripe
