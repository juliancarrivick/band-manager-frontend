#!/bin/bash

# Ensure you have installed [flyctl](https://fly.io/docs/flyctl/installing/), set up the
# [Wireguard VPN](https://fly.io/docs/reference/private-networking/#creating-your-tunnel-configuration)
# and connected to it prior to running this script.

# This script does not perform an unattended setup.
# Make sure you run it from the api/ folder

APP_NAME=orchestrate-api
APP_DOMAIN=api.orchestrate.community
APP_DB=orchestrate_api # By default Fly replaces - with _, but we'll be explicit here
APP_DB_USER=$APP_DB
DB_APP_NAME=orchestrate-db
SECRETS=( \
    AppConfiguration__FrontEndUri=https://app.orchestrate.community \
    JWT__Secret=xxx \
    JWT__ValidIssuer=https://$APP_DOMAIN \
    # Others available, see AppConfiguration.cs
)
DB_DUMP=./orchestrate.dump

fly launch --copy-config --name $APP_NAME

# Seperate secrets by space, from https://stackoverflow.com/a/9429887
JOINED_SECRETS=$(IFS=" "; echo "${SECRETS[*]}")
fly secrets set --app $APP_NAME $JOINED_SECRETS

fly postgres create --name $DB_APP_NAME
fly postgres attach --app $APP_NAME --postgres-app $DB_APP_NAME --database-name $APP_DB

echo -n Postgres password (as output above):
read -s POSTGRES_PASSWORD
echo # Force new line for Postgres output
export PGPASSWORD="$POSTGRES_PASSWORD"

pg_restore -h $DB_APP_NAME.internal -U postgres -d $APP_DB $DB_DUMP
psql -h $DB_APP_NAME.internal -U postgres -d $APP_DB -c "ALTER SCHEMA public OWNER TO $APP_DB_USER;"
psql -h $DB_APP_NAME.internal -U postgres -d $APP_DB -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO $APP_DB_USER;"

fly certs create --app $APP_NAME $APP_DOMAIN

PRIMARY_REGION=$(awk -F "=" '/primary_region/ {print $2}' fly.toml | cut -d '"' -f 2)
# This won't work if the database hasn't had the schema applied yet
fly machine run . run-periodic-jobs --schedule=hourly --restart=on-fail --vm-memory 512 --app=$APP_NAME --region=$PRIMARY_REGION --metadata role=periodic-job-runner

# You are now ready to deploy if you like
# fly deploy --app $APP_NAME
