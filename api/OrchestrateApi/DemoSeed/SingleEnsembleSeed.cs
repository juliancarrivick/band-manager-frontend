using System.Globalization;
using Bogus;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MailingLists;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.DemoSeed;

public class SingleEnsembleSeed(
    OrchestrateContext DbContext,
    ITenantService TenantService,
    MemberBillingPeriodService BillingPeriodService)
{
    private const string Abbreviation = "community-music-inc";

    private OrchestrateFakes Fake { get; } = new OrchestrateFakes(DateTime.UtcNow);

    private Randomizer Random => Fake.Random;
    private Bogus.DataSets.Date BogusDate => Fake.Date;

    public async Task<Tenant> SeedAsync(CancellationToken cancellationToken = default)
    {
        var tenant = await DbContext.Tenant
            .IgnoreQueryFilters()
            .SingleOrDefaultAsync(e => e.Abbreviation == Abbreviation, cancellationToken);
        if (tenant != null)
        {
            DbContext.Remove(tenant);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        tenant = new Tenant
        {
            Name = "Community Music Inc",
            Abbreviation = Abbreviation,
            TimeZone = "Australia/Perth",
            DateFounded = Fake.Date.Past(5, DateTime.UtcNow.AddYears(-OrchestrateFakes.TenantAgeYears)),
            DateRegistered = DateTime.UtcNow.AddYears(-OrchestrateFakes.TenantAgeYears),
            Plan =
            {
                Type = PlanType.Demo,
            },
        };
        var logoFile = new EmbeddedFile(typeof(IInvoiceRenderer), "Resources.GroupLogoPlaceholder.png");
        tenant.Logo = logoFile.AsByteArray();

        DbContext.Add(tenant);
        await DbContext.SaveChangesAsync(cancellationToken);

        using var tenantOverride = TenantService.ForceOverrideTenantId(tenant.Id);

        var scores = Add(Fake.Score.GenerateBetweenPerTenantAgeYears(15, 25));
        var members = Add(Fake.Member.GenerateBetweenPerTenantAgeYears(15, 30));
        AddAssets(members);

        var ensemble = AddEnsemble(members);
        AddConcerts(ensemble, members, scores);

        await AddMemberBillingAsync(members, cancellationToken);

        var contacts = Add(Fake.Contact.GenerateBetweenPerTenantAgeYears(10, 20));
        AddMailingLists([ensemble], members, contacts);

        await DbContext.SaveChangesAsync(cancellationToken);

        return tenant;
    }

    private void AddAssets(List<Member> members)
    {
        var assets = Add(Fake.Asset.GenerateBetweenPerTenantAgeYears(10, 15));

        var loanableAssets = assets.Where(e => e.IsActiveAt(DateTime.UtcNow)).ToList();
        var loanableMembers = members.Where(e => e.IsActiveAt(DateTime.UtcNow)).ToList();
        Add(Random.ListItems(loanableAssets, 5, 8).Select(e =>
        {
            var member = Random.ListItem(loanableMembers);
            return new AssetLoan
            {
                Asset = e,
                Member = member,
                DateBorrowed = BogusDate.Between(Latest(member.DateJoined, e.DateAcquired), DateTime.UtcNow),
            };
        }));
    }

    private static DateTime Latest(DateTime? first, DateTime? second) => new[] { first, second }.Where(e => e.HasValue).Select(e => e!.Value).Max();

    private Ensemble AddEnsemble(List<Member> members)
    {
        var ensemble = Add(Fake.Ensemble.Generate());
        Add(members.Select(e => new EnsembleMembership
        {
            Ensemble = ensemble,
            Member = e,
            DateJoined = e.DateJoined,
            DateLeft = e.DateLeft,
        }));

        return ensemble;
    }

    private void AddConcerts(Ensemble ensemble, List<Member> members, List<Score> scores)
    {
        var concerts = Add(Fake.Concert.GenerateBetweenPerTenantAgeYears(3, 6));
        foreach (var concert in concerts)
        {
            var performance = Add(new Performance
            {
                Concert = concert,
                Ensemble = ensemble,
            });

            var availableMembers = members.Where(e => e.IsActiveAt(concert.Date)).ToList();
            Add(Random.AlmostAllListItems(availableMembers, Random.Int(1, 4)).Select(e => new PerformanceMember
            {
                Performance = performance,
                Member = e,
            }));

            var availableScores = scores
                .Where(e => !e.DatePurchased.HasValue || e.DatePurchased.Value < concert.Date)
                .ToList();
            Add(Random.ListItems(availableScores, 4, 8).Select(e => new PerformanceScore
            {
                Performance = performance,
                Score = e,
            }));
        }
    }

    private async Task AddMemberBillingAsync(List<Member> members, CancellationToken cancellationToken)
    {
        var billingConfig = Add(Fake.BillingConfig.Generate());

        // To ensure we still generate invoices
        billingConfig.IsEnabled = true;

        for (var i = 1; i < 12; i += 3)
        {
            Add(new MemberBillingPeriodConfig
            {
                Name = $"Term {i / 3 + 1}",
                AutoGenerateMonth = i,
                EndOfPeriodMonth = i + 2,
            });
        }

        // We need everything to be populated so that the billing service can be used
        await DbContext.SaveChangesAsync(cancellationToken);

        var firstMemberJoinDate = members.Select(e => e.DateJoined).OrderBy(e => e).First();
        for (var i = firstMemberJoinDate; i < DateTime.UtcNow; i = i.AddMonths(1))
        {
            var billingPeriod = await BillingPeriodService.TryCreateBillingPeriodAsync(i);
            if (billingPeriod == null)
            {
                continue;
            }

            // Generate all the invoices here
            await BillingPeriodService.AutoAdvanceBillingPeriodsAsync(cancellationToken);

            billingPeriod.Generated = i;
            billingPeriod.NotificationContactNotified = BogusDate.Between(billingPeriod.Due, billingPeriod.Due.AddHours(5));
            billingPeriod.Sent = BogusDate.Between(billingPeriod.Generated.Value, billingPeriod.Generated.Value.AddDays(14));
            billingPeriod.EarlyBirdDue = billingPeriod.Sent.Value.AddDays(billingConfig.EarlyBirdDiscountDaysValid);

            billingPeriod.State = MemberBillingPeriodState.Open;
            if (billingPeriod.Due.AddMonths(1) < DateTime.UtcNow)
            {
                billingPeriod.State = MemberBillingPeriodState.Closed;
                billingPeriod.Closed = BogusDate.Between(billingPeriod.Due.AddDays(7), billingPeriod.Due.AddDays(14));
            }

            var invoices = await DbContext.MemberInvoice
                .Where(e => e.MemberBillingPeriodId == billingPeriod.Id)
                .ToListAsync(cancellationToken);
            foreach (var invoice in invoices)
            {
                invoice.Sent = billingPeriod.Sent;
                invoice.SendStatus = Random.Enum<MemberInvoiceSendStatus>();
                invoice.SendStatusUpdated = BogusDate.Between(invoice.Sent.Value, invoice.Sent.Value.AddHours(4));
                if (Random.Bool(0.95f))
                {
                    invoice.Paid = BogusDate.Between(invoice.Sent.Value.AddDays(3), billingPeriod.Due.AddDays(7));
                }
            }

            // Ensure we have saved so next loop iteration of AutoAdvance doesn't send any emails
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        await DbContext.SaveChangesAsync(cancellationToken);
    }

    private void AddMailingLists(List<Ensemble> ensembles, List<Member> members, List<Contact> contacts)
    {
        const int daysToGenerate = MailingListsPageController.UsageCutOffDaysAgo;
        Faker<MailingListMessageMetadata> messageFaker(MailingList ml) => Fake.Create<MailingListMessageMetadata>()
            .RuleFor(o => o.MailingList, () => ml)
            .RuleFor(o => o.Sent, f => f.Date.Recent(daysToGenerate));

        foreach (var ensemble in ensembles)
        {
            var ml = Add(new MailingList
            {
                Name = ensemble.Name,
                Slug = ensemble.Name.ToLower(CultureInfo.CurrentCulture).Replace(' ', '-'),
                AllowedSenders = AllowedSenders.RecipientsOnly,
                ReplyTo = ReplyStrategy.MailingList,
                Ensembles = [ensemble],
            });
            Add(messageFaker(ml).GenerateBetween(daysToGenerate * 2, daysToGenerate * 4));
        }

        var friends = Add(new MailingList
        {
            Name = "Friends",
            Slug = "friends",
            AllowedSenders = AllowedSenders.GlobalSendOnly,
            ReplyTo = ReplyStrategy.Sender,
            Members = Random.ListItems(members, 6).ToHashSet(),
            Contacts = Random.ListItems(contacts, 8).ToHashSet(),
        });
        Add(messageFaker(friends).GenerateBetween(daysToGenerate / 4, 3 * daysToGenerate / 4));
    }

    private T Add<T>(T entity) => (T)DbContext.Add(entity!).Entity;

    private List<T> Add<T>(IEnumerable<T> entities)
    {
        var entityList = entities.ToList();
        return Add(entityList);
    }

    private List<T> Add<T>(List<T> entities)
    {
        DbContext.AddRange((IEnumerable<object>)entities);
        return entities;
    }
}
