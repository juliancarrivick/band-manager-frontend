using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.Security;

namespace OrchestrateApi.DemoSeed;

[ApiController]
[Route("demo-seed")]
public class DemoSeedController(SingleEnsembleSeed seed) : ControllerBase
{
    [HttpPost("single-ensemble")]
    [IsGlobalAdminAuthorize]
    public async Task<SeedInfoDto> SeedAsync(CancellationToken cancellationToken)
    {
        var tenant = await seed.SeedAsync(cancellationToken);
        return new SeedInfoDto(tenant.Id, tenant.Abbreviation, tenant.Name);
    }
}

public record class SeedInfoDto(Guid Id, string Abbreviation, string Name);
