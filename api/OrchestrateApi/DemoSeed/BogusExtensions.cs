using Bogus;
using Bogus.DataSets;

namespace OrchestrateApi.DemoSeed;

public static class BogusRandomExtensions
{
    public static IList<T> AlmostAllListItems<T>(this Randomizer random, IList<T> items, int excludeCount)
    {
        var count = random.Int(items.Count - excludeCount, items.Count);
        count = Math.Clamp(count, 0, items.Count);
        return random.ListItems(items, count);
    }

    public static IList<T> ListItems<T>(this Randomizer random, IList<T> items, int min, int max)
    {
        var count = random.Int(min, max);
        count = Math.Clamp(count, 0, items.Count);
        return random.ListItems(items, count);
    }
}

public static class BogusMusicExtensions
{
    // Taken from https://github.com/dariusk/corpora/blob/master/data/music/instruments.json
    public static string Instrument(this Music music) => music.Random.ArrayElement(instruments);

    public static string MusicTitle(this Faker faker) => faker.Lorem.Sentence(3);

    public static string AmebGrade(this Music music) => music.Random.ArrayElement(grades);

    public static string EnsembleType(this Music music) => music.Random.ArrayElement(ensembles);

#pragma warning disable format
    private static readonly string[] grades = ["1", "1.5", "2", "2.5", "3", "3.5", "4", "4.5", "5"];
    private static readonly string[] instruments = [
        "Accordian", "Air Horn", "Baby Grand Piano", "Bagpipe", "Banjo", "Bass Guitar", "Bassoon",
        "Bugle", "Calliope", "Cello", "Clarinet", "Clavichord", "Concertina", "Didgeridoo", "Dobro",
        "Dulcimer", "Fiddle", "Fife", "Flugelhorn", "Flute", "French Horn", "Glockenspiel", "Grand Piano",
        "Guitar", "Harmonica", "Harp", "Harpsichord", "Hurdy-gurdy", "Kazoo", "Kick Drum", "Lute", "Lyre",
        "Mandolin", "Marimba", "Mellotran", "Melodica", "Oboe", "Pan Flute", "Piano", "Piccolo", "Pipe Organ",
        "Saxaphone", "Sitar", "Sousaphone", "Tambourine", "Theremin", "Trombone", "Tuba", "Ukulele", "Viola",
        "Violin", "Vuvuzela", "Washtub Bass", "Xylophone", "Zither"
    ];
    private static readonly string[] ensembles = ["Choir", "Big Band", "Concert Band", "Orchestra"];
#pragma warning restore format
}
