using Bogus;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.DemoSeed;

public class OrchestrateFakes(DateTime refDate)
{
    public const int TenantAgeYears = 2;

    public Randomizer Random { get; } = new();

    public Bogus.DataSets.Date Date { get; } = new() { LocalSystemClock = () => refDate };

    public Faker<Asset> Asset { get; } = Create<Asset>(refDate)
        .RuleFor(o => o.Description, f => f.Music.Instrument())
        .RuleFor(o => o.Quantity, f => f.Random.WeightedRandom([1, 2, 3, 4, 5], [0.8f, 0.05f, 0.05f, 0.05f, 0.05f]))
        .RuleFor(o => o.SerialNo, f => f.Random.Replace("???###"))
        .RuleFor(o => o.DateAcquired, f => f.Date.Past(TenantAgeYears).OrNull(f))
        .RuleFor(o => o.ValuePaid, f => f.Random.Decimal(0, 5000).OrNull(f))
        .RuleFor(o => o.DateDiscarded, (f, o) => o.DateAcquired.HasValue
            ? f.Date.Between(o.DateAcquired.Value, DateTime.UtcNow).OrNull(f)
            : null)
        .RuleFor(o => o.ValueDiscarded, (f, o) => o.DateDiscarded.HasValue && o.ValuePaid.HasValue
            ? f.Random.Decimal(0, o.ValuePaid.Value)
            : null)
        .RuleFor(o => o.InsuranceValue, (f, o) => o.ValuePaid.HasValue
            ? f.Random.Decimal(0, o.ValuePaid.Value)
            : f.Random.Decimal(0, 5000))
        .RuleFor(o => o.Notes, f => f.Lorem.Sentence().OrDefault(f, 0.5f, string.Empty));

    public Faker<Score> Score { get; } = Create<Score>(refDate)
        .RuleFor(o => o.Title, f => f.MusicTitle())
        .RuleFor(o => o.Composer, f => f.Person.FullName.OrNull(f, 0.25f))
        .RuleFor(o => o.Arranger, f => f.Person.FullName.OrNull(f, 0.5f))
        .RuleFor(o => o.InLibrary, f => f.Random.Bool())
        .RuleFor(o => o.IsOwned, (f, o) => o.InLibrary && f.Random.Bool())
        .RuleFor(o => o.Duration, f => f.Random.Int(120, 420).OrNull(f))
        .RuleFor(o => o.Grade, f => f.Music.AmebGrade().OrNull(f, 0.75f))
        .RuleFor(o => o.Genre, f => f.Music.Genre().OrNull(f, 0.2f))
        .RuleFor(o => o.DatePurchased, f => f.Date.Past(TenantAgeYears))
        .RuleFor(o => o.ValuePaid, f => f.Random.Decimal(30, 100))
        .RuleFor(o => o.Location, f => f.Address.StreetName().OrNull(f, 0.8f))
        .RuleFor(o => o.Notes, f => f.Lorem.Sentence().OrNull(f, 0.8f));

    public Faker<Member> Member { get; } = Create<Member>(refDate)
        .RuleFor(o => o.FirstName, f => f.Person.FirstName)
        .RuleFor(o => o.LastName, f => f.Person.LastName)
        .RuleFor(o => o.DateJoined, f => f.Date.Past(TenantAgeYears))
        .RuleFor(o => o.DateLeft, (f, o) => f.Date.Between(o.DateJoined, DateTime.UtcNow).OrNull(f, 0.66f))
        .RuleFor(o => o.Details, (f, o) => Create<MemberDetails>(refDate)
            .RuleFor(o => o.Address, f => f.Address.FullAddress())
            .RuleFor(o => o.Email, f => f.Internet.Email(o.FirstName, o.LastName))
            .RuleFor(o => o.PhoneNo, f => f.Person.Phone)
            .RuleFor(o => o.FeeClass, f => f.Random.WeightedRandom(
                new FeeClass[] { FeeClass.Full, FeeClass.Concession, FeeClass.Special }, new[] { 0.8f, 0.15f, 0.05f }))
            .RuleFor(o => o.Notes, f => f.Lorem.Sentence().OrNull(f, 0.8f))
            .Generate())
        .RuleFor(o => o.MemberInstrument, f => Create<MemberInstrument>(refDate)
            .RuleFor(o => o.InstrumentName, f => f.Music.Instrument())
            .GenerateBetween(1, 3));

    public Faker<Ensemble> Ensemble { get; } = Create<Ensemble>(refDate)
        .RuleFor(o => o.Name, f => f.Music.EnsembleType())
        .RuleFor(o => o.Status, () => EnsembleStatus.Active);

    public Faker<Concert> Concert { get; } = Create<Concert>(refDate)
        .RuleFor(o => o.Occasion, f => f.Lorem.Sentence(3))
        .RuleFor(o => o.Date, f => f.Date.Past(TenantAgeYears))
        .RuleFor(o => o.Location, f => f.Address.City())
        .RuleFor(o => o.Notes, f => f.Lorem.Sentence().OrNull(f, 0.25f));

    public Faker<MemberBillingConfig> BillingConfig { get; } = Create<MemberBillingConfig>(refDate)
        .RuleFor(o => o.NotificationContactName, f => f.Person.FullName)
        .RuleFor(o => o.NotificationContactEmail, (f, o) => f.Internet.Email(o.NotificationContactName))
        .RuleFor(o => o.InvoicePrefix, () => "INV")
        .RuleFor(o => o.NextInvoiceNumber, () => 1)
        .RuleFor(o => o.BankName, f => f.Company.CompanyName())
        .RuleFor(o => o.BankAccountName, f => f.Finance.AccountName())
        .RuleFor(o => o.BankBsb, f => f.Finance.RoutingNumber())
        .RuleFor(o => o.BankAccountNo, f => f.Finance.Account())
        .RuleFor(o => o.PaymentInstructions, f => f.Lorem.Sentence())
        .RuleFor(o => o.EarlyBirdDiscountDaysValid, () => 30)
        .RuleFor(o => o.EarlyBirdDiscountDollars, () => 10)
        .RuleFor(o => o.AssociationPeriodFeeDollars, f => f.Random.Int(5, 15) * 10);

    public Faker<Contact> Contact { get; } = Create<Contact>(refDate)
        .RuleFor(o => o.Name, f => f.Person.FullName)
        .RuleFor(o => o.Email, (f, o) => f.Internet.Email(o.Name).OrNull(f, 0.5f))
        .RuleFor(o => o.PhoneNo, f => f.Person.Phone.OrNull(f, 0.5f))
        .RuleFor(o => o.Affiliation, f => f.Company.CompanyName().OrNull(f, 0.25f))
        .RuleFor(o => o.ArchivedOn, f => f.Date.Past(TenantAgeYears).OrNull(f, 0.5f))
        .RuleFor(o => o.Notes, f => f.Lorem.Sentence().OrNull(f, 0.75f));

    public Faker<T> Create<T>() where T : class => Create<T>(refDate);

    public static Faker<T> Create<T>(DateTime refDate) where T : class
        => new Faker<T>().UseDateTimeReference(refDate);
}

public static class OrchestrateFakesExtensions
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1002")]
    public static List<T> GenerateBetweenPerTenantAgeYears<T>(this Faker<T> faker, int min, int max)
        where T : class
    {
        return faker.GenerateBetween(min * OrchestrateFakes.TenantAgeYears, max * OrchestrateFakes.TenantAgeYears);
    }
}

