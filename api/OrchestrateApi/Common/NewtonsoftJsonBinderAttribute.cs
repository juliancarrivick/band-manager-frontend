using System.Buffers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;

namespace OrchestrateApi.Common
{
    // Only add this to support camelCase in patch updates
    // (and so we don't have to add the Content-Type: application/json-patch+json header)
    // Once the migration to breeze has been completed this can be removed
    // Adapted from https://gist.github.com/martijnboland/868e2e13cda1d420afc70059dc7bb24f
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1813")]
    public class NewtonsoftJsonBinderAttribute : ActionFilterAttribute, IControllerModelConvention, IActionModelConvention
    {
        public void Apply(ControllerModel controller)
        {
            foreach (var action in controller.Actions)
            {
                Apply(action);
            }
        }

        public void Apply(ActionModel action)
        {
            var parameters = action.Parameters.Where(p => p.BindingInfo?.BindingSource == BindingSource.Body);
            foreach (var p in parameters)
            {
                p.BindingInfo!.BinderType = typeof(NewtonsoftJsonBodyModelBinder);
            }
        }

        private class NewtonsoftJsonBodyModelBinder : BodyModelBinder
        {
            public NewtonsoftJsonBodyModelBinder(
                ILoggerFactory loggerFactory,
                ArrayPool<char> charPool,
                IHttpRequestStreamReaderFactory readerFactory,
                ObjectPoolProvider objectPoolProvider,
                IOptions<MvcOptions> mvcOptions,
                IOptions<MvcNewtonsoftJsonOptions> jsonOptions)
                : base(GetInputFormatters(loggerFactory, charPool, objectPoolProvider, mvcOptions, jsonOptions), readerFactory)
            {
            }

            private static IInputFormatter[] GetInputFormatters(
                ILoggerFactory loggerFactory,
                ArrayPool<char> charPool,
                ObjectPoolProvider objectPoolProvider,
                IOptions<MvcOptions> mvcOptions,
                IOptions<MvcNewtonsoftJsonOptions> jsonOptions)
            {
                var jsonOptionsValue = jsonOptions.Value;
                return new IInputFormatter[]
                {
                new NewtonsoftJsonInputFormatter(
                    loggerFactory.CreateLogger<NewtonsoftJsonBodyModelBinder>(),
                    jsonOptionsValue.SerializerSettings,
                    charPool,
                    objectPoolProvider,
                    mvcOptions.Value,
                    jsonOptionsValue)
                };
            }
        }
    }
}
