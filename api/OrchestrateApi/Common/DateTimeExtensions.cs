namespace OrchestrateApi.Common
{
    // TODO #73 This can probably be replaced with NodaTime
    public static class DateTimeExtensions
    {
        public static DateTime InTimeZone(this DateTime dateTime, string tzName)
        {
            if (dateTime.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException($"{nameof(dateTime)} must be DateTimeKind.Utc");
            }

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(tzName);
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeZoneInfo);
        }

        public static DateTime ToUtcFromTimeZone(this DateTime dateTime, string tzName)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(tzName);
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, timeZoneInfo);
        }

        public static DateTime AsUtc(this DateTime datetime) => DateTime.SpecifyKind(datetime, DateTimeKind.Utc);

        public static DateTime FromUnixTimestamp(long timestamp)
        {
            return DateTime.UnixEpoch.AddSeconds(timestamp);
        }

        public static string ToDateString(this DateTime dateTime)
        {
            return dateTime.ToString("d", Culture.Default);
        }
    }
}
