using System.Globalization;

namespace OrchestrateApi.Common
{
    public static class Culture
    {
        private const string DefaultLocale = "en-AU";

        public static CultureInfo Default { get; } = new CultureInfo(DefaultLocale);
    }
}
