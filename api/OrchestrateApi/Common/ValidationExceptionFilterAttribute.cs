using System.Net;
using System.Text.Json;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace OrchestrateApi.Common;

public sealed class ValidationExceptionFilterAttribute : ExceptionFilterAttribute
{
    public override void OnException(ExceptionContext context)
    {
        if (context.Exception is not ValidationException validationException)
        {
            return;
        }

        var validationErrorDto = new ValidationFailureErrorResponse
        {
            ValidationResults = GenerateValidationErrorDto(validationException.Errors),
        };
        context.Result = new JsonResult(validationErrorDto)
        {
            StatusCode = (int)HttpStatusCode.BadRequest,
            SerializerSettings = new JsonSerializerOptions(JsonSerializerDefaults.Web)
            {
                DictionaryKeyPolicy = JsonNamingPolicy.CamelCase
            },
        };
    }

    internal static IDictionary<string, object> GenerateValidationErrorDto(
        IEnumerable<ValidationFailure> validationFailures)
    {
        var validationErrorModel = new Dictionary<string, object>();
        foreach (var error in validationFailures)
        {
            var propertyPath = error.PropertyName.Split(".");
            var currModel = validationErrorModel;

            while (propertyPath.Length > 1)
            {
                if (!currModel.TryGetValue(propertyPath[0], out var m)
                    || m is not Dictionary<string, object> nestedModel)
                {
                    nestedModel = new Dictionary<string, object>();
                    currModel[propertyPath[0]] = nestedModel;
                }
                currModel = nestedModel;
                propertyPath = propertyPath[1..];
            }

            if (!currModel.TryGetValue(propertyPath[0], out var e)
                || e is not List<ErrorDto> errorList)
            {
                errorList = new List<ErrorDto>();
                currModel[propertyPath[0]] = errorList;
            }

            errorList.Add(new ErrorDto
            {
                ErrorCode = error.ErrorCode,
                ErrorMessage = error.ErrorMessage
            });
        }

        return validationErrorModel;
    }

    internal class ValidationFailureErrorResponse : IErrorResponse
    {
        public string Type => "ValidationFailure";

        public string Message => "Data is not valid";

        public required IDictionary<string, object> ValidationResults { get; init; }
    }

    internal class ErrorDto
    {
        public required string ErrorCode { get; set; }

        public required string ErrorMessage { get; set; }
    }
}
