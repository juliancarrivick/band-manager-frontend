using System.Linq.Expressions;
using System.Reflection;
using LinqKit;

namespace OrchestrateApi.Common;

[System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1040")]
public interface ITemporalLifetime { }

[AttributeUsage(AttributeTargets.Property, Inherited = false)]
public sealed class TemporalLifetimeStartAttribute : Attribute { }

[AttributeUsage(AttributeTargets.Property, Inherited = false)]
public sealed class TemporalLifetimeFinishAttribute : Attribute { }

public static class TemporalLifetimeExtensions
{
    private static readonly Expression<Func<DateTime?, DateTime?, DateTime, bool>> IsActiveAtBaseExpr
        = (start, finish, instant) => (start == null || start <= instant) && (finish == null || finish >= instant);
    private static readonly Func<DateTime?, DateTime?, DateTime, bool> IsActiveAtBaseFunc = IsActiveAtBaseExpr.Compile();

    private static Type StartAttribute => typeof(TemporalLifetimeStartAttribute);
    private static Type FinishAttribute => typeof(TemporalLifetimeFinishAttribute);

    public static bool IsActiveAt<T>(this T entity, DateTime instant)
        where T : ITemporalLifetime
    {
        // There's probably a more efficient way of doing this, but doing it with reflection
        // like this removed the need to have additional properties on the model classes, and
        // also allows any datetime properties to be used as the start and end flags
        // We can revisit if performance is an issue
        var start = GetTemporalPropertyValue(entity, StartAttribute);
        var finish = GetTemporalPropertyValue(entity, FinishAttribute);
        return IsActiveAtBaseFunc(start, finish, instant);
    }

    public static IQueryable<T> WhereIsActiveNow<T>(this IQueryable<T> entities)
        where T : ITemporalLifetime => entities.WhereIsActiveAt(DateTime.UtcNow);

    public static IQueryable<T> WhereIsActiveAt<T>(this IQueryable<T> entities, DateTime instant)
        where T : ITemporalLifetime
    {
        return entities.Where(IsActiveAtExpr<T>(instant).Expand());
    }

    public static Expression<Func<T, bool>> IsActiveNowExpr<T>()
        where T : ITemporalLifetime => IsActiveAtExpr<T>(DateTime.UtcNow);

    public static Expression<Func<T, bool>> IsActiveAtExpr<T>(DateTime instant)
        where T : ITemporalLifetime
    {
        var startPropertyExpr = GetTemporalPropertyExpr<T>(StartAttribute);
        var finishPropertyExpr = GetTemporalPropertyExpr<T>(FinishAttribute);
        return e => IsActiveAtBaseExpr.Invoke(startPropertyExpr.Invoke(e), finishPropertyExpr.Invoke(e), instant);
    }

    private static Expression<Func<T, DateTime?>> GetTemporalPropertyExpr<T>(Type attribute)
        where T : ITemporalLifetime
    {
        var property = GetTemporalLifetimeProperty<T>(attribute);
        var param = Expression.Parameter(typeof(T));
        return Expression.Lambda<Func<T, DateTime?>>(
            Expression.Convert(Expression.Property(param, property.GetMethod!), typeof(DateTime?)),
            true,
            new[] { param });
    }

    private static DateTime? GetTemporalPropertyValue<T>(T obj, Type attribute)
        where T : ITemporalLifetime
    {
        var property = GetTemporalLifetimeProperty<T>(attribute);
        return (DateTime?)property.GetValue(obj);
    }

    private static PropertyInfo GetTemporalLifetimeProperty<T>(Type attribute)
        where T : ITemporalLifetime
    {
        var property = typeof(T)
            .GetProperties()
            .SingleOrDefault(p => p.GetCustomAttributes(attribute, inherit: false).Length != 0);
        if (property == null)
        {
            throw new InvalidOperationException($"{typeof(T)} must have a property with attribute {attribute.Name}");
        }
        if (!property.PropertyType.IsAssignableTo(typeof(DateTime?)))
        {
            throw new InvalidOperationException($"{attribute.Name} must be placed on a {typeof(DateTime)} or {typeof(DateTime?)} property");
        }

        return property;
    }
}
