using System.Diagnostics.CodeAnalysis;

namespace OrchestrateApi.Common;

// Heavily inspired by https://josef.codes/my-take-on-the-result-class-in-c-sharp/

// Allow implicit operators
#pragma warning disable CA2225

public abstract class Result
{
    public bool Succeeded { get; protected set; }
    public bool Failed => !Succeeded;
}

public abstract class Result<T> : Result
{
    private T data;

    protected Result(T data)
    {
        this.data = data;
    }

    public T Data
    {
        get => Succeeded ? this.data : throw new InvalidOperationException("No data when result didn't succeed");
        protected set => this.data = value;
    }

    public static implicit operator Result<T>(T data) => new SucceededResult<T>(data);
    public static implicit operator Result<T>(ErrorResult error) => new ErrorResult<T>(error);
    public static implicit operator Result<T>(UserErrorResult error) => new UserErrorResult<T>(error);
    public static implicit operator Result<T>(ServerErrorResult error) => new ServerErrorResult<T>(error);
}

public class SucceededResult : Result
{
    public SucceededResult()
    {
        Succeeded = true;
    }
}

public class SucceededResult<T> : Result<T>
{
    public SucceededResult(T data) : base(data)
    {
        Succeeded = true;
    }
}

public class ResultError
{
    public required string Code { get; set; }
    public required string Description { get; set; }
}

public interface IErrorResult
{
    public string Message { get; }
    IReadOnlyCollection<ResultError> Errors { get; }
}

public class ErrorResult : Result, IErrorResult
{
    public ErrorResult(string message, params ResultError[] errors)
    {
        Message = message;
        Errors = errors;
    }

    public string Message { get; private set; }

    public IReadOnlyCollection<ResultError> Errors { get; private set; }
}

public class UserErrorResult : ErrorResult
{
    public UserErrorResult(string message, params ResultError[] errors) : base(message, errors)
    {
    }
}

public class ServerErrorResult : ErrorResult
{
    public ServerErrorResult(string message, params ResultError[] errors) : base(message, errors)
    {
    }
}

public class ErrorResult<T> : Result<T>, IErrorResult
{
    public ErrorResult(string message, params ResultError[] errors) : base(default!)
    {
        Message = message;
        Errors = errors;
    }

    public ErrorResult(IErrorResult errorResult) : base(default!)
    {
        Message = errorResult.Message;
        Errors = errorResult.Errors;
    }

    public string Message { get; private set; }

    public IReadOnlyCollection<ResultError> Errors { get; private set; }

    public static implicit operator ErrorResult<T>(ErrorResult error) => new ErrorResult<T>(error);
}

public class UserErrorResult<T> : ErrorResult<T>
{
    public UserErrorResult(string message, params ResultError[] errors) : base(message, errors)
    {
    }

    public UserErrorResult(IErrorResult errorResult) : base(errorResult)
    {
    }
}

public class ServerErrorResult<T> : ErrorResult<T>
{
    public ServerErrorResult(string message, params ResultError[] errors) : base(message, errors)
    {
    }

    public ServerErrorResult(IErrorResult errorResult) : base(errorResult)
    {
    }
}
