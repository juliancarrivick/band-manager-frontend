using OrchestrateApi.Security;

namespace System.Security.Claims
{
    public static class ClaimsPrincipalExtensions
    {
        // Namespaced this according to convention (even though we aren't using auth0)
        // https://auth0.com/docs/get-started/apis/scopes/sample-use-cases-scopes-and-claims#add-custom-claims-to-a-token
        public const string GlobalAdminClaimName = "https://orchestrate.community/is_global_admin";

        public static bool HasPermissionInTenant(this ClaimsPrincipal user, Guid tenantId, FeaturePermission permission)
        {
            return user.PermissionsInTenant(tenantId).Contains(permission);
        }

        public static ICollection<FeaturePermission> PermissionsInTenant(this ClaimsPrincipal user, Guid tenantId)
        {
            var roles = user.RolesInTenant(tenantId);
            var permissions = roles.SelectMany(e => StandardRole.GetRolePermissions(e));
            return permissions.Distinct().ToList();
        }

        public static ICollection<string> RolesInTenant(this ClaimsPrincipal user, Guid tenantId)
        {
            if (user == null)
            {
                return Array.Empty<string>();
            }

            // At some point might want to make this apply only if use has no roles in specified tenant
            if (user.IsGlobalAdmin())
            {
                return new[] { StandardRole.Administrator };
            }

            return user.GetTenantRoles()
                .Where(tr => tr.TenantId == tenantId)
                .Select(c => c.Role)
                .ToList();
        }

        public static IEnumerable<Guid> AccessibleTenants(this ClaimsPrincipal user)
        {
            if (user == null)
            {
                return Enumerable.Empty<Guid>();
            }

            return user.GetTenantRoles()
                .Select(tr => tr.TenantId)
                .Distinct();
        }

        public static bool IsGlobalAdmin(this ClaimsPrincipal user)
        {
            return user != null
                && user.Claims.FirstOrDefault(c => c.Type == GlobalAdminClaimName) is Claim globalAdminClaim
                && globalAdminClaim.Value == bool.TrueString;
        }

        public static IEnumerable<TenantRole> GetTenantRoles(this ClaimsPrincipal user) => user.Claims.GetTenantRoles();

        public static IEnumerable<TenantRole> GetTenantRoles(this IEnumerable<Claim> claims)
        {
            return claims
                .GetClaimValues(ClaimTypes.Role)
                .Select(r => TenantRole.Parse(r));
        }

        public static Guid UserId(this ClaimsPrincipal user)
        {
            return new Guid(user.Claims.GetClaimValues(ClaimTypes.NameIdentifier).Single());
        }

        public static string Username(this ClaimsPrincipal user)
        {
            return user.Claims.GetClaimValues(ClaimTypes.Name).Single();
        }

        public static string DisplayName(this ClaimsPrincipal user) => DisplayName(user.Claims);

        public static string DisplayName(this IEnumerable<Claim> claims)
        {
            var names = new List<string>();

            var first = claims.GetClaimValues(ClaimTypes.GivenName).SingleOrDefault();
            if (!string.IsNullOrWhiteSpace(first))
            {
                names.Add(first);
            }

            var last = claims.GetClaimValues(ClaimTypes.Surname).SingleOrDefault();
            if (!string.IsNullOrWhiteSpace(last))
            {
                names.Add(last);
            }

            return names.Count > 0
                ? string.Join(" ", names)
                : claims.GetClaimValues(ClaimTypes.Name).Single();
        }

        public static IEnumerable<string> GetClaimValues(this IEnumerable<Claim> claims, string claimType)
        {
            return claims
                .Where(i => i.Type == claimType)
                .Select(i => i.Value);
        }
    }
}
