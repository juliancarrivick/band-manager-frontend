using System.Web;

namespace OrchestrateApi.Common
{
    public interface IAppConfiguration
    {
        string EmailProvider { get; }
        string PostmarkAccountToken { get; }
        string PostmarkServerToken { get; }
        string DefaultFromEmail { get; }
        string DefaultFromName { get; }
        Uri FrontEndUri { get; }
        string NotificationEmail { get; }
        string ToEmailOverride { get; }
        bool EmailWhitelistEnabled { get; }
        IEnumerable<string> EmailWhitelist { get; }
        TimeSpan TokenLifetime { get; }
        string JwtIssuer { get; }
        string JwtSecret { get; }
        string ConnectionString { get; }
        string StripeSecretKey { get; }
        string StripeWebhookSecret { get; }
        string StripeNotForProfitCouponId { get; }
        bool PeriodicJobCheckEnabled { get; }
        TimeSpan PeriodicJobCheckInterval { get; }
        TimeSpan PeriodicJobMaxExpectedTime { get; }
        TimeSpan BillingPeriodTransitionCheckInterval { get; }
        TimeSpan BillingPeriodUpcomingActionNotificationInterval { get; }
        TimeSpan OrphanedBlobCleanupInterval { get; }
        TimeSpan TrialTimeSpan { get; }
        TimeSpan TrialEndCheckInterval { get; }
        string? HcaptchaSiteKey { get; }
        string? HcaptchaSecret { get; }
        string MailingListApexDomain { get; }
        int MaxMailingListCount { get; }
        TimeSpan PostmarkSuppressionSyncInterval { get; }
        int DbMaxTransientFailureRetryCount { get; }
        TimeSpan DbTransientFailureTrackingAgeOutPeriod { get; }
    }

    public class AppConfiguration(IConfiguration configuration) : IAppConfiguration
    {
        public string EmailProvider => GetAppConfig("EmailProvider", "Postmark");

        public string PostmarkAccountToken => GetAppConfig("PostmarkAccountToken");

        public string PostmarkServerToken => GetAppConfig("PostmarkServerToken");

        public string DefaultFromEmail => GetAppConfig("DefaultFromEmail");

        public string DefaultFromName => GetAppConfig("DefaultFromName");

        public Uri FrontEndUri => new Uri(GetAppConfig("FrontEndUri"));

        public string NotificationEmail => GetAppConfig("NotificationEmail");

        public string ToEmailOverride => GetAppConfig("ToEmailOverride");

        public bool EmailWhitelistEnabled => GetAppConfig<bool>("EmailWhitelistEnabled");

        public IEnumerable<string> EmailWhitelist => GetAppConfigArray<string>("EmailWhitelist");

        public TimeSpan TokenLifetime => GetAppConfig<int?>("TokenLifetimeHours") is int tokenLifetimeHours
            ? TimeSpan.FromHours(tokenLifetimeHours)
            : TimeSpan.FromHours(24);

        public string JwtIssuer => configuration["JWT:ValidIssuer"] ?? string.Empty;

        public string JwtSecret => configuration["JWT:Secret"] ?? string.Empty;

        public string ConnectionString => ConvertUrlStringToConnectionString(configuration["DATABASE_URL"]);

        public string StripeSecretKey => configuration["Stripe:SecretKey"] ?? string.Empty;

        public string StripeWebhookSecret => configuration["Stripe:WebhookSecret"] ?? string.Empty;

        public string StripeNotForProfitCouponId => GetAppConfig("StripeNotForProfitCouponId");

        public bool PeriodicJobCheckEnabled => GetAppConfig<bool?>("PeriodicJobCheckEnabled") ?? false;

        public TimeSpan PeriodicJobCheckInterval => GetAppConfig<int?>("PeriodicJobCheckIntervalHours") is { } hours
            ? TimeSpan.FromHours(hours)
            : TimeSpan.FromHours(1);

        public TimeSpan PeriodicJobMaxExpectedTime => GetAppConfig<int?>("PeriodicJobMaxExpectedTimeHours") is { } hours
            ? TimeSpan.FromHours(hours)
            : TimeSpan.FromHours(1);

        public TimeSpan BillingPeriodTransitionCheckInterval => GetAppConfig<int?>("BillingPeriodTransitionCheckIntervalHours") is int checkHours
            ? TimeSpan.FromHours(checkHours)
            : TimeSpan.FromHours(6);

        public TimeSpan BillingPeriodUpcomingActionNotificationInterval => GetAppConfig<int?>("BillingPeriodUpcomingActionNotificationIntervalDays") is int notificationDays
            ? TimeSpan.FromDays(notificationDays)
            : TimeSpan.FromDays(5);

        public TimeSpan OrphanedBlobCleanupInterval => GetAppConfig<int?>("OrphanedBlobCleanupIntervalHours") is int hours
            ? TimeSpan.FromHours(hours)
            : TimeSpan.FromDays(1);

        public TimeSpan TrialTimeSpan => GetAppConfig<int?>("TrialTimeSpanDays") is int days
            ? TimeSpan.FromDays(days)
            : TimeSpan.FromDays(30);

        public TimeSpan TrialEndCheckInterval => GetAppConfig<int?>("TrialEndCheckIntervalHours") is int hours
            ? TimeSpan.FromHours(hours)
            : TimeSpan.FromDays(1);

        public string? HcaptchaSiteKey => configuration["Hcaptcha:SiteKey"];
        public string? HcaptchaSecret => configuration["Hcaptcha:Secret"];

        public string MailingListApexDomain => GetAppConfig("MailingListApexDomain");

        public int MaxMailingListCount => GetAppConfig<int?>("MaxMailingListCount") is { } count
            ? count
            : 5;

        public TimeSpan PostmarkSuppressionSyncInterval => GetAppConfig<int?>("PostmarkSuppressionSyncIntervalHours") is int hours
            ? TimeSpan.FromHours(hours)
            : TimeSpan.FromDays(1);

        public int DbMaxTransientFailureRetryCount => GetAppConfig<int?>("DbMaxTransientFailureRetryCount") is { } count
            ? count
            : 5;

        public TimeSpan DbTransientFailureTrackingAgeOutPeriod => GetAppConfig<int?>("DbTransientFailureTrackingAgeOutPeriodMinutes") is { } minutes
            ? TimeSpan.FromMinutes(minutes)
            : TimeSpan.FromMinutes(5);

        internal static string ConvertUrlStringToConnectionString(string? databaseUrl)
        {
            // Adapted from https://gist.github.com/snow-jallen/bbcef9a7c50056e227e066010c043a4c#file-startup-cs
            // Hopefully will be able to remove this if https://github.com/npgsql/npgsql/pull/2733 gets merged
            var uri = Uri.TryCreate(databaseUrl, UriKind.Absolute, out var parsedUri)
                ? parsedUri
                : throw new InvalidOperationException($"Invalid database uri: {databaseUrl}");
            var queryParameters = HttpUtility.ParseQueryString(uri.Query);

            var connectionStringParts = new List<string>
            {
                $"Host={uri.Host}",
                $"SSL Mode={queryParameters.Get("sslmode") ?? "Prefer"}",
                "Trust Server Certificate=true",
            };

            if (uri.Port > 0)
            {
                connectionStringParts.Add($"Port={uri.Port}");
            }

            var database = uri.Segments.LastOrDefault();
            if (database != null)
            {
                connectionStringParts.Add($"Database={database}");
            }

            if (!string.IsNullOrEmpty(uri.UserInfo))
            {
                var userParts = uri.UserInfo.Split(':');
                connectionStringParts.Add($"Username={userParts.First()}");

                if (userParts.Length > 1)
                {
                    connectionStringParts.Add($"Password={userParts.Last()}");
                }
            }

            return string.Join("; ", connectionStringParts);
        }

        private string GetAppConfig(string property, string? defaultIfNullOrWhitespace = null)
        {
            if (string.IsNullOrWhiteSpace(defaultIfNullOrWhitespace))
            {
                defaultIfNullOrWhitespace = string.Empty;
            }

            var value = configuration[$"AppConfiguration:{property}"];
            return string.IsNullOrWhiteSpace(value)
                ? defaultIfNullOrWhitespace
                : value;
        }

        private T? GetAppConfig<T>(string property)
        {
            return configuration.GetValue<T>($"AppConfiguration:{property}");
        }

        private T[] GetAppConfigArray<T>(string property)
        {
            return configuration.GetSection($"AppConfiguration:{property}").Get<T[]>() ?? [];
        }
    }
}
