using System.Diagnostics.CodeAnalysis;

namespace OrchestrateApi.Common
{
    public interface IFileType
    {
        string MimeType { get; }

        bool Test(IEnumerable<byte> file);

        Task<bool> TestAsync(Stream stream, CancellationToken cancellationToken);
    }

    public static class FileType
    {
        // Adapted from https://stackoverflow.com/a/13614746/17403538
        public static IFileType Jpeg { get; } = new SentinelFileType("image/jpeg", [0xFF, 0xD8, 0xFF]);
        public static IFileType Png { get; } = new SentinelFileType("image/png", [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A]);
    }

    public class FileTypeDetector
    {
        public static FileTypeDetector Images { get; } = new FileTypeDetector
        {
            AvailableFileTypes = [FileType.Jpeg, FileType.Png],
        };

        public static FileTypeDetector All { get; } = new FileTypeDetector
        {
            AvailableFileTypes = [.. Images.AvailableFileTypes],
        };

        public IEnumerable<IFileType> AvailableFileTypes { get; init; } = [];

        public IFileType DefaultFileType { get; init; } = new StaticFileType("application/octet-stream");

        public IFileType ParseFileTypeOrDefault(byte[] file)
        {
            return TryParseFileType(file, out var fileType)
                ? fileType
                : DefaultFileType;
        }

        public bool AnyMatchingFileType(byte[] file) => AvailableFileTypes.Any(ft => ft.Test(file));

        public async Task<bool> AnyMatchingFileTypeAsync(Stream stream, CancellationToken cancellationToken = default)
        {
            if (!stream.CanSeek)
            {
                throw new ArgumentException("Stream must be seekable");
            }

            var position = stream.Position;
            try
            {
                foreach (var fileType in AvailableFileTypes)
                {
                    if (await fileType.TestAsync(stream, cancellationToken))
                    {
                        return true;
                    }

                    stream.Position = position;
                }

                return false;
            }
            finally
            {
                stream.Position = position;
            }
        }

        public bool TryParseFileType(byte[] file, [NotNullWhen(true)] out IFileType? fileType)
        {
            foreach (var ft in AvailableFileTypes)
            {
                if (ft.Test(file))
                {
                    fileType = ft;
                    return true;
                }
            }

            fileType = null;
            return false;
        }
    }

    internal class SentinelFileType : IFileType
    {
        public SentinelFileType(string mimeType, byte[] sentinel)
        {
            MimeType = mimeType;
            Sentinel = sentinel ?? throw new ArgumentNullException(nameof(sentinel));
        }

        public string MimeType { get; }

        public byte[] Sentinel { get; }


        public bool Test(IEnumerable<byte> file) => file.Take(Sentinel.Length).SequenceEqual(Sentinel);

        public async Task<bool> TestAsync(Stream stream, CancellationToken cancellationToken)
        {
            var buffer = new byte[Sentinel.Length];
            _ = await stream.ReadAsync(buffer.AsMemory(), cancellationToken);
            return Test(buffer);
        }
    }

    internal class StaticFileType : IFileType
    {
        public StaticFileType(string mimeType)
        {
            MimeType = mimeType;
        }

        public string MimeType { get; }

        public bool Test(IEnumerable<byte> file) => true;

        public Task<bool> TestAsync(Stream stream, CancellationToken cancellationToken) => Task.FromResult(true);
    }
}
