using System.Text.Json;
using System.Text.Json.Serialization;

namespace OrchestrateApi.Common;

// Adapted from https://stackoverflow.com/a/66352365/17403538
public abstract class PolymorphicJsonConverter<T> : JsonConverter<T>
{
    protected abstract string DiscriminatorProperty { get; }

    public override bool CanConvert(Type typeToConvert)
    {
        return typeof(T).IsAssignableFrom(typeToConvert);
    }

    public override T Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var discriminator = GetDiscriminatorValue(reader);
        var targetType = SelectTargetType(discriminator);
        if (!targetType.IsAssignableTo(typeof(T)))
        {
            throw new InvalidOperationException($"Target type {targetType} is not assignable to {typeof(T)}");
        }

        // Theoretically this converter will cause a StackOverflow with the Deserialize
        // call below, but I've found it works OK if we never try to deserialize the base
        // class directly. So we enforce that targetType isn't the base class.
        // See https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-converters-how-to?pivots=dotnet-6-0#an-alternative-way-to-do-polymorphic-deserialization
        if (targetType == typeof(T))
        {
            throw new InvalidOperationException("Target type should not be the base type");
        }

        var parsedEvent = JsonSerializer.Deserialize(ref reader, targetType, options);
        return (T)parsedEvent!;
    }

    public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }

    protected abstract Type SelectTargetType(string? discriminator);

    private string? GetDiscriminatorValue(Utf8JsonReader reader)
    {
        if (reader.TokenType != JsonTokenType.StartObject)
        {
            throw new JsonException("Expected a JSON object");
        }

        while (reader.Read())
        {
            if (reader.TokenType != JsonTokenType.PropertyName)
            {
                throw new JsonException("Expected a JSON property name");
            }

            if (reader.GetString() == DiscriminatorProperty)
            {
                break;
            }

            reader.Skip();
        }

        if (!reader.Read())
        {
            throw new JsonException("Did not expect end of input");
        }

        if (reader.TokenType != JsonTokenType.String)
        {
            throw new JsonException("Expected discriminator to be of type string");
        }

        return reader.GetString();
    }
}
