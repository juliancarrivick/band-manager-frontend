using System.Globalization;
using System.Text.RegularExpressions;

namespace OrchestrateApi.Common
{
    public static partial class StringExtensions
    {
        public static string? ToSnakeCase(this string? input, CultureInfo? culture = null)
        {
            if (string.IsNullOrEmpty(input)) { return input; }

            culture ??= CultureInfo.InvariantCulture;
            return LowerCaseFollowedByUpperCase()
                .Replace(input, "$1_$2")
                .ToLower(culture);
        }

        public static string ToCamelCase(this string input)
        {
            if (string.IsNullOrEmpty(input)) { return input; }

            return $"{char.ToLowerInvariant(input[0])}{input[1..]}";
        }

        public static string Shorten(this string input, int length, string? placeholder = null)
        {
            if (string.IsNullOrEmpty(input) || input.Length <= length) { return input; }

            placeholder ??= string.Empty;
            return input[..length] + placeholder;
        }

        [GeneratedRegex("([a-z0-9])([A-Z])")]
        private static partial Regex LowerCaseFollowedByUpperCase();
    }
}
