using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Email
{
    public interface IEmailService
    {
        EmailMessageAddress DefaultFrom { get; }

        Task<EmailResult> SendAsync(EmailMessage email);
        Task<IList<EmailResult>> SendAsync(IList<EmailMessage> emails);
        Task ProcessEventsAsync(IList<EmailEvent> events);
    }

    public class EmailService : IEmailService
    {
        public const string EmailEnvironmentMetadataKey = "EnvironmentName";
        public const string EmailTypeKey = "EmailType";
        public const string TenantIdKey = "TenantId";
        public const string TenantNameKey = "TenantName";

        private readonly IHostEnvironment environment;
        private readonly IAppConfiguration configuration;
        private readonly OrchestrateContext dbContext;
        private readonly IEmailProvider emailProvider;
        private readonly IEnumerable<IEmailEventHandler> eventHandlers;
        private readonly ILogger logger;

        public EmailService(
            IHostEnvironment environment,
            IAppConfiguration configuration,
            OrchestrateContext dbContext,
            IEnumerable<IEmailProvider> emailProviders,
            IEnumerable<IEmailEventHandler> eventHandlers,
            ILogger<EmailService> logger)
        {
            this.environment = environment;
            this.configuration = configuration;
            this.dbContext = dbContext;
            this.eventHandlers = eventHandlers;
            this.logger = logger;

            this.emailProvider = emailProviders.Single(e =>
            {
                return string.Equals(e.Name, configuration.EmailProvider.Trim(), StringComparison.OrdinalIgnoreCase);
            });
        }

        public EmailMessageAddress DefaultFrom => new(this.configuration.DefaultFromName, this.configuration.DefaultFromEmail);

        // Only exposed for testing
        internal string ActiveProvider => this.emailProvider.Name;
        internal bool DropNonProdEmailsWithoutOverrideOrAllowlist { get; set; } = true;
        internal bool DropDemoEmails { get; set; } = true;

        private string ToEmailOverride => this.configuration.ToEmailOverride;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1031")]
        public async Task ProcessEventsAsync(IList<EmailEvent> events)
        {
            var eventsToHandle = events
                .WhereWithDiscardCountAction(
                    e => e.IsFromEnvironment(this.environment.EnvironmentName),
                    numDiscarded => this.logger.LogInformation("Discarding {NumDiscarded} events from other environments", numDiscarded))
                .WhereWithDiscardCountAction(
                    e => e.IsKnownEvent,
                    numDiscarded => this.logger.LogInformation("Discarding {NumDiscarded} unknown events", numDiscarded));

            if (eventsToHandle.Count == 0)
            {
                return;
            }

            foreach (var h in eventHandlers)
            {
                try
                {
                    var numEventsHandled = await h.HandleAsync(eventsToHandle);
                    this.logger.LogInformation("{NumEventsHandled} events handled by {Handler}", numEventsHandled, h.GetType().Name);
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error when processing email events in {Handler}", h.GetType());
                }
            };
        }

        public async Task<EmailResult> SendAsync(EmailMessage email)
        {
            var results = await SendAsync(new[] { email });
            return results.First();
        }

        public async Task<IList<EmailResult>> SendAsync(IList<EmailMessage> emails)
        {
            if (DropNonProdEmailsWithoutOverrideOrAllowlist && !this.environment.IsProduction()
                && string.IsNullOrWhiteSpace(ToEmailOverride) && !this.configuration.EmailWhitelistEnabled)
            {
                return EmailHelpers.FailAllEmails(emails, $"{nameof(ToEmailOverride)} not set or"
                    + $" {nameof(this.configuration.EmailWhitelistEnabled)} is false"
                    + $" in non-production environment");
            }

            var tenant = await this.dbContext.GetTenantAsync();
            if (DropDemoEmails && tenant != null && tenant.Plan.Type == PlanType.Demo)
            {
                return EmailHelpers.FailAllEmails(emails, "Not sending emails to demo tenant");
            }

            foreach (var email in emails)
            {
                email.Metadata[EmailEnvironmentMetadataKey] = this.environment.EnvironmentName;
                email.Metadata[EmailTypeKey] = email.Type;

                if (tenant != null)
                {
                    email.Metadata[TenantIdKey] = tenant.Id.ToString();
                    email.Metadata[TenantNameKey] = tenant.Name;
                }
            }

            var skippedResults = new List<EmailResult>();
            var messagesToSend = emails.Where(e => PreProcessEmail(e, skippedResults)).ToList();

            var sentResults = messagesToSend.Count > 0
                ? await this.emailProvider.SendAsync(messagesToSend)
                : Array.Empty<EmailResult>();

            var allResults = skippedResults.Union(sentResults).ToList();
            return allResults;
        }

        private bool PreProcessEmail(EmailMessage email, List<EmailResult> skipped)
        {
            if (!this.environment.IsProduction())
            {
                // If we use square brackets gmail strips them from what is displayed to the user :(
                var subjPrefix = $"<{this.environment.EnvironmentName}>";
                if (!email.Subject.Contains(subjPrefix))
                {
                    email.Subject = $"{subjPrefix} {email.Subject}";
                }
                if (!string.IsNullOrWhiteSpace(email.TextContent))
                {
                    email.TextContent = "*** THIS EMAIL IS FROM A TEST ENVIRONMENT ***\n"
                        + "If you are not expecting this email, please ignore its contents.\n"
                        + "---\n\n"
                        + email.TextContent;
                }
                if (!string.IsNullOrWhiteSpace(email.HtmlContent))
                {
                    email.HtmlContent = "<p><strong>THIS EMAIL IS FROM A TEST ENVIRONMENT</strong><br/>"
                        + "If you are not expecting this email, please ignore its contents.</p>"
                        + "<hr/>"
                        + email.HtmlContent;
                }
            }

            if (!string.IsNullOrEmpty(ToEmailOverride))
            {
                this.logger.LogDebug("Email to {ToAddress} is being overridden", email.To?.Address);
                email.To = new EmailMessageAddress(ToEmailOverride);
            }

            if (this.configuration.EmailWhitelistEnabled
                && !this.configuration.EmailWhitelist.Select(e => e.Trim()).Contains(email.To?.Address.Trim()))
            {
                skipped.Add(ErrorResult(email, "Email not sent as it is not on the list of allowed email addresses"));
                return false;
            }

            if (string.IsNullOrEmpty(email.To?.Address))
            {
                skipped.Add(ErrorResult(email, "Email not sent as it has no to address"));
                return false;
            }

            if (email.From == null)
            {
                email.From = DefaultFrom;
            }

            if (string.IsNullOrWhiteSpace(email.From.Address))
            {
                this.logger.LogError("From.Email was not set");
                skipped.Add(ErrorResult(email, "Email not sent as it has no from address"));
                return false;
            }

            if (string.IsNullOrWhiteSpace(email.TextContent) && string.IsNullOrWhiteSpace(email.HtmlContent))
            {
                this.logger.LogError("{EmailType} has no content", email.Type);
                skipped.Add(ErrorResult(email, "Email not sent as it has no content"));
            }

            return true;
        }

        private static EmailResult ErrorResult(EmailMessage email, string message) => new EmailResult
        {
            Email = email,
            Status = EmailResultStatus.Error,
            StatusMessage = message,
        };
    }
}
