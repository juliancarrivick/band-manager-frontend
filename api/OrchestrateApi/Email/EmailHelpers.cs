namespace OrchestrateApi.Email;

public static class EmailHelpers
{
    public const string FooterSentinal = "\u200B\u200B\u200B";
    public const string UnsubscribeLinkStartSentinal = "\u200C\u200C\u200C";
    public const string UnsubscribeLinkEndSentinal = "\u200D\u200D\u200D";
    public const string UnsubscribeLinkHtmlEncodedStartSentinal = "&zwnj;&zwnj;&zwnj;";
    public const string UnsubscribeLinkHtmlEncodedEndSentinal = "&zwj;&zwj;&zwj;";

    internal const string DefaultEmailFailureReason = "Error when sending email, please try sending again.";

    public static IList<EmailResult> FailAllEmails(IList<EmailMessage> emails, string? reason = null, object? originalResult = null)
    {
        reason ??= DefaultEmailFailureReason;

        return emails
            .Select(e => new EmailResult
            {
                Email = e,
                Status = EmailResultStatus.Error,
                StatusMessage = reason,
                OriginalResult = originalResult,
            })
            .ToList();
    }

    public static bool ContainsFooterSentinal(string plainOrHtmlContent)
        => plainOrHtmlContent.Contains(FooterSentinal);

    public static string? ReplacePlainTextUnsubscribeLink(string? plainContent, string newLink)
        => ReplaceBetweenSentinals(plainContent, UnsubscribeLinkStartSentinal, UnsubscribeLinkEndSentinal, newLink);

    public static string? ReplaceHtmlUnsubscribeLink(string? htmlContent, string newLink)
    {
        if (string.IsNullOrWhiteSpace(htmlContent))
        {
            return htmlContent;
        }

        // Some clients replace HTML encoding with actual Unicode character, other replace
        // the other way, so make sure we normalise first.
        htmlContent = htmlContent
            .Replace(UnsubscribeLinkStartSentinal, UnsubscribeLinkHtmlEncodedStartSentinal)
            .Replace(UnsubscribeLinkEndSentinal, UnsubscribeLinkHtmlEncodedEndSentinal);
        return ReplaceBetweenSentinals(
            htmlContent,
            UnsubscribeLinkHtmlEncodedStartSentinal,
            UnsubscribeLinkHtmlEncodedEndSentinal,
            newLink);
    }

    public static string BuildUnsubscribeHtml(string link) => $@"<a href=""{link}"">here</a>";

    private static string? ReplaceBetweenSentinals(string? value, string startSentinal, string endSentinal, string content)
    {
        if (string.IsNullOrWhiteSpace(value))
        {
            return value;
        }

        int startIndex = value.IndexOf(
            startSentinal,
            StringComparison.Ordinal);
        if (startIndex < 0)
        {
            return value;
        }

        int endIndex = value.IndexOf(
            endSentinal,
            startIndex + startSentinal.Length,
            StringComparison.Ordinal);
        if (endIndex < 0)
        {
            return value;
        }

        // Don't want to remove the start sentinal
        startIndex += startSentinal.Length;

        int lengthToRemove = endIndex - startIndex;
        value = value.Remove(startIndex, lengthToRemove);
        value = value.Insert(startIndex, content);
        return value;
    }
}
