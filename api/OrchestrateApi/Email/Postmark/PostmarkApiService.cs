using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using PostmarkDotNet;
using Postmark.Model.MessageStreams;
using OrchestrateApi.Email.Postmark;
using OrchestrateApi.Security;
using PostmarkDotNet.Model.Webhooks;
using Microsoft.EntityFrameworkCore;

namespace OrchestrateApi.Email;

public interface IPostmarkApiService
{
    IPostmarkAdapter? GetTenantClient(Tenant? tenant);
    Task<IPostmarkAdapter> GetOrCreateClientAsync(Tenant? tenant);
    string? GetMailingListStreamId(MailingList mailingList);
    Task<string> GetOrCreateMailingListStreamIdAsync(IPostmarkAdapter client, MailingList mailingList);
    Task<IEnumerable<IPostmarkAdapter>> GetAllClients();
}

public class PostmarkApiService(
    OrchestrateContext dbContext,
    IAppConfiguration appConfig,
    IHostEnvironment hostEnvironment,
    ILogger<PostmarkApiService> logger)
    : IPostmarkApiService
{
    private readonly PostmarkAdminClient adminClient = new(appConfig.PostmarkAccountToken);
    private readonly PostmarkAdapter defaultServerClient = new(appConfig.PostmarkServerToken);

    internal string ServerDeliveryType { get; set; } = "Live";

    public IPostmarkAdapter? GetTenantClient(Tenant? tenant)
    {
        if (tenant is null)
        {
            return null;
        }

        if (!hostEnvironment.IsProduction())
        {
            return defaultServerClient;
        }

        return tenant.PostmarkServerToken is { } token
            ? new PostmarkAdapter(token)
            : null;
    }

    public async Task<IPostmarkAdapter> GetOrCreateClientAsync(Tenant? tenant)
    {
        if (tenant is null || !hostEnvironment.IsProduction())
        {
            return defaultServerClient;
        }

        if (string.IsNullOrWhiteSpace(tenant.PostmarkServerToken))
        {
            var server = await adminClient.CreateServerAsync(tenant.Name, deliveryType: ServerDeliveryType);
            tenant.PostmarkServerToken = server.ApiTokens.First();
            await dbContext.SaveChangesAsync();
        }

        return new PostmarkAdapter(tenant.PostmarkServerToken);
    }

    public string? GetMailingListStreamId(MailingList mailingList)
    {
        return hostEnvironment.IsProduction()
            ? mailingList.PostmarkStreamId
            : "broadcast";
    }

    public async Task<string> GetOrCreateMailingListStreamIdAsync(IPostmarkAdapter client, MailingList mailingList)
    {
        if (!hostEnvironment.IsProduction())
        {
            return "broadcast";
        }

        if (string.IsNullOrWhiteSpace(mailingList.PostmarkStreamId))
        {
            var messageStream = await client.CreateMessageStream(
                mailingList.Slug,
                MessageStreamType.Broadcasts,
                mailingList.Name);

            mailingList.PostmarkStreamId = messageStream.ID;
            await dbContext.SaveChangesAsync();

            var webhookResponse = await defaultServerClient.GetWebhookConfigurationsAsync();
            var webhook = webhookResponse.Webhooks
                .FirstOrDefault(w => w.HttpAuth.Username == UserService.ApiUsername);
            if (webhook is not null)
            {
                await client.CreateWebhookConfigurationAsync(
                    webhook.Url,
                    messageStream: mailingList.PostmarkStreamId,
                    httpAuth: webhook.HttpAuth,
                    triggers: new WebhookConfigurationTriggers
                    {
                        Bounce = new() { Enabled = true },
                        SpamComplaint = new() { Enabled = true },
                        SubscriptionChange = new() { Enabled = true },
                    });
            }
            else
            {
                logger.LogError("Unable to find appropriate webhook in default postmark stream");
            }
        }

        return mailingList.PostmarkStreamId;
    }

    public async Task<IEnumerable<IPostmarkAdapter>> GetAllClients()
    {
        IEnumerable<IPostmarkAdapter> clients = [defaultServerClient];
        if (hostEnvironment.IsProduction())
        {
            var tokens = await dbContext.Tenant
                .IgnoreQueryFilters()
                .Where(e => e.PostmarkServerToken != null)
                .Select(e => e.PostmarkServerToken!)
                .ToListAsync();
            clients = clients.Concat(tokens.Select(token => new PostmarkAdapter(token)));
        }

        return clients;
    }
}
