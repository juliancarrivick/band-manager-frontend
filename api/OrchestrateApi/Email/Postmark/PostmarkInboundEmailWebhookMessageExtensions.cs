using PostmarkDotNet;
using PostmarkDotNet.Webhooks;

namespace OrchestrateApi.Email.Postmark;

public static class PostmarkInboundWebhookMessageExtensions
{
    public static IncomingEmail ToIncomingEmail(this PostmarkInboundWebhookMessage message) => new IncomingEmail
    {
        From = message.FromFull.ToRecipient(),
        To = message.ToFull.Select(r => r.ToRecipient()).ToList(),
        Cc = message.CcFull.Select(r => r.ToRecipient()).ToList(),
        Bcc = message.BccFull.Select(r => r.ToRecipient()).ToList(),
        Subject = message.Subject,
        TextContent = message.TextBody,
        HtmlContent = message.HtmlBody,
        Attachments = message.Attachments.Select(a => a.ToEmailAttachment()).ToList(),
        Headers = message.Headers.Select(h => h.ToHeader()).ToList(),
        SpamInfo = message.BuildSpamInfo(),
        SpfInfo = message.GetSpfInfo(),
    };

    private static IncomingEmailRecipient ToRecipient(this AddressFull recipient) => new IncomingEmailRecipient
    {
        Email = recipient.Email,
        Name = recipient.Name,
    };

    private static EmailAttachment ToEmailAttachment(this Attachment attachment) => new EmailAttachment
    {
        Name = attachment.Name,
        MimeType = attachment.ContentType,
        Base64Content = attachment.Content,
    };

    private static IncomingEmailHeader ToHeader(this Header header)
        => new IncomingEmailHeader(header.Name, header.Value);

    private static IncomingEmailSpamInfo? BuildSpamInfo(this PostmarkInboundWebhookMessage message)
    {
        var status = message.GetHeaderValue("X-Spam-Status");
        bool? isSpam = status?.Trim() switch
        {
            string s when s.Equals("Yes", StringComparison.OrdinalIgnoreCase) => true,
            string s when s.Equals("No", StringComparison.OrdinalIgnoreCase) => false,
            _ => null
        };

        if (isSpam is null)
        {
            return null;
        }

        var rawScore = message.GetHeaderValue("X-Spam-Score");
        double? score = double.TryParse(rawScore, out var parsedScore)
            ? parsedScore
            : null;
        return new IncomingEmailSpamInfo
        {
            IsSpam = isSpam.Value,
            Score = score,
        };
    }

    private static IncomingEmailSpfInfo? GetSpfInfo(this PostmarkInboundWebhookMessage message)
    {
        if (message.GetHeaderValue("Received-SPF") is not { } spf)
        {
            return null;
        }

        var status = spf.Trim() switch
        {
            string s when s.StartsWithIgnoreCase("neutral") => IncomingEmailSpfStatus.Neutral,
            string s when s.StartsWithIgnoreCase("pass") => IncomingEmailSpfStatus.Pass,
            string s when s.StartsWithIgnoreCase("softfail") => IncomingEmailSpfStatus.SoftFail,
            string s when s.StartsWithIgnoreCase("fail") => IncomingEmailSpfStatus.Fail,
            _ => IncomingEmailSpfStatus.Unknown,
        };

        return new IncomingEmailSpfInfo(status, spf);
    }

    private static bool StartsWithIgnoreCase(this string s, string value)
        => s.StartsWith(value, StringComparison.OrdinalIgnoreCase);

    private static string? GetHeaderValue(this PostmarkInboundWebhookMessage message, string headerName)
    {
        var header = message.Headers.FirstOrDefault(
            h => string.Equals(headerName, h.Name, StringComparison.OrdinalIgnoreCase));
        return header?.Value;
    }
}
