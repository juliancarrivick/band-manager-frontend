using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using Stripe;

namespace OrchestrateApi.PaymentProcessor;

public class StripeEventHandler(
    OrchestrateContext dbContext,
    IPaymentProcessor paymentProcessor,
    ILogger<StripeEventHandler> logger)
{
    public async Task HandleEvent(Event @event)
    {
        if (GetSubscriptionEvent(@event, EventTypes.CustomerSubscriptionCreated) is { } createdSubscription)
        {
            await HandleSubscriptionCreated(createdSubscription);
        }
        if (GetInvoiceEvent(@event, EventTypes.InvoicePaid) is { } paidInvoice)
        {
            await HandleInvoicePaid(paidInvoice);
        }
        else if (GetSubscriptionEvent(@event, EventTypes.SubscriptionScheduleUpdated) is { } updatedSubscription)
        {
            await HandleSubscriptionUpdate(updatedSubscription);
        }
        else if (GetSubscriptionEvent(@event, EventTypes.SubscriptionScheduleCanceled) is { } cancelledSubscription)
        {
            await HandleSubscriptionCancelled(cancelledSubscription);
        }
        else
        {
            logger.LogInformation("No handler for Stripe event {EventType}", @event.Type);
        }
    }

    private static Invoice? GetInvoiceEvent(Event @event, string eventType)
    {
        return @event.Type == eventType && @event.Data.Object is Invoice i
            ? i
            : null;
    }

    private static Subscription? GetSubscriptionEvent(Event @event, string eventType)
    {
        return @event.Type == eventType && @event.Data.Object is Subscription s
            ? s
            : null;
    }

    private async Task HandleSubscriptionCreated(Subscription subscription)
    {
        var tenant = await GetTenantByStripeCustomerIdAsync(subscription.CustomerId);
        if (tenant == null)
        {
            logger.LogWarning("Unknown customerId: {CustomerId}", subscription.CustomerId);
            return;
        }

        var productIds = subscription.Items.Select(i => i.Price.ProductId).ToList();
        var updatedPlan = await dbContext.PaidPlanMetadata
            .Where(e => productIds.Contains(e.StripeProductId))
            .Select(e => (PlanType?)e.Type)
            .SingleOrDefaultAsync();
        if (!updatedPlan.HasValue)
        {
            logger.LogWarning(
                "Unable to determine plan from subscription: {SubscriptionId}",
                subscription.Id);
            return;
        }

        tenant.Plan.Type = updatedPlan.Value;
        tenant.Plan.PeriodEnd = subscription.CurrentPeriodEnd;
        await dbContext.SaveChangesAsync();
    }

    private async Task HandleInvoicePaid(Invoice invoice)
    {
        var tenant = await GetTenantByStripeCustomerIdAsync(invoice.CustomerId);
        if (tenant == null)
        {
            logger.LogWarning("Unknown customerId: {CustomerId}", invoice.CustomerId);
            return;
        }

        var subscription = invoice.SubscriptionId != null
            ? await paymentProcessor.GetSubscriptionAsync(invoice.SubscriptionId)
            : null;
        if (subscription == null)
        {
            logger.LogWarning(
                "Not processing invoice ({InvoiceId}) without/with unknown subscription",
                invoice.Id);
            return;
        }

        if (subscription.Status == SubscriptionStatuses.Active)
        {
            tenant.Plan.PeriodEnd = subscription.CurrentPeriodEnd;
            await dbContext.SaveChangesAsync();
        }
    }

    private async Task HandleSubscriptionUpdate(Subscription subscription)
    {
        var tenant = await GetTenantByStripeCustomerIdAsync(subscription.CustomerId);
        if (tenant == null)
        {
            logger.LogWarning("Unknown customerId: {CustomerId}", subscription.CustomerId);
            return;
        }

        var productIds = subscription.Items.Select(i => i.Price.ProductId).ToList();
        var updatedPlan = await dbContext.PaidPlanMetadata
            .Where(e => productIds.Contains(e.StripeProductId))
            .Select(e => (PlanType?)e.Type)
            .SingleOrDefaultAsync();
        if (!updatedPlan.HasValue)
        {
            logger.LogWarning(
                "Unable to determine plan from subscription: {SubscriptionId}",
                subscription.Id);
            return;
        }

        tenant.Plan.Type = updatedPlan.Value;
        await dbContext.SaveChangesAsync();
    }

    private async Task HandleSubscriptionCancelled(Subscription subscription)
    {
        var tenant = await GetTenantByStripeCustomerIdAsync(subscription.CustomerId);
        if (tenant == null)
        {
            logger.LogWarning("Unknown customerId: {CustomerId}", subscription.CustomerId);
            return;
        }

        tenant.Plan.Type = PlanType.Free;
        tenant.Plan.PeriodEnd = null;
        await dbContext.SaveChangesAsync();
    }

    private async Task<Tenant?> GetTenantByStripeCustomerIdAsync(string customerId)
    {
        return await dbContext.Tenant
            .IgnoreQueryFilters()
            .SingleOrDefaultAsync(e => e.Plan.StripeCustomerId == customerId);
    }
}
