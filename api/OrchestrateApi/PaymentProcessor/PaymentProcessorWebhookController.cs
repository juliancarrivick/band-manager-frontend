using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.Common;

namespace OrchestrateApi.PaymentProcessor;

[ApiController]
[Route("payment-processor-webhook")]
public class SubscriptionWebhookController : ControllerBase
{
    private const string StripeSignatureHeader = "Stripe-Signature";

    private readonly IAppConfiguration appConfiguration;
    private readonly IBackgroundTaskQueue backgroundTaskQueue;
    private readonly ILogger logger;

    public SubscriptionWebhookController(
        IAppConfiguration appConfiguration,
        IBackgroundTaskQueue backgroundTaskQueue,
        ILogger<SubscriptionWebhookController> logger)
    {
        this.appConfiguration = appConfiguration;
        this.backgroundTaskQueue = backgroundTaskQueue;
        this.logger = logger;
    }

    [HttpPost("stripe-event")]
    public async Task<ActionResult> HandleStripeEventAsync()
    {
        var json = await BodyAsStringAsync();
        try
        {
            var stripeEvent = Stripe.EventUtility.ConstructEvent(
                json,
                Request.Headers[StripeSignatureHeader],
                appConfiguration.StripeWebhookSecret);
            await this.backgroundTaskQueue.QueueAsync(new StripeEventBackgroundWorkItem(stripeEvent));

            return Ok();
        }
        catch (Stripe.StripeException e)
        {
            this.logger.LogError(
                e,
                "Invalid data sent to stripe-event webhook: {Preview}",
                json.Shorten(40, "..."));
            return BadRequest();
        }
    }

    private async Task<string> BodyAsStringAsync()
    {
        using var reader = new StreamReader(Request.Body);
        return await reader.ReadToEndAsync();
    }

    private class StripeEventBackgroundWorkItem : IBackgroundWorkItem
    {
        private readonly Stripe.Event stripeEvent;

        public StripeEventBackgroundWorkItem(Stripe.Event stripeEvent)
        {
            this.stripeEvent = stripeEvent;
        }

        public async Task DoWork(IServiceProvider services, CancellationToken cancellationToken)
        {
            var handler = services.GetRequiredService<StripeEventHandler>();
            await handler.HandleEvent(this.stripeEvent);
        }
    }
}
