using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using Stripe;

namespace OrchestrateApi.PaymentProcessor;

// Yes, this uses Stripe types directly, but let's extract into abstractions if we ever move
// payment processors
public interface IPaymentProcessor
{
    Task ApplyNotForProfitDiscountAsync(Customer customer);
    Task<Stripe.BillingPortal.Session> CreateBillingPortalSessionAsync(string customerId, Uri returnUrl);
    Task<Result<Stripe.Checkout.Session>> CreateCheckoutSessionAsync(CheckoutSessionOptions options);
    Task<Customer> CreateCustomerAsync(string name, string email);
    Task<Customer?> GetCustomerAsync(string customerId);
    Task<Customer?> GetCustomerWithSubscriptionsAsync(string customerId);
    Task<IList<Invoice>> GetMostRecentPaidInvoicesAsync(string customerId, int last = 12);
    Task<PaymentMethod?> GetPaymentMethodAsync(string paymentMethodId);
    Task<Subscription?> GetSubscriptionAsync(string subscriptionId);
    Task<Invoice?> GetUpcomingInvoiceAsync(string customerId, string subscriptionId);
}

public class CheckoutSessionOptions
{
    public required string CustomerId { get; set; }

    public required PaidPlanMetadata Plan { get; set; }

    public required string? PromoCode { get; set; }

    public required Uri ReturnUrl { get; set; }
}
