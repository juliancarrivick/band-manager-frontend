using OrchestrateApi.Common;

namespace OrchestrateApi.PaymentProcessor;

public sealed class TrialEndCheckPeriodicJob : IPeriodicJob
{
    private readonly PaymentProcessorService paymentProcessorService;

    public TrialEndCheckPeriodicJob(
        PaymentProcessorService paymentProcessorService,
        IAppConfiguration appConfiguration)
    {
        RunPeriod = appConfiguration.TrialEndCheckInterval;
        this.paymentProcessorService = paymentProcessorService;
    }

    public string Name => "TrialEndCheck";
    public TimeSpan RunPeriod { get; }

    public async Task Run(CancellationToken cancellationToken)
    {
        await paymentProcessorService.TransitionExpiredTrials(cancellationToken);
    }
}
