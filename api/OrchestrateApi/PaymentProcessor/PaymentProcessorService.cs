using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Platform;

namespace OrchestrateApi.PaymentProcessor;

public class PaymentProcessorService
{
    private readonly OrchestrateContext dbContext;
    private readonly IEmailService emailService;
    private readonly FrontEnd frontEnd;
    private readonly ILogger logger;

    public PaymentProcessorService(
        OrchestrateContext dbContext,
        IEmailService emailService,
        FrontEnd frontEnd,
        ILogger<PaymentProcessorService> logger)
    {
        this.dbContext = dbContext;
        this.emailService = emailService;
        this.frontEnd = frontEnd;
        this.logger = logger;
    }

    public async Task TransitionExpiredTrials(CancellationToken cancellationToken = default)
    {
        var expiredTrialTenants = await this.dbContext.Tenant
            .IgnoreQueryFilters()
            .Where(e => e.Plan.Type == PlanType.Trial && e.Plan.PeriodEnd < DateTime.UtcNow)
            .ToListAsync(cancellationToken);
        if (expiredTrialTenants.Count == 0)
        {
            this.logger.LogInformation("No trials have ended");
            return;
        }

        foreach (var tenant in expiredTrialTenants)
        {
            tenant.Plan.Type = PlanType.Free;
            tenant.Plan.PeriodEnd = null;
            this.logger.LogInformation("{TenantName}'s trial has finished, moving to free plan", tenant.Name);
        }

        await this.dbContext.SaveChangesAsync(cancellationToken);

        var maxMembersFree = await this.dbContext.PlanMetadata
            .Where(e => e.Type == PlanType.Free)
            .Select(e => e.MemberLimit)
            .SingleOrDefaultAsync(cancellationToken);
        if (!maxMembersFree.HasValue)
        {
            this.logger.LogError("Free plan should have a member limit");
        }

        var memberLimitPhrase = maxMembersFree.HasValue
            ? $"maximum of {maxMembersFree.Value} members"
            : "limited number of members";
        var emails = expiredTrialTenants
            .Where(tenant => !string.IsNullOrWhiteSpace(tenant.ContactAddress))
            .Select(tenant => new EmailMessage
            {
                To = new(tenant.Name, tenant.ContactAddress),
                Subject = "Orchestrate trial has finished",
                HtmlContent = $"""
                    <p>Hello there {tenant.Name},</p>

                    <p>
                        Your trial period on Orchestrate has now completed and you have been moved
                        to the free plan, which is fully featured, but has a {memberLimitPhrase}.
                    </p>

                    <p>
                        If you would like to upgrade to a paid plan you may do so from
                        <a href="{this.frontEnd.TenantSubscriptionSettings(tenant)}">
                            Subscription Settings
                        </a>.
                    </p>

                    <p>
                        Thanks from the team at Orchestrate for giving Orchestrate a try!
                    </p>
                    """,
            })
            .ToList();
        await this.emailService.SendAsync(emails);
    }
}
