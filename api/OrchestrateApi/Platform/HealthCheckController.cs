using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Platform;

[ApiController]
[Host("*:9091")]
[Route("health-check")]
public class HealthCheckController : ControllerBase
{
    private readonly OrchestrateContext dbContext;

    public HealthCheckController(OrchestrateContext dbContext)
    {
        this.dbContext = dbContext;
    }

    [HttpGet("db")]
    public async Task<IActionResult> DbHealthCheck()
    {
        return await this.dbContext.Database.CanConnectAsync()
            ? Ok()
            : StatusCode(StatusCodes.Status500InternalServerError);
    }
}
