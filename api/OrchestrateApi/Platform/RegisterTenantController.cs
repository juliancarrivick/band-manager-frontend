using System.Text.Json.Serialization;
using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Security;
using OrchestrateApi.Validation;

namespace OrchestrateApi.Platform;

[ApiController]
[Route("tenant")]
[ValidationExceptionFilter]
public class RegisterTenantController : ControllerBase
{
    private readonly OrchestrateContext dbContext;
    private readonly UserManager<OrchestrateUser> userManager;
    private readonly UserService userService;
    private readonly ITenantService tenantService;
    private readonly IEmailService emailService;
    private readonly AutoMapper.IMapper mapper;
    private readonly IValidator<RegisterTenantDto> dtoValidator;
    private readonly IAppConfiguration appConfiguration;
    private readonly HttpClient httpClient;
    private readonly ILogger logger;
    private readonly FrontEnd frontEnd;

    public RegisterTenantController(
        OrchestrateContext dbContext,
        UserManager<OrchestrateUser> userManager,
        UserService userService,
        ITenantService tenantService,
        IEmailService emailService,
        AutoMapper.IMapper mapper,
        IValidator<RegisterTenantDto> dtoValidator,
        IAppConfiguration appConfiguration,
        HttpClient httpClient,
        ILogger<RegisterTenantController> logger,
        FrontEnd frontEnd,
        IWebHostEnvironment environment)
    {
        this.dbContext = dbContext;
        this.userManager = userManager;
        this.userService = userService;
        this.tenantService = tenantService;
        this.emailService = emailService;
        this.mapper = mapper;
        this.dtoValidator = dtoValidator;
        this.appConfiguration = appConfiguration;
        this.httpClient = httpClient;
        this.logger = logger;
        this.frontEnd = frontEnd;

        VerifyCaptcha = !environment.IsDevelopment();
    }

    // Allow disable for testing
    internal bool VerifyCaptcha { get; set; }

    [HttpPost("register")]
    public async Task<ActionResult<RegistrationResultDto>> RegisterTenant(RegisterTenantDto dto)
    {
        if (VerifyCaptcha && !await VerifyHcaptchaReponse(dto.HcaptchaResponse))
        {
            return BadRequest(new BasicErrorResponse("Unable to verify captcha.", new[]
            {
                new ResultError {Code="CAPTCHA_FAIL", Description = "Failed to validate captcha"},
            }));
        }

        await this.dtoValidator.ValidateAndThrowAsync(dto);

        using var transaction = await this.dbContext.Database.BeginOrStubExistingTransactionAsync();

        var tenant = CreateTenant(dto);
        await this.dbContext.SaveChangesAsync();
        using var tenantOverride = this.tenantService.ForceOverrideTenantId(tenant.Id);

        var user = await this.userService.GetOrCreateUserAsync(dto.RegisteringUser.Email);
        var member = new Member
        {
            FirstName = dto.RegisteringUser.FirstName,
            LastName = dto.RegisteringUser.LastName,
            DateJoined = DateTime.UtcNow,
            Details = new MemberDetails
            {
                Address = ".",
                Email = dto.RegisteringUser.Email,
                FeeClass = FeeClass.Full,
                PhoneNo = ".",
                User = user,
            },
        };
        this.dbContext.Add(member);
        await this.dbContext.SaveChangesAsync();
        await this.userManager.AddToRoleAsync(user, StandardRole.Administrator);

        await transaction.CommitAsync();

        await this.emailService.SendAsync(new[]
        {
            new EmailMessage
            {
                From = this.emailService.DefaultFrom with { Name = "Orchestrate" },
                To = new($"{member.FirstName} {member.LastName}", member.Details.Email),
                Subject = "Thank you for registering with Orchestrate",
                HtmlContent = await GenerateWelcomeEmailContent(tenant, member),
            },
            new EmailMessage
            {
                From = this.emailService.DefaultFrom,
                To = new(this.appConfiguration.NotificationEmail),
                Subject = $"Registration: {tenant.Name}",
                HtmlContent = $"""
                <p>
                    {member.FirstName} {member.LastName} has registered
                    <a href="{frontEnd.TenantHome(tenant)}">{tenant.Name}</a>
                    on Orchestrate.
                </p>
                """,
            },
        });

        return new RegistrationResultDto
        {
            PostRegisterAction = user.EmailConfirmed
                ? PostRegisterAction.Redirect
                : PostRegisterAction.CheckEmail,
            Tenant = this.mapper.Map<Security.TenantDto>(tenant),
        };
    }

    private async Task<bool> VerifyHcaptchaReponse(string hcaptchaResponse)
    {
        if (string.IsNullOrWhiteSpace(this.appConfiguration.HcaptchaSiteKey)
            || string.IsNullOrWhiteSpace(this.appConfiguration.HcaptchaSecret))
        {
            this.logger.LogError("HCaptcha is not configured");
            return false;
        }

        if (string.IsNullOrWhiteSpace(hcaptchaResponse))
        {
            return false;
        }

        var formContent = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("response", hcaptchaResponse),
            new KeyValuePair<string, string>("secret", this.appConfiguration.HcaptchaSecret),
            new KeyValuePair<string, string>("sitekey", this.appConfiguration.HcaptchaSiteKey),
        };
        if (Request.HttpContext.Connection.RemoteIpAddress is { } ip)
        {
            formContent.Add(new KeyValuePair<string, string>("remoteip", ip.ToString()));
        }

        using var httpContent = new FormUrlEncodedContent(formContent);
        var httpResponse = await this.httpClient.PostAsync(
            new Uri("https://hcaptcha.com/siteverify"),
            httpContent);
        if (!httpResponse.IsSuccessStatusCode)
        {
            this.logger.LogError(
                "Hcaptcha verification request failed ({Code}): {Body}",
                httpResponse.StatusCode,
                await httpResponse.Content.ReadAsStringAsync());
            return false;
        }

        var responseBody = await httpResponse.Content.ReadFromJsonAsync<HCaptchaSiteVerifyResponse>();
        var success = responseBody?.Success ?? false;
        if (!success)
        {
            this.logger.LogError(
                "Hcaptcha verification failed: {ErrorCodes}",
                string.Join(", ", responseBody?.ErrorCodes ?? Array.Empty<string>()));
        }

        return success;
    }

    private Tenant CreateTenant(RegisterTenantDto dto)
    {
        // Note: This method will eventually have much more logic in it :)
        var tenant = this.mapper.Map<Tenant>(dto.Tenant);
        tenant.DateRegistered = DateTime.UtcNow;
        tenant.ContactAddress = dto.RegisteringUser.Email;

        tenant.Plan.Type = PlanType.Trial;
        tenant.Plan.PeriodEnd = tenant.DateRegistered.Add(this.appConfiguration.TrialTimeSpan);

        this.dbContext.Add(tenant);
        return tenant;
    }

    private async Task<string> GenerateWelcomeEmailContent(Tenant tenant, Member member)
    {
        ArgumentNullException.ThrowIfNull(member?.Details?.User);

        var msg = $"""
            <p>Hi {member.FirstName}</p>

            <p>
                Thank you for registering {tenant.Name} for a trial of
                <a href="https://orchestrate.community">Orchestrate</a>.
            </p>
            """;

        var tenantUri = this.frontEnd.TenantHomeWithGetStartedUri(tenant, isFirstRun: true);
        if (member!.Details!.User!.EmailConfirmed)
        {
            msg += $"""
                <p>
                    To get started, go to
                    <a href="{tenantUri.AbsoluteUri}">{tenantUri.AbsoluteUri}</a>
                    and log in.
                </p>
                """;
        }
        else
        {
            var (passwordUrl, expiry) = await this.userService.GenerateResetLinkAsync(
                member.Details.User,
                tenantUri.AbsolutePath);
            msg += $"""
                <p>
                    To get started, set a password and log in at this link:
                    <a href="{passwordUrl.AbsoluteUri}">{passwordUrl.AbsoluteUri}</a>.
                </p>

                <p>This link will expire in {UserService.GetTokenExpiryEstimate(expiry)}.</p>

                <p>
                    Then, you may access your group at
                    <a href="{tenantUri.AbsoluteUri}">{tenantUri.AbsoluteUri}</a>
                </p>
                """;
        }

        return msg;
    }
}

public class HCaptchaSiteVerifyResponse
{
    // More available, see https://docs.hcaptcha.com/#verify-the-user-response-server-side
    public required bool Success { get; set; }

    [JsonPropertyName("error-codes")]
    public IList<string>? ErrorCodes { get; set; }
}

public class RegisterTenantAutoMapperProfile : AutoMapper.Profile
{
    public RegisterTenantAutoMapperProfile()
    {
        CreateMap<TenantDto, Tenant>(AutoMapper.MemberList.Source)
            .ForMember(e => e.Website, src => src.AllowNull())
            .ForMember(e => e.Logo, src => src.AllowNull());

        // TODO Move map to Security namespace
        CreateMap<Tenant, Security.TenantDto>()
            .ForMember(e => e.TenantId, src => src.MapFrom(e => e.Id))
            .ForMember(e => e.Logo, src => src.MapFrom(e => e.Logo != null ? e.Logo.ToDataUrl() : null));
    }
}

public enum PostRegisterAction
{
    Redirect,
    CheckEmail,
}

public class RegistrationResultDto
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public required PostRegisterAction PostRegisterAction { get; set; }

    public required Security.TenantDto Tenant { get; set; }
}

public class RegisterTenantDto
{
    public required string HcaptchaResponse { get; set; }

    public required UserDto RegisteringUser { get; set; }

    public required TenantDto Tenant { get; set; }
}

public class RegisterTenantDtoValidator : AbstractValidator<RegisterTenantDto>
{
    public RegisterTenantDtoValidator(
        IValidator<TenantDto> tenantDtoValidator,
        IValidator<UserDto> userDtoValidator)
    {
        RuleFor(e => e.Tenant).SetValidator(tenantDtoValidator);
        RuleFor(e => e.RegisteringUser).SetValidator(userDtoValidator);
    }
}

public class UserDto
{
    // Can hopefully be cleaned up with C# 12: https://github.com/dotnet/csharplang/issues/140
    private string email = null!;
    private string firstName = null!;
    private string lastName = null!;

    public required string Email { get => this.email; set => this.email = value.Trim(); }

    public required string FirstName { get => this.firstName; set => this.firstName = value.Trim(); }

    public required string LastName { get => this.lastName; set => this.lastName = value.Trim(); }
}

public class UserDtoValidator : AbstractValidator<UserDto>
{
    public UserDtoValidator()
    {
        RuleFor(e => e.Email).EmailAddress();
        RuleFor(e => e.FirstName).NotEmpty();
        RuleFor(e => e.LastName).NotEmpty();
    }
}

public class TenantDto
{
    // Can hopefully be cleaned up with C# 12: https://github.com/dotnet/csharplang/issues/140
    private string name = null!;
    private string abbr = null!;
    private string timeZone = null!;
    private string? website;

    public required string Name { get => this.name; set => this.name = value.Trim(); }

    public required string Abbreviation { get => this.abbr; set => this.abbr = value.Trim(); }

    public required string TimeZone { get => this.timeZone; set => this.timeZone = value.Trim(); }

    public string? Website { get => this.website; set => this.website = value?.Trim(); }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1819")]
    public byte[]? Logo { get; set; }

    public DateTime? DateFounded { get; set; }
}

public class TenantDtoValidator : AbstractValidator<TenantDto>
{
    public const string LogoTooLargeCode = "LogoTooLarge";
    public const string LogoInvalidImageCode = "LogoInvalidImage";
    public const int MaxLogoSizeMegabytes = 2;

    public TenantDtoValidator(OrchestrateContext dbContext)
    {
        RuleFor(t => t.Name).NotEmpty();
        RuleFor(t => t.Name).CaseInsensitiveUnique(
            dbContext.Tenant.IgnoreQueryFilters(),
            e => e.Name,
            "A group with that abbreviation already exists");

        RuleFor(t => t.Abbreviation).NotEmpty();
        RuleFor(t => t.Abbreviation).EmailAndUrlSlug();
        RuleFor(t => t.Abbreviation).CaseInsensitiveUnique(
            dbContext.Tenant.IgnoreQueryFilters(),
            e => e.Abbreviation,
            "A group with that abbreviation already exists");

        RuleFor(t => t.TimeZone)
            .Must(IsValidTimeZone)
            .WithMessage("Unknown timezone");
        RuleFor(t => t.Logo)
            .Must(logo => logo == null || logo.Length < MaxLogoSizeMegabytes * 1024 * 1024)
            .WithErrorCode(LogoTooLargeCode)
            .WithMessage($"Logo must be less than {MaxLogoSizeMegabytes}MB, please resize or compress");
        RuleFor(t => t.Logo)
            .Must(logo => logo == null || FileTypeDetector.Images.AnyMatchingFileType(logo))
            .WithErrorCode(LogoInvalidImageCode)
            .WithMessage("{PropertyName} is not a valid PNG or JPEG");
    }

    private static bool IsValidTimeZone(string timezone)
    {
        try
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById(timezone);
            return tz is not null;
        }
        catch (TimeZoneNotFoundException)
        {
            return false;
        }
    }
}

