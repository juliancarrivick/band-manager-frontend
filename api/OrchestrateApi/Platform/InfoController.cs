using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace OrchestrateApi.Platform;

[ApiController]
[Route("info")]
public class InfoController : ControllerBase
{
    [HttpGet]
    public IActionResult GetInfo()
    {
        return Ok(new
        {
            Assembly.GetExecutingAssembly().GetName().Version,
        });
    }
}
