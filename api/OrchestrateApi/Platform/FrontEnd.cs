using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Platform;

public class FrontEnd
{
    private readonly IAppConfiguration configuration;

    public FrontEnd(IAppConfiguration configuration)
    {
        this.configuration = configuration;
    }

    public Uri TenantHome(Tenant tenant)
    {
        var groupUrl = new UriBuilder(this.configuration.FrontEndUri)
        {
            Path = $"/group/{tenant.Abbreviation}/home",
        };
        return groupUrl.Uri;
    }

    public Uri TenantHomeWithGetStartedUri(Tenant tenant, bool isFirstRun = false)
    {
        var helpRoute = isFirstRun
            ? "view/get-started;isFirstRun=true"
            : "view/get-started";
        var groupUrl = new UriBuilder(this.configuration.FrontEndUri)
        {
            Path = $"/group/{tenant.Abbreviation}/(home//help:{helpRoute})",
        };
        return groupUrl.Uri;
    }

    public Uri TenantSubscriptionSettings(Tenant tenant)
    {
        var builder = new UriBuilder(this.configuration.FrontEndUri)
        {
            Path = $"/group/{tenant.Abbreviation}/settings;section=subscription",
        };
        return builder.Uri;
    }
}
