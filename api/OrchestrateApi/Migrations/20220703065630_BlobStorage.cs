﻿using System;
using System.Text.Json;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace OrchestrateApi.Migrations
{
    public partial class BlobStorage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "cover_photo_blob_id",
                table: "concert",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "blobs",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    mime_type = table.Column<string>(type: "text", nullable: false),
                    size_bytes = table.Column<long>(type: "bigint", nullable: false),
                    created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    modified = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    storage_strategy_name = table.Column<string>(type: "text", nullable: false),
                    storage_strategy_access_data = table.Column<JsonDocument>(type: "jsonb", nullable: false),
                    tenant_id = table.Column<Guid>(type: "uuid", nullable: false),
                    bytes = table.Column<byte[]>(type: "bytea", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_blobs", x => x.id);
                    table.ForeignKey(
                        name: "fk_blobs_tenant_tenant_id",
                        column: x => x.tenant_id,
                        principalTable: "tenant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "asset_attachment",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    asset_id = table.Column<int>(type: "integer", nullable: false),
                    blob_id = table.Column<Guid>(type: "uuid", nullable: false),
                    tenant_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_asset_attachment", x => x.id);
                    table.ForeignKey(
                        name: "fk_asset_attachment_asset_asset_id",
                        column: x => x.asset_id,
                        principalTable: "asset",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_asset_attachment_blobs_blob_id",
                        column: x => x.blob_id,
                        principalTable: "blobs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_asset_attachment_tenant_tenant_id",
                        column: x => x.tenant_id,
                        principalTable: "tenant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "concert_attachment",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    concert_id = table.Column<int>(type: "integer", nullable: false),
                    blob_id = table.Column<Guid>(type: "uuid", nullable: false),
                    tenant_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_concert_attachment", x => x.id);
                    table.ForeignKey(
                        name: "fk_concert_attachment_blobs_blob_id",
                        column: x => x.blob_id,
                        principalTable: "blobs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_concert_attachment_concert_concert_id",
                        column: x => x.concert_id,
                        principalTable: "concert",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_concert_attachment_tenant_tenant_id",
                        column: x => x.tenant_id,
                        principalTable: "tenant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "score_attachment",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    score_id = table.Column<int>(type: "integer", nullable: false),
                    blob_id = table.Column<Guid>(type: "uuid", nullable: false),
                    tenant_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_score_attachment", x => x.id);
                    table.ForeignKey(
                        name: "fk_score_attachment_blobs_blob_id",
                        column: x => x.blob_id,
                        principalTable: "blobs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_score_attachment_score_score_id",
                        column: x => x.score_id,
                        principalTable: "score",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_score_attachment_tenant_tenant_id",
                        column: x => x.tenant_id,
                        principalTable: "tenant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_concert_cover_photo_blob_id",
                table: "concert",
                column: "cover_photo_blob_id");

            migrationBuilder.CreateIndex(
                name: "ix_asset_attachment_asset_id_blob_id",
                table: "asset_attachment",
                columns: new[] { "asset_id", "blob_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_asset_attachment_blob_id",
                table: "asset_attachment",
                column: "blob_id");

            migrationBuilder.CreateIndex(
                name: "ix_asset_attachment_tenant_id",
                table: "asset_attachment",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_blobs_tenant_id",
                table: "blobs",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_concert_attachment_blob_id",
                table: "concert_attachment",
                column: "blob_id");

            migrationBuilder.CreateIndex(
                name: "ix_concert_attachment_concert_id_blob_id",
                table: "concert_attachment",
                columns: new[] { "concert_id", "blob_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_concert_attachment_tenant_id",
                table: "concert_attachment",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_score_attachment_blob_id",
                table: "score_attachment",
                column: "blob_id");

            migrationBuilder.CreateIndex(
                name: "ix_score_attachment_score_id_blob_id",
                table: "score_attachment",
                columns: new[] { "score_id", "blob_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_score_attachment_tenant_id",
                table: "score_attachment",
                column: "tenant_id");

            migrationBuilder.AddForeignKey(
                name: "fk_concert_blobs_cover_photo_blob_id",
                table: "concert",
                column: "cover_photo_blob_id",
                principalTable: "blobs",
                principalColumn: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_concert_blobs_cover_photo_blob_id",
                table: "concert");

            migrationBuilder.DropTable(
                name: "asset_attachment");

            migrationBuilder.DropTable(
                name: "concert_attachment");

            migrationBuilder.DropTable(
                name: "score_attachment");

            migrationBuilder.DropTable(
                name: "blobs");

            migrationBuilder.DropIndex(
                name: "ix_concert_cover_photo_blob_id",
                table: "concert");

            migrationBuilder.DropColumn(
                name: "cover_photo_blob_id",
                table: "concert");
        }
    }
}
