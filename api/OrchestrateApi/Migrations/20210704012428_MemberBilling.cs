﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace OrchestrateApi.Migrations
{
    public partial class MemberBilling : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "fee_class",
                table: "member",
                nullable: false,
                defaultValue: "Full",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true,
                oldDefaultValue: "Standard");

            migrationBuilder.AddColumn<bool>(
                name: "is_special",
                table: "ensemble_membership",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "member_billing_period_fee_dollars",
                table: "ensemble",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "member_billing_config",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    is_enabled = table.Column<bool>(nullable: false),
                    association_period_fee_dollars = table.Column<int>(nullable: false),
                    concession_rate = table.Column<decimal>(nullable: false),
                    early_bird_discount_dollars = table.Column<int>(nullable: false),
                    early_bird_discount_days_valid = table.Column<int>(nullable: false),
                    notification_contact_name = table.Column<string>(nullable: false),
                    notification_contact_email = table.Column<string>(nullable: false),
                    bank_name = table.Column<string>(nullable: true),
                    bank_account_name = table.Column<string>(nullable: true),
                    bank_bsb = table.Column<string>(nullable: false),
                    bank_account_no = table.Column<string>(nullable: false),
                    payment_instructions = table.Column<string>(nullable: true),
                    website = table.Column<string>(nullable: true),
                    time_zone_name = table.Column<string>(nullable: false),
                    invoice_prefix = table.Column<string>(nullable: false),
                    next_invoice_number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_member_billing_config", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "member_billing_period",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: false),
                    state = table.Column<string>(nullable: false, defaultValue: "Created"),
                    early_bird_due = table.Column<DateTime>(nullable: true),
                    due = table.Column<DateTime>(nullable: false),
                    generated = table.Column<DateTime>(nullable: true),
                    sent = table.Column<DateTime>(nullable: true),
                    closed = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_member_billing_period", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "member_billing_period_config",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: false),
                    auto_generate_month = table.Column<int>(nullable: false),
                    end_of_period_month = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_member_billing_period_config", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "member_invoice",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    member_billing_period_id = table.Column<int>(nullable: false),
                    member_id = table.Column<int>(nullable: false),
                    is_concession = table.Column<bool>(nullable: false),
                    reference = table.Column<string>(nullable: false),
                    sent = table.Column<DateTime>(nullable: true),
                    send_status = table.Column<string>(nullable: true),
                    send_status_updated = table.Column<DateTime>(nullable: true),
                    send_status_message = table.Column<string>(nullable: true),
                    paid = table.Column<DateTime>(nullable: true),
                    notes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_member_invoice", x => x.id);
                    table.ForeignKey(
                        name: "fk_member_invoice_member_billing_period_member_billing_period_~",
                        column: x => x.member_billing_period_id,
                        principalTable: "member_billing_period",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_member_invoice_member_member_id",
                        column: x => x.member_id,
                        principalTable: "member",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "member_invoice_line_item",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    member_invoice_id = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: false),
                    amount_cents = table.Column<int>(nullable: false),
                    ordinal = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_member_invoice_line_item", x => x.id);
                    table.ForeignKey(
                        name: "fk_member_invoice_line_item_member_invoice_member_invoice_id",
                        column: x => x.member_invoice_id,
                        principalTable: "member_invoice",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_member_invoice_member_billing_period_id",
                table: "member_invoice",
                column: "member_billing_period_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_invoice_member_id",
                table: "member_invoice",
                column: "member_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_invoice_line_item_member_invoice_id",
                table: "member_invoice_line_item",
                column: "member_invoice_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "member_billing_config");

            migrationBuilder.DropTable(
                name: "member_billing_period_config");

            migrationBuilder.DropTable(
                name: "member_invoice_line_item");

            migrationBuilder.DropTable(
                name: "member_invoice");

            migrationBuilder.DropTable(
                name: "member_billing_period");

            migrationBuilder.DeleteData(
                table: "asp_net_roles",
                keyColumn: "id",
                keyValue: "593f971c-9f4b-4e3f-b49a-86ace2bd0faa");

            migrationBuilder.DropColumn(
                name: "is_special",
                table: "ensemble_membership");

            migrationBuilder.DropColumn(
                name: "member_billing_period_fee_dollars",
                table: "ensemble");

            migrationBuilder.AlterColumn<string>(
                name: "fee_class",
                table: "member",
                type: "text",
                nullable: true,
                defaultValue: "Standard",
                oldClrType: typeof(string),
                oldDefaultValue: "Full");
        }
    }
}
