﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrchestrateApi.Migrations
{
    public partial class NotificationContactTracking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "notification_contact_notified",
                table: "member_billing_period",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "notification_contact_notified",
                table: "member_billing_period");
        }
    }
}
