﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrchestrateApi.Migrations
{
    public partial class MultiTenancy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_member_details_email",
                table: "member_details");

            migrationBuilder.DropIndex(
                name: "ix_ensemble_name",
                table: "ensemble");

            migrationBuilder.DropPrimaryKey(
                name: "pk_asp_net_user_roles",
                table: "asp_net_user_roles");

            migrationBuilder.DropColumn(
                name: "time_zone_name",
                table: "member_billing_config");

            migrationBuilder.DropColumn(
                name: "website",
                table: "member_billing_config");

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "score",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "performance_score",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "performance_member",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "performance",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "member_invoice_line_item",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "member_invoice",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "member_instrument",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "member_details",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "member_billing_period_config",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "member_billing_period",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "member_billing_config",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "member",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "ensemble_role",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "ensemble_membership",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "ensemble",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "concert",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "asset_loan",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "asset",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<bool>(
                name: "is_global_admin",
                table: "asp_net_users",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "tenant_id",
                table: "asp_net_user_roles",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "pk_asp_net_user_roles",
                table: "asp_net_user_roles",
                columns: new[] { "tenant_id", "user_id", "role_id" });

            migrationBuilder.CreateTable(
                name: "tenant",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    abbreviation = table.Column<string>(type: "text", nullable: false),
                    time_zone = table.Column<string>(type: "text", nullable: false),
                    website = table.Column<string>(type: "text", nullable: true),
                    logo = table.Column<byte[]>(type: "bytea", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_tenant", x => x.id);
                });
            migrationBuilder.Sql(@"
                INSERT INTO tenant (id, name, abbreviation, time_zone)
                VALUES ('00000000-0000-0000-0000-000000000000', 'Tenant', 'tenant', 'Australia/Perth')
            ");

            migrationBuilder.CreateIndex(
                name: "ix_score_tenant_id",
                table: "score",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_performance_score_tenant_id",
                table: "performance_score",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_performance_member_tenant_id",
                table: "performance_member",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_performance_tenant_id",
                table: "performance",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_invoice_line_item_tenant_id",
                table: "member_invoice_line_item",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_invoice_tenant_id",
                table: "member_invoice",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_instrument_tenant_id",
                table: "member_instrument",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_details_tenant_id_email",
                table: "member_details",
                columns: new[] { "tenant_id", "email" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_member_billing_period_config_tenant_id",
                table: "member_billing_period_config",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_billing_period_tenant_id",
                table: "member_billing_period",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_billing_config_tenant_id",
                table: "member_billing_config",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_member_tenant_id",
                table: "member",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_ensemble_role_tenant_id",
                table: "ensemble_role",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_ensemble_membership_tenant_id",
                table: "ensemble_membership",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_ensemble_tenant_id_name",
                table: "ensemble",
                columns: new[] { "tenant_id", "name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_concert_tenant_id",
                table: "concert",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_asset_loan_tenant_id",
                table: "asset_loan",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_asset_tenant_id",
                table: "asset",
                column: "tenant_id");

            migrationBuilder.CreateIndex(
                name: "ix_asp_net_user_roles_user_id",
                table: "asp_net_user_roles",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "ix_tenant_abbreviation",
                table: "tenant",
                column: "abbreviation",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_tenant_name",
                table: "tenant",
                column: "name",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "fk_asp_net_user_roles_tenant_tenant_id",
                table: "asp_net_user_roles",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_asset_tenant_tenant_id",
                table: "asset",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_asset_loan_tenant_tenant_id",
                table: "asset_loan",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_concert_tenant_tenant_id",
                table: "concert",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_ensemble_tenant_tenant_id",
                table: "ensemble",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_ensemble_membership_tenant_tenant_id",
                table: "ensemble_membership",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_ensemble_role_tenant_tenant_id",
                table: "ensemble_role",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_member_tenant_tenant_id",
                table: "member",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_member_billing_config_tenant_tenant_id",
                table: "member_billing_config",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_member_billing_period_tenant_tenant_id",
                table: "member_billing_period",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_member_billing_period_config_tenant_tenant_id",
                table: "member_billing_period_config",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_member_details_tenant_tenant_id",
                table: "member_details",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_member_instrument_tenant_tenant_id",
                table: "member_instrument",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_member_invoice_tenant_tenant_id",
                table: "member_invoice",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_member_invoice_line_item_tenant_tenant_id",
                table: "member_invoice_line_item",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_performance_tenant_tenant_id",
                table: "performance",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_performance_member_tenant_tenant_id",
                table: "performance_member",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_performance_score_tenant_tenant_id",
                table: "performance_score",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_score_tenant_tenant_id",
                table: "score",
                column: "tenant_id",
                principalTable: "tenant",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_asp_net_user_roles_tenant_tenant_id",
                table: "asp_net_user_roles");

            migrationBuilder.DropForeignKey(
                name: "fk_asset_tenant_tenant_id",
                table: "asset");

            migrationBuilder.DropForeignKey(
                name: "fk_asset_loan_tenant_tenant_id",
                table: "asset_loan");

            migrationBuilder.DropForeignKey(
                name: "fk_concert_tenant_tenant_id",
                table: "concert");

            migrationBuilder.DropForeignKey(
                name: "fk_ensemble_tenant_tenant_id",
                table: "ensemble");

            migrationBuilder.DropForeignKey(
                name: "fk_ensemble_membership_tenant_tenant_id",
                table: "ensemble_membership");

            migrationBuilder.DropForeignKey(
                name: "fk_ensemble_role_tenant_tenant_id",
                table: "ensemble_role");

            migrationBuilder.DropForeignKey(
                name: "fk_member_tenant_tenant_id",
                table: "member");

            migrationBuilder.DropForeignKey(
                name: "fk_member_billing_config_tenant_tenant_id",
                table: "member_billing_config");

            migrationBuilder.DropForeignKey(
                name: "fk_member_billing_period_tenant_tenant_id",
                table: "member_billing_period");

            migrationBuilder.DropForeignKey(
                name: "fk_member_billing_period_config_tenant_tenant_id",
                table: "member_billing_period_config");

            migrationBuilder.DropForeignKey(
                name: "fk_member_details_tenant_tenant_id",
                table: "member_details");

            migrationBuilder.DropForeignKey(
                name: "fk_member_instrument_tenant_tenant_id",
                table: "member_instrument");

            migrationBuilder.DropForeignKey(
                name: "fk_member_invoice_tenant_tenant_id",
                table: "member_invoice");

            migrationBuilder.DropForeignKey(
                name: "fk_member_invoice_line_item_tenant_tenant_id",
                table: "member_invoice_line_item");

            migrationBuilder.DropForeignKey(
                name: "fk_performance_tenant_tenant_id",
                table: "performance");

            migrationBuilder.DropForeignKey(
                name: "fk_performance_member_tenant_tenant_id",
                table: "performance_member");

            migrationBuilder.DropForeignKey(
                name: "fk_performance_score_tenant_tenant_id",
                table: "performance_score");

            migrationBuilder.DropForeignKey(
                name: "fk_score_tenant_tenant_id",
                table: "score");

            migrationBuilder.DropTable(
                name: "tenant");

            migrationBuilder.DropIndex(
                name: "ix_score_tenant_id",
                table: "score");

            migrationBuilder.DropIndex(
                name: "ix_performance_score_tenant_id",
                table: "performance_score");

            migrationBuilder.DropIndex(
                name: "ix_performance_member_tenant_id",
                table: "performance_member");

            migrationBuilder.DropIndex(
                name: "ix_performance_tenant_id",
                table: "performance");

            migrationBuilder.DropIndex(
                name: "ix_member_invoice_line_item_tenant_id",
                table: "member_invoice_line_item");

            migrationBuilder.DropIndex(
                name: "ix_member_invoice_tenant_id",
                table: "member_invoice");

            migrationBuilder.DropIndex(
                name: "ix_member_instrument_tenant_id",
                table: "member_instrument");

            migrationBuilder.DropIndex(
                name: "ix_member_details_tenant_id_email",
                table: "member_details");

            migrationBuilder.DropIndex(
                name: "ix_member_billing_period_config_tenant_id",
                table: "member_billing_period_config");

            migrationBuilder.DropIndex(
                name: "ix_member_billing_period_tenant_id",
                table: "member_billing_period");

            migrationBuilder.DropIndex(
                name: "ix_member_billing_config_tenant_id",
                table: "member_billing_config");

            migrationBuilder.DropIndex(
                name: "ix_member_tenant_id",
                table: "member");

            migrationBuilder.DropIndex(
                name: "ix_ensemble_role_tenant_id",
                table: "ensemble_role");

            migrationBuilder.DropIndex(
                name: "ix_ensemble_membership_tenant_id",
                table: "ensemble_membership");

            migrationBuilder.DropIndex(
                name: "ix_ensemble_tenant_id_name",
                table: "ensemble");

            migrationBuilder.DropIndex(
                name: "ix_concert_tenant_id",
                table: "concert");

            migrationBuilder.DropIndex(
                name: "ix_asset_loan_tenant_id",
                table: "asset_loan");

            migrationBuilder.DropIndex(
                name: "ix_asset_tenant_id",
                table: "asset");

            migrationBuilder.DropPrimaryKey(
                name: "pk_asp_net_user_roles",
                table: "asp_net_user_roles");

            migrationBuilder.DropIndex(
                name: "ix_asp_net_user_roles_user_id",
                table: "asp_net_user_roles");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "score");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "performance_score");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "performance_member");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "performance");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "member_invoice_line_item");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "member_invoice");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "member_instrument");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "member_details");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "member_billing_period_config");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "member_billing_period");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "member_billing_config");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "member");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "ensemble_role");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "ensemble_membership");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "ensemble");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "concert");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "asset_loan");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "asset");

            migrationBuilder.DropColumn(
                name: "is_global_admin",
                table: "asp_net_users");

            migrationBuilder.DropColumn(
                name: "tenant_id",
                table: "asp_net_user_roles");

            migrationBuilder.AddColumn<string>(
                name: "time_zone_name",
                table: "member_billing_config",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "website",
                table: "member_billing_config",
                type: "text",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "pk_asp_net_user_roles",
                table: "asp_net_user_roles",
                columns: new[] { "user_id", "role_id" });

            migrationBuilder.CreateIndex(
                name: "ix_member_details_email",
                table: "member_details",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_ensemble_name",
                table: "ensemble",
                column: "name",
                unique: true);
        }
    }
}
