﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrchestrateApi.Migrations
{
    /// <inheritdoc />
    public partial class PeriodicJobs : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "periodic_job_record",
                columns: table => new
                {
                    name = table.Column<string>(type: "text", nullable: false),
                    in_progress_start_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    in_progress_instance_name = table.Column<string>(type: "text", nullable: true),
                    completed_result = table.Column<string>(type: "text", nullable: true),
                    completed_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    completed_instance_name = table.Column<string>(type: "text", nullable: true),
                    concurrency_token = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_periodic_job_record", x => x.name);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "periodic_job_record");
        }
    }
}
