﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrchestrateApi.Migrations
{
    public partial class MemberTableSplit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "member_details",
                columns: table => new
                {
                    member_id = table.Column<int>(type: "integer", nullable: false),
                    phone_no = table.Column<string>(type: "text", nullable: false),
                    email = table.Column<string>(type: "text", nullable: true),
                    address = table.Column<string>(type: "text", nullable: false),
                    fee_class = table.Column<string>(type: "text", nullable: false, defaultValue: "Full"),
                    notes = table.Column<string>(type: "text", nullable: true),
                    user_id = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_member_details", x => x.member_id);
                    table.ForeignKey(
                        name: "fk_member_details_asp_net_users_user_id",
                        column: x => x.user_id,
                        principalTable: "asp_net_users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_member_details_member_member_id",
                        column: x => x.member_id,
                        principalTable: "member",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_member_details_email",
                table: "member_details",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_member_details_user_id",
                table: "member_details",
                column: "user_id");

            migrationBuilder.Sql(@"
                INSERT INTO member_details
                SELECT id, phone_no, email, address, fee_class, notes, user_id
                FROM member;
            ");

            migrationBuilder.DropForeignKey(
                name: "fk_member_asp_net_users_user_id",
                table: "member");

            migrationBuilder.DropIndex(
                name: "ix_member_email",
                table: "member");

            migrationBuilder.DropIndex(
                name: "ix_member_user_id",
                table: "member");

            migrationBuilder.DropColumn(
                name: "address",
                table: "member");

            migrationBuilder.DropColumn(
                name: "email",
                table: "member");

            migrationBuilder.DropColumn(
                name: "fee_class",
                table: "member");

            migrationBuilder.DropColumn(
                name: "membership_class",
                table: "member");

            migrationBuilder.DropColumn(
                name: "notes",
                table: "member");

            migrationBuilder.DropColumn(
                name: "phone_no",
                table: "member");

            migrationBuilder.DropColumn(
                name: "user_id",
                table: "member");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_joined",
                table: "member",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "date_joined",
                table: "member",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "member",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "email",
                table: "member",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "fee_class",
                table: "member",
                type: "text",
                nullable: false,
                defaultValue: "Full");

            migrationBuilder.AddColumn<string>(
                name: "membership_class",
                table: "member",
                type: "text",
                nullable: true,
                defaultValue: "Ordinary");

            migrationBuilder.AddColumn<string>(
                name: "notes",
                table: "member",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "phone_no",
                table: "member",
                type: "character varying(12)",
                maxLength: 12,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "user_id",
                table: "member",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_member_email",
                table: "member",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_member_user_id",
                table: "member",
                column: "user_id");

            migrationBuilder.AddForeignKey(
                name: "fk_member_asp_net_users_user_id",
                table: "member",
                column: "user_id",
                principalTable: "asp_net_users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
                UPDATE member m
                SET 
                    phone_no = d.phone_no,
                    email = d.email,
                    address = d.address,
                    fee_class = d.fee_class,
                    notes = d.notes,
                    user_id = d.user_id
                FROM member_details d
                WHERE m.id = d.member_id;
            ");

            migrationBuilder.DropTable(
                name: "member_details");
        }
    }
}
