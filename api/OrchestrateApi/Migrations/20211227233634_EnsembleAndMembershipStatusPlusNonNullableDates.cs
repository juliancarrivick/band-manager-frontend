﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrchestrateApi.Migrations
{
    public partial class EnsembleAndMembershipStatusPlusNonNullableDates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "date_joined",
                table: "ensemble_membership",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "type",
                table: "ensemble_membership",
                type: "text",
                nullable: false,
                defaultValue: "Regular");

            migrationBuilder.AddColumn<string>(
                name: "status",
                table: "ensemble",
                type: "text",
                nullable: false,
                defaultValue: "Active");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date",
                table: "concert",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.Sql(@"
                UPDATE ensemble e
                SET status = CASE WHEN is_hidden = true THEN 'Disbanded' ELSE 'Active' END
            ");
            migrationBuilder.Sql(@"
                UPDATE ensemble_membership m
                SET type = CASE WHEN is_special = true THEN 'Special' ELSE 'Regular' END
            ");

            migrationBuilder.DropColumn(
                name: "is_special",
                table: "ensemble_membership");

            migrationBuilder.DropColumn(
                name: "is_hidden",
                table: "ensemble");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "date_joined",
                table: "ensemble_membership",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AddColumn<bool>(
                name: "is_special",
                table: "ensemble_membership",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "is_hidden",
                table: "ensemble",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date",
                table: "concert",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.Sql(@"
                UPDATE ensemble e
                SET is_hidden = CASE WHEN status = 'Disbanded' THEN true ELSE false END
            ");
            migrationBuilder.Sql(@"
                UPDATE ensemble_membership m
                SET is_special = CASE WHEN type = 'Special' THEN true ELSE false END
            ");

            migrationBuilder.DropColumn(
                name: "type",
                table: "ensemble_membership");

            migrationBuilder.DropColumn(
                name: "status",
                table: "ensemble");
        }
    }
}
