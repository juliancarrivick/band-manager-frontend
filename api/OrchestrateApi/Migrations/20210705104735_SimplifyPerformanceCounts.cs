﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OrchestrateApi.Migrations
{
    public partial class SimplifyPerformanceCounts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "counts_as_seperate",
                table: "performance");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "counts_as_seperate",
                table: "performance",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }
    }
}
