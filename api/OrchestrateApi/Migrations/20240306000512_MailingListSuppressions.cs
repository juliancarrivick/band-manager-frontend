﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace OrchestrateApi.Migrations
{
    /// <inheritdoc />
    public partial class MailingListSuppressions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "postmark_server_token",
                table: "tenant",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "discriminator",
                table: "plan_metadata",
                type: "character varying(21)",
                maxLength: 21,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "postmark_stream_id",
                table: "mailing_list",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "mailing_list_suppression",
                columns: table => new
                {
                    tenant_id = table.Column<Guid>(type: "uuid", nullable: false),
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    mailing_list_id = table.Column<int>(type: "integer", nullable: false),
                    email = table.Column<string>(type: "text", nullable: false),
                    added_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    reason = table.Column<string>(type: "text", nullable: false),
                    is_removable = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_mailing_list_suppression", x => x.id);
                    table.ForeignKey(
                        name: "fk_mailing_list_suppression_mailing_list_mailing_list_id",
                        column: x => x.mailing_list_id,
                        principalTable: "mailing_list",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_mailing_list_suppression_tenant_tenant_id",
                        column: x => x.tenant_id,
                        principalTable: "tenant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_message_metadata_mailing_list_id",
                table: "mailing_list_message_metadata",
                column: "mailing_list_id");

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_suppression_mailing_list_id",
                table: "mailing_list_suppression",
                column: "mailing_list_id");

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_suppression_tenant_id",
                table: "mailing_list_suppression",
                column: "tenant_id");

            migrationBuilder.AddForeignKey(
                name: "fk_mailing_list_message_metadata_mailing_list_mailing_list_id",
                table: "mailing_list_message_metadata",
                column: "mailing_list_id",
                principalTable: "mailing_list",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_mailing_list_message_metadata_mailing_list_mailing_list_id",
                table: "mailing_list_message_metadata");

            migrationBuilder.DropTable(
                name: "mailing_list_suppression");

            migrationBuilder.DropIndex(
                name: "ix_mailing_list_message_metadata_mailing_list_id",
                table: "mailing_list_message_metadata");

            migrationBuilder.DropColumn(
                name: "postmark_server_token",
                table: "tenant");

            migrationBuilder.DropColumn(
                name: "postmark_stream_id",
                table: "mailing_list");

            migrationBuilder.AlterColumn<string>(
                name: "discriminator",
                table: "plan_metadata",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(21)",
                oldMaxLength: 21);
        }
    }
}
