﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace OrchestrateApi.Migrations
{
    /// <inheritdoc />
    public partial class MailingLists : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "mailing_list",
                columns: table => new
                {
                    tenant_id = table.Column<Guid>(type: "uuid", nullable: false),
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: false),
                    slug = table.Column<string>(type: "text", nullable: false),
                    allowed_senders = table.Column<string>(type: "text", nullable: false),
                    reply_to = table.Column<string>(type: "text", nullable: false),
                    footer = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_mailing_list", x => x.id);
                    table.ForeignKey(
                        name: "fk_mailing_list_tenant_tenant_id",
                        column: x => x.tenant_id,
                        principalTable: "tenant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "mailing_list_message_metadata",
                columns: table => new
                {
                    tenant_id = table.Column<Guid>(type: "uuid", nullable: false),
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    mailing_list_id = table.Column<int>(type: "integer", nullable: false),
                    sent = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_mailing_list_message_metadata", x => x.id);
                    table.ForeignKey(
                        name: "fk_mailing_list_message_metadata_tenant_tenant_id",
                        column: x => x.tenant_id,
                        principalTable: "tenant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "mailing_list_contact",
                columns: table => new
                {
                    mailing_list_id = table.Column<int>(type: "integer", nullable: false),
                    contact_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_mailing_list_contact", x => new { x.contact_id, x.mailing_list_id });
                    table.ForeignKey(
                        name: "fk_mailing_list_contact_contact_contact_id",
                        column: x => x.contact_id,
                        principalTable: "contact",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_mailing_list_contact_mailing_list_mailing_list_id",
                        column: x => x.mailing_list_id,
                        principalTable: "mailing_list",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "mailing_list_ensemble",
                columns: table => new
                {
                    mailing_list_id = table.Column<int>(type: "integer", nullable: false),
                    ensemble_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_mailing_list_ensemble", x => new { x.ensemble_id, x.mailing_list_id });
                    table.ForeignKey(
                        name: "fk_mailing_list_ensemble_ensemble_ensemble_id",
                        column: x => x.ensemble_id,
                        principalTable: "ensemble",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_mailing_list_ensemble_mailing_list_mailing_list_id",
                        column: x => x.mailing_list_id,
                        principalTable: "mailing_list",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "mailing_list_member",
                columns: table => new
                {
                    mailing_list_id = table.Column<int>(type: "integer", nullable: false),
                    member_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_mailing_list_member", x => new { x.mailing_list_id, x.member_id });
                    table.ForeignKey(
                        name: "fk_mailing_list_member_mailing_list_mailing_list_id",
                        column: x => x.mailing_list_id,
                        principalTable: "mailing_list",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_mailing_list_member_member_member_id",
                        column: x => x.member_id,
                        principalTable: "member",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_tenant_id_name",
                table: "mailing_list",
                columns: new[] { "tenant_id", "name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_tenant_id_slug",
                table: "mailing_list",
                columns: new[] { "tenant_id", "slug" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_contact_mailing_list_id",
                table: "mailing_list_contact",
                column: "mailing_list_id");

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_ensemble_mailing_list_id",
                table: "mailing_list_ensemble",
                column: "mailing_list_id");

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_member_member_id",
                table: "mailing_list_member",
                column: "member_id");

            migrationBuilder.CreateIndex(
                name: "ix_mailing_list_message_metadata_tenant_id",
                table: "mailing_list_message_metadata",
                column: "tenant_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "mailing_list_contact");

            migrationBuilder.DropTable(
                name: "mailing_list_ensemble");

            migrationBuilder.DropTable(
                name: "mailing_list_member");

            migrationBuilder.DropTable(
                name: "mailing_list_message_metadata");

            migrationBuilder.DropTable(
                name: "mailing_list");
        }
    }
}
