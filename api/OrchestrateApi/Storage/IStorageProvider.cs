using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Storage;

public interface IStorageProvider
{
    Task<IStreamableDisposible?> ReadBlobAsync(OrchestrateBlob blob, CancellationToken cancellationToken);

    Task<long> WriteBlobAsync(OrchestrateBlob blob, Stream bytes, CancellationToken cancellationToken);

    Task<bool> DeleteBlobAsync(OrchestrateBlob blob, CancellationToken cancellationToken);
}
