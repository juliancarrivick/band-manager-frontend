namespace OrchestrateApi.Storage;

// This concept borrowed from https://stackoverflow.com/a/58613644/17403538
public interface IStreamableDisposible : IDisposable
{
    public Stream Stream { get; }
}
