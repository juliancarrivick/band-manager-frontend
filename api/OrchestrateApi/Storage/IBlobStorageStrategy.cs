using System.Linq.Expressions;
using System.Security.Claims;
using System.Text.Json;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Storage;

/// <summary>
/// Represents a single implementation of security against a stored blob. Strategies can also
/// implement <see cref="ILinkedStorageStrategy" /> and <see cref="IValidatingStorageStrategy" />
/// </summary>
public interface IBlobStorageStrategy
{
    /// <summary>The name of this storage strategy</summary>
    string Name { get; }

    /// <summary>
    /// Attempt to parse the given access data into the strategy specific structure
    /// returning true if successful, false otherwise. The result of doing this should
    /// be used to pass into <see cref="UserCanRead"/> or <see cref="UserCanWrite"/>
    /// and written into the database.
    /// </summary>
    bool TryParseAccessData(JsonDocument rawData, out object? accessData);

    /// <summary>
    /// Is the provided object valid to be passed into <see cref="UserCanRead" /> and
    /// <see cref="UserCanWrite" />?
    /// </summary>
    bool IsValidAccessData(object? accessData);

    /// <summary>
    /// True if the given user has read access to a blob with the provided accessData, false otherwise.
    /// </summary>
    bool UserCanRead(ClaimsPrincipal user, Guid tenantId, object? accessData);

    /// <summary>
    /// True if the given user has write access to a blob with the provided accessData, false otherwise.
    /// </summary>
    bool UserCanWrite(ClaimsPrincipal user, Guid tenantId, object? accessData);

    /// <summary>
    /// Returns an expression which can be used to filter for all blobs the user can read using this strategy
    /// </summary>
    Expression<Func<OrchestrateBlob, bool>> ReadableBlobsExpr(ClaimsPrincipal user, Guid tenantId);

    /// <summary>
    /// Returns a database query which will return all the valid blob ids used by this storage strategy.
    /// Note, this shouldn't check the blobs table for blobs which use this security strategy as they may
    /// be orphaned (indeed this API is used to detect orphaned blobs).
    /// </summary>
    IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext);
}

public abstract class BlobStorageStrategy<TAccessData> : IBlobStorageStrategy
{
    public abstract string Name { get; }

    public bool TryParseAccessData(JsonDocument rawData, out object? accessData)
    {
        if (TryParseAccessDataTyped(rawData, out var typedAccessData))
        {
            accessData = typedAccessData;
            return true;
        }
        else
        {
            accessData = null;
            return false;
        }
    }

    public bool IsValidAccessData(object? accessData) => accessData is TAccessData;

    public abstract bool TryParseAccessDataTyped(JsonDocument rawData, out TAccessData? accessData);

    public bool UserCanRead(ClaimsPrincipal user, Guid tenantId, object? accessData)
        => UserCanRead(user, tenantId, (TAccessData?)accessData);

    public abstract bool UserCanRead(ClaimsPrincipal user, Guid tenantId, TAccessData? accessData);

    public bool UserCanWrite(ClaimsPrincipal user, Guid tenantId, object? accessData)
        => UserCanWrite(user, tenantId, (TAccessData?)accessData);

    public abstract bool UserCanWrite(ClaimsPrincipal user, Guid tenantId, TAccessData? accessData);

    public abstract Expression<Func<OrchestrateBlob, bool>> ReadableBlobsExpr(ClaimsPrincipal user, Guid tenantId);

    public abstract IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext);
}
