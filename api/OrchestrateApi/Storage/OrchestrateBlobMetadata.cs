using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Storage;

public class OrchestrateBlobMetadata
{
    public Guid Id { get; set; }

    public string Name { get; set; } = null!;

    public string MimeType { get; set; } = null!;

    public DateTime Modified { get; set; }

    public long SizeBytes { get; set; }

    public OrchestrateBlobStorageStrategyData AccessStrategy { get; set; } = null!;

    public static OrchestrateBlobMetadata FromBlob(OrchestrateBlob blob) => new OrchestrateBlobMetadata
    {
        Id = blob.Id,
        Name = blob.Name,
        MimeType = blob.MimeType,
        Modified = blob.Modified,
        SizeBytes = blob.SizeBytes,
        AccessStrategy = blob.StorageStrategy,
    };
}
