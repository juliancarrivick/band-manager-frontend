using System.Linq.Expressions;
using System.Security.Claims;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Storage;

public record class StorageStrategyDto(string Name, object? AccessData);

public class StorageService
{
    private readonly ITenantService tenantService;
    private readonly OrchestrateContext dbContext;
    private readonly IStorageProvider storageProvider;
    private readonly ILogger logger;

    public StorageService(
        ITenantService tenantService,
        OrchestrateContext dbContext,
        IStorageProvider storageProvider,
        IEnumerable<IBlobStorageStrategy> storageStrategies,
        ILogger<StorageService> logger)
    {
        this.tenantService = tenantService;
        this.dbContext = dbContext;
        this.storageProvider = storageProvider;
        this.logger = logger;
        StorageStrategies = storageStrategies;
    }

    public IEnumerable<IBlobStorageStrategy> StorageStrategies { get; }

    private IQueryable<OrchestrateBlob> Blobs => this.dbContext.Blobs;

    public async Task<OrchestrateBlob?> GetReadableBlobAsync(ClaimsPrincipal user, Guid id, CancellationToken cancellationToken = default)
    {
        if (this.tenantService.TenantId is not Guid tenantId)
        {
            return null;
        }

        var blob = await Blobs.SingleOrDefaultAsync(e => e.Id == id, cancellationToken);
        return GetBlobAccessStrategyAndData(blob).UserCanRead(user, tenantId)
            ? blob
            : null;
    }

    public async Task<IList<OrchestrateBlob>> GetReadableBlobsAsync(ClaimsPrincipal user, IEnumerable<Guid> ids, CancellationToken cancellationToken = default)
    {
        if (this.tenantService.TenantId is not Guid tenantId)
        {
            return Array.Empty<OrchestrateBlob>();
        }

        var blobs = await Blobs
            .Where(e => ids.Contains(e.Id))
            .ToListAsync(cancellationToken);
        return blobs
            .Where(e => GetBlobAccessStrategyAndData(e).UserCanRead(user, tenantId))
            .ToList();
    }

    public async Task<OrchestrateBlob?> GetWritableBlobAsync(ClaimsPrincipal user, Guid id, CancellationToken cancellationToken = default)
    {
        if (this.tenantService.TenantId is not Guid tenantId)
        {
            return null;
        }

        var blob = await Blobs.SingleOrDefaultAsync(e => e.Id == id, cancellationToken);
        return GetBlobAccessStrategyAndData(blob).UserCanWrite(user, tenantId)
            ? blob
            : null;
    }

    public async Task UpdateBlobStorageStrategyAsync(OrchestrateBlob blob, StorageStrategyDto ssDto, CancellationToken cancellationToken = default)
    {
        if (GetStorageStrategy(ssDto.Name) is not { } provider
            || !provider.IsValidAccessData(ssDto.AccessData))
        {
            throw new InvalidOperationException("Invalid storage strategy or access data was provided");
        }

        blob.StorageStrategy = new OrchestrateBlobStorageStrategyData
        {
            Name = provider.Name,
            AccessData = JsonSerializer.SerializeToDocument(ssDto.AccessData),
        };

        await this.dbContext.SaveChangesAsync(cancellationToken);
    }

    public Task<IStreamableDisposible?> ReadBlobAsync(OrchestrateBlob blob, CancellationToken cancellationToken = default)
    {
        return this.storageProvider.ReadBlobAsync(blob, cancellationToken);
    }

    public async Task<OrchestrateBlob> CreateBlobAsync(IUploadedBlob uploadedBlob, StorageStrategyDto ssDto, CancellationToken cancellationToken = default)
    {
        if (GetStorageStrategy(ssDto.Name) is not { } provider
            || !provider.IsValidAccessData(ssDto.AccessData))
        {
            throw new InvalidOperationException("Invalid storage strategy or access data was provided");
        }

        var blob = new OrchestrateBlob
        {
            Name = uploadedBlob.Name,
            MimeType = uploadedBlob.MimeType,
            Created = DateTime.UtcNow,
            Modified = DateTime.UtcNow,
            StorageStrategy = new OrchestrateBlobStorageStrategyData
            {
                Name = provider.Name,
                AccessData = JsonSerializer.SerializeToDocument(ssDto.AccessData),
            },
        };

        this.dbContext.Add(blob);
        await this.dbContext.SaveChangesAsync(cancellationToken);

        using (var bytes = uploadedBlob.OpenReadStream())
        {
            blob.SizeBytes = await this.storageProvider.WriteBlobAsync(blob, bytes, cancellationToken);
            if (blob.SizeBytes < 0)
            {
                throw new InvalidOperationException("Unable to save blob");
            }
        }

        await this.dbContext.SaveChangesAsync(cancellationToken);

        return blob;
    }

    public async Task<OrchestrateBlob?> WriteBlobAsync(Guid id, IUploadedBlob uploadedBlob, CancellationToken cancellationToken = default)
    {
        var blob = await Blobs.SingleOrDefaultAsync(e => e.Id == id, cancellationToken);
        if (blob == null)
        {
            return null;
        }

        blob.Name = uploadedBlob.Name;
        blob.MimeType = uploadedBlob.MimeType;

        using (var bytes = uploadedBlob.OpenReadStream())
        {
            blob.SizeBytes = await this.storageProvider.WriteBlobAsync(blob, bytes, cancellationToken);
            if (blob.SizeBytes < 0)
            {
                throw new InvalidOperationException("Unable to save blob");
            }
        }

        blob.Modified = DateTime.UtcNow;
        await this.dbContext.SaveChangesAsync(cancellationToken);

        return blob;
    }

    public async Task DeleteBlobAsync(OrchestrateBlob blob, CancellationToken cancellationToken = default)
    {
        var deletedSuccessfully = await this.storageProvider.DeleteBlobAsync(blob, cancellationToken);
        if (!deletedSuccessfully)
        {
            throw new InvalidOperationException("Unable to delete blob");
        }

        this.dbContext.Remove(blob);
        await this.dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task CleanupOrphanedBlobsAsync(CancellationToken cancellationToken = default)
    {
        var storageStrategyNames = StorageStrategies.Select(s => s.Name);
        var unknownBlobCount = await this.dbContext.Blobs
            .CountAsync(e => !storageStrategyNames.Contains(e.StorageStrategy.Name), cancellationToken);
        if (unknownBlobCount > 0)
        {
            this.logger.LogError("{BlobCount} blobs with unknown storage strategies", unknownBlobCount);
        }

        var orphanedBlobs = new List<OrchestrateBlob>();
        foreach (var storageStrategy in StorageStrategies)
        {
            var validBlobs = storageStrategy.ValidBlobIds(this.dbContext);
            orphanedBlobs.AddRange(await this.dbContext.Blobs
                .Where(e => e.StorageStrategy.Name == storageStrategy.Name)
                .Where(e => !validBlobs.Contains(e.Id))
                .ToListAsync(cancellationToken));
        }

        foreach (var blob in orphanedBlobs)
        {
            await DeleteBlobAsync(blob, cancellationToken);
            this.logger.LogInformation("Delete {BlobId}", blob.Id);
        }

        this.logger.LogInformation("Deleted {BlobCount} blobs", orphanedBlobs.Count);
    }

    public IBlobStorageStrategy? GetStorageStrategy(string storageStrategyName)
    {
        return StorageStrategies.FirstOrDefault(e => e.Name == storageStrategyName);
    }

    private BlobAccessorChecker GetBlobAccessStrategyAndData(OrchestrateBlob? blob)
    {
        static BlobAccessorChecker DenyChecker() => new BlobAccessorChecker(new DenyBlobAccessStrategy(), null);

        if (blob == null)
        {
            return DenyChecker();
        }

        using var logScope = this.logger.BeginScope("Blob: {BlobId}", blob.Id);

        var storageStrategy = GetStorageStrategy(blob.StorageStrategy.Name);
        if (storageStrategy == null)
        {
            this.logger.LogError("Unknown StorageStrategy ({StorageStrategy})", blob.StorageStrategy.Name);
            return DenyChecker();
        }

        if (!storageStrategy.TryParseAccessData(blob.StorageStrategy.AccessData, out var accessData))
        {
            this.logger.LogError("Invalid AccessData: {AccessData}", blob.StorageStrategy.AccessData);
            return DenyChecker();
        }

        return new BlobAccessorChecker(storageStrategy, accessData);
    }

    private record class BlobAccessorChecker(IBlobStorageStrategy Strategy, object? Data)
    {
        public bool UserCanRead(ClaimsPrincipal user, Guid tenantId) => Strategy.UserCanRead(user, tenantId, Data);

        public bool UserCanWrite(ClaimsPrincipal user, Guid tenantId) => Strategy.UserCanWrite(user, tenantId, Data);
    }

    private class DenyBlobAccessStrategy : IBlobStorageStrategy
    {
        public string Name => "Deny";

        public bool TryParseAccessData(JsonDocument rawData, out object? data)
        {
            data = null;
            return true;
        }

        public bool IsValidAccessData(object? data) => data is null;

        public bool UserCanRead(ClaimsPrincipal user, Guid tenantId, object? data) => false;

        public bool UserCanWrite(ClaimsPrincipal user, Guid tenantId, object? data) => false;

        public Expression<Func<OrchestrateBlob, bool>> ReadableBlobsExpr(ClaimsPrincipal user, Guid tenantId) => e => false;

        public IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext) => throw new NotImplementedException();
    }
}
