using System.Net;

namespace OrchestrateApi.Storage;

public interface IUploadedBlob
{
    public string Name { get; }

    public string MimeType { get; }

    public Stream OpenReadStream();
}

public class UploadedFormFile : IUploadedBlob
{
    private readonly IFormFile file;

    public UploadedFormFile(IFormFile file)
    {
        this.file = file;
    }

    public string Name => WebUtility.HtmlEncode(this.file.FileName);
    public string MimeType => this.file.ContentType;

    public Stream OpenReadStream() => this.file.OpenReadStream();
}
