using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Storage;

/// <summary>
/// Represents a storage strategy that stores blobs that are linked to some other entity
/// </summary>
public interface ILinkedStorageStrategy
{
    /// <summary>
    /// Attempt to parse the given linkData into a strategy specific structure to be used in
    /// <see cref="CreateLinkingEntity"/> and <see cref="CreateLinkFilterExpression"/>
    /// If <see cref="IsLinked"/> returns false, this should throw <see cref="NotImplementedException"/>.
    /// </summary>
    bool TryParseLinkData(JsonDocument rawData, [NotNullWhen(true)] out object? linkData);

    /// <summary>
    /// Is the provided object valid to be passed into <see cref="CreateLinkingEntity" /> and
    /// <see cref="LinkedBlobIds" />?
    /// </summary>
    bool IsValidLinkData(object? linkData);

    /// <summary>
    /// Create the entity used to link the given blob to an entity described by linkData, returning
    /// it without performing any processing.
    /// If <see cref="IsLinked"/> returns false, this should throw <see cref="NotImplementedException"/>.
    /// </summary>
    object CreateLinkingEntity(Guid blobId, object linkData);

    /// <summary>
    /// Create a queryable that contains the BlobIds that match the given linkData.
    /// If <see cref="IsLinked"/> returns false, this should throw <see cref="NotImplementedException"/>.
    /// </summary>
    IQueryable<Guid> LinkedBlobIds(OrchestrateContext dbContext, object linkData);
}

public static class LinkedStorageStrategyExtensions
{
    public static bool IsLinked(this IBlobStorageStrategy storageStrategy) => storageStrategy is ILinkedStorageStrategy;

    /// <summary>
    /// Returns true if the storage strategy isn't linking, or if the linking data passed in is not null
    /// and valid. False otherwise.
    /// </summary>
    public static bool TryParseRequiredLinkData(this IBlobStorageStrategy storageStrategy, JsonDocument? rawData, out object? linkData)
    {
        linkData = null;
        return storageStrategy is not ILinkedStorageStrategy linkedStorageStrategy
            || (rawData != null && linkedStorageStrategy.TryParseLinkData(rawData, out linkData));
    }

    /// <summary>
    /// Returns true if the storage strategy isn't linking, or if the linking data passed in is null
    /// or valid. False otherwise.
    /// </summary>
    public static bool TryParseOptionalLinkData(this IBlobStorageStrategy storageStrategy, JsonDocument? rawData, out object? linkData)
    {
        linkData = null;
        return storageStrategy is not ILinkedStorageStrategy linkedStorageStrategy
            || rawData == null
            || linkedStorageStrategy.TryParseLinkData(rawData, out linkData);
    }

    /// <summary>Returns the blobIds that are assocated with the optional link data, null otherwise</summary>
    public static IQueryable<Guid>? LinkedBlobIds(this IBlobStorageStrategy storageStrategy, OrchestrateContext dbContext, object? linkData)
    {
        return storageStrategy is ILinkedStorageStrategy linkedStorageStrategy && linkData != null
            ? linkedStorageStrategy.LinkedBlobIds(dbContext, linkData)
            : null;
    }

    /// <summary>
    /// For the given blob and linkData create a linking entity that will be used for filtering later.
    /// If this isn't a linked storage strategy, then will return null. If it is, and no linkData is provided
    /// then a ArgumentNullException will be thrown
    /// </summary>
    public static object? TryCreateLinkingEntity(this IBlobStorageStrategy storageStrategy, Guid blobId, object? linkData)
    {
        if (storageStrategy is ILinkedStorageStrategy linkedStorageStrategy)
        {
            if (linkData == null)
            {
                throw new ArgumentNullException(nameof(linkData), "linkData should not be null for ILinkedStorageStrategy");
            }

            return linkedStorageStrategy.CreateLinkingEntity(blobId, linkData);
        }
        else
        {
            return null;
        }
    }
}

public abstract class LinkedBlobStorageStrategy<TAccessData, TLinkData> : BlobStorageStrategy<TAccessData>, ILinkedStorageStrategy
{
    public bool TryParseLinkData(JsonDocument rawData, [NotNullWhen(true)] out object? linkData)
    {
        if (TryParseLinkDataTyped(rawData, out var typedAccessData))
        {
            linkData = typedAccessData;
            return true;
        }
        else
        {
            linkData = null;
            return false;
        }
    }

    public bool IsValidLinkData(object? linkData) => linkData is TLinkData;

    public abstract bool TryParseLinkDataTyped(JsonDocument rawData, [NotNullWhen(true)] out TLinkData? linkData);

    public object CreateLinkingEntity(Guid blobId, object linkData) => CreateLinkingEntity(blobId, (TLinkData)linkData);

    public abstract object CreateLinkingEntity(Guid blobId, TLinkData linkData);

    public IQueryable<Guid> LinkedBlobIds(OrchestrateContext dbContext, object linkData)
        => LinkedBlobIds(dbContext, (TLinkData)linkData);

    public abstract IQueryable<Guid> LinkedBlobIds(OrchestrateContext dbContext, TLinkData linkData);
}
