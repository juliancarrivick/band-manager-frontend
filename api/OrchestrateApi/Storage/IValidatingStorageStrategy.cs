namespace OrchestrateApi.Storage;

/// <summary>
/// Represents a storage strategy that has specific requirements on the types of files that are stored
/// </summary>
public interface IValidatingStorageStrategy
{
    /// <summary>
    /// Validate the uploaded blob against the requirements for this storage strategy
    /// </summary>
    Task<BlobValidationResult> ValidateAsync(IUploadedBlob blob, CancellationToken cancellationToken);
}

public static class ValidatingStorageStrategyExtensions
{
    public static async Task<BlobValidationResult?> TryValidate(this IBlobStorageStrategy storageStrategy, IUploadedBlob blob, CancellationToken cancellationToken)
    {
        return storageStrategy is IValidatingStorageStrategy validator
            ? await validator.ValidateAsync(blob, cancellationToken)
            : null;
    }
}

public class BlobValidationResult
{
    public bool IsValid { get; set; }

    public string? Message { get; set; }
}
