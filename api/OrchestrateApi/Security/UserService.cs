using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.Members;
using OrchestrateApi.Platform;

namespace OrchestrateApi.Security
{
    public class UserService(
        OrchestrateContext dbContext,
        UserManager<OrchestrateUser> userManager,
        IEmailService emailService,
        IAppConfiguration configuration,
        FrontEnd frontEnd) : IMemberInviter
    {
        public const string ApiUsername = "api_user";

        public async Task<bool> CheckApiUserCredentialsAsync(string username, string password)
        {
            if (ApiUsername != username)
            {
                return false;
            }

            var user = await userManager.FindByNameAsync(ApiUsername);
            if (user == null)
            {
                return false;
            }

            return await userManager.CheckPasswordAsync(user, password);
        }

        public async Task<IdentityResult> UpdateUserEmail(OrchestrateUser user, string? newEmail)
        {
            var result = await userManager.SetEmailAsync(user, newEmail);
            if (!result.Succeeded)
            {
                return result;
            }

            result = await userManager.SetUserNameAsync(user, newEmail);
            if (!result.Succeeded)
            {
                return result;
            }

            var membersToUpdate = await dbContext.Member
                .IgnoreQueryFilters()
                .Select(m => m.Details!)
                .Where(m => m.UserId == user.Id && m.Email != newEmail)
                .ToListAsync();
            foreach (var member in membersToUpdate)
            {
                member.Email = newEmail;
            }
            await dbContext.SaveChangesAsync();

            // TODO #124 Send confirmation email with EmailConfirmationToken

            return IdentityResult.Success;
        }

        public async Task<JwtSecurityToken> GenerateBearerTokenAsync(OrchestrateUser user)
        {
            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.UserName ?? string.Empty),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            if (user.IsGlobalAdmin)
            {
                claims.Add(new Claim(ClaimsPrincipalExtensions.GlobalAdminClaimName, bool.TrueString));
            }

            var member = await dbContext.Member
                .IgnoreQueryFilters()
                .OrderByDescending(m => m.DateJoined)
                .FirstOrDefaultAsync(m => m.Details!.UserId == user.Id);
            if (member != null)
            {
                claims.Add(new Claim(ClaimTypes.GivenName, member.FirstName));
                claims.Add(new Claim(ClaimTypes.Surname, member.LastName));
            }

            var tenantRoles = await ActiveUserRoles(allTenants: true)
                .Where(e => e.UserId == user.Id)
                .Select(e => new { e.TenantId, e.Role })
                .Distinct()
                .Select(e => new TenantRole(e.TenantId, e.Role))
                .ToListAsync();
            foreach (var tenantRole in tenantRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, tenantRole.Serialise()));
            }

            var salt = configuration.JwtSecret;
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(salt));
            var token = new JwtSecurityToken(
                issuer: configuration.JwtIssuer,
                audience: configuration.FrontEndUri.AbsoluteUri,
                expires: DateTime.UtcNow.Add(configuration.TokenLifetime),
                claims: claims,
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));

            return token;
        }

        /// <summary>
        /// Gets user roles for the current tenant, or all tenants if specified
        /// Beware, if you set allTenants:true and use subsequent Where clauses
        /// on tenanted tables they will be using .IgnoreQueryFilters()
        /// See https://github.com/dotnet/efcore/issues/17347
        /// </summary>
        public IQueryable<UserTenantRole> ActiveUserRoles(bool allTenants = false)
        {
            // Should all of this method be a DB view perhaps?

            IQueryable<T> TenantedEntities<T>(IQueryable<T> entities) where T : class, ITenanted
                => allTenants ? entities.IgnoreQueryFilters() : entities;

            var activeMembers =
                from member in TenantedEntities(dbContext.Member)
                    .Where(e => e.Details!.UserId != null)
                    .WhereIsActiveNow()
                select member;

            // Some of this complexity could be avoided by not using Identity and instead joining
            // via a new MemberRole table (though this would make other parts more complicated)
            var directRolesQuery =
                from userRole in TenantedEntities(dbContext.UserRoles)
                join member in activeMembers
                    on new { TenantId = EF.Property<Guid>(userRole, ITenanted.Column), userRole.UserId }
                    equals new { TenantId = EF.Property<Guid>(member, ITenanted.Column), UserId = member.Details!.UserId!.Value }
                join role in dbContext.Roles on userRole.RoleId equals role.Id
                select new UserTenantRole
                {
                    UserId = member.Details!.UserId!.Value,
                    TenantId = EF.Property<Guid>(userRole, ITenanted.Column),
                    MemberId = member.Id,
                    EnsembleId = null,
                    Role = role!.Name!,
                };

            var ensembleRolesQuery =
                from membership in TenantedEntities(dbContext.EnsembleMembership).WhereIsActiveNow()
                join member in activeMembers on membership.MemberId equals member.Id
                join ensembleRole in TenantedEntities(dbContext.EnsembleRole)
                    on membership.EnsembleId equals ensembleRole.EnsembleId
                select new UserTenantRole
                {
                    UserId = membership.Member!.Details!.UserId!.Value,
                    TenantId = EF.Property<Guid>(membership, ITenanted.Column),
                    MemberId = membership.MemberId,
                    EnsembleId = membership.EnsembleId,
                    Role = ensembleRole.Role!.Name!,
                };

            return directRolesQuery.Union(ensembleRolesQuery);
        }

        public async Task RequestResetPassword(string? email, string? redirect = null)
        {
            email = email?.Trim();
            if (string.IsNullOrWhiteSpace(email)
                || await userManager.FindByEmailAsync(email) is not OrchestrateUser user)
            {
                return;
            }

            var member = await dbContext.Member
                .IgnoreQueryFilters()
                .OrderByDescending(e => e.DateJoined)
                .FirstOrDefaultAsync(e => e.Details!.UserId == user.Id);
            var (resetLink, expiry) = await GenerateResetLinkAsync(user, redirect);
            await emailService.SendAsync(new EmailMessage
            {
                To = new EmailMessageAddress(
                    member != null ? $"{member.FirstName} {member.LastName}" : user.UserName,
                    email),
                Subject = "Password Reset Request",
                HtmlContent = $"""
                    <p>Hi {member?.FirstName ?? "there"},</p>

                    <p>We recently received a request to reset your password.</p>

                    <p>If this was not you, please disregard this email.</p>

                    <p>
                        Otherwise please click this link to reset your password:
                        <a href="{resetLink}">{resetLink}</a>
                    </p>

                    <p>This link will expire in {GetTokenExpiryEstimate(expiry)}.</p>
                    """,
            });
        }

        public async Task<Member> InviteMember(Member member)
        {
            member = await dbContext.Member
                .Include(e => e.Details!.User)
                .SingleAsync(e => e.Id == member.Id);

            if (member.Details?.Email is null)
            {
                throw new InvalidOperationException("Member must have an email address");
            }

            if (member.Details!.User == null)
            {
                var existingUser = await userManager.FindByEmailAsync(member.Details.Email!);
                member.Details.User = existingUser ?? await CreateUserAsync(member.Details.Email!);
                await dbContext.SaveChangesAsync();
            }

            var tenant = await dbContext.Tenant.SingleAsync();
            await emailService.SendAsync(new EmailMessage
            {
                From = emailService.DefaultFrom with { Name = tenant.Name },
                To = new($"{member.FirstName} {member.LastName}", member.Details.Email),
                Subject = $"Invitation to {tenant.Name}",
                HtmlContent = await GenerateInvitationEmailContent(tenant, member),
            });

            return member;
        }

        public async Task<OrchestrateUser> GetOrCreateUserAsync(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            user ??= await CreateUserAsync(email);
            return user;
        }

        private async Task<OrchestrateUser> CreateUserAsync(string email)
        {
            var user = new OrchestrateUser
            {
                UserName = email,
                Email = email,
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            var result = await userManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                throw new InvalidOperationException("Unable to create user");
            }

            return user;
        }

        private async Task<string> GenerateInvitationEmailContent(Tenant tenant, Member member)
        {
            var msg = $"""
                <p>Hi {member.FirstName},</p>

                <p>
                    You have been invited to access {tenant.Name} on
                    <a href="https://orchestrate.community">Orchestrate</a>,
                    a platform dedicated to community music groups.
                </p>
                """;

            if (await userManager.IsEmailConfirmedAsync(member.Details!.User!))
            {
                var welcomeUri = frontEnd.TenantHomeWithGetStartedUri(tenant);
                msg += $"""
                    <p>
                        You may access this group at the following location:
                        <a href="{welcomeUri.AbsoluteUri}">{welcomeUri.AbsoluteUri}</a>
                    <p>
                    """;
            }
            else
            {
                var (resetLink, expiry) = await GenerateResetLinkAsync(member.Details!.User!);
                msg += $"""
                    <p>If you are not part of this group, please disregard this email.</p>

                    <p>
                        Otherwise you may set a password and log in by clicking this link:
                        <a href="{resetLink}">{resetLink}</a>
                    </p>

                    <p>This link will expire in {GetTokenExpiryEstimate(expiry)}.</p>
                    """;
            }

            return msg;
        }

        public async Task<(Uri, DateTime)> GenerateResetLinkAsync(OrchestrateUser user, string? redirect = null)
        {
            ArgumentNullException.ThrowIfNull(user);

            var frontendUri = configuration.FrontEndUri;
            var token = await userManager.GeneratePasswordResetTokenAsync(user);
            var tokenExpiry = DateTime.UtcNow.Add(configuration.TokenLifetime);

            var queryParams = new List<string>
            {
                $"email={Uri.EscapeDataString(user.Email!)}",
                $"token={Uri.EscapeDataString(token)}",
                $"expiry={Uri.EscapeDataString(tokenExpiry.ToString("o"))}",
            };
            if (!string.IsNullOrWhiteSpace(redirect))
            {
                queryParams.Add($"redirect={Uri.EscapeDataString(redirect)}");
            }

            var uriBuilder = new UriBuilder(frontendUri)
            {
                Path = "set-password",
                Query = string.Join("&", queryParams),
            };
            return (uriBuilder.Uri, tokenExpiry);
        }

        public static string GetTokenExpiryEstimate(DateTime expiry)
        {
            var timespan = expiry - DateTime.UtcNow;
            return $"{Math.Round(timespan.TotalHours):F0} hours";
        }
    }

    public class UserTenantRole
    {
        public Guid UserId { get; set; }

        public Guid TenantId { get; set; }

        public int MemberId { get; set; }

        public int? EnsembleId { get; set; }

        public required string Role { get; set; }
    }
}
