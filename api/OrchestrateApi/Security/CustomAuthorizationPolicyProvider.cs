using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace OrchestrateApi.Security
{
    public class CustomAuthorizationPolicyProvider : IAuthorizationPolicyProvider
    {
        private readonly AuthorizationPolicy DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
            .RequireAuthenticatedUser()
            .Build();

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync() => Task.FromResult(DefaultPolicy);

        public Task<AuthorizationPolicy?> GetFallbackPolicyAsync() => Task.FromResult<AuthorizationPolicy?>(null);

        public Task<AuthorizationPolicy?> GetPolicyAsync(string policyName)
        {
            if (HasTenantPermissionRequirement.Parse(policyName) is { } htpRequirement)
            {
                return Task.FromResult(BuildPolicyFromRequirements(htpRequirement));
            }
            else if (StandardTenantPermissionRequirement.Parse(policyName) is { } stpRequirement)
            {
                return Task.FromResult(BuildPolicyFromRequirements(stpRequirement));
            }
            else if (policyName == IsGlobalAdminRequirement.PolicyName)
            {
                return Task.FromResult(BuildPolicyFromRequirements(new IsGlobalAdminRequirement()));
            }
            else if (policyName == IsApiUserRequirement.PolicyName)
            {
                return Task.FromResult(BuildPolicyFromRequirements(new IsApiUserRequirement()));
            }

            return Task.FromResult<AuthorizationPolicy?>(null);
        }

        private static AuthorizationPolicy? BuildPolicyFromRequirements(params IAuthorizationRequirement[] requirements)
        {
            var policy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme);
            policy.AddRequirements(requirements);
            return policy.Build();
        }
    }
}
