using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace Microsoft.AspNetCore.Identity;

public static class UserManagerExtensions
{
    public static async Task<IEnumerable<FeaturePermission>> GetPermissionsAsync(
        this UserManager<OrchestrateUser> userManager,
        OrchestrateUser user)
    {
        var roles = await userManager.GetRolesAsync(user);
        var permissions = roles.SelectMany(e => StandardRole.GetRolePermissions(e));
        return permissions.Distinct();
    }
}
