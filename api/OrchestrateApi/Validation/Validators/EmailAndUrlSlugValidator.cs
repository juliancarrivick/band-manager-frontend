using FluentValidation;
using FluentValidation.Validators;

namespace OrchestrateApi.Validation.Validators;

public class EmailAndUrlSlugValidator<T> : PropertyValidator<T, string>
{
    public override string Name => "EmailAndUrlSlugValidator";

    public override bool IsValid(ValidationContext<T> context, string value)
        => value.All(c => char.IsLetterOrDigit(c) || c == '-');

    protected override string GetDefaultMessageTemplate(string errorCode)
        => "Must be composed of letters, numbers and dashes ('-')";
}
