using System.Linq.Expressions;
using FluentValidation;
using OrchestrateApi.Common;
using OrchestrateApi.Validation.Validators;

namespace OrchestrateApi.Validation;

public static class RuleBuilderExtensions
{
    public static IRuleBuilderInitial<T, TProperty> Optional<T, TProperty>(
        this IRuleBuilderInitial<T, Optional<TProperty>> ruleBuilder)
    {
        return new OptionalRuleBuilder<T, TProperty>(ruleBuilder);
    }

    public static IRuleBuilderOptions<T, Optional<TProperty>> SetOptionalValidator<T, TProperty>(
        this IRuleBuilder<T, Optional<TProperty>> ruleBuilder,
        IValidator<TProperty> validator)
    {
        return ruleBuilder.SetValidator(new OptionalValidator<TProperty>(validator));
    }

    public static IRuleBuilderOptions<T, string> CaseInsensitiveUnique<T, TEntity>(
        this IRuleBuilder<T, string> ruleBuilder,
        IQueryable<TEntity> collection,
        Expression<Func<TEntity, string>> getField,
        string notUniqueMessage)
    {
        return ruleBuilder.SetAsyncValidator(new CaseInsensitiveUniqueValidator<T, TEntity>(
            collection,
            getField)
        {
            ErrorMessage = notUniqueMessage,
        });
    }

    public static IRuleBuilderOptions<T, string> EmailAndUrlSlug<T>(
        this IRuleBuilder<T, string> ruleBuilder)
    {
        return ruleBuilder.SetValidator(new EmailAndUrlSlugValidator<T>());
    }
}
