using FluentValidation;
using FluentValidation.Validators;
using OrchestrateApi.Common;
using OrchestrateApi.Validation.Validators;

namespace OrchestrateApi.Validation;

public class OptionalRuleBuilder<T, TProperty> : IRuleBuilderInitial<T, TProperty>, IRuleBuilderOptions<T, TProperty>
{
    private readonly IRuleBuilderInitial<T, Optional<TProperty>> actualRuleBuilder;

    public OptionalRuleBuilder(IRuleBuilderInitial<T, Optional<TProperty>> actualRuleBuilder)
    {
        this.actualRuleBuilder = actualRuleBuilder;
    }

    public IRuleBuilderOptions<T, TProperty> DependentRules(Action action)
    {
        throw new NotImplementedException("Dependent rules not supported for Optional properties");
    }

    public IRuleBuilderOptions<T, TProperty> SetAsyncValidator(IAsyncPropertyValidator<T, TProperty> validator)
    {
        this.actualRuleBuilder.SetAsyncValidator(new OptionalAsyncPropertyValidator<T, TProperty>(validator));
        return this;
    }

    public IRuleBuilderOptions<T, TProperty> SetValidator(IPropertyValidator<T, TProperty> validator)
    {
        this.actualRuleBuilder.SetValidator(new OptionalPropertyValidator<T, TProperty>(validator));
        return this;
    }

    public IRuleBuilderOptions<T, TProperty> SetValidator(IValidator<TProperty> validator, params string[] ruleSets)
    {
        this.actualRuleBuilder.SetValidator(new OptionalValidator<TProperty>(validator), ruleSets);
        return this;
    }

    public IRuleBuilderOptions<T, TProperty> SetValidator<TValidator>(Func<T, TValidator> validatorProvider, params string[] ruleSets)
        where TValidator : IValidator<TProperty>
    {
        this.actualRuleBuilder.SetValidator(
            t => new OptionalValidator<TProperty>(validatorProvider(t)),
            ruleSets);
        return this;
    }

    public IRuleBuilderOptions<T, TProperty> SetValidator<TValidator>(Func<T, TProperty, TValidator> validatorProvider, params string[] ruleSets)
        where TValidator : IValidator<TProperty>
    {
        this.actualRuleBuilder.SetValidator(
            (t, p) => new OptionalValidator<TProperty>(p.HasValue
                ? validatorProvider(t, p.Value)
                : new AlwaysValidValidator()),
            ruleSets);
        return this;
    }

    private class AlwaysValidValidator : AbstractValidator<TProperty>
    {
    }
}
