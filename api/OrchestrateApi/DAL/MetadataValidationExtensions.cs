using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Breeze.Persistence;
using OrchestrateApi.Common;

namespace OrchestrateApi.DAL
{
    public static class MetadataValidationExtensions
    {
        private static readonly Dictionary<Type, string> DataTypeValidatorNameMapping = new()
        {
            // Map this so that default phone validator in BreezeJS "just works" (otherwise we need
            // to register an alias from the 'phone' validator to 'phoneNumber' which is the DataTypeName
            // of the PhoneAttribute)
            [typeof(PhoneAttribute)] = "phone",
        };

        /// <summary>
        /// Add custom validators that Breeze doesn't add to the metadata by default.
        /// Consumers of the metadata will need to provide implementations of these
        /// custom validator (if they aren't already provided: http://breeze.github.io/doc-js/validation.html)
        /// </summary>
        public static void PopulateCustomValidators(this BreezeMetadata metadata)
        {
            foreach (var type in metadata.StructuralTypes)
            {
                type.PopulateCustomValidators();
            }
        }

        private static void PopulateCustomValidators(this MetaType metaType)
        {
            if (metaType.GetClrType() is not Type clrType)
            {
                return;
            }

            foreach (var property in metaType.DataProperties)
            {
                var clrProperty = clrType.GetProperty(property.NameOnServer);
                if (clrProperty == null)
                {
                    // Shadow property (e.g. TenantId)
                    continue;
                }

                var validationAttributeTypes = clrProperty.CustomAttributes
                    .Select(a => a.AttributeType)
                    .Where(a => a.IsAssignableTo(typeof(ValidationAttribute)));

                foreach (var attributeType in validationAttributeTypes)
                {
                    var attributeInstance = Attribute.GetCustomAttribute(clrProperty, attributeType);
                    if (attributeInstance is DataTypeAttribute dataTypeAttribute)
                    {
                        var validatorName = DataTypeValidatorNameMapping.GetValueOrDefault(
                            dataTypeAttribute.GetType(),
                            dataTypeAttribute.GetDataTypeName().ToCamelCase());
                        property.Validators.Add(new MetaValidator(validatorName));
                    }
                    else if (attributeInstance is RangeAttribute rangeAttribute)
                    {
                        property.Validators.Add(new RangeMetaValidator
                        {
                            Minimum = rangeAttribute.Minimum,
                            Maximum = rangeAttribute.Maximum,
                        });
                    }
                }
            }
        }

        private static Type? GetClrType(this MetaType type)
        {
            return Assembly.GetExecutingAssembly().GetType($"{type.Namespace}.{type.ShortName}");
        }

        private class RangeMetaValidator : MetaValidator
        {
            public RangeMetaValidator() : base("range") { }

            public required object Minimum { get; set; }

            public required object Maximum { get; set; }
        }
    }
}
