namespace OrchestrateApi.DAL.Hook
{
    public interface IEntityHookFactoryRegistrar
    {
        IEntityHookFactory? GetHookFactory(Type entityType);
    }

    public class EntityHookFactoryRegistrar : IEntityHookFactoryRegistrar
    {
        private readonly IServiceProvider services;

        public EntityHookFactoryRegistrar(IServiceProvider services)
        {
            this.services = services;
        }

        public IEntityHookFactory? GetHookFactory(Type entityType)
        {
            var factoryType = typeof(IEntityHookFactory<>).MakeGenericType(entityType);
            return (IEntityHookFactory?)this.services.GetService(factoryType);
        }
    }
}
