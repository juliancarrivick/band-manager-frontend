using Breeze.Persistence;
using Microsoft.EntityFrameworkCore;
using EntityState = Breeze.Persistence.EntityState;

namespace OrchestrateApi.DAL.Hook
{
    public interface IEntityHook
    {
        Type EntityType { get; }

        IEnumerable<EntityError> Validate(EntityInfo entityInfo);

        Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<EntityInfo> entityInfos, CancellationToken cancellationToken);

        Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<EntityInfo> entityInfos, CancellationToken cancellationToken);
    }

    public interface IEntityHook<T> : IEntityHook
    {
        IEnumerable<EntityError> Validate(IEntityInfo<T> entityInfo);

        Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<T>> entityInfos, CancellationToken cancellationToken);

        Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<T>> entityInfos, CancellationToken cancellationToken);
    }

    public abstract class EntityHook<T> : IEntityHook<T>
    {
        public Type EntityType => typeof(T);

        public required OrchestratePersistenceManager PersistenceManager { get; init; }

        public required Dictionary<Type, List<EntityInfo>> SaveMap { get; init; }

        public OrchestrateContext Context => PersistenceManager.Context;

        public IEnumerable<EntityError> Validate(EntityInfo entityInfo) => Validate(new EntityInfo<T>(entityInfo));

        public abstract IEnumerable<EntityError> Validate(IEntityInfo<T> entityInfo);

        public Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<EntityInfo> entityInfos, CancellationToken cancellationToken)
        {
            var typedEntityInfos = entityInfos.Select(e => new EntityInfo<T>(e)).ToList();
            return BeforeSaveAsync(typedEntityInfos, cancellationToken);
        }

        public abstract Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<T>> entityInfos, CancellationToken cancellationToken);

        public Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<EntityInfo> entityInfos, CancellationToken cancellationToken)
        {
            var typedEntityInfos = entityInfos.Select(e => new EntityInfo<T>(e)).ToList();
            return AfterSaveAsync(typedEntityInfos, cancellationToken);
        }

        public abstract Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<T>> entityInfos, CancellationToken cancellationToken);

        protected void ReturnToClient(object entity, EntityState entityState)
        {
            EntityInfo? existingEntry = null;
            if (SaveMap.TryGetValue(entity.GetType(), out var entityInfos))
            {
                var key = Context.Model.FindEntityType(entity.GetType())?.FindPrimaryKey();
                if (key == null)
                {
                    throw new InvalidOperationException($"Unable to find primary key of type {entity.GetType().Name}");
                }

                var entityKey = key.Properties.Select(p => p.PropertyInfo!.GetValue(entity)).ToArray();
                existingEntry = entityInfos.FirstOrDefault(e =>
                {
                    var candidateKey = key.Properties.Select(p => p.PropertyInfo!.GetValue(e.Entity));
                    return candidateKey.SequenceEqual(entityKey);
                });
            }

            if (existingEntry == null)
            {
                SaveMap.Add(PersistenceManager.CreateEntityInfo(entity, entityState));
                return;
            }

            bool matchesExistingToTargetState(EntityState existingState, IEnumerable<EntityState> targetState)
            {
                return existingEntry.EntityState == existingState && targetState.Contains(entityState);
            }
            var isUnchangedToModifiedOrDeleted = matchesExistingToTargetState(
                EntityState.Unchanged, new[] { EntityState.Modified, EntityState.Deleted });
            var isModifiedToModified = matchesExistingToTargetState(
                EntityState.Modified, new[] { EntityState.Modified });
            if (!isUnchangedToModifiedOrDeleted && !isModifiedToModified)
            {
                throw new InvalidOperationException($"Unable to add entity of state {entityState} when one already exists in state {existingEntry.EntityState}");
            }

            entityInfos?.Remove(existingEntry);
            SaveMap.Add(PersistenceManager.CreateEntityInfo(entity, entityState));
        }
    }
}
