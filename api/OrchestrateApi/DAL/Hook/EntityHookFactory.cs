using Breeze.Persistence;

namespace OrchestrateApi.DAL.Hook
{
    public interface IEntityHookFactory
    {
        Type EntityType { get; }

        IEntityHook BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap);
    }

    public interface IEntityHookFactory<T> : IEntityHookFactory
    {
        new IEntityHook<T> BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap);
    }

    public abstract class EntityHookFactory<T> : IEntityHookFactory<T>
    {
        public Type EntityType => typeof(T);

        IEntityHook IEntityHookFactory.BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap)
        {
            return BuildEntityHook(persistenceManager, saveMap);
        }

        public abstract IEntityHook<T> BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap);
    }
}
