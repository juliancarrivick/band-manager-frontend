using Breeze.Persistence;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using EntityState = Breeze.Persistence.EntityState;

namespace OrchestrateApi.DAL.Hook
{
    public class EnsembleMembershipHookFactory : EntityHookFactory<EnsembleMembership>
    {
        public override IEntityHook<EnsembleMembership> BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap)
        {
            return new EnsembleMembershipHook
            {
                PersistenceManager = persistenceManager,
                SaveMap = saveMap,
            };
        }
    }

    public class EnsembleMembershipHook : EntityHook<EnsembleMembership>
    {
        public override IEnumerable<EntityError> Validate(IEntityInfo<EnsembleMembership> entityInfo) => Enumerable.Empty<EntityError>();

        public override Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<EnsembleMembership>> entityInfos, CancellationToken cancellationToken)
            => Task.FromResult(Enumerable.Empty<EntityError>());

        public async override Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<EnsembleMembership>> entityInfos, CancellationToken cancellationToken)
        {
            foreach (var memberId in entityInfos.Select(e => e.Entity.MemberId).Distinct())
            {
                var member = await Context.Member.FindAsync(new object[] { memberId }, cancellationToken);
                if (member is null)
                {
                    continue;
                }

                var hasActiveMemberships = await Context.EnsembleMembership
                    .Where(e => e.MemberId == memberId)
                    .WhereIsActiveNow()
                    .AnyAsync(cancellationToken);

                if (!member.DateLeft.HasValue && !hasActiveMemberships)
                {
                    member.DateLeft = DateTime.UtcNow;
                    ReturnToClient(member, EntityState.Modified);
                }
                else if (member.DateLeft.HasValue && hasActiveMemberships)
                {
                    member.DateLeft = null;
                    ReturnToClient(member, EntityState.Modified);
                }
            }

            return Enumerable.Empty<EntityError>();
        }
    }
}
