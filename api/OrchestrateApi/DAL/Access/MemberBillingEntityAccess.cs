using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.DAL.Access
{
    public static class MemberBillingEntityAccess
    {
        public static IEnumerable<IEntityAccessor> Accessors { get; } = new List<IEntityAccessor>
        {
            BuildStandardMemberBillingAccess<MemberBillingPeriod>(),
            BuildStandardMemberBillingAccess<MemberInvoice>(),
            BuildStandardMemberBillingAccess<MemberBillingConfig>(),
            BuildStandardMemberBillingAccess<MemberBillingPeriodConfig>(),
        };

        private static EntityAccessor<T> BuildStandardMemberBillingAccess<T>()
        {
            return new EntityAccessorBuilder<T>()
                .CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.MemberBillingRead))
                .CanReadAllRows()
                .CanWriteToTableWhen(queryData => queryData.HasPermission(FeaturePermission.MemberBillingWrite))
                .Build();
        }
    }
}
