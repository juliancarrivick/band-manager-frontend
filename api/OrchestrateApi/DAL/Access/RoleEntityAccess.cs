using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.DAL.Access
{
    public static class RoleEntityAccess
    {
        public static IEnumerable<IEntityAccessor> Accessors { get; } = new List<IEntityAccessor>
        {
            new EntityAccessorBuilder<OrchestrateRole>()
                .CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.PlatformAccess))
                .CanReadAllRows()
                .CanWriteToTableWhen(queryData => false)
                .Build(),
        };
    }
}
