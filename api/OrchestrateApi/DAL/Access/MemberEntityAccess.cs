using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.DAL.Access
{
    public static class MemberEntityAccess
    {
        public static IEnumerable<IEntityAccessor> Accessors { get; } = new List<IEntityAccessor>
        {
            new EntityAccessorBuilder<Member>()
                .CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.PlatformAccess))
                .CanReadAllRows()
                .CanWriteToTableWhen(queryData => queryData.HasPermission(FeaturePermission.MemberWrite))
                .Build(),
            new EntityAccessorBuilder<MemberDetails>()
                .CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.MemberDetailRead))
                .CanReadAllRows()
                .CanWriteToTableWhen(queryData => queryData.HasPermission(FeaturePermission.MemberDetailWrite))
                .Build(),
            new EntityAccessorBuilder<MemberInstrument>()
                .CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.PlatformAccess))
                .CanReadAllRows()
                .CanWriteToTableWhen(queryData => queryData.HasPermission(FeaturePermission.MemberDetailWrite))
                .Build(),
        };
    }
}
