using Breeze.Persistence;

using SaveMap = System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<Breeze.Persistence.EntityInfo>>;

namespace OrchestrateApi.DAL
{
    public static class SaveMapExtensions
    {
        public static void Add(this SaveMap saveMap, EntityInfo entityInfo)
        {
            if (!saveMap.TryGetValue(entityInfo.Entity.GetType(), out var otherEntities))
            {
                saveMap[entityInfo.Entity.GetType()] = new();
                otherEntities = saveMap[entityInfo.Entity.GetType()];
            }

            otherEntities.Add(entityInfo);
        }

        public static IEnumerable<T> EntitiesOfType<T>(this SaveMap saveMap)
        {
            return saveMap.TryGetValue(typeof(T), out var entityInfos)
                ? entityInfos.Select(e => e.Entity).Cast<T>()
                : Enumerable.Empty<T>();
        }

        public static SaveMap ShallowCopy(this SaveMap saveMap)
        {
            return saveMap.ToDictionary(kvp => kvp.Key, kvp => new List<EntityInfo>(kvp.Value));
        }
    }
}
