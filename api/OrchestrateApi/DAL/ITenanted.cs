using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OrchestrateApi.DAL
{
    public interface ITenanted
    {
        public const string Column = "TenantId";
    }

    public static class ITenantedExtensions
    {
        public static Guid TenantIdConcrete(this ITenanted entity, OrchestrateContext context)
        {
            return context.Entry(entity).Property<Guid>(ITenanted.Column).CurrentValue;
        }

        public static IQueryable<T> WhereInTenant<T>(this IQueryable<T> queryable, Guid tenantId)
            where T : ITenanted
        {
            return queryable.Where(e => EF.Property<Guid>(e, ITenanted.Column) == tenantId);
        }

        public static IndexBuilder<T> HasTenantedIndex<T>(
            this EntityTypeBuilder<T> entityTypeBuilder,
            Expression<Func<T, object?>> indexExpression)
            where T : class, ITenanted
        {
            var members = indexExpression.GetMemberAccessList().Select(m => m.Name);
            return entityTypeBuilder.HasIndex(new[] { ITenanted.Column }.Concat(members).ToArray());
        }
    }
}
