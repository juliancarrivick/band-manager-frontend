using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.DAL
{
    public class OrchestrateUserStore
        : UserStore<
            OrchestrateUser,
            OrchestrateRole,
            OrchestrateContext,
            Guid,
            IdentityUserClaim<Guid>,
            OrchestrateUserRole,
            IdentityUserLogin<Guid>,
            IdentityUserToken<Guid>,
            IdentityRoleClaim<Guid>>
    {
        private readonly ITenantService tenantService;

        public OrchestrateUserStore(
            OrchestrateContext context,
            ITenantService tenantService,
            IdentityErrorDescriber? describer = null)
            : base(context, describer)
        {
            this.tenantService = tenantService;
        }

        protected override Task<OrchestrateUserRole?> FindUserRoleAsync(Guid userId, Guid roleId, CancellationToken cancellationToken)
        {
            return Context.UserRoles.FindAsync(new object?[] { this.tenantService.TenantId, userId, roleId }, cancellationToken).AsTask();
        }
    }
}
