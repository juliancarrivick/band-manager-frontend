using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Claims;
using System.Text.Json;
using Microsoft.EntityFrameworkCore.Query;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Access;
using OrchestrateApi.Security;

namespace OrchestrateApi.DAL
{
    internal class SecuredQueryExpressionVisitor : ExpressionVisitor
    {
        private static readonly MethodInfo GenericQueryableWhereMethod = ReflectionUtils.GetGenericMethodInfo(
            () => Queryable.Where(Array.Empty<int>().AsQueryable(), i => true));

        private readonly IEntityAccessorRegistrar entityAccessorRegistrar;
        private readonly SecuredQueryData securedQueryData;

        public SecuredQueryExpressionVisitor(
            IEntityAccessorRegistrar entityAccessorRegistrar,
            SecuredQueryData securedQueryData)
        {
            this.entityAccessorRegistrar = entityAccessorRegistrar;
            this.securedQueryData = securedQueryData;
        }

        [return: NotNullIfNotNull(nameof(node))]
        public override Expression? Visit(Expression? node)
        {
            node = base.Visit(node);

            // Initially I thought that we would want these filters to be after the global query filters
            // to improve performance but the database actually will rearrange as it sees necessary and
            // it won't affect the result at all
            // See https://dba.stackexchange.com/questions/115723/are-where-clauses-applied-in-the-order-they-are-written
            if (node is QueryRootExpression qrExpr)
            {
                var accessor = this.entityAccessorRegistrar.GetEntityAccessor(qrExpr.ElementType);
                var condition = accessor.CanReadTable(this.securedQueryData)
                    ? accessor.CanReadRow(this.securedQueryData)
                    : NoRowReadExpression(qrExpr.ElementType);
                if (condition != null)
                {
                    var whereMethodGeneric = GenericQueryableWhereMethod.MakeGenericMethod(qrExpr.ElementType);
                    node = Expression.Call(null, whereMethodGeneric, new[] { node, condition });
                }
            }

            return node;
        }

        private static LambdaExpression NoRowReadExpression(Type entityType)
        {
            return Expression.Lambda(
                Expression.Constant(false),
                true,
                [Expression.Parameter(entityType)]);
        }
    }

    public class SecuredQueryData
    {
        private const string SentinalString = "__SecuredQueryData__";

        public string Sentinal => SentinalString;
        public string? UserName { get; init; }
        public ISet<string> Roles { get; init; } = new HashSet<string>();
        public ISet<FeaturePermission> Permissions { get; init; } = new HashSet<FeaturePermission>();

        public static SecuredQueryData ForUserInTenant(ClaimsPrincipal? user, Guid? tenantId)
        {
            if (user == null)
            {
                return new SecuredQueryData();
            }

            return new SecuredQueryData
            {
                UserName = user.Identity?.Name,
                Roles = tenantId.HasValue
                    ? user.RolesInTenant(tenantId.Value).ToHashSet()
                    : new HashSet<string>(),
                Permissions = tenantId.HasValue
                    ? user.PermissionsInTenant(tenantId.Value).ToHashSet()
                    : new HashSet<FeaturePermission>(),
            };
        }

        public static bool TryParseFromQueryTags(ISet<string> tags, [NotNullWhen(true)] out SecuredQueryData? data)
        {
            foreach (var tag in tags)
            {
                if (TryParseFromQueryTag(tag, out data))
                {
                    return true;
                }
            }

            data = null;
            return false;
        }

        public static bool TryParseFromQueryTag(string tag, [NotNullWhen(true)] out SecuredQueryData? data)
        {
            if (!tag.Contains(SentinalString))
            {
                data = null;
                return false;
            }

            data = JsonSerializer.Deserialize<SecuredQueryData>(tag);
            return data is not null;
        }

        public string ToQueryTag()
        {
            return JsonSerializer.Serialize(this);
        }

        public bool HasAtLeastOneRole(params string[] roles)
        {
            return HasAtLeastOneRole((IEnumerable<string>)roles);
        }

        public bool HasAtLeastOneRole(IEnumerable<string> roles)
        {
            return roles.Any(r => Roles.Contains(r));
        }

        public bool HasPermission(FeaturePermission permission)
        {
            return Permissions.Contains(permission);
        }
    }
}
