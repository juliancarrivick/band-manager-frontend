namespace OrchestrateApi.DAL;

public interface IMembership
{
    public DateTime DateJoined { get; }

    public DateTime? DateLeft { get; }
}
