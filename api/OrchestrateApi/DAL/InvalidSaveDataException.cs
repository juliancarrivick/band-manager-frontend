namespace OrchestrateApi.DAL
{
    [Serializable]
    public class InvalidSaveDataException : Exception
    {
        public InvalidSaveDataException() { }
        public InvalidSaveDataException(string message) : base(message) { }
        public InvalidSaveDataException(string message, Exception inner) : base(message, inner) { }
    }
}
