namespace OrchestrateApi.DAL.Model;

public enum PlanType
{
    Trial,
    Free,
    Small,
    Medium,
    Large,
    Unlimited,
    Demo,
}

public class PlanMetadata
{
    public PlanType Type { get; set; }

    public int? MemberLimit { get; set; }
}

public class PaidPlanMetadata : PlanMetadata
{
    public int MonthlyFeeCents { get; set; }

    public required string StripeProductId { get; set; }

    public required string StripePriceId { get; set; }
}
