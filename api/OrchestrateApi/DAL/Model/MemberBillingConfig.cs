using System.ComponentModel.DataAnnotations;

namespace OrchestrateApi.DAL.Model
{
    public class MemberBillingConfig : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public bool IsEnabled { get; set; }

        public int AssociationPeriodFeeDollars { get; set; }

        public decimal ConcessionRate { get; set; }

        public int EarlyBirdDiscountDollars { get; set; }

        public int EarlyBirdDiscountDaysValid { get; set; }

        [Required]
        public required string NotificationContactName { get; set; }

        [Required]
        [EmailAddress]
        public required string NotificationContactEmail { get; set; }

        public string? BankName { get; set; }

        public string? BankAccountName { get; set; }

        [Required]
        public required string BankBsb { get; set; }

        [Required]
        public required string BankAccountNo { get; set; }

        public string? PaymentInstructions { get; set; }

        [Required]
        public required string InvoicePrefix { get; set; }

        public int NextInvoiceNumber { get; set; }
    }
}
