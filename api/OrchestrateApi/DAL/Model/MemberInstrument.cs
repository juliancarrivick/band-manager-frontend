using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model
{
    public class MemberInstrument : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public int MemberId { get; set; }

        [Required]
        public required string InstrumentName { get; set; }

        [ForeignKey(nameof(MemberId))]
        public Member? Member { get; set; }
    }
}
