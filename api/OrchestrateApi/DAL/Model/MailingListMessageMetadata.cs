using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model;

public class MailingListMessageMetadata : ITenanted
{
    public int Id { get; set; }

    public int MailingListId { get; set; }

    public required DateTime Sent { get; set; }

    [ForeignKey(nameof(MailingListId))]
    public MailingList? MailingList { get; set; }
}
