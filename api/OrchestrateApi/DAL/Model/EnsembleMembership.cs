using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Common;

namespace OrchestrateApi.DAL.Model
{
    public enum MembershipType
    {
        Regular,
        Special,
    }

    public class EnsembleMembership : ITenanted, IMembership, ITemporalLifetime
    {
        [Key]
        public int Id { get; set; }

        public int MemberId { get; set; }

        public int EnsembleId { get; set; }

        [TemporalLifetimeStart]
        public DateTime DateJoined { get; set; }

        [TemporalLifetimeFinish]
        public DateTime? DateLeft { get; set; }

        public MembershipType Type { get; set; }

        [ForeignKey(nameof(EnsembleId))]
        public Ensemble? Ensemble { get; set; }

        [ForeignKey(nameof(MemberId))]
        public Member? Member { get; set; }
    }
}
