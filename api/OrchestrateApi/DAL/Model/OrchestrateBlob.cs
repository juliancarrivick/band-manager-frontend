using System.ComponentModel.DataAnnotations;
using System.Text.Json;

namespace OrchestrateApi.DAL.Model;

public class OrchestrateBlob : ITenanted
{
    public Guid Id { get; set; }

    [Required]
    public string Name { get; set; } = null!;

    [Required]
    public string MimeType { get; set; } = null!;

    public long SizeBytes { get; set; }

    public DateTime Created { get; set; }

    public DateTime Modified { get; set; }

    public OrchestrateBlobStorageStrategyData StorageStrategy { get; set; } = null!;
}

public class OrchestrateBlobBackingStorage
{
    public OrchestrateBlobBackingStorage()
    {
        throw new InvalidOperationException("This entity is not meant to be instantiated directly");
    }

    public Guid Id { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1819")]
    public byte[] Bytes { get; set; } = null!;
}

public class OrchestrateBlobStorageStrategyData
{
    private static readonly JsonDocument NullDocument = JsonDocument.Parse("null");

    public string Name { get; set; } = string.Empty;

    public JsonDocument AccessData { get; set; } = NullDocument;
}
