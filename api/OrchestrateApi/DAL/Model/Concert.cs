using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model
{
    public class Concert : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public Guid? CoverPhotoBlobId { get; set; }

        [Required]
        public string Occasion { get; set; } = null!;

        public string? Location { get; set; }

        public DateTime Date { get; set; }

        public string? Notes { get; set; }

        public virtual ICollection<Performance> Performance { get; set; } = new HashSet<Performance>();

        public virtual ICollection<ConcertAttachment> Attachments { get; set; } = new HashSet<ConcertAttachment>();
    }

    public class ConcertAttachment : ITenanted
    {
        public int Id { get; set; }

        public int ConcertId { get; set; }

        public Guid BlobId { get; set; }

        [ForeignKey(nameof(ConcertId))]
        public virtual Concert? Concert { get; set; }
    }
}
