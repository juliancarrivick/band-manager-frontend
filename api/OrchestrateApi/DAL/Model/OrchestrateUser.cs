using Microsoft.AspNetCore.Identity;

namespace OrchestrateApi.DAL.Model
{
    public class OrchestrateUser : IdentityUser<Guid>
    {
        public bool IsGlobalAdmin { get; set; }
    }
}
