using System.ComponentModel.DataAnnotations;

namespace OrchestrateApi.DAL.Model
{
    public enum MemberBillingPeriodState
    {
        /// <summary>Billing Period has been created, but no invoices have been created yet</summary>
        Created,
        /// <summary>Invoices have been created but in a draft state to be manually edited if required</summary>
        Draft,
        /// <summary>Invoices have been sent out and are awaiting collection</summary>
        Open,
        /// <summary>This billing period has finished and no more payments are expected</summary>
        Closed,
    }

    public class MemberBillingPeriod : ITenanted
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public required string Name { get; set; }

        public MemberBillingPeriodState State { get; set; }

        // TODO #73 Use LocalDate for this?
        public DateTime? EarlyBirdDue { get; set; }

        // TODO #73 Use LocalDate for this?
        public DateTime Due { get; set; }

        public DateTime? Generated { get; set; }

        public DateTime? Sent { get; set; }

        public DateTime? Closed { get; set; }

        public DateTime? NotificationContactNotified { get; set; }

        public ICollection<MemberInvoice> Invoices { get; set; } = new HashSet<MemberInvoice>();
    }
}
