namespace OrchestrateApi.DAL
{
    [Serializable]
    public class UnauthorisedSaveException : Exception
    {
        public UnauthorisedSaveException() { }
        public UnauthorisedSaveException(string message) : base(message) { }
        public UnauthorisedSaveException(string message, Exception inner) : base(message, inner) { }
    }
}
