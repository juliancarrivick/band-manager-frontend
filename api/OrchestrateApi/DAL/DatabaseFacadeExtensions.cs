using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace OrchestrateApi.DAL;

public static class DatabaseFacadeExtensions
{
    /// <summary>
    /// Begin a transaction, or if one is already in progress, return a stubbed transaction
    /// that does nothing, as the transaction is being managed elsewhere.
    /// Mainly useful for testing, where the tests maintain a transaction and we can't have two
    /// transactions in progress at the same time
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA2000")]
    public static async Task<IDbContextTransaction> BeginOrStubExistingTransactionAsync(
        this DatabaseFacade database,
        CancellationToken cancellationToken = default)
    {
        return database.CurrentTransaction == null
            ? await database.BeginTransactionAsync(cancellationToken)
            : new StubbedDbContextTransaction(database.CurrentTransaction);
    }
}

public sealed class StubbedDbContextTransaction : IDbContextTransaction
{
    private readonly IDbContextTransaction transaction;

    public StubbedDbContextTransaction(IDbContextTransaction transaction)
    {
        this.transaction = transaction;
    }

    public Guid TransactionId => this.transaction.TransactionId;

    public void Commit()
    {
        // Noop
    }

    public Task CommitAsync(CancellationToken cancellationToken = default)
    {
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        // Noop
    }

    public ValueTask DisposeAsync() => ValueTask.CompletedTask;

    public void Rollback()
    {
        // Noop
    }

    public Task RollbackAsync(CancellationToken cancellationToken = default)
    {
        return Task.CompletedTask;
    }
}
