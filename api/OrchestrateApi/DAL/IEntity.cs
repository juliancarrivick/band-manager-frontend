namespace OrchestrateApi.DAL
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
