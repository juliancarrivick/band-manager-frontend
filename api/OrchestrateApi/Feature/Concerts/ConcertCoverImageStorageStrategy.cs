using System.Linq.Expressions;
using System.Security.Claims;
using System.Text.Json;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Feature.Concerts;

public class ConcertCoverImageStorageStrategy : IBlobStorageStrategy, IValidatingStorageStrategy
{
    public string Name => "ConcertCoverImage";

    public bool TryParseAccessData(JsonDocument rawData, out object? accessData)
    {
        accessData = null;
        return true;
    }

    public bool IsValidAccessData(object? accessData) => accessData is null;

    public bool UserCanRead(ClaimsPrincipal user, Guid tenantId, object? accessData) => user.HasPermissionInTenant(tenantId, FeaturePermission.ConcertRead);

    public bool UserCanWrite(ClaimsPrincipal user, Guid tenantId, object? accessData) => user.HasPermissionInTenant(tenantId, FeaturePermission.ConcertWrite);

    public Expression<Func<OrchestrateBlob, bool>> ReadableBlobsExpr(ClaimsPrincipal user, Guid tenantId) => throw new NotImplementedException();

    public async Task<BlobValidationResult> ValidateAsync(IUploadedBlob blob, CancellationToken cancellationToken)
    {
        using var stream = blob.OpenReadStream();
        return new BlobValidationResult
        {
            IsValid = await FileTypeDetector.Images.AnyMatchingFileTypeAsync(stream, cancellationToken),
            Message = "Not a valid image",
        };
    }

    public IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext)
    {
        return dbContext.Concert
            .Where(e => e.CoverPhotoBlobId != null)
            .Select(e => e.CoverPhotoBlobId!.Value);
    }
}
