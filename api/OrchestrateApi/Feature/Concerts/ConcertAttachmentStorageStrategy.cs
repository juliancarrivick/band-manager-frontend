using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Feature.Concerts;

public class ConcertAttachmentStorageStrategy : EntityAttachmentStorageStrategy
{
    public override string Name => "ConcertAttachment";

    public override FeaturePermission SecuredReadPermission => FeaturePermission.ConcertRead;

    public override FeaturePermission WriteAndSensitiveReadPermission => FeaturePermission.ConcertWrite;

    public override object CreateLinkingEntity(Guid blobId, EntityAttachmentLinkData linkData) => new ConcertAttachment
    {
        BlobId = blobId,
        ConcertId = linkData.EntityId,
    };

    public override IQueryable<Guid> LinkedBlobIds(OrchestrateContext dbContext, EntityAttachmentLinkData linkData)
    {
        return dbContext.Concert
                .Where(e => e.Id == linkData.EntityId)
                .SelectMany(e => e.Attachments.Select(a => a.BlobId));
    }

    public override IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext)
    {
        return dbContext.Concert.SelectMany(e => e.Attachments.Select(a => a.BlobId));
    }
}
