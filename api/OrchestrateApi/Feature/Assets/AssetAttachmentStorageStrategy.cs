using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Feature.Assets;

public class AssetAttachmentStorageStrategy : EntityAttachmentStorageStrategy
{
    public override string Name => "AssetAttachment";

    public override FeaturePermission SecuredReadPermission => FeaturePermission.AssetRead;

    public override FeaturePermission WriteAndSensitiveReadPermission => FeaturePermission.AssetWrite;

    public override object CreateLinkingEntity(Guid blobId, EntityAttachmentLinkData linkData) => new AssetAttachment
    {
        BlobId = blobId,
        AssetId = linkData.EntityId
    };

    public override IQueryable<Guid> LinkedBlobIds(OrchestrateContext dbContext, EntityAttachmentLinkData linkData)
    {
        return dbContext.Asset
            .Where(e => e.Id == linkData.EntityId)
            .SelectMany(e => e.Attachments.Select(a => a.BlobId));
    }

    public override IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext)
    {
        return dbContext.Asset.SelectMany(e => e.Attachments.Select(a => a.BlobId));
    }
}
