using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Assets
{
    [StandardTenantPermissionAuthorize(FeaturePermission.AssetRead, FeaturePermission.AssetWrite)]
    [ApiController]
    [Route("asset_loan")]
    public class AssetLoanController : ControllerBase
    {
        private readonly StandardEntityQueries<AssetLoan> queries;

        public AssetLoanController(OrchestrateContext context)
        {
            this.queries = new StandardEntityQueries<AssetLoan>(this, context);
        }

        [HttpPatch("{id}")]
        [NewtonsoftJsonBinder]
        public Task<IActionResult> PatchAssetLoan(int id, JsonPatchDocument<AssetLoan> patchDocument)
        {
            return queries.Patch(id, patchDocument);
        }

        [HttpDelete("{id}")]
        public Task DeleteAssetLoan(int id)
        {
            return queries.Delete(id);
        }
    }
}
