using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Assets
{
    [StandardTenantPermissionAuthorize(FeaturePermission.AssetRead, FeaturePermission.AssetWrite)]
    [ApiController]
    [Route("asset")]
    public class AssetController : ControllerBase
    {
        public AssetController(OrchestrateContext context)
        {
            DbContext = context;
        }

        private OrchestrateContext DbContext { get; }

        [HttpGet("{assetId:int}/loan")]
        public async Task<ActionResult<IEnumerable<object>>> GetAssetLoans(int assetId)
        {
            return await DbContext.AssetLoan
                .Include(e => e.Member)
                .Where(e => e.AssetId == assetId)
                .Select(e => new
                {
                    e.Id,
                    e.AssetId,
                    e.MemberId,
                    e.DateBorrowed,
                    e.DateReturned,
                    Member = new
                    {
                        e.Member!.Id,
                        e.Member.FirstName,
                        e.Member.LastName,
                    }
                })
                .ToArrayAsync();
        }

        [HttpGet("{assetId}/loan/current")]
        public async Task<ActionResult<object?>> GetCurrentAssetLoan(int assetId)
        {
            return await DbContext.AssetLoan
                .Include(e => e.Member)
                .Where(e => e.AssetId == assetId)
                .Where(e => e.DateReturned == null)
                .Select(e => new
                {
                    e.Id,
                    e.AssetId,
                    e.MemberId,
                    e.DateBorrowed,
                    e.DateReturned,
                    Member = new
                    {
                        e.Member!.Id,
                        e.Member.FirstName,
                        e.Member.LastName,
                    }
                })
                .FirstOrDefaultAsync();
        }

        [HttpPost("{assetId}/loan/return")]
        public async Task<ActionResult<object?>> ReturnCurrentAssetLoan(int assetId)
        {
            var currentLoan = await DbContext.AssetLoan
                .Include(e => e.Member)
                .FirstOrDefaultAsync(e => e.AssetId == assetId && e.DateReturned == null);

            if (currentLoan != null)
            {
                currentLoan.DateReturned = DateTime.UtcNow;
                await DbContext.SaveChangesAsync();
            }
            else
            {
                currentLoan = DbContext.AssetLoan
                    .Include(e => e.Member)
                    .Where(e => e.AssetId == assetId)
                    .OrderByDescending(e => e.DateReturned)
                    .FirstOrDefault();
            }

            if (currentLoan == null || currentLoan.Member == null)
            {
                return (object?)null;
            }

            return new
            {
                currentLoan.Id,
                currentLoan.AssetId,
                currentLoan.MemberId,
                currentLoan.DateBorrowed,
                currentLoan.DateReturned,
                Member = new
                {
                    currentLoan.Member.Id,
                    currentLoan.Member.FirstName,
                    currentLoan.Member.LastName,
                }
            };
        }

        [HttpPost("{assetId}/loan/to/member/{memberId}")]
        public async Task<ActionResult<object?>> LoanAssetToMember(int assetId, int memberId)
        {
            if (!await DbContext.Asset.AnyAsync(e => e.Id == assetId)) return NotFound();
            if (!await DbContext.Member.AnyAsync(e => e.Id == memberId)) return NotFound();

            var currentLoan = await DbContext.AssetLoan.FirstOrDefaultAsync(e => e.AssetId == assetId && e.DateReturned == null);
            if (currentLoan != null)
            {
                return BadRequest($"Asset is already on loan to member with Id {currentLoan.MemberId}");
            }

            var loanEntry = await DbContext.AssetLoan.AddAsync(new AssetLoan
            {
                AssetId = assetId,
                MemberId = memberId,
                DateBorrowed = DateTime.UtcNow,
            });
            await DbContext.SaveChangesAsync();

            return await DbContext.AssetLoan
                .Include(e => e.Member)
                .Select(e => new
                {
                    e.Id,
                    e.AssetId,
                    e.MemberId,
                    e.DateBorrowed,
                    e.DateReturned,
                    Member = new
                    {
                        e.Member!.Id,
                        e.Member.FirstName,
                        e.Member.LastName,
                    }
                })
                .FirstOrDefaultAsync(e => e.Id == loanEntry.Entity.Id);
        }
    }
}
