using System.Data;
using AutoMapper.QueryableExtensions;
using Breeze.AspNetCore;
using Breeze.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Assets;
using OrchestrateApi.Feature.Scores;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Feature
{
    [Route("breeze/[action]")]
    [BreezeJsonSerialization]
    [BreezeQueryFilter]
    [Authorize]
    public class OrchestrateBreezeController : ControllerBase
    {
        [ActivatorUtilitiesConstructor]
        public OrchestrateBreezeController(
            OrchestratePersistenceManager persistenceManager,
            ITenantService tenantService,
            IEnumerable<IBlobStorageStrategy> storageStrategies,
            AutoMapper.IMapper mapper)
        {
            PersistenceManager = persistenceManager;
            TenantService = tenantService;
            StorageStrategies = storageStrategies;
            Mapper = mapper;
        }

        private OrchestratePersistenceManager PersistenceManager { get; }
        private ITenantService TenantService { get; }
        private IEnumerable<IBlobStorageStrategy> StorageStrategies { get; }
        private AutoMapper.IMapper Mapper { get; }

        [HttpGet]
        public string Metadata()
        {
            return PersistenceManager.Metadata();
        }

        [HttpGet]
        public IQueryable<Tenant> Tenant() => PersistenceManager
            .AuthorisedEntitySet<Tenant>()
            .ProjectTo<Tenant>(Mapper.ConfigurationProvider);

        [HttpGet]
        public IQueryable<Asset> Asset()
        {
            return PersistenceManager.AuthorisedEntitySet<Asset>();
        }

        [HttpGet]
        public IQueryable<AssetMetadataDto> AssetsWithMetadata()
        {
            var assetStorageStrategy = StorageStrategies.OfType<AssetAttachmentStorageStrategy>().Single();
            return Asset().Select(e => new AssetMetadataDto
            {
                Asset = e,
                AttachmentCount = PersistenceManager.Context.Blobs
                    .Where(b => e.Attachments.Select(a => a.BlobId).Contains(b.Id))
                    .Where(assetStorageStrategy.ReadableBlobsExpr(User, TenantService.TenantId.GetValueOrDefault(Guid.Empty)))
                    .Count(),
            });
        }

        [HttpGet]
        public IQueryable<AssetLoan> AssetLoan()
        {
            return PersistenceManager.AuthorisedEntitySet<AssetLoan>();
        }

        [HttpGet]
        public IQueryable<Ensemble> Ensemble()
        {
            return PersistenceManager.AuthorisedEntitySet<Ensemble>();
        }

        [HttpGet]
        public IQueryable<EnsembleMembership> EnsembleMembership()
        {
            return PersistenceManager.AuthorisedEntitySet<EnsembleMembership>();
        }

        [HttpGet]
        public IQueryable<EnsembleRole> EnsembleRole()
        {
            return PersistenceManager.AuthorisedEntitySet<EnsembleRole>();
        }

        [HttpGet]
        public IQueryable<Score> Score()
        {
            return PersistenceManager.AuthorisedEntitySet<Score>();
        }

        [HttpGet]
        public IQueryable<ScoreMetadataDto> ScoresWithMetadata()
        {
            var scoreStorageStrategy = StorageStrategies.OfType<ScoreAttachmentStorageStrategy>().Single();
            return PersistenceManager.AuthorisedEntitySet<Score>().Select(e => new ScoreMetadataDto
            {
                Score = e,
                PlayCount = e.PerformanceScore.Count,
                AttachmentCount = PersistenceManager.Context.Blobs
                    .Where(b => e.Attachments.Select(a => a.BlobId).Contains(b.Id))
                    .Where(scoreStorageStrategy.ReadableBlobsExpr(User, TenantService.TenantId.GetValueOrDefault(Guid.Empty)))
                    .Count(),
            });
        }

        [HttpGet]
        public IQueryable<MemberBillingPeriod> MemberBillingPeriod()
        {
            return PersistenceManager.AuthorisedEntitySet<MemberBillingPeriod>();
        }

        [HttpGet]
        public IQueryable<MemberInvoice> MemberInvoice()
        {
            return PersistenceManager.AuthorisedEntitySet<MemberInvoice>();
        }

        [HttpGet]
        public IQueryable<MemberBillingConfig> MemberBillingConfig()
        {
            return PersistenceManager.AuthorisedEntitySet<MemberBillingConfig>();
        }

        [HttpGet]
        public IQueryable<MemberBillingPeriodConfig> MemberBillingPeriodConfig()
        {
            return PersistenceManager.AuthorisedEntitySet<MemberBillingPeriodConfig>();
        }

        [HttpGet]
        public IQueryable<Member> Member()
        {
            return PersistenceManager.AuthorisedEntitySet<Member>();
        }

        [HttpGet]
        public IQueryable<Concert> Concert()
        {
            return PersistenceManager.AuthorisedEntitySet<Concert>();
        }

        [HttpGet]
        public IQueryable<Performance> Performance()
        {
            return PersistenceManager.AuthorisedEntitySet<Performance>();
        }

        [HttpGet]
        public IQueryable<PerformanceMember> PerformanceMember()
        {
            return PersistenceManager.AuthorisedEntitySet<PerformanceMember>();
        }

        [HttpGet]
        public IQueryable<PerformanceScore> PerformanceScore()
        {
            return PersistenceManager.AuthorisedEntitySet<PerformanceScore>();
        }

        [HttpGet]
        public IQueryable<OrchestrateRole> OrchestrateRole()
        {
            return PersistenceManager.AuthorisedEntitySet<OrchestrateRole>();
        }

        [HttpPost]
        public async Task<ActionResult<SaveResult>> SaveChanges([FromBody] JObject saveBundle)
        {
            return await PersistenceManager.AuthorisedSaveChangesAsync(saveBundle);
        }
    }

    public class BreezeSensitiveDataRemovalMapper : AutoMapper.Profile
    {
        // Horrible hacks to prevent sensitive information being read
        // Needs to be kept in sync with OrchestratePersistenceManager.cs
        public BreezeSensitiveDataRemovalMapper()
        {
            CreateMap<Tenant, Tenant>()
                .ForMember(e => e.PostmarkServerToken, src => src.AddTransform(e => null));
        }
    }
}
