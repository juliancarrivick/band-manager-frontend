using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.TenantSecurity
{
    [HasTenantPermissionAuthorize(FeaturePermission.PlatformSecurityAccess)]
    [ApiController]
    [Route("users")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1034")]
    public class UserController : ControllerBase
    {
        private readonly UserManager<OrchestrateUser> userManager;
        private readonly RoleManager<OrchestrateRole> roleManager;
        private readonly UserService userService;
        private readonly OrchestrateContext context;

        public UserController(
            UserManager<OrchestrateUser> userManager,
            RoleManager<OrchestrateRole> roleManager,
            UserService userService,
            OrchestrateContext context)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.userService = userService;
            this.context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<TenantUserDto>> GetUsers()
        {
            // DefaultIfEmpty() produces a left join rather than inner
            // See https://docs.microsoft.com/en-us/dotnet/csharp/linq/perform-left-outer-joins
            var userTenantRolesQuery =
                from member in context.Member
                    .Where(e => e.Details!.UserId != null)
                join userTenantRole in this.userService.ActiveUserRoles()
                    on member.Id equals userTenantRole.MemberId into gj
                from userTenantRole in gj.DefaultIfEmpty() // So members who don't have active access are still listed
                join ensemble in context.Ensemble
                    on userTenantRole.EnsembleId equals ensemble.Id
                    into gj2
                from ensemble in gj2.DefaultIfEmpty() // So direct roles still are returned
                join user in context.Users on member.Details!.UserId equals user.Id
                select new
                {
                    // Below workaround for select with left join
                    // See https://github.com/dotnet/efcore/issues/22517#issuecomment-692263172
                    User = user == null ? null : new { user.Id, user.UserName, user.EmailConfirmed },
                    Member = member == null ? null : new { member.Id, member.FirstName, member.LastName },
                    Ensemble = ensemble == null ? null : new { ensemble.Id, ensemble.Name },
                    userTenantRole.Role,
                };

            var userTenantRoles = await userTenantRolesQuery.ToListAsync();
            var tenantuserDtos = userTenantRoles
                .GroupBy(e => new { e.Member, e.User }, e => new { e.Ensemble, e.Role })
                .Select(e => new TenantUserDto
                {
                    UserId = e.Key.User.Id,
                    Username = e.Key.User.UserName,
                    Name = $"{e.Key.Member.FirstName} {e.Key.Member.LastName}",
                    MemberId = e.Key.Member.Id,
                    Status = e.All(e2 => e2.Role == null) ? UserStatus.NoAccess
                        : e.Key.User.EmailConfirmed ? UserStatus.Active
                        : UserStatus.Pending,
                    Roles = e.Where(e2 => e2.Ensemble == null && e2.Role != null).Select(e2 => e2.Role),
                    EnsembleRoles = e.Where(e2 => e2.Ensemble != null && e2.Role != null)
                        .GroupBy(e2 => e2.Ensemble, e2 => e2.Role)
                        .Select(e2 => new EnsembleRolesDto
                        {
                            EnsembleId = e2.Key.Id,
                            EnsembleName = e2.Key.Name,
                            Roles = e2,
                        }),
                });

            var result = tenantuserDtos.ToList();
            return result;
        }

        [HttpPost("{userId}/role/{roleName}")]
        public async Task<IActionResult> AddUserToRole(string userId, string roleName)
        {
            var user = await this.userManager.FindByIdAsync(userId);
            if (user == null) { return NotFound(); }

            var roleExists = await this.roleManager.RoleExistsAsync(roleName);
            if (!roleExists) { return NotFound(); }

            var hasRole = await this.userManager.IsInRoleAsync(user, roleName);
            if (hasRole) { return Ok(); }

            var results = await this.userManager.AddToRoleAsync(user, roleName);
            if (!results.Succeeded) { return StatusCode(500); }

            return Ok();
        }

        [HttpDelete("{userId}/role/{roleName}")]
        public async Task<IActionResult> RemoveUserFromRole(string userId, string roleName)
        {
            var user = await this.userManager.FindByIdAsync(userId);
            if (user == null) { return NotFound(); }

            var hasRole = await this.userManager.IsInRoleAsync(user, roleName);
            if (!hasRole) { return Ok(); }

            var results = await this.userManager.RemoveFromRoleAsync(user, roleName);
            if (!results.Succeeded) { return StatusCode(500); }

            return Ok();
        }

        // TODO Use in global admin area
        [IsGlobalAdminAuthorize]
        [HttpPost]
        public async Task<ActionResult<UserDto>> CreateUser(NewUserModel model)
        {
            if (model.Password != model.ConfirmPassword)
            {
                return BadRequest("Passwords don't match");
            }

            var user = new OrchestrateUser
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username,
                Email = model.Email,
            };
            var result = await this.userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                var messages = result.Errors.Select(i => i.Description);
                return BadRequest(string.Join(", ", messages));
            }

            return new UserDto
            {
                Id = user.Id,
                Username = user.UserName,
                TenantRoles = Enumerable.Empty<TenantRolesDto>(),
            };
        }

        // TODO Use in global admin area
        [IsGlobalAdminAuthorize]
        [HttpDelete]
        [Route("{userId}")]
        public async Task<ActionResult> DeleteUser(string userId)
        {
            // TODO Disassociate UserId with MemberId, and if no linked members delete User?
            // Does this even make sense in a MultiTenant context? Maybe only for GlobalAdmins
            var user = await this.userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return NotFound();
            }

            var result = await this.userManager.DeleteAsync(user);
            if (!result.Succeeded)
            {
                return StatusCode(500);
            }

            return Ok();
        }

        // TODO Use in global admin area
        [IsGlobalAdminAuthorize]
        [HttpPost]
        [Route("{userId}/reset-password")]
        public async Task<ActionResult> ResetUserPassword(string userId, UpdatePasswordModel model)
        {
            var user = await this.userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return NotFound();
            }

            var token = await this.userManager.GeneratePasswordResetTokenAsync(user);
            var result = await this.userManager.ResetPasswordAsync(user, token, model.Password);

            if (!result.Succeeded)
            {
                var messages = result.Errors.Select(i => i.Description);
                return BadRequest(messages);
            }

            return Ok();
        }

        public enum UserStatus
        {
            Active,
            Pending,
            NoAccess,
        }

        public class TenantUserDto
        {
            public required Guid UserId { get; set; }

            public required string Username { get; set; }

            public required int MemberId { get; set; }

            public required string Name { get; set; }

            [JsonConverter(typeof(JsonStringEnumConverter))]
            public required UserStatus Status { get; set; }

            public required IEnumerable<string> Roles { get; set; }

            public required IEnumerable<EnsembleRolesDto> EnsembleRoles { get; set; }
        }

        public class EnsembleRolesDto
        {
            public required int EnsembleId { get; set; }

            public required string EnsembleName { get; set; }

            public required IEnumerable<string> Roles { get; set; }
        }

        public class NewUserModel
        {
            public required string Username { get; set; }

            public required string Email { get; set; }

            public required string Password { get; set; }

            public required string ConfirmPassword { get; set; }
        }

        public class UpdatePasswordModel
        {
            public required string Password { get; set; }
        }
    }
}
