using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature;

public class AssetMetadataDto
{
    public Asset Asset { get; set; } = null!;

    public int AttachmentCount { get; set; }
}

public class ScoreMetadataDto
{
    public Score Score { get; set; } = null!;

    public int PlayCount { get; set; }

    public int AttachmentCount { get; set; }
}
