using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.MailingLists;

[ApiController]
[Route("create-mailing-list-dialog")]
[StandardTenantPermissionAuthorize(FeaturePermission.MailingListRead, FeaturePermission.MailingListWrite)]
[ValidationExceptionFilter]
public class CreateMailingListDialogController(
    IAppConfiguration appConfiguration,
    MailingListService mailingListService,
    OrchestrateContext dbContext,
    IValidator<CreateMailingListDto> createDtoValidator,
    AutoMapper.IMapper mapper)
    : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<CreateMailingListDialogDataDto>> GetData()
    {
        var tenant = await dbContext.GetTenantAsync();
        if (tenant == null)
        {
            return NotFound();
        }

        return new CreateMailingListDialogDataDto
        {
            AddressParameters = mailingListService.AddressParameters(tenant),
        };
    }

    [HttpPost]
    public async Task<ActionResult<MailingListDto>> CreateMailingList([FromBody] CreateMailingListDto dto)
    {
        if (await dbContext.MailingList.CountAsync() >= appConfiguration.MaxMailingListCount)
        {
            return BadRequest(new BasicErrorResponse("Maximum number of mailing lists reached"));
        }

        await createDtoValidator.ValidateAndThrowAsync(dto.SanitiseAndReturn());

        var mailingList = mapper.Map<MailingList>(dto);
        dbContext.Add(mailingList);
        await dbContext.SaveChangesAsync();
        return mapper.Map<MailingListDto>(mailingList);
    }
}

public class CreateMailingListDialogAutoMapperProfile : AutoMapper.Profile
{
    public CreateMailingListDialogAutoMapperProfile()
    {
        CreateMap<CreateMailingListDto, MailingList>(AutoMapper.MemberList.Source);
    }
}

public class CreateMailingListDialogDataDto
{
    public required MailingListAddressParameters AddressParameters { get; set; }
}

public class CreateMailingListDto : ISanitisable
{
    public required string Name { get; set; }
    public required string Slug { get; set; }
    public required AllowedSenders AllowedSenders { get; set; }
    public required ReplyStrategy ReplyTo { get; set; }
    public string? Footer { get; set; }

    public void Sanitise()
    {
        Name = Sanitiser.StringToEmpty(Name);
        Slug = Sanitiser.StringToEmpty(Slug);
        Footer = Sanitiser.StringToNull(Footer);
    }
}

public class CreateMailingListDtoValidator : AbstractValidator<CreateMailingListDto>
{
    public CreateMailingListDtoValidator(
        MailingListService mailingListService,
        OrchestrateContext dbContext)
    {
        MailingListValidationHelpers.CreateNameRules(
            dbContext, () => RuleFor(e => e.Name));
        MailingListValidationHelpers.CreateSlugRules(
            dbContext, mailingListService, () => RuleFor(e => e.Slug));
    }
}
