using System.Linq.Expressions;
using AutoMapper.QueryableExtensions;
using FluentValidation;
using LinqKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Email.Postmark;
using OrchestrateApi.Security;
using OrchestrateApi.Validation;

namespace OrchestrateApi.Feature.MailingLists;

[ApiController]
[Route("edit-mailing-list-page")]
[StandardTenantPermissionAuthorize(FeaturePermission.MailingListRead, FeaturePermission.MailingListWrite)]
[ValidationExceptionFilter]
public class EditMailingListPageController(
    OrchestrateContext dbContext,
    MailingListService mailingListService,
    RecipientListService recipientListService,
    PostmarkService postmarkService,
    AutoMapper.IMapper mapper) : ControllerBase
{
    [HttpGet("data/{id}")]
    public async Task<ActionResult<PageDataDto>> GetPageData(int id)
    {
        var tenant = await dbContext.GetTenantAsync();
        var mailingList = await dbContext.MailingList
            .Include(e => e.Ensembles)
            .Include(e => e.Members)
            .Include(e => e.Contacts)
            .Include(e => e.Suppressions)
            .SingleOrDefaultAsync(e => e.Id == id);
        if (tenant == null || mailingList == null)
        {
            return NotFound();
        }

        return new PageDataDto
        {
            MailingList = mapper.Map<MailingListDto>(mailingList),
            Recipients = new SelectedRecipientsDto
            {
                EnsembleIds = mailingList.Ensembles.Select(e => e.Id).ToList(),
                Members = mailingList.Members.Select(mapper.Map<MemberDto>).ToList(),
                Contacts = mailingList.Contacts.Select(mapper.Map<ContactDto>).ToList(),
            },
            Suppressions = mailingList.Suppressions.Select(mapper.Map<SuppressionDto>).ToList(),
            ReferenceData = new PageReferenceDataDto
            {
                AddressParameters = mailingListService.AddressParameters(tenant),
                Ensembles = await dbContext.Ensemble
                    .Where(e => e.Status != EnsembleStatus.Disbanded)
                    .ProjectTo<EnsembleDto>(mapper.ConfigurationProvider, new
                    {
                        isActiveNowExpr = TemporalLifetimeExtensions.IsActiveNowExpr<EnsembleMembership>(),
                    })
                    .ToListAsync(),
                Members = await dbContext.Member
                    .WhereIsActiveNow()
                    .ProjectTo<MemberDto>(mapper.ConfigurationProvider)
                    .ToListAsync(),
                Contacts = await dbContext.Contact
                    .Where(e => e.ArchivedOn == null)
                    .ProjectTo<ContactDto>(mapper.ConfigurationProvider)
                    .ToListAsync(),
            },
        };
    }

    [HttpPost("build-recipient-list")]
    public async Task<IEnumerable<RecipientDto>> BuildRecipientList([FromBody] RecipientListBuilderDto dto)
    {
        var builder = new RecipientListBuilder();

        builder.MergeRecipients(
            await recipientListService.GetEnsembleRecipients(dto.EnsembleIds),
            (r, s) => r.Ensemble = s);
        builder.MergeRecipients(
            await recipientListService.GetMemberRecipients(dto.MemberIds),
            (r, s) => r.Member = s);
        builder.MergeRecipients(
            await recipientListService.GetContactRecipients(dto.ContactIds),
            (r, s) => r.Contact = s);
        builder.ApplySuppressions(dto.Suppressions);

        return builder.Recipients;
    }

    [HttpPatch("{id}")]
    public async Task<ActionResult<MailingListDto>> UpdateMailingList(int id, [FromBody] UpdateMailingListAndRecipientsDto dto)
    {
        var mailingList = await dbContext.MailingList
            .Include(e => e.Ensembles)
            .Include(e => e.Members)
            .Include(e => e.Contacts)
            .Include(e => e.Suppressions)
            .SingleOrDefaultAsync(e => e.Id == id);
        if (mailingList == null)
        {
            return NotFound();
        }

        var validator = new UpdateMailingListAndRecipientsDtoValidator(
            new UpdateMailingListDtoValidator(id, mailingListService, dbContext));
        await validator.ValidateAndThrowAsync(dto.SanitiseAndReturn());

        if (dto.MailingList.TryGet(out var mailingListDto))
        {
            mapper.Map(mailingListDto, mailingList);
        }
        if (dto.EnsembleIds.TryGet(out var newEnsembleIds))
        {
            var targetEnsembles = await dbContext.Ensemble.Where(e => newEnsembleIds.Contains(e.Id)).ToListAsync();
            SyncCollection(mailingList.Ensembles, targetEnsembles);
        }
        if (dto.MemberIds.TryGet(out var newMemberIds))
        {
            var targetMembers = await dbContext.Member.Where(e => newMemberIds.Contains(e.Id)).ToListAsync();
            SyncCollection(mailingList.Members, targetMembers);
        }
        if (dto.ContactIds.TryGet(out var newContactIds))
        {
            var targetContacts = await dbContext.Contact.Where(e => newContactIds.Contains(e.Id)).ToListAsync();
            SyncCollection(mailingList.Contacts, targetContacts);
        }
        if (dto.RemovedSuppressions.TryGet(out var removedSuppressions))
        {
            var matchingSuppressions = removedSuppressions
                .SelectMany(r => mailingList.Suppressions
                    .Where(s => s.Email.Equals(r, StringComparison.OrdinalIgnoreCase)))
                .Where(s => s.IsRemovable)
                .ToList();
            await postmarkService.RemoveSuppressions(
                mailingList,
                matchingSuppressions.Select(s => s.Email).ToList());
            dbContext.RemoveRange(matchingSuppressions);
        }

        await dbContext.SaveChangesAsync();

        return mapper.Map<MailingListDto>(mailingList);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteMailingList(int id)
    {
        if (await dbContext.MailingList.SingleOrDefaultAsync(e => e.Id == id) is not { } mailingList)
        {
            return Ok();
        }

        await postmarkService.DeleteMailingListStreamAsync(mailingList);

        dbContext.Remove(mailingList);
        await dbContext.SaveChangesAsync();
        return Ok();
    }

    private static void SyncCollection<T>(ICollection<T> toModify, ICollection<T> expectedElements)
    {
        var toRemove = toModify.Where(e => !expectedElements.Contains(e)).ToList();
        foreach (var element in toRemove)
        {
            toModify.Remove(element);
        }

        var toAdd = expectedElements.Where(e => !toModify.Contains(e)).ToList();
        foreach (var element in toAdd)
        {
            toModify.Add(element);
        }
    }

    private class RecipientListBuilder
    {
        private readonly List<RecipientDto> recipients = [];

        public IEnumerable<RecipientDto> Recipients => this.recipients;

        public void MergeRecipients(
            IEnumerable<MailingListRecipient> recipients,
            Action<RecipientDto, RecipientSource> setSource)
        {
            foreach (var r in recipients)
            {
                var sourceStatus = r.IsActive switch
                {
                    true => RecipientSource.Active,
                    false => RecipientSource.Inactive,
                };
                if (r.Email is { } email && GetExisting(email) is { } existingRecipient)
                {
                    setSource(existingRecipient, sourceStatus);
                }
                else
                {
                    var dto = new RecipientDto(r);
                    setSource(dto, sourceStatus);
                    this.recipients.Add(dto);
                }
            }
        }

        public void ApplySuppressions(IEnumerable<string> suppressions)
        {
            foreach (var s in suppressions)
            {
                if (GetExisting(s) is { } recipient)
                {
                    recipient.IsSuppressed = true;
                }
            }
        }

        private RecipientDto? GetExisting(string email)
        {
            return this.recipients.FirstOrDefault(r => r.Email is { } rEmail
                && rEmail.Equals(email, StringComparison.OrdinalIgnoreCase));
        }
    }
}

public class MailingListPageAutoMapperProfile : AutoMapper.Profile
{
    public MailingListPageAutoMapperProfile()
    {
        Expression<Func<EnsembleMembership, bool>> isActiveNowExpr = null!;
        CreateMap<Ensemble, EnsembleDto>()
            .ForMember(e => e.NumRecipients, src => src.MapFrom(e => e.EnsembleMembership
                .AsQueryable()
                .Count(isActiveNowExpr.Expand())));
        CreateMap<Member, MemberDto>()
            .ForMember(e => e.Name, src => src.MapFrom(e => $"{e.FirstName} {e.LastName}"))
            .ForMember(e => e.Email, src => src.MapFrom(e => e.Details!.Email));
        CreateMap<Contact, ContactDto>();
        CreateMap<MailingListSuppression, SuppressionDto>();

        CreateMap<UpdateMailingListDto, MailingList>(AutoMapper.MemberList.Source);
        CreateMap<Optional<AllowedSenders>, AllowedSenders>().ConvertUsing(new OptionalMapper<AllowedSenders>());
        CreateMap<Optional<ReplyStrategy>, ReplyStrategy>().ConvertUsing(new OptionalMapper<ReplyStrategy>());
    }
}

public class PageDataDto
{
    public required MailingListDto MailingList { get; set; }
    public required SelectedRecipientsDto Recipients { get; set; }
    public required IEnumerable<SuppressionDto> Suppressions { get; set; }
    public required PageReferenceDataDto ReferenceData { get; set; }
}

public class SelectedRecipientsDto
{
    public required IList<int> EnsembleIds { get; set; }
    public required IList<MemberDto> Members { get; set; }
    public required IList<ContactDto> Contacts { get; set; }
}

public class SuppressionDto
{
    public required string Email { get; set; }
    public required DateTime AddedAt { get; set; }
    public required string Reason { get; set; }
    public required bool IsRemovable { get; set; }
}

public class PageReferenceDataDto
{
    public required MailingListAddressParameters AddressParameters { get; set; }
    public required IList<EnsembleDto> Ensembles { get; set; }
    public required IList<MemberDto> Members { get; set; }
    public required IList<ContactDto> Contacts { get; set; }
}

public class EnsembleDto
{
    public required int Id { get; set; }
    public required string Name { get; set; }
    public required int NumRecipients { get; set; }
}

public class MemberDto
{
    public required int Id { get; set; }
    public required string Name { get; set; }
    public required string? Email { get; set; }
}

public class ContactDto
{
    public required int Id { get; set; }
    public required string Name { get; set; }
    public required string? Email { get; set; }
}

public class UpdateMailingListAndRecipientsDto : ISanitisable
{
    public Optional<UpdateMailingListDto> MailingList { get; set; }
    public Optional<IList<int>> EnsembleIds { get; set; }
    public Optional<IList<int>> MemberIds { get; set; }
    public Optional<IList<int>> ContactIds { get; set; }
    public Optional<IList<string>> RemovedSuppressions { get; set; }

    public void Sanitise()
    {
        if (MailingList.HasValue)
        {
            MailingList.Value.Sanitise();
        }
        if (RemovedSuppressions.HasValue)
        {
            RemovedSuppressions = RemovedSuppressions.Value.Select(s => s.Trim()).ToList();
        }
    }
}

// TODO instead of internal, use the to-come ServiceLifetime.None attribute
internal class UpdateMailingListAndRecipientsDtoValidator : AbstractValidator<UpdateMailingListAndRecipientsDto>
{
    public UpdateMailingListAndRecipientsDtoValidator(
        IValidator<UpdateMailingListDto> updateMailingListValidator)
    {
        RuleFor(e => e.MailingList).SetOptionalValidator(updateMailingListValidator);
    }
}

public class UpdateMailingListDto : ISanitisable
{
    public Optional<string> Name { get; set; }
    public Optional<string> Slug { get; set; }
    public Optional<AllowedSenders> AllowedSenders { get; set; }
    public Optional<ReplyStrategy> ReplyTo { get; set; }
    public Optional<string?> Footer { get; set; }

    public void Sanitise()
    {
        Name = Sanitiser.StringToEmpty(Name);
        Slug = Sanitiser.StringToEmpty(Slug);
        Footer = Sanitiser.StringToNull(Footer);
    }
}

// TODO instead of internal, use the to-come ServiceLifetime.None attribute
internal class UpdateMailingListDtoValidator : AbstractValidator<UpdateMailingListDto>
{
    public UpdateMailingListDtoValidator(
        int id,
        MailingListService mailingListService,
        OrchestrateContext dbContext)
    {
        MailingListValidationHelpers.CreateNameRules(
            dbContext, () => RuleFor(e => e.Name).Optional(), id);
        MailingListValidationHelpers.CreateSlugRules(
            dbContext, mailingListService, () => RuleFor(e => e.Slug).Optional(), id);
    }
}

public class RecipientListBuilderDto
{
    public required IList<int> EnsembleIds { get; init; }
    public required IList<int> MemberIds { get; init; }
    public required IList<int> ContactIds { get; init; }
    public required IList<string> Suppressions { get; init; }
}

public enum RecipientSource
{
    /// <summary>Source is not applicable for this recipient</summary>
    NotApplicable,
    /// <summary>Recipient is included using this source, but won't receive emails because it isn't active</summary>
    Inactive,
    /// <summary>Recipient is included using this source, and it is active</summary>
    Active,
}

public class RecipientDto(MailingListRecipient sourceRecipient)
{
    public string Name { get; } = sourceRecipient.Name;
    public string? Email { get; } = sourceRecipient.Email;
    public bool IsSuppressed { get; set; }
    public RecipientSource Ensemble { get; set; } = RecipientSource.NotApplicable;
    public RecipientSource Member { get; set; } = RecipientSource.NotApplicable;
    public RecipientSource Contact { get; set; } = RecipientSource.NotApplicable;

    public string? DropReason => BuildDropReason();

    private string? BuildDropReason()
    {
        var reasons = new List<string>();

        if (string.IsNullOrWhiteSpace(Email))
        {
            reasons.Add("don't have a valid email address");
        }

        if (IsSuppressed)
        {
            reasons.Add("have unsubscribed from emails to this mailing list");
        }

        var checks = new List<Tuple<RecipientSource, string>>{
            Tuple.Create(Ensemble, "are a member of a disbanded ensemble"),
            Tuple.Create(Member, "are an inactive member"),
            Tuple.Create(Contact, "are an archived contact"),
        };
        if (!checks.Any(c => c.Item1 == RecipientSource.Active))
        {
            foreach (var c in checks.Where(c => c.Item1 == RecipientSource.Inactive))
            {
                reasons.Add(c.Item2);
            }
        }

        if (reasons.Count == 0)
        {
            return null;
        }

        var preamble = "Recipient will not be sent emails as they";
        return reasons.Count == 1
            ? $"{preamble} {reasons[0]}"
            : $"{preamble} {string.Join(", ", reasons.GetRange(0, reasons.Count - 1))} and {reasons.Last()}";
    }
}
