using FluentValidation;
using FluentValidation.Validators;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Feature.MailingLists;

public class GeneratedEmailValidator<T> : AsyncPropertyValidator<T, string>
{
    // https://stackoverflow.com/a/574698
    public const int MaxEmailLength = 254;

    private readonly MailingListService mailingListService;
    private readonly OrchestrateContext dbContext;

    public GeneratedEmailValidator(
        MailingListService mailingListService,
        OrchestrateContext dbContext)
    {
        this.mailingListService = mailingListService;
        this.dbContext = dbContext;
    }

    public override string Name => "GeneratedEmail";

    public override async Task<bool> IsValidAsync(ValidationContext<T> context, string value, CancellationToken cancellation)
    {
        var tenant = await dbContext.GetTenantAsync(cancellation);
        if (tenant is null)
        {
            return false;
        }

        var email = mailingListService.MailingListEmail(tenant, value);
        return email.Length <= MaxEmailLength;
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return "Generated email address has a max length of 254 characters";
    }
}
