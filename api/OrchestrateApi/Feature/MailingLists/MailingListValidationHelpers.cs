using FluentValidation;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Validation;
using OrchestrateApi.Validation.Validators;

namespace OrchestrateApi.Feature.MailingLists;

public static class MailingListValidationHelpers
{
    public static void CreateNameRules<T>(
        OrchestrateContext dbContext,
        Func<IRuleBuilderInitial<T, string>> createRule,
        int? id = null)
    {
        createRule().NotEmpty();
        createRule().SetAsyncValidator(new CaseInsensitiveUniqueValidator<T, MailingList>(
            id, dbContext.MailingList, e => e.Id, e => e.Name)
        {
            ErrorMessage = "A mailing list with that name already exists",
        });
    }

    public static void CreateSlugRules<T>(
        OrchestrateContext dbContext,
        MailingListService mailingListService,
        Func<IRuleBuilderInitial<T, string>> createRule,
        int? id = null)
    {
        createRule().NotEmpty();
        createRule().EmailAndUrlSlug();
        createRule().SetAsyncValidator(new GeneratedEmailValidator<T>(mailingListService, dbContext));
        createRule().SetAsyncValidator(new CaseInsensitiveUniqueValidator<T, MailingList>(
            id, dbContext.MailingList, e => e.Id, e => e.Slug)
        {
            ErrorMessage = "A mailing list with that address already exists",
        });
    }
}
