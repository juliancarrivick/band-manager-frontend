using LinqKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.MailingLists;

[ApiController]
[Route("mailing-lists-page")]
[StandardTenantPermissionAuthorize(FeaturePermission.MailingListRead, FeaturePermission.MailingListWrite)]
[ValidationExceptionFilter]
public class MailingListsPageController(
    IAppConfiguration appConfiguration,
    MailingListService mailingListService,
    RecipientListService recipientListService,
    OrchestrateContext dbContext) : ControllerBase
{
    internal const int UsageCutOffDaysAgo = 45;

    [HttpGet]
    public async Task<ActionResult<MailingListsSummaryDto>> GetSummary()
    {
        var tenant = await dbContext.GetTenantAsync();
        if (tenant == null)
        {
            return NotFound();
        }

        var mailingListEmailExpr = mailingListService.MailingListEmailExpr(tenant);
        var recipientCountExpr = recipientListService.GetRecipientCountExpr();
        var usageCutoff = DateTime.UtcNow - TimeSpan.FromDays(UsageCutOffDaysAgo);
        return new MailingListsSummaryDto
        {
            MailingLists = await dbContext.MailingList
                .AsExpandable()
                .Select(e => new MailingListSummaryDto
                {
                    Id = e.Id,
                    Name = e.Name,
                    Email = mailingListEmailExpr.Invoke(e),
                    LastUsed = e.Messages.Select(m => (DateTime?)m.Sent).OrderByDescending(d => d).FirstOrDefault(),
                    RecipientCount = recipientCountExpr.Invoke(e),
                })
                .OrderBy(e => e.Name)
                .ToListAsync(),
            UsageCutOffDaysAgo = UsageCutOffDaysAgo,
            UsageHistory = await dbContext.MailingListMessageMetadata
                .Where(e => e.Sent > usageCutoff)
                .GroupBy(e => new
                {
                    e.MailingListId,
                    e.MailingList!.Name,
                    TimeZoneInfo.ConvertTimeBySystemTimeZoneId(e.Sent, tenant.TimeZone).Date,
                })
                .Select(e => new UsageDatum
                {
                    Date = DateOnly.FromDateTime(e.Key.Date),
                    MailingListId = e.Key.MailingListId,
                    MailingListName = e.Key.Name,
                    SendCount = e.Count(),
                })
                .ToListAsync(),
            MaxMailingListCount = appConfiguration.MaxMailingListCount,
        };
    }
}

public class MailingListsSummaryDto
{
    public required IList<MailingListSummaryDto> MailingLists { get; init; }
    public required IList<UsageDatum> UsageHistory { get; init; }
    public required int UsageCutOffDaysAgo { get; init; }
    public required int MaxMailingListCount { get; init; }
}

public class MailingListSummaryDto
{
    public required int Id { get; init; }
    public required string Name { get; init; }
    public required string Email { get; init; }
    public required int RecipientCount { get; init; }
    public required DateTime? LastUsed { get; init; }
}

public class UsageDatum
{
    public required DateOnly Date { get; init; }
    public required int MailingListId { get; init; }
    public required string MailingListName { get; init; }
    public required int SendCount { get; init; }
}
