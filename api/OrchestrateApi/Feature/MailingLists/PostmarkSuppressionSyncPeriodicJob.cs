using OrchestrateApi.Common;
using OrchestrateApi.Email.Postmark;

namespace OrchestrateApi.Feature.MailingLists;

public sealed class PostmarkSuppressionSyncPeriodicJob(
    PostmarkService postmarkService,
    TenantContextIterator tenantContextIterator,
    IAppConfiguration appConfiguration)
    : IPeriodicJob
{
    public string Name => "PostmarkSuppressionSync";

    public TimeSpan RunPeriod { get; } = appConfiguration.PostmarkSuppressionSyncInterval;

    public async Task Run(CancellationToken cancellationToken)
    {
        await tenantContextIterator.ForEachTenant(
            Name,
            (_) => postmarkService.AddAllMissingSuppressions(),
            cancellationToken);
    }
}
