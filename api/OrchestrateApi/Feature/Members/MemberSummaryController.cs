using LinqKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Members
{
    [StandardTenantPermissionAuthorize(FeaturePermission.MemberRead, FeaturePermission.MemberWrite)]
    [ApiController]
    [Route("member")]
    public class MemberSummaryController : ControllerBase
    {
        private readonly OrchestrateContext context;

        public MemberSummaryController(OrchestrateContext context)
        {
            this.context = context;
        }

        [HttpGet("summary")]
        public Task<IEnumerable<object>> GetMemberSummary()
        {
            return GetMemberSummary(false);
        }

        [HttpGet("current/summary")]
        public Task<IEnumerable<object>> GetCurrentMemberSummary()
        {
            return GetMemberSummary(true);
        }

        private async Task<IEnumerable<object>> GetMemberSummary(bool isActiveOnly)
        {
            var memberIsActiveExpr = TemporalLifetimeExtensions.IsActiveNowExpr<Member>();
            var membershipIsActiveExpr = TemporalLifetimeExtensions.IsActiveNowExpr<EnsembleMembership>();
            return await context.Member
                .AsExpandable()
                .Include(e => e.MemberInstrument)
                .Include(e => e.EnsembleMembership)
                    .ThenInclude(e => e.Ensemble)
                .Include(e => e.PerformanceMember)
                .Where(e => !isActiveOnly || memberIsActiveExpr.Invoke(e))
                .Select(e => new
                {
                    e.Id,
                    e.FirstName,
                    e.LastName,
                    PerformanceCount = e.PerformanceMember.Count,
                    Ensembles = e.EnsembleMembership
                        .Where(membershipIsActiveExpr.Compile())
                        .Select(e => e.Ensemble!.Name),
                    Instruments = e.MemberInstrument.Select(e => e.InstrumentName),
                })
                .ToArrayAsync();
        }
    }
}
