using Breeze.Persistence;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Members
{
    [HasTenantPermissionAuthorize(FeaturePermission.MemberWrite)]
    [ApiController]
    [Route("member")]
    public class MemberController : ControllerBase
    {
        private readonly OrchestratePersistenceManager persistenceManager;
        private readonly UserService userService;

        public MemberController(
            OrchestratePersistenceManager persistenceManager,
            UserService userService)
        {
            this.persistenceManager = persistenceManager;
            this.userService = userService;
        }

        [HttpPost("invite")]
        [BreezeJsonSerialization]
        public async Task<ActionResult<SaveResult>> InviteMember(JObject saveBundle, CancellationToken cancellationToken)
        {
            var inviteMember = new InviteMemberCustomSave(userService);
            return await this.persistenceManager.CustomSaveAsync(saveBundle, inviteMember, null, cancellationToken);
        }
    }
}
