using OrchestrateApi.DAL;
using OrchestrateApi.DAL.CustomSave;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using EntityState = Microsoft.EntityFrameworkCore.EntityState;

namespace OrchestrateApi.Feature.Members
{
    public interface IMemberInviter
    {
        Task<Member> InviteMember(Member member);
    }

    public class InviteMemberCustomSave : SingleEntityCustomSave<Member>
    {
        public InviteMemberCustomSave(IMemberInviter memberInviter)
        {
            MemberInviter = memberInviter;
        }

        public IMemberInviter MemberInviter { get; }

        public override FeaturePermission RequiredPermission { get; } = FeaturePermission.MemberWrite;

        internal override bool EntityIsPreSaveValid(Member? entity) => entity != null;

        internal override Task ModifyEntity(OrchestrateContext context, Member entity, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        internal override async Task<IEnumerable<object>> SaveLogicAsync(OrchestrateContext context, Member member, CancellationToken cancellationToken)
        {
            var invitedMember = await MemberInviter.InviteMember(member);

            // Detach so we can clear the primed user without it affecting any data
            // (we don't want user returned to front end)
            var detailsEntry = context.Entry(invitedMember.Details!);
            detailsEntry.State = EntityState.Detached;
            detailsEntry.Entity.User = null;

            return new[] { detailsEntry.Entity };
        }
    }
}
