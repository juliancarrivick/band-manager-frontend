using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.PaymentProcessor;

namespace OrchestrateApi.Feature.Settings;

public class SubscriptionSettingsService
{
    private readonly OrchestrateContext dbContext;
    private readonly IPaymentProcessor paymentProcessor;
    private readonly ILogger logger;

    public SubscriptionSettingsService(
        OrchestrateContext dbContext,
        IPaymentProcessor paymentProcessor,
        ILogger<SubscriptionSettingsService> logger)
    {
        this.dbContext = dbContext;
        this.paymentProcessor = paymentProcessor;
        this.logger = logger;
    }

    public async Task<SubscriptionDetailsDto> DefaultDetailsAsync(Tenant tenant)
    {
        var dto = new SubscriptionDetailsDto
        {
            CurrentPlan = tenant.Plan.Type,
            IsAllowedToActivatePaidPlan = await this.dbContext.PaidPlanMetadata.AnyAsync()
                && TenantAllowedToActivatePaidPlan(tenant),
            IsNotForProfit = tenant.Plan.IsNotForProfit,
            DateRegistered = tenant.DateRegistered,
            AccessEndsDate = tenant.Plan.PeriodEnd,
            CanAccessCustomerPortal = !string.IsNullOrWhiteSpace(tenant.Plan.StripeCustomerId),
            MaximumMemberCount = await PlanTypeToMemberLimit(tenant.Plan.Type),
            MonthlyFeeDollars = 0,
            PreviousInvoices = Array.Empty<PreviousInvoiceDto>(),
        };

        if (dto.CurrentPlan == PlanType.Trial)
        {
            dto.NextPlan = PlanType.Free;
        }

        return dto;
    }

    public async Task<Stripe.Customer?> GetOrCreateCustomerAsync(Tenant tenant)
    {
        if (tenant.Plan.StripeCustomerId != null)
        {
            var customer = await this.paymentProcessor.GetCustomerAsync(tenant.Plan.StripeCustomerId);
            if (customer == null)
            {
                this.logger.LogError("Unknown customerId {CustomerId}", tenant.Plan.StripeCustomerId);
            }

            return customer;
        }
        else
        {
            var customer = await this.paymentProcessor.CreateCustomerAsync(tenant.Name, tenant.ContactAddress);
            tenant.Plan.StripeCustomerId = customer.Id;
            await this.dbContext.SaveChangesAsync();
            return customer;
        }
    }

    private async Task<int?> PlanTypeToMemberLimit(PlanType type)
    {
        return await this.dbContext.PlanMetadata
            .Where(s => s.Type == type)
            .Select(s => s.MemberLimit)
            .FirstOrDefaultAsync();
    }

    public ContactDetailsDto CustomerToContactDetailsDto(Stripe.Customer customer)
    {
        var contactDetails = new ContactDetailsDto
        {
            Name = customer.Name,
            Email = customer.Email,
        };

        if (customer.Address != null)
        {
            var lines = new List<string>();
            void AddAddressLine(string? line)
            {
                if (string.IsNullOrWhiteSpace(line)) { return; }
                lines.Add(line);
            }
            AddAddressLine(customer.Address.Line1);
            AddAddressLine(customer.Address.Line2);
            AddAddressLine(customer.Address.City);

            var statePostCodeLine = new List<string>();
            if (!string.IsNullOrWhiteSpace(customer.Address.State))
            {
                statePostCodeLine.Add(customer.Address.State);
            }
            if (!string.IsNullOrWhiteSpace(customer.Address.PostalCode))
            {
                statePostCodeLine.Add(customer.Address.PostalCode);
            }

            AddAddressLine(string.Join(" ", statePostCodeLine));
            AddAddressLine(customer.Address.Country);

            contactDetails.Address = string.Join('\n', lines);
        }

        return contactDetails;
    }

    public async Task<PaymentMethodDto?> GetPaymentMethodDtoAsync(Stripe.Subscription subscription)
    {
        var paymentMethod = await this.paymentProcessor.GetPaymentMethodAsync(subscription.DefaultPaymentMethodId);
        if (paymentMethod == null)
        {
            return null;
        }

        return new PaymentMethodDto
        {
            Type = paymentMethod.Type switch
            {
                StripePaymentMethodType.AuBecsDebit => "Direct Debit",
                StripePaymentMethodType.CreditCard => "Credit Card",
                _ => paymentMethod.Type,
            },
            Preview = paymentMethod.Type switch
            {
                StripePaymentMethodType.AuBecsDebit => $"BSB: ***-{paymentMethod.AuBecsDebit.BsbNumber[^3..]}"
                    + $", Account: ****{paymentMethod.AuBecsDebit.Last4}",
                StripePaymentMethodType.CreditCard => $"****-****-****-{paymentMethod.Card.Last4}"
                    + $", Expires {paymentMethod.Card.ExpMonth:00}/{paymentMethod.Card.ExpYear % 100:00}",
                _ => "Open Customer Portal to see details",
            },
        };
    }

    public async Task<UpcomingInvoiceDto?> GetUpcomingInvoiceDtoAsync(
        IList<PaidPlanMetadata> paidPlanMetadata, Tenant tenant, Stripe.Subscription subscription)
    {
        var invoice = await this.paymentProcessor.GetUpcomingInvoiceAsync(subscription.CustomerId, subscription.Id);
        if (invoice == null)
        {
            return null;
        }

        var planDetails = invoice.Lines.FirstOrDefault();
        if (planDetails == null)
        {
            return null;
        }

        var productId = planDetails.Price.ProductId;
        var plan = paidPlanMetadata.SingleOrDefault(e => e.StripeProductId == productId);

        return new UpcomingInvoiceDto
        {
            BillingDate = invoice.PeriodEnd,
            Description = $"Orchestrate - {plan?.Type ?? tenant.Plan.Type}",
            StartDate = planDetails.Period.Start,
            EndDate = planDetails.Period.End,
            TotalDollars = invoice.Total / 100M,
        };
    }

    public async Task<IEnumerable<PreviousInvoiceDto>> GetPreviousInvoiceDtosAsync(
        IList<PaidPlanMetadata> paidPlanMetadata, Stripe.Customer customer)
    {
        var invoices = await this.paymentProcessor.GetMostRecentPaidInvoicesAsync(customer.Id);
        return invoices.Select(i => GetPreviousInvoiceDto(paidPlanMetadata, i)).WhereNotNull().ToList();

    }

    private static PreviousInvoiceDto? GetPreviousInvoiceDto(
        IList<PaidPlanMetadata> paidPlanMetadata, Stripe.Invoice invoice)
    {
        var planDetails = invoice.Lines.FirstOrDefault();
        if (planDetails == null)
        {
            return null;
        }

        var productId = planDetails.Price.ProductId;
        var plan = paidPlanMetadata.SingleOrDefault(e => e.StripeProductId == productId);

        return new PreviousInvoiceDto
        {
            Description = plan != null
                ? $"Orchestrate - {plan.Type}"
                : "Access to Orchestrate",
            StartDate = planDetails.Period.Start,
            EndDate = planDetails.Period.End,
            TotalDollars = invoice.Total / 100M,
            Url = new Uri(invoice.HostedInvoiceUrl),
        };
    }

    public bool TenantAllowedToActivatePaidPlan(Tenant tenant)
    {
        return tenant.Plan.Type switch
        {
            PlanType.Free => true,
            PlanType.Trial => true,
            _ => false,
        };
    }

    public void NotForProfitDiscountSanityChecks(Tenant tenant, Stripe.Customer customer)
    {
        if (tenant.Plan.StripeCustomerId != customer.Id)
        {
            throw new InvalidOperationException("Tenant does not match customer");
        }

        if (tenant.Plan.IsNotForProfit && customer.Discount == null)
        {
            this.logger.LogError("Tenant is not for profit, but is not receiving a discount");
        }
    }

    public void PlanTypeSanityChecks(
        IList<PaidPlanMetadata> paidPlanMetadata,
        Tenant tenant,
        Stripe.Subscription? subscription)
    {
        if (subscription != null && tenant.Plan.StripeCustomerId != subscription.CustomerId)
        {
            throw new InvalidOperationException("Tenant does not match subscription");
        }

        using var planScope = this.logger.BeginScope(tenant.Plan.Type);

        var isPaidSubscription = paidPlanMetadata.Any(s => s.Type == tenant.Plan.Type);
        if (subscription == null || !subscription.Items.Any())
        {
            if (isPaidSubscription)
            {
                this.logger.LogError(
                    "Tenant is on paid plan ({Plan}) but has no stripe subscription, should be free",
                    tenant.Plan.Type);
            }

            return;
        }
        else if (!isPaidSubscription)
        {
            this.logger.LogError("Tenant is on unpaid plan but has subscriptions");
            return;
        }

        if (subscription.Items.Count() > 1)
        {
            this.logger.LogError("Subscribed to more than one item");
        }

        var actualProductIds = subscription.Items.Select(i => i.Price.ProductId).ToList();
        var actualPlanTypes = paidPlanMetadata
            .Where(s => actualProductIds.Contains(s.StripeProductId))
            .Select(s => s.Type)
            .ToList();
        if (actualProductIds.Count != actualPlanTypes.Count)
        {
            this.logger.LogError("Tenant is subscribed to unknown products");
        }
        if (actualPlanTypes.Count == 0)
        {
            this.logger.LogError(
                "Expected tenant to be {Expected}, but they aren't subscribed to any known plans, should be free",
                tenant.Plan.Type);
            return;
        }

        if (actualPlanTypes.Count > 1)
        {
            this.logger.LogError("Tenant is subscribed to more than one plan!");
        }
        if (!actualPlanTypes.Contains(tenant.Plan.Type))
        {
            this.logger.LogError(
                "Expected tenant to have {Expected} plan",
                string.Join(", ", actualPlanTypes.Select(s => s.ToString())));
        }
    }
}
