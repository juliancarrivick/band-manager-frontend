using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MemberBilling.State
{
    public class CreatedState : BillingPeriodState
    {
        public CreatedState(BillingPeriodStateContext stateContext, ILogger<CreatedState> logger)
            : base(stateContext, logger)
        {
        }

        public override MemberBillingPeriodState State => MemberBillingPeriodState.Created;

        public override async Task GenerateInvoices(string? generatingUserEmail = null)
        {
            Logger.LogInformation("Generating invoices");

            if (BillingPeriod.Generated == null)
            {
                BillingPeriod.Generated = DateTime.UtcNow;
                await DbContext.SaveChangesAsync();
            }

            var memberIdsWithExistingInvoices = await DbContext.MemberInvoice
                .Where(e => e.MemberBillingPeriodId == BillingPeriod.Id)
                .Select(e => e.MemberId)
                .ToListAsync();
            var billableMembers = await DbContext.Member
                .Include(e => e.Details)
                .Where(e => e.Details!.FeeClass != FeeClass.Special)
                .WhereIsActiveAt(BillingPeriod.Generated.Value)
                .Where(e => !memberIdsWithExistingInvoices.Contains(e.Id))
                .OrderBy(e => e.FirstName)
                .ThenBy(e => e.LastName)
                .ToListAsync();

            var memberInvoiceFactory = new MemberInvoiceFactory(DbContext);
            foreach (var member in billableMembers)
            {
                var memberInvoice = await memberInvoiceFactory.Build(BillingConfig, BillingPeriod, member);
                await DbContext.AddAsync(memberInvoice);
                await DbContext.SaveChangesAsync();
            }

            Logger.LogInformation("{Count} invoices generated", billableMembers.Count);

            if (generatingUserEmail != BillingConfig.NotificationContactEmail)
            {
                var email = BillingPeriodNotificationEmail.Create<BillingPeriodGeneratedNotificationEmail>(
                    AppConfig, Tenant, BillingConfig, BillingPeriod);
                await EmailService.SendAsync(email);
                BillingPeriod.NotificationContactNotified = DateTime.UtcNow;
            }

            StateContext.SetState(MemberBillingPeriodState.Draft);
            await DbContext.SaveChangesAsync();
        }


    }
}
