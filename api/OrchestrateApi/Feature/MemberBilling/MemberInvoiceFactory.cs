using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MemberBilling
{
    public class MemberInvoiceFactory
    {
        private readonly OrchestrateContext context;

        public MemberInvoiceFactory(OrchestrateContext context)
        {
            this.context = context;
        }

        public async Task<MemberInvoice> BuildAddAndSave(MemberBillingConfig billingConfig, MemberBillingPeriod billingPeriod, Member member)
        {
            var invoice = await Build(billingConfig, billingPeriod, member);
            await this.context.AddAsync(invoice);
            await this.context.SaveChangesAsync();
            return invoice;
        }

        public async Task<MemberInvoice> Build(MemberBillingConfig billingConfig, MemberBillingPeriod billingPeriod, Member member)
        {
            if (billingPeriod.State == MemberBillingPeriodState.Closed)
            {
                throw new InvalidOperationException("Cannot create an invoice in a closed billing period");
            }

            var memberInvoice = new MemberInvoice
            {
                MemberBillingPeriod = billingPeriod,
                Member = member,
                IsConcession = member.Details!.FeeClass == FeeClass.Concession,
                Reference = GenerateInvoiceReference(billingConfig),
            };

            memberInvoice.LineItems.Add(new MemberInvoiceLineItem
            {
                Description = "Association Fee",
                AmountCents = billingConfig.AssociationPeriodFeeDollars * 100,
                Ordinal = 0,
            });

            var currentBillableEnsembles = await context.EnsembleMembership
                .Where(e => e.MemberId == member.Id)
                .WhereIsActiveNow()
                .Where(e => e.Ensemble!.MemberBillingPeriodFeeDollars > 0)
                .Include(e => e.Ensemble)
                .OrderBy(e => e.Ensemble!.Name)
                .ToListAsync();
            foreach (var membership in currentBillableEnsembles)
            {
                var description = $"{membership.Ensemble!.Name} Fee";
                if (member.Details.FeeClass == FeeClass.Special || membership.Type == MembershipType.Special)
                {
                    description += " (Special)";
                }
                else if (memberInvoice.IsConcession)
                {
                    description += " (Concession)";
                }

                memberInvoice.LineItems.Add(new MemberInvoiceLineItem
                {
                    Description = description,
                    AmountCents = membership.Type == MembershipType.Special
                        ? 0
                        : CentsToBillMember(billingConfig, member, membership.Ensemble.MemberBillingPeriodFeeDollars),
                    Ordinal = memberInvoice.LineItems.Count,
                });
            }

            return memberInvoice;
        }

        private static int CentsToBillMember(MemberBillingConfig billingConfig, Member member, int dollars)
        {
            var multiplyer = member.Details!.FeeClass switch
            {
                FeeClass.Full => 1,
                FeeClass.Concession => billingConfig.ConcessionRate,
                FeeClass.Special => 0,
                _ => 0,
            };
            return (int)(dollars * 100 * multiplyer);
        }

        private static string GenerateInvoiceReference(MemberBillingConfig billingConfig)
        {
            var invoiceNumber = billingConfig.NextInvoiceNumber;
            billingConfig.NextInvoiceNumber++;

            return $"{billingConfig.InvoicePrefix}-{invoiceNumber:D6}";
        }
    }
}
