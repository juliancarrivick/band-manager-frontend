using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MemberBilling
{
    public class MemberBillingPeriodService
    {
        private readonly IServiceProvider serviceProvider;
        private readonly OrchestrateContext context;
        private readonly ILogger logger;

        public MemberBillingPeriodService(
            IServiceProvider serviceProvider,
            OrchestrateContext context,
            ILogger<MemberBillingPeriodService> logger)
        {
            this.serviceProvider = serviceProvider;
            this.context = context;
            this.logger = logger;
        }

        public BillingPeriodStateContext BuildStateContext(Tenant tenant, MemberBillingConfig billingConfig, MemberBillingPeriod billingPeriod)
        {
            return new BillingPeriodStateContext(this.serviceProvider, tenant, billingConfig, billingPeriod);
        }

        public async Task<MemberBillingPeriod?> TryCreateNextBillingPeriodAsync()
        {
            var latestDueDate = await this.context.MemberBillingPeriod
                .OrderByDescending(e => e.Due)
                .Select(e => (DateTime?)e.Due)
                .FirstOrDefaultAsync();

            // Ensure if we've been disabled a long time, the next one we generate will be in the future
            if (!latestDueDate.HasValue || latestDueDate < DateTime.UtcNow)
            {
                latestDueDate = DateTime.UtcNow;
            }

            // TODO probably can optimise this to be a single query, but this fast enough for now
            for (var i = 0; i < 12; i++)
            {
                var result = await TryCreateBillingPeriodAsync(latestDueDate.Value.AddMonths(i));
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Attempt to create a billing period which is configured to be generated in the specified month
        /// or if not specified, the current month. If successful, the billing period will be returned,
        /// otherwise null will be returned.
        /// </summary>
        public async Task<MemberBillingPeriod?> TryCreateBillingPeriodAsync(DateTime? candidateDate = null)
        {
            if (candidateDate == null)
            {
                candidateDate = DateTime.UtcNow;
            }

            if (candidateDate.Value.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException($"{nameof(candidateDate)} must be of DateTimeKind.Utc");
            }

            this.logger.LogInformation("Checking for billing period to be generated at {Date}", candidateDate);

            var tenant = await this.context.Tenant.SingleAsync();

            // Check against the month and year in the target timezone, not what it is at UTC
            // (otherwise we'll generate late/early depending on the timezone)
            candidateDate = candidateDate.Value.InTimeZone(tenant.TimeZone);

            var billingConfig = await this.context.MemberBillingConfig.FirstOrDefaultAsync();
            if (billingConfig == null)
            {
                this.logger.LogInformation("MemberBillingConfig not created, aborting billing period check");
                return null;
            }
            if (!billingConfig.IsEnabled)
            {
                this.logger.LogInformation("Billing is disabled, aborting billing period check");
                return null;
            }


            var candidateMonth = candidateDate.Value.Month;
            var billingPeriodConfig = await this.context.MemberBillingPeriodConfig
                .FirstOrDefaultAsync(i => i.AutoGenerateMonth == candidateMonth);
            if (billingPeriodConfig == null)
            {
                this.logger.LogInformation("No MemberBillingPeriodConfig with appropriate AutoGenerateMonth");
                return null;
            }

            var candidateYear = candidateDate.Value.Year;
            var existingBillingPeriod = await this.context.MemberBillingPeriod
                .FirstOrDefaultAsync(i => i.Due.Year == candidateYear && i.Due.Month == billingPeriodConfig.EndOfPeriodMonth);
            if (existingBillingPeriod != null)
            {
                this.logger.LogInformation("Billing period has already been generated ([{BillingPeriodName}] at {Date})",
                    existingBillingPeriod.Name, existingBillingPeriod.Generated);
                return null;
            }

            // 23:59:59 is close enough for our purposes, ideally in #73 we'll just use a local date
            var daysInDueMonth = DateTime.DaysInMonth(candidateYear, billingPeriodConfig.EndOfPeriodMonth);
            var localDueDate = new DateTime(candidateYear, billingPeriodConfig.EndOfPeriodMonth, daysInDueMonth, 23, 59, 59, DateTimeKind.Unspecified);
            var billingPeriod = new MemberBillingPeriod
            {
                Name = $"{candidateYear} - {billingPeriodConfig.Name}",
                State = MemberBillingPeriodState.Created,
                Due = localDueDate.ToUtcFromTimeZone(tenant.TimeZone),
            };

            await this.context.AddAsync(billingPeriod);
            await this.context.SaveChangesAsync();

            this.logger.LogInformation("Generated billing period {BillingPeriodName}", billingPeriod.Name);

            return billingPeriod;
        }

        public async Task AutoAdvanceBillingPeriodsAsync(CancellationToken cancellationToken)
        {
            var tenant = await context.Tenant.SingleAsync(cancellationToken);
            var billingConfig = await context.MemberBillingConfig.SingleOrDefaultAsync(cancellationToken);
            if (billingConfig == null)
            {
                // TODO Handle this better in #136
                this.logger.LogInformation("MemberBillingConfig not created, aborting billing period advancement");
                return;
            }

            var potentialBillingPeriods = await context.MemberBillingPeriod
                .Where(e => e.State != MemberBillingPeriodState.Closed)
                .ToListAsync(cancellationToken);
            foreach (var billingPeriod in potentialBillingPeriods)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                try
                {
                    // TODO Use cancellation token to abort if necessary (some steps in here can take a long time)
                    var stateContext = BuildStateContext(tenant, billingConfig, billingPeriod);

                    // Only apply non-manual transitions (i.e. don't auto send or close)
                    // Go in reverse order so we only transition one state
                    await stateContext.PerformTemporalTasks();
                    await stateContext.GenerateInvoices();
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Exception while auto advancing {BillingPeriodName}", billingPeriod.Name);
                    throw;
                }
            }
        }
    }
}
