using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;

namespace OrchestrateApi.Feature.MemberBilling
{
    public abstract class BillingPeriodNotificationEmail : EmailMessage
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        protected BillingPeriodNotificationEmail() { }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        protected Tenant Tenant { get; private set; }
        protected MemberBillingConfig BillingConfig { get; private set; }
        protected MemberBillingPeriod BillingPeriod { get; private set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1056")]
        protected string BillingPeriodUrl { get; private set; }

        public static T Create<T>(
            IAppConfiguration appConfig,
            Tenant tenant,
            MemberBillingConfig billingConfig,
            MemberBillingPeriod billingPeriod)
            where T : BillingPeriodNotificationEmail, new()
        {
            var email = new T
            {
                Tenant = tenant,
                SendContext = new() { Tenant = tenant },
                BillingConfig = billingConfig,
                BillingPeriod = billingPeriod,
            };

            var reviewUri = new UriBuilder(appConfig.FrontEndUri)
            {
                Path = $"group/{tenant.Abbreviation}/member-billing/period/{billingPeriod.Id}",
            };
            email.BillingPeriodUrl = reviewUri.Uri.AbsoluteUri;

            // TODO Add first name here for use in the below emails
            email.To = new EmailMessageAddress(
                billingConfig.NotificationContactName,
                billingConfig.NotificationContactEmail);

            email.Initialise();

            return email;
        }

        protected abstract void Initialise();
    }

    public class BillingPeriodGeneratedNotificationEmail : BillingPeriodNotificationEmail
    {
        protected override void Initialise()
        {
            Subject = "Member invoices ready for review";
            HtmlContent = $"""
                <p>Hi,</p>

                <p>
                The member invoices for period {BillingPeriod.Name} in {Tenant.Name} have been
                generated and are ready to review at {BillingPeriodUrl}.
                </p>
                """;
        }
    }

    public class BillingPeriodEarlyBirdEndingSoonNotificationEmail : BillingPeriodNotificationEmail
    {
        protected override void Initialise()
        {
            Subject = "Early bird period ending soon";
            HtmlContent = $"""
                <p>Hi,</p>

                <p>
                    The early bird period for {BillingPeriod.Name} in {Tenant.Name} will be ending
                    on {BillingPeriod.EarlyBirdDue!.Value.InTimeZone(Tenant.TimeZone).ToDateString()}.
                    To minimise the number of members who get a payment reminder when the early bird
                    period has ended it is advisable to review received early bird payments prior to
                    this date and mark them as paid. This can be done at {BillingPeriodUrl}.
                </p>
                """;
        }
    }

    public class BillingPeriodEarlyBirdEndedNotificationEmail : BillingPeriodNotificationEmail
    {
        protected override void Initialise()
        {
            Subject = "Early bird period ended";
            HtmlContent = $"""
                <p>Hi,</p>

                <p>
                    The early bird period for {BillingPeriod.Name} in {Tenant.Name} has now ended.
                    It is advisable to review received early bird payments and mark them as paid in
                    the next few days. This can be done at {BillingPeriodUrl}.
                </p>
                """;
        }
    }

    public class BillingPeriodDueSoonNotificationEmail : BillingPeriodNotificationEmail
    {
        protected override void Initialise()
        {
            Subject = "Billing period due soon";
            HtmlContent = $"""
                <p>Hi,</p>

                <p>
                    {BillingPeriod.Name} in {Tenant.Name} will be due on
                    {BillingPeriod.Due.InTimeZone(Tenant.TimeZone).ToDateString()}.
                    To minimise the number of members who get an overdue payment notification it is
                    advisable to review received invoice payments by this date and mark them as paid.
                    This can be done at {BillingPeriodUrl}.
                </p>
                """;
        }
    }

    public class BillingPeriodOverdueNotificationEmail : BillingPeriodNotificationEmail
    {
        protected override void Initialise()
        {
            Subject = "Billing period past due";
            HtmlContent = $"""
                <p>Hi,</p>

                <p>
                    {BillingPeriod.Name} in {Tenant.Name} is now past its due date.
                    Please verify you have received all the invoice payments you are expecting,
                    marking them as paid, and when you are ready close the billing period.
                    This can be done at {BillingPeriodUrl}.
                </p>
                """;
        }
    }
}
