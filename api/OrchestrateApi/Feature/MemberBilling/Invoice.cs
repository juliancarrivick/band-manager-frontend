namespace OrchestrateApi.Feature.MemberBilling
{
    public class Invoice
    {
        public required string Title { get; set; }
        public string? Subtitle { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1819")]
        public required byte[] Logo { get; set; }
        public required InvoiceContactInformation ContactInformation { get; init; }
        public required string Reference { get; set; }
        public DateTime Due { get; set; }
        public DateTime? Paid { get; set; }
        public required string InvoicedTo { get; set; }
        public IList<InvoiceLineItem> LineItems { get; } = new List<InvoiceLineItem>();
        public required InvoiceRemittanceAdvice RemittanceAdvice { get; init; }

        public Invoice AddLineItem(InvoiceLineItem lineItem)
        {
            LineItems.Add(lineItem);
            return this;
        }

    }

    public class InvoiceContactInformation
    {
        public required string OrganisationName { get; set; }
        public string? Abn { get; set; }
        public string? StreetAddress { get; set; }
        public string? PostalAddress { get; set; }
        public string? PhoneNo { get; set; }
        public string? Website { get; set; }
    }

    public class InvoiceLineItem
    {
        public required string Description { get; set; }
        public required int AmountCents { get; set; }
    }

    public class InvoiceRemittanceAdvice
    {
        public required string ContactEmail { get; set; }
        public string? BankName { get; set; }
        public string? BankAccountName { get; set; }
        public required string BankBsb { get; set; }
        public required string BankAccountNo { get; set; }
        public string? Remarks { get; set; }
    }
}
