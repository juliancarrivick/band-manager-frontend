using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Email;

namespace OrchestrateApi.Feature.MemberBilling
{
    public class MemberInvoiceEmailEventHandler : IEmailEventHandler
    {
        private readonly OrchestrateContext context;
        private readonly ILogger logger;

        public MemberInvoiceEmailEventHandler(
            OrchestrateContext context,
            ILogger<MemberInvoiceEmailEventHandler> logger)
        {
            this.context = context;
            this.logger = logger;
        }


        public async Task<int> HandleAsync(IList<EmailEvent> events)
        {
            var applicableEvents = events
                .WhereWithDiscardCountAction(
                    e => e.IsType(MemberInvoiceSender.MemberInvoiceEmailType),
                    numDiscarded => this.logger.LogInformation("Discarding {NumDiscarded} email events that aren't for MemberInvoices", numDiscarded));
            var numProcessed = 0;

            // Not the most efficient way of doing this, but for the numbers we'll be processing I
            // think OK for now
            foreach (var e in applicableEvents)
            {
                using var scope = this.logger.BeginScope(e);

                if (!e.Metadata.TryGetValue(MemberInvoiceSender.MemberInvoiceIdKey, out var unparsedId)
                    || !int.TryParse(unparsedId, out var memberInvoiceId))
                {
                    this.logger.LogError($"Unable to determine memberInvoiceId from MemberInvoiceEmailType event");
                    continue;
                }

                var memberInvoice = await this.context.MemberInvoice
                    .IgnoreQueryFilters()
                    .SingleOrDefaultAsync(e => e.Id == memberInvoiceId);
                if (memberInvoice == null)
                {
                    this.logger.LogError("Unable to find member invoice with Id = {MemberInvoiceId} from MemberInvoiceEmailType", memberInvoiceId);
                    continue;
                }

                memberInvoice.SendStatus = e.Status?.ToSendStatus();
                memberInvoice.SendStatusUpdated = e.DateTime;
                memberInvoice.SendStatusMessage = e.Summary;
                if (!string.IsNullOrWhiteSpace(e.Details))
                {
                    memberInvoice.SendStatusMessage += $" ({e.Details})";
                }

                numProcessed++;
            }

            await this.context.SaveChangesAsync();
            return numProcessed;
        }
    }
}
