using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.MemberBilling.State;

namespace OrchestrateApi.Feature.MemberBilling
{
    public enum StateTransitionResult
    {
        Successful,
        NotAllowed,
    }

    public interface IBillingPeriodState
    {
        Task GenerateInvoices(string? generatingUserEmail = null);
        Task<StateTransitionResult> SendInvoices();
        Task PerformTemporalTasks();
        Task<StateTransitionResult> CloseBillingPeriod();
    }

    public class BillingPeriodStateContext : IBillingPeriodState
    {
        private readonly ILoggerFactory loggerFactory;

        public BillingPeriodStateContext(
            IServiceProvider serviceProvider,
            Tenant tenant,
            MemberBillingConfig billingConfig,
            MemberBillingPeriod billingPeriod)
        {
            this.loggerFactory = GetService<ILoggerFactory>(serviceProvider);
            Tenant = tenant;
            BillingConfig = billingConfig;
            BillingPeriod = billingPeriod;

            DbContext = GetService<OrchestrateContext>(serviceProvider);
            AppConfig = GetService<IAppConfiguration>(serviceProvider);
            EmailService = GetService<IEmailService>(serviceProvider);
            MemberInvoiceSender = GetService<IMemberInvoiceSender>(serviceProvider);
        }

        public BillingPeriodStateContext(
            ILoggerFactory loggerFactory,
            Tenant tenant,
            MemberBillingConfig billingConfig,
            MemberBillingPeriod billingPeriod,
            OrchestrateContext dbContext,
            IAppConfiguration appConfig,
            IEmailService emailService,
            IMemberInvoiceSender memberInvoiceSender)
        {
            this.loggerFactory = loggerFactory;
            Tenant = tenant;
            BillingConfig = billingConfig;
            BillingPeriod = billingPeriod;
            MemberInvoiceSender = memberInvoiceSender;

            DbContext = dbContext;
            AppConfig = appConfig;
            EmailService = emailService;
        }

        internal OrchestrateContext DbContext { get; }
        internal IAppConfiguration AppConfig { get; }
        internal IEmailService EmailService { get; }
        internal IMemberInvoiceSender MemberInvoiceSender { get; }

        internal Tenant Tenant { get; }
        internal MemberBillingConfig BillingConfig { get; }
        internal MemberBillingPeriod BillingPeriod { get; }

        private BillingPeriodState State => BillingPeriod.State switch
        {
            MemberBillingPeriodState.Created => new CreatedState(this, this.loggerFactory.CreateLogger<CreatedState>()),
            MemberBillingPeriodState.Draft => new DraftState(this, this.loggerFactory.CreateLogger<DraftState>()),
            MemberBillingPeriodState.Open => new OpenState(this, this.loggerFactory.CreateLogger<OpenState>()),
            MemberBillingPeriodState.Closed => new ClosedState(this, this.loggerFactory.CreateLogger<ClosedState>()),
            _ => throw new InvalidOperationException($"Unknown billing period state: {BillingPeriod.State}"),
        };

        public Task GenerateInvoices(string? generatingUserEmail = null) => CallOnState(s => s.GenerateInvoices(generatingUserEmail));
        public Task<StateTransitionResult> SendInvoices() => CallOnState(s => s.SendInvoices());
        public Task PerformTemporalTasks() => CallOnState(s => s.PerformTemporalTasks());
        public Task<StateTransitionResult> CloseBillingPeriod() => CallOnState(s => s.CloseBillingPeriod());

        internal void SetState(MemberBillingPeriodState state)
        {
            BillingPeriod.State = state;
        }

        private async Task CallOnState(Func<BillingPeriodState, Task> doWithState)
        {
            using var state = State;
            await doWithState(state);
        }

        private async Task<StateTransitionResult> CallOnState(Func<BillingPeriodState, Task<StateTransitionResult>> doWithState)
        {
            using var state = State;
            return await doWithState(state);
        }

        private static T GetService<T>(IServiceProvider provider)
        {
            return (T)provider.GetRequiredService(typeof(T));
        }
    }
}
