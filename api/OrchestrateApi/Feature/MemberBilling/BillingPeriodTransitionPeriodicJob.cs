using OrchestrateApi.Common;

namespace OrchestrateApi.Feature.MemberBilling
{
    public sealed class BillingPeriodTransitionPeriodicJob(
        TenantContextIterator tenantContextIterator,
        MemberBillingPeriodService billingPeriodService,
        ILogger<BillingPeriodTransitionPeriodicJob> logger,
        IAppConfiguration appConfig)
        : IPeriodicJob
    {
        public string Name => "BillingPeriodTransition";
        public TimeSpan RunPeriod => appConfig.BillingPeriodTransitionCheckInterval;

        private ILogger<BillingPeriodTransitionPeriodicJob> Logger { get; } = logger;

        public async Task Run(CancellationToken cancellationToken)
        {
            await tenantContextIterator.ForEachTenant(
                nameof(BillingPeriodTransitionPeriodicJob),
                async (tenant) =>
                {
                    Logger.LogInformation("Beginning billing period generation");
                    var billingPeriod = await billingPeriodService.TryCreateBillingPeriodAsync();
                    if (billingPeriod == null)
                    {
                        Logger.LogInformation("No billing periods due to be generated");
                    }
                    else
                    {
                        Logger.LogInformation("Generated {BillingPeriodName} (Id = {BillingPeriodId})",
                            billingPeriod.Name, billingPeriod.Id);
                    }

                    Logger.LogInformation("Auto advancing billing periods if necessary");
                    await billingPeriodService.AutoAdvanceBillingPeriodsAsync(cancellationToken);
                    Logger.LogInformation("Auto advancing of billing periods complete");
                },
                cancellationToken);
        }
    }
}
