using System.Globalization;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;

namespace OrchestrateApi.Feature.MemberBilling
{
    public interface IMemberInvoiceSender
    {
        Task SendAsync(Tenant tenant, MemberBillingConfig billingConfig, IEnumerable<MemberInvoice> memberInvoices);
    }

    public class MemberInvoiceSender : IMemberInvoiceSender
    {
        internal const string MemberInvoiceEmailType = "MemberInvoice";
        internal const string MemberInvoiceIdKey = "MemberInvoiceId";

        private readonly IEmailService emailService;
        private readonly IMemberInvoiceRenderingService memberInvoiceRenderingService;

        public MemberInvoiceSender(
            IEmailService emailService,
            IMemberInvoiceRenderingService memberInvoiceRenderingService)
        {
            this.emailService = emailService;
            this.memberInvoiceRenderingService = memberInvoiceRenderingService;
        }

        public async Task SendAsync(Tenant tenant, MemberBillingConfig billingConfig, IEnumerable<MemberInvoice> memberInvoices)
        {
            var emails = memberInvoices.Select(e => GenerateEmail(tenant, billingConfig, e)).ToList();
            var emailResults = await this.emailService.SendAsync(emails);
            foreach (var result in emailResults)
            {
                if (result.Email.CustomData is MemberInvoice memberInvoice)
                {
                    memberInvoice.Sent = DateTime.UtcNow;
                    memberInvoice.SendStatus = result.Status.ToSendStatus();
                    memberInvoice.SendStatusUpdated = null;
                    memberInvoice.SendStatusMessage = result.StatusMessage;
                }
            }
        }

        private EmailMessage GenerateEmail(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice invoice)
        {
            var email = new EmailMessage
            {
                From = this.emailService.DefaultFrom with { Name = tenant.Name },
                ReplyTo = new(billingConfig.NotificationContactName, billingConfig.NotificationContactEmail),
                To = new EmailMessageAddress(
                    invoice.Member!.FirstName + " " + invoice.Member.LastName,
                    invoice.Member!.Details!.Email!),
                Subject = invoice.MemberBillingPeriod!.Name
                    + (invoice.Paid.HasValue ? " receipt" : " invoice"),
                CustomData = invoice,
                Type = MemberInvoiceEmailType,
                Metadata =
                {
                    [MemberInvoiceIdKey] = invoice.Id.ToString(CultureInfo.InvariantCulture),
                },
                SendContext = new() { Tenant = tenant },
            };

            email.HtmlContent = $"<p>Hi {invoice.Member.FirstName},</p>";
            email.HtmlContent += invoice.Paid.HasValue
                ? GenerateReceiptHtml(invoice)
                : GenerateInvoiceHtml(tenant, billingConfig, invoice);

            email.Attachments.Add(new EmailAttachment
            {
                Name = $"{invoice.Reference}.pdf",
                MimeType = "application/pdf",
                Base64Content = Convert.ToBase64String(this.memberInvoiceRenderingService.RenderAsPdf(tenant, billingConfig, invoice)),
            });

            return email;
        }

        private static string GenerateReceiptHtml(MemberInvoice invoice)
        {
            var totalDollars = invoice.LineItems.Sum(l => l.AmountCents) / 100M;
            var receiptHtml = $"""
                <p>
                    Thank you for your payment of ${totalDollars:0.00} for
                    {invoice.MemberBillingPeriod!.Name}. Please find a copy of your receipt for your
                    records.
                </p>
                """;

            return receiptHtml;
        }

        private static string GenerateInvoiceHtml(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice invoice)
        {
            var localDueDate = invoice.MemberBillingPeriod!.Due.InTimeZone(tenant.TimeZone);
            var invoiceHtml = "";

            if (invoice.MemberBillingPeriod.Due < DateTime.UtcNow)
            {
                invoiceHtml += $"""
                    <p>
                        Your {invoice.MemberBillingPeriod.Name} invoice is now overdue. Please pay
                        it as soon as possible. If you have already paid this invoice, please ignore
                        this message as payment may not have been received just yet.
                    </p>
                    """;
            }
            else if (invoice.MemberBillingPeriod.EarlyBirdDue is DateTime
                && invoice.MemberBillingPeriod.EarlyBirdDue < DateTime.UtcNow)
            {
                invoiceHtml += $"""
                    <p>
                        Your {invoice.MemberBillingPeriod.Name} invoice is now past the early bird
                        period and is due on {localDueDate.ToDateString()}. If you have already paid
                        this invoice, please ignore this message as payment may not have been
                        received just yet.
                    </p>
                    """;
            }
            else
            {
                invoiceHtml += "<p>";

                invoiceHtml += $"Your {invoice.MemberBillingPeriod.Name} invoice has now been "
                    + $"generated and is due on {localDueDate.ToDateString()}.";

                if (invoice.MemberBillingPeriod.EarlyBirdDue is DateTime earlyBirdDue)
                {
                    var localEarlyBirdDueDate = earlyBirdDue.InTimeZone(tenant.TimeZone);
                    invoiceHtml += $" If you pay by {localEarlyBirdDueDate.ToDateString()} "
                        + $"then you are eligible for an early bird discount of "
                        + billingConfig.EarlyBirdDiscountDollars.ToString("C", Culture.Default);
                }

                invoiceHtml += "</p>";
            }

            invoiceHtml += $"""
                <p>
                    Please see the attached invoice for details and how to pay. If any information
                    is wrong, please get in touch with {billingConfig.NotificationContactName} at
                    {billingConfig.NotificationContactEmail} to have it corrected and a new invoice
                    sent out.
                </p>
                """;

            return invoiceHtml;
        }
    }
}
