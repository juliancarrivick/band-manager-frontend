using MigraDocCore.DocumentObjectModel;
using MigraDocCore.DocumentObjectModel.MigraDoc.DocumentObjectModel.Shapes;
using MigraDocCore.DocumentObjectModel.Tables;
using MigraDocCore.Rendering;
using OrchestrateApi.Common;
using PdfSharpCore.Fonts;
using PdfSharpCore.Utils;
using SixLabors.ImageSharp.PixelFormats;

// Heavily based off https://github.com/myvas/PdfSharpCore/blob/master/samples/Invoice/InvoiceForm.cs
namespace OrchestrateApi.Feature.MemberBilling
{
    public interface IInvoiceRenderer
    {
        void RenderToStream(Invoice invoice, Stream outputStream);
    }

    public class SharpPdfInvoiceRenderer : IInvoiceRenderer
    {
        static SharpPdfInvoiceRenderer()
        {
            // See https://github.com/ststeiger/PdfSharpCore/issues/96
            ImageSource.ImageSourceImpl = new ImageSharpImageSource<Rgba32>();
            GlobalFontSettings.FontResolver = new EmbeddedFontResolver();
        }

        public void RenderToStream(Invoice invoice, Stream outputStream)
        {
            var pdfRenderer = new PdfDocumentRenderer
            {
                Document = BuildDocument(invoice),
            };

            pdfRenderer.RenderDocument();
            pdfRenderer.PdfDocument.Save(outputStream);
        }

        private static Document BuildDocument(Invoice invoice)
        {
            var document = new Document
            {
                Info =
                {
                    Title = invoice.Title + (invoice.Paid.HasValue ? " Receipt" : " Invoice"),
                    Author = invoice.ContactInformation.OrganisationName,
                }
            };

            DefineStyles(document);

            var section = document.AddSection();
            AddPreamble(invoice, section);
            AddLineItemTable(invoice, section);
            AddPostamble(invoice, section);

            return document;
        }

        private static void DefineStyles(Document document)
        {
            // Get the predefined style Normal.
            var style = document.Styles["Normal"];

            // Because all styles are derived from Normal, the next line changes the
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "OpenSans";

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

            // Create a new style called Heading based on style Normal
            style = document.Styles.AddStyle("Heading", "Normal");
            style.Font.Size = 18;

            // Create a new style called RightAlign based on style Normal
            style = document.Styles.AddStyle("RightAlign", "Normal");
            style.ParagraphFormat.Alignment = ParagraphAlignment.Right;
            style.Font.Size = 9;
        }

        private static void AddPreamble(Invoice invoice, Section section)
        {
            if (invoice.Logo != null)
            {
                var image = section.AddImage(ImageSource.FromBinary("Organisation Logo", () => invoice.Logo));
                image.LockAspectRatio = true;
                image.Height = 40;
            }

            // Add the heading
            var paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "1.8cm";
            paragraph.Style = "Heading";
            paragraph.AddFormattedText(invoice.Title, TextFormat.Bold);

            if (!string.IsNullOrWhiteSpace(invoice.Subtitle))
            {
                paragraph = section.AddParagraph(invoice.Subtitle);
            }

            paragraph.Format.SpaceAfter = "0.5cm";
            paragraph.AddTab();

            // add the address
            section.AddParagraph(invoice.ContactInformation.OrganisationName, "RightAlign");
            if (invoice.ContactInformation.Abn is string abn)
            {
                section.AddParagraph($"ABN: {abn}", "RightAlign");
            }
            if (invoice.ContactInformation.StreetAddress is string streetAddress)
            {
                section.AddParagraph($"Street Address: {streetAddress}", "RightAlign");
            }
            if (invoice.ContactInformation.PostalAddress is string postalAddress)
            {
                section.AddParagraph($"Postal Address: {postalAddress}", "RightAlign");
            }
            if (invoice.ContactInformation.PhoneNo is string phoneNo)
            {
                section.AddParagraph($"Phone: {phoneNo}", "RightAlign");
            }
            if (invoice.ContactInformation.Website is string website)
            {
                section.AddParagraph(website, "RightAlign");
            }

            // invoice number
            paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "0.25cm";
            paragraph.Format.Alignment = ParagraphAlignment.Right;
            paragraph.Format.Font.Bold = true;
            paragraph.AddFormattedText("Invoice Reference:");

            paragraph = section.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Right;
            paragraph.Format.Font.Bold = true;
            paragraph.Format.Font.Size = 12;
            paragraph.AddFormattedText(invoice.Reference);

            if (!invoice.Paid.HasValue)
            {
                paragraph = section.AddParagraph();
                paragraph.Format.Font.Bold = true;
                paragraph.Format.Font.Size = 12;
                paragraph.AddFormattedText($"Due: {invoice.Due:dd/MM/yyyy}");
            }

            // date (and paid indication, if relevant)
            paragraph = section.AddParagraph();
            paragraph.AddFormattedText(invoice.Paid.HasValue ? "Date Paid: " : "Issued: ");
            paragraph.AddFormattedText((invoice.Paid ?? DateTime.UtcNow).ToString("dd/MM/yyyy", Culture.Default));

            paragraph.Format.SpaceAfter = "0.3cm";

            // invoice organisation & address
            paragraph = section.AddParagraph();
            paragraph.AddFormattedText("Invoiced To: ");
            paragraph = section.AddParagraph();
            paragraph.AddFormattedText(invoice.InvoicedTo);
            paragraph.Format.Font.Size = 12;
            paragraph.Format.SpaceAfter = "0.3cm";
        }

        private static void AddLineItemTable(Invoice invoice, Section section)
        {
            // Create the item table
            var table = section.AddTable();
            table.Borders.Color = Colors.Black;
            table.Borders.Width = 0.75;
            table.Borders.Left.Width = 0.75;
            table.Borders.Right.Width = 0.75;
            table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            var column = table.AddColumn("12cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            // Create the header of the table
            var row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.TopPadding = 3;
            row.BottomPadding = 3;

            row.Cells[0].AddParagraph("Description");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Bottom;

            row.Cells[1].AddParagraph("Amount");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Bottom;

            table.SetEdge(0, 0, 2, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);

            PopulateLineItems(invoice, table);
        }

        private static void PopulateLineItems(Invoice invoice, Table table)
        {
            // Iterate the invoice items
            var lastItem = invoice.LineItems.LastOrDefault();
            foreach (var item in invoice.LineItems)
            {
                var lineItemRow = table.AddRow();
                lineItemRow.TopPadding = 2;
                lineItemRow.BottomPadding = 2;

                lineItemRow.Cells[0].Format.Alignment = ParagraphAlignment.Left;
                lineItemRow.Cells[0].Borders.Bottom.Visible = item == lastItem;
                lineItemRow.Cells[0].AddParagraph(item.Description);

                var amountDollars = item.AmountCents / 100M;
                lineItemRow.Cells[1].AddParagraph(amountDollars.ToString("C", Culture.Default));
                lineItemRow.Cells[1].Borders.Bottom.Visible = item == lastItem;
            }

            // Add an invisible row as a space line to the table
            var row = table.AddRow();
            row.Borders.Visible = false;

            // Add the total paid row
            row = table.AddRow();
            row.Cells[0].AddParagraph("Total " + (invoice.Paid.HasValue ? "Paid" : "Due"));
            row.Cells[0].Borders.Visible = false;
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Right;

            var totalCents = invoice.LineItems.Sum(e => e.AmountCents);
            var totalDollars = totalCents / 100M;
            row.Cells[1].AddParagraph(totalDollars.ToString("C", Culture.Default));

            // Set the borders of the specified cell range
            table.SetEdge(1, table.Rows.Count - 2, 1, 2, Edge.Box, BorderStyle.Single, 0.75);
        }

        private static void AddPostamble(Invoice invoice, Section section)
        {
            // add remittance advice
            if (invoice.Paid.HasValue)
            {
                var paragraph = section.AddParagraph();
                paragraph.Format.SpaceBefore = "1cm";
                paragraph.AddText("Enquiries: ");
                paragraph.AddText(invoice.RemittanceAdvice.ContactEmail);
                paragraph.Format.Font.Size = 9;
            }
            else
            {
                var p = section.AddParagraph();
                p.Format.SpaceBefore = "1cm";

                var remittanceTable = section.AddTable();
                remittanceTable.Format.Alignment = ParagraphAlignment.Right;
                var remittanceTableColumn = remittanceTable.AddColumn("4cm");
                remittanceTableColumn.Format.Alignment = ParagraphAlignment.Left;
                remittanceTableColumn = remittanceTable.AddColumn("8cm");
                remittanceTableColumn.Format.Alignment = ParagraphAlignment.Right;

                var remittanceTableRow = remittanceTable.AddRow();
                remittanceTableRow.Cells[0].AddParagraph("Remittance Advice:");
                remittanceTableRow.Cells[1].AddParagraph(invoice.RemittanceAdvice.ContactEmail);

                if (!string.IsNullOrWhiteSpace(invoice.RemittanceAdvice.BankName))
                {
                    remittanceTableRow = remittanceTable.AddRow();
                    remittanceTableRow.Cells[0].AddParagraph("Bank Name:");
                    remittanceTableRow.Cells[1].AddParagraph(invoice.RemittanceAdvice.BankName);
                }

                if (!string.IsNullOrWhiteSpace(invoice.RemittanceAdvice.BankAccountName))
                {
                    remittanceTableRow = remittanceTable.AddRow();
                    remittanceTableRow.Cells[0].AddParagraph("Account Name:");
                    remittanceTableRow.Cells[1].AddParagraph(invoice.RemittanceAdvice.BankAccountName);
                }

                remittanceTableRow = remittanceTable.AddRow();
                remittanceTableRow.Cells[0].AddParagraph("BSB:");
                remittanceTableRow.Cells[1].AddParagraph(invoice.RemittanceAdvice.BankBsb);

                remittanceTableRow = remittanceTable.AddRow();
                remittanceTableRow.Cells[0].AddParagraph("Account:");
                remittanceTableRow.Cells[1].AddParagraph(invoice.RemittanceAdvice.BankAccountNo);

                if (!string.IsNullOrWhiteSpace(invoice.RemittanceAdvice.Remarks))
                {
                    var paragraph = section.AddParagraph();
                    paragraph.Format.SpaceBefore = "1cm";
                    paragraph.AddFormattedText(invoice.RemittanceAdvice.Remarks);
                }
            }
        }
    }

    internal class EmbeddedFontResolver : IFontResolver
    {
        public string DefaultFontName => "OpenSans";

        public byte[] GetFont(string faceName)
        {
            var file = new EmbeddedFile(typeof(EmbeddedFontResolver), $"Resources.{faceName}.ttf");
            return file.AsByteArray();
        }

        public FontResolverInfo ResolveTypeface(string familyName, bool isBold, bool isItalic)
        {
            // Comes through as "Open Sans" from somewhere out of my control
            familyName = familyName.Replace(" ", "", StringComparison.InvariantCulture);

            if (!familyName.Equals("OPENSANS", StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("Only OpenSans is supported");
            }

            return new FontResolverInfo($"{familyName}-{GetStyle(isBold, isItalic)}");
        }

        private static string GetStyle(bool isBold, bool isItalic)
        {
            if (isBold && isItalic)
            {
                return "BoldItalic";
            }
            else if (isBold)
            {
                return "Bold";
            }
            else if (isItalic)
            {
                return "Italic";
            }
            else
            {
                return "Regular";
            }
        }
    }
}
