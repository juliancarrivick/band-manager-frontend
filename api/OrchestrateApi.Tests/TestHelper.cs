using Microsoft.Extensions.Logging.Abstractions;
using OrchestrateApi.Common;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1724")]
    public static class TestHelper
    {
        public static readonly IConfiguration RawConfiguration = new ConfigurationBuilder()
            .ConfigureForTesting()
            .Build();

        public static AppConfiguration AppConfig { get; } = new AppConfiguration(RawConfiguration);

        public static EmbeddedFile TestImage => new EmbeddedFile(typeof(IInvoiceRenderer), "Resources.GroupLogoPlaceholder.png");

        public static IConfigurationBuilder ConfigureForTesting(this IConfigurationBuilder builder)
        {
            return builder
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Testing.json", optional: true)
                .AddEnvironmentVariables();
        }

        public static ILoggerFactory BuildMockLoggerFactory() => new MockLoggerFactory();
        public static ILogger BuildMockLogger() => new NullLogger<object>();
        public static ILogger<T> BuildMockLogger<T>() => new NullLogger<T>();

        private sealed class MockLoggerFactory : ILoggerFactory
        {
            public void AddProvider(ILoggerProvider provider)
            {
                throw new NotImplementedException();
            }

            public ILogger CreateLogger(string categoryName)
            {
                return new NullLogger<object>();
            }

            public void Dispose()
            {
                // Noop
            }
        }
    }
}
