using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Security
{
    public sealed class UserServiceTests : FullIntegrationTestHarness
    {
        private readonly UserService userService;
        private readonly UserManager<OrchestrateUser> userManager;
        private readonly ITenantService tenantService;
        private readonly MockEmailService emailService;

        public UserServiceTests()
        {
            this.userService = GetService<UserService>();
            this.userManager = GetService<UserManager<OrchestrateUser>>();
            this.tenantService = GetService<ITenantService>();
            this.emailService = GetServiceAs<IEmailService, MockEmailService>();
        }

        [Fact]
        public async Task UpdateUserEmailUpdatesEmailAndUsername()
        {
            var user = new OrchestrateUser
            {
                UserName = "old@email.com",
                Email = "old@email.com",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await this.userManager.CreateAsync(user);

            await this.userService.UpdateUserEmail(user, "new@email.com");

            Assert.Equal("new@email.com", user.UserName);
            Assert.Equal("new@email.com", user.Email);
        }

        [Fact]
        public async Task MemberEmailsInAllTenantsAreUpdated()
        {
            var user = new OrchestrateUser
            {
                UserName = "old@email.com",
                Email = "old@email.com",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await this.userManager.CreateAsync(user);
            await CreateMemberInNewTenantForUserAsync(user);
            await CreateMemberInNewTenantForUserAsync(user);
            await CreateMemberInNewTenantForUserAsync(user);

            await this.userService.UpdateUserEmail(user, "new@email.com");

            var memberDetails = await Context.Member
                .IgnoreQueryFilters()
                .Select(e => e.Details!)
                .Where(e => e.UserId == user.Id)
                .ToListAsync();
            Assert.Equal(3, memberDetails.Count);
            Assert.All(memberDetails, e => Assert.Equal("new@email.com", e.Email));
        }

        [Fact]
        public async Task GeneratedTokenHasExpectedClaims()
        {
            var user = new OrchestrateUser
            {
                UserName = "old@email.com",
                Email = "old@email.com",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await this.userManager.CreateAsync(user);
            var tenantA = await CreateMemberInNewTenantForUserAsync(user, StandardRole.Administrator);
            var tenantB = await CreateMemberInNewTenantForUserAsync(user, StandardRole.Viewer);

            var token = await this.userService.GenerateBearerTokenAsync(user);

            Assert.Equal("old@email.com", token.Claims.GetClaimValues(ClaimTypes.Name).Single());
            Assert.Equal(user.Id.ToString(), token.Claims.GetClaimValues(ClaimTypes.NameIdentifier).Single());
            Assert.True(Guid.TryParse(token.Claims.GetClaimValues(JwtRegisteredClaimNames.Jti).Single(), out _));
            Assert.Null(token.Claims.GetClaimValues(ClaimsPrincipalExtensions.GlobalAdminClaimName).SingleOrDefault());
            Assert.Equal("a", token.Claims.GetClaimValues(ClaimTypes.GivenName).Single());
            Assert.Equal("b", token.Claims.GetClaimValues(ClaimTypes.Surname).Single());
            var roles = token.Claims.GetClaimValues(ClaimTypes.Role).ToList();
            Assert.Equal(2, roles.Count);
            Assert.Single(roles, r => r == new TenantRole(tenantA.Id, StandardRole.Administrator).Serialise());
            Assert.Single(roles, r => r == new TenantRole(tenantB.Id, StandardRole.Viewer).Serialise());
        }

        [Fact]
        public async Task GlobalAdminHasAppropriateClaim()
        {
            var user = new OrchestrateUser
            {
                UserName = "old@email.com",
                Email = "old@email.com",
                IsGlobalAdmin = true,
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await this.userManager.CreateAsync(user);

            var token = await this.userService.GenerateBearerTokenAsync(user);

            Assert.Equal(bool.TrueString, token.Claims.GetClaimValues(ClaimsPrincipalExtensions.GlobalAdminClaimName).SingleOrDefault());
        }

        [Fact]
        public async Task UserWithNoMemberDoesntHaveNamePopulated()
        {
            var user = new OrchestrateUser
            {
                UserName = "old@email.com",
                Email = "old@email.com",
                IsGlobalAdmin = true,
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await this.userManager.CreateAsync(user);

            var token = await this.userService.GenerateBearerTokenAsync(user);

            Assert.Null(token.Claims.GetClaimValues(ClaimTypes.GivenName).SingleOrDefault());
            Assert.Null(token.Claims.GetClaimValues(ClaimTypes.Surname).SingleOrDefault());
        }

        [Theory]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("user@email.com", true)]
        [InlineData(" user@email.com ", true)]
        public async Task RequestResetPasswordSanitisesInput(string? email, bool isSent)
        {
            var user = new OrchestrateUser
            {
                UserName = "user@email.com",
                Email = "user@email.com",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await this.userManager.CreateAsync(user);

            await this.userService.RequestResetPassword(email);

            if (isSent)
            {
                Assert.NotNull(this.emailService.LastEmail);
            }
            else
            {
                Assert.Null(this.emailService.LastEmail);
            }
        }

        [Theory]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData(" ", false)]
        [InlineData("/home", true)]
        public async Task RequestResetPasswordAddsRedirectParameterIfPresent(string? redirect, bool hasParam)
        {
            var user = new OrchestrateUser
            {
                UserName = "user@email.com",
                Email = "user@email.com",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await this.userManager.CreateAsync(user);

            await this.userService.RequestResetPassword(user.Email, redirect);
            Assert.NotNull(this.emailService.LastEmail);
            if (hasParam)
            {
                Assert.NotNull(redirect);
                var param = $"redirect={Uri.EscapeDataString(redirect)}";
                Assert.Contains(param, this.emailService.LastEmail.HtmlContent);
            }
            else
            {
                Assert.DoesNotContain("redirect=", this.emailService.LastEmail.HtmlContent);
            }
        }

        [Theory]
        [InlineData(true, "a b", "Hi a,")]
        [InlineData(false, "user@email.com", "Hi there,")]
        public async Task PopulatesMemberIfPresent(bool createEmail, string expectedToName, string expectedGreeting)
        {
            var user = new OrchestrateUser
            {
                UserName = "user@email.com",
                Email = "user@email.com",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await this.userManager.CreateAsync(user);
            if (createEmail)
            {
                await CreateMemberInNewTenantForUserAsync(user, StandardRole.Viewer);
            }

            await this.userService.RequestResetPassword(user.Email);
            var email = this.emailService.LastEmail;
            Assert.NotNull(email);
            Assert.Equal(expectedToName, email.To?.Name);
            Assert.Equal(user.Email, email.To?.Address);
            Assert.Equal("Password Reset Request", email.Subject);
            Assert.Contains(expectedGreeting, email.HtmlContent);
        }

        private async Task<Tenant> CreateMemberInNewTenantForUserAsync(OrchestrateUser user, params string[] roles)
        {
            using var tenantRef = this.tenantService.ForceOverrideTenantId(Guid.NewGuid());
            var tenantEntry = Context.Add(new Tenant
            {
                Id = this.tenantService.TenantId!.Value,
                Name = Guid.NewGuid().ToString(),
                Abbreviation = Guid.NewGuid().ToString(),
                TimeZone = "Australia/Perth",
            });
            Context.Add(new Member
            {
                FirstName = "a",
                LastName = "b",
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Details = new MemberDetails
                {
                    Address = "abc",
                    PhoneNo = "01234",
                    Email = user.Email,
                    User = user,
                }
            });
            await Context.SaveChangesAsync();

            foreach (var role in roles)
            {
                var result = await this.userManager.AddToRoleAsync(user, role);
                Assert.True(result.Succeeded);
            }

            return tenantEntry.Entity;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.userManager.Dispose();
            }
        }
    }

    public class UserServiceActiveUserRolesTests : FullIntegrationTestHarness
    {
        private readonly Tenant tenantA;
        private readonly Tenant tenantB;

        // Would be better to populate this from a query to the seed data
        private readonly int SeededRoleCount = 2;

        public UserServiceActiveUserRolesTests()
        {
            UserService = GetService<UserService>();
            UserManager = GetService<UserManager<OrchestrateUser>>();

            this.tenantA = new Tenant { Id = Guid.NewGuid(), Name = "Tenant A", Abbreviation = "a", TimeZone = "Australia/Perth" };
            this.tenantB = new Tenant { Id = Guid.NewGuid(), Name = "Tenant B", Abbreviation = "b", TimeZone = "Australia/Perth" };
            Context.Add(this.tenantA);
            Context.Add(this.tenantB);
            Context.SaveChanges();
        }

        private UserService UserService { get; }
        private UserManager<OrchestrateUser> UserManager { get; }

        [Fact]
        public async Task DirectRolesAreCorrectlyIsolatedAcrossTenants()
        {
            var user = await AddMemberToTenants(this.tenantA, this.tenantB);
            await AddDirectRolesToUserInTenant(user, this.tenantA, StandardRole.Administrator);
            await AddDirectRolesToUserInTenant(user, this.tenantB, StandardRole.Viewer);

            var tenantARoles = await ActiveUserRoles(this.tenantA);
            Assert.Single(tenantARoles);
            Assert.Equal(user.Id, tenantARoles[0].UserId);
            Assert.Equal(this.tenantA.Id, tenantARoles[0].TenantId);
            Assert.Equal((await MemberInTenant(user, this.tenantA)).Id, tenantARoles[0].MemberId);
            Assert.Null(tenantARoles[0].EnsembleId);
            Assert.Equal(StandardRole.Administrator, tenantARoles[0].Role);

            var tenantBRoles = await ActiveUserRoles(this.tenantB);
            Assert.Single(tenantBRoles);
            Assert.Equal(user.Id, tenantBRoles[0].UserId);
            Assert.Equal(this.tenantB.Id, tenantBRoles[0].TenantId);
            Assert.Equal((await MemberInTenant(user, this.tenantB)).Id, tenantBRoles[0].MemberId);
            Assert.Null(tenantBRoles[0].EnsembleId);
            Assert.Equal(StandardRole.Viewer, tenantBRoles[0].Role);

            var allRoles = await ActiveUserRoles();
            Assert.Equal(2 + SeededRoleCount, allRoles.Count);
            Assert.Single(allRoles, r => r.TenantId == this.tenantA.Id && r.Role == StandardRole.Administrator);
            Assert.Single(allRoles, r => r.TenantId == this.tenantB.Id && r.Role == StandardRole.Viewer);
        }

        [Fact]
        public async Task DirectRolesAreOnlyReturnedForActiveMembers()
        {
            var user = await AddMemberToTenants(this.tenantA, this.tenantB);
            await AddDirectRolesToUserInTenant(user, this.tenantA, StandardRole.Administrator);
            await AddDirectRolesToUserInTenant(user, this.tenantB, StandardRole.Viewer);
            var memberA = await MemberInTenant(user, this.tenantA);
            memberA.DateLeft = DateTime.UtcNow.AddHours(-1);
            await Context.SaveChangesAsync();

            var tenantARoles = await ActiveUserRoles(this.tenantA);
            Assert.Empty(tenantARoles);

            var tenantBRoles = await ActiveUserRoles(this.tenantB);
            Assert.Single(tenantBRoles);
            Assert.Equal(StandardRole.Viewer, tenantBRoles[0].Role);

            var allRoles = await ActiveUserRoles();
            Assert.Single(allRoles, e => e.TenantId == this.tenantB.Id);
            Assert.Single(allRoles, e => e.TenantId == this.tenantB.Id && e.Role == StandardRole.Viewer);
        }

        [Fact]
        public async Task EnsembleRolesAreCorrectlyIsolatedAcrossTenant()
        {
            var user = await AddMemberToTenants(this.tenantA, this.tenantB);
            var ensembleAMembership = await AddEnsembleRolesToUserInTenant(user, this.tenantA, "Ensemble A", StandardRole.Administrator);
            var ensembleBMembership = await AddEnsembleRolesToUserInTenant(user, this.tenantB, "Ensemble B", StandardRole.Viewer);
            var ensembleCMembership = await AddEnsembleRolesToUserInTenant(user, this.tenantB, "Ensemble C", StandardRole.Editor);

            var tenantARoles = await ActiveUserRoles(this.tenantA);
            Assert.Single(tenantARoles);
            Assert.Equal(user.Id, tenantARoles[0].UserId);
            Assert.Equal(this.tenantA.Id, tenantARoles[0].TenantId);
            Assert.Equal((await MemberInTenant(user, this.tenantA)).Id, tenantARoles[0].MemberId);
            Assert.Equal(ensembleAMembership.EnsembleId, tenantARoles[0].EnsembleId);
            Assert.Equal(StandardRole.Administrator, tenantARoles[0].Role);

            var tenantBRoles = await ActiveUserRoles(this.tenantB);
            Assert.Equal(2, tenantBRoles.Count);
            var ensembleBRole = tenantBRoles.Single(e => e.EnsembleId == ensembleBMembership.EnsembleId);
            Assert.Equal(user.Id, ensembleBRole.UserId);
            Assert.Equal(this.tenantB.Id, ensembleBRole.TenantId);
            Assert.Equal(ensembleBMembership.MemberId, ensembleBRole.MemberId);
            Assert.Equal(StandardRole.Viewer, ensembleBRole.Role);
            var ensembleCRole = tenantBRoles.Single(e => e.EnsembleId == ensembleCMembership.EnsembleId);
            Assert.Equal(user.Id, ensembleCRole.UserId);
            Assert.Equal(this.tenantB.Id, ensembleCRole.TenantId);
            Assert.Equal(ensembleCMembership.MemberId, ensembleCRole.MemberId);
            Assert.Equal(StandardRole.Editor, ensembleCRole.Role);

            var allRoles = await ActiveUserRoles();
            Assert.Equal(3 + SeededRoleCount, allRoles.Count);
            Assert.Single(allRoles, r => r.TenantId == this.tenantA.Id && r.Role == StandardRole.Administrator);
            Assert.Single(allRoles, r => r.TenantId == this.tenantB.Id && r.Role == StandardRole.Viewer);
            Assert.Single(allRoles, r => r.TenantId == this.tenantB.Id && r.Role == StandardRole.Editor);
        }

        [Fact]
        public async Task EnsembleRolesAreOnlyReturnedForActiveMembers()
        {
            var user = await AddMemberToTenants(this.tenantA, this.tenantB);
            var ensembleAMembership = await AddEnsembleRolesToUserInTenant(user, this.tenantA, "Ensemble A", StandardRole.Administrator);
            ensembleAMembership.DateLeft = DateTime.UtcNow.AddHours(-1);
            var ensembleBMembership = await AddEnsembleRolesToUserInTenant(user, this.tenantB, "Ensemble B", StandardRole.Viewer);

            var tenantARoles = await ActiveUserRoles(this.tenantA);
            Assert.Empty(tenantARoles);

            var tenantBRoles = await ActiveUserRoles(this.tenantB);
            Assert.Single(tenantBRoles);
            Assert.Equal(user.Id, tenantBRoles[0].UserId);
            Assert.Equal(this.tenantB.Id, tenantBRoles[0].TenantId);
            Assert.Equal((await MemberInTenant(user, this.tenantB)).Id, tenantBRoles[0].MemberId);
            Assert.Equal(StandardRole.Viewer, tenantBRoles[0].Role);

            var allRoles = await ActiveUserRoles();
            Assert.Single(allRoles, e => e.TenantId == this.tenantB.Id);
            Assert.Single(allRoles, r => r.TenantId == this.tenantB.Id && r.EnsembleId == ensembleBMembership.EnsembleId && r.Role == StandardRole.Viewer);
        }

        [Fact]
        public async Task EnsembleRolesAreOnlyReturnedForActiveMemberships()
        {
            var user = await AddMemberToTenants(this.tenantA, this.tenantB);
            var ensembleAMembership = await AddEnsembleRolesToUserInTenant(user, this.tenantA, "Ensemble A", StandardRole.Administrator);
            ensembleAMembership.Member!.DateLeft = DateTime.UtcNow.AddHours(-1);
            var ensembleBMembership = await AddEnsembleRolesToUserInTenant(user, this.tenantB, "Ensemble B", StandardRole.Viewer);

            var tenantARoles = await ActiveUserRoles(this.tenantA);
            Assert.Empty(tenantARoles);

            var tenantBRoles = await ActiveUserRoles(this.tenantB);
            Assert.Single(tenantBRoles);
            Assert.Equal(user.Id, tenantBRoles[0].UserId);
            Assert.Equal(this.tenantB.Id, tenantBRoles[0].TenantId);
            Assert.Equal((await MemberInTenant(user, this.tenantB)).Id, tenantBRoles[0].MemberId);
            Assert.Equal(StandardRole.Viewer, tenantBRoles[0].Role);

            var allRoles = await ActiveUserRoles();
            Assert.Single(allRoles, e => e.TenantId == this.tenantB.Id);
            Assert.Single(allRoles, r => r.TenantId == this.tenantB.Id && r.EnsembleId == ensembleBMembership.EnsembleId && r.Role == StandardRole.Viewer);
        }

        private async Task<OrchestrateUser> AddMemberToTenants(params Tenant[] tenants)
        {
            var email = "a@email.com";
            var user = new OrchestrateUser
            {
                Email = email,
                UserName = email,
            };
            await UserManager.CreateAsync(user);

            foreach (var tenant in tenants)
            {
                using var tenantOverride = TenantService.ForceOverrideTenantId(tenant.Id);
                Context.Add(new Member
                {
                    FirstName = "a",
                    LastName = "b",
                    DateJoined = DateTime.UtcNow.AddDays(-1),
                    Details = new MemberDetails
                    {
                        Email = email,
                        Address = "abc",
                        PhoneNo = "1234",
                        User = user,
                    }
                });
            }

            await Context.SaveChangesAsync();

            return user;
        }

        private async Task AddDirectRolesToUserInTenant(OrchestrateUser user, Tenant tenant, params string[] roles)
        {
            using var tenantOverride = TenantService.ForceOverrideTenantId(tenant.Id);
            foreach (var role in roles)
            {
                await UserManager.AddToRoleAsync(user, role);
            }
        }

        private async Task<EnsembleMembership> AddEnsembleRolesToUserInTenant(OrchestrateUser user, Tenant tenant, string ensemble, params string[] roles)
        {
            using var tenantOverride = TenantService.ForceOverrideTenantId(tenant.Id);
            var ensembleEntry = Context.Add(new Ensemble { Name = ensemble });
            var membershipEntry = Context.Add(new EnsembleMembership
            {
                Member = await MemberInTenant(user, tenant),
                Ensemble = ensembleEntry.Entity,
                DateJoined = DateTime.UtcNow.AddDays(-1),
            });
            foreach (var role in roles)
            {
                Context.Add(new EnsembleRole
                {
                    Ensemble = ensembleEntry.Entity,
                    Role = await Context.Roles.SingleAsync(e => e.Name == role),
                });
            }
            await Context.SaveChangesAsync();
            return membershipEntry.Entity;
        }

        private async Task<IList<UserTenantRole>> ActiveUserRoles(Tenant? tenant = null)
        {
            if (tenant == null)
            {
                return await UserService.ActiveUserRoles(allTenants: true).ToListAsync();
            }
            else
            {
                using var tenantOverride = TenantService.ForceOverrideTenantId(tenant.Id);
                return await UserService.ActiveUserRoles().ToListAsync();
            }
        }

        private async Task<Member> MemberInTenant(OrchestrateUser user, Tenant tenant)
        {
            using var tenantOverride = TenantService.ForceOverrideTenantId(tenant.Id);
            return await Context.Member.SingleAsync(e => e.Details!.UserId == user.Id);
        }
    }
}
