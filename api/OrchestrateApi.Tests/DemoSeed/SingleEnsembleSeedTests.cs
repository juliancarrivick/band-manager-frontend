using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DemoSeed;

namespace OrchestrateApi.Tests.DemoSeed;

public class SingleEnsembleSeedTests : FullIntegrationTestHarness
{
    [Fact]
    public async Task SmokeTest()
    {
        var seed = GetService<SingleEnsembleSeed>();
        var tenant = await seed.SeedAsync();

        using var tenantOverride = GetService<ITenantService>().ForceOverrideTenantId(tenant.Id);

        Assert.True(await Context.Asset.AnyAsync());
        Assert.True(await Context.Member.AnyAsync());
        Assert.True(await Context.Score.AnyAsync());
        Assert.True(await Context.Ensemble.AnyAsync());
        Assert.True(await Context.Concert.AnyAsync());
    }
}
