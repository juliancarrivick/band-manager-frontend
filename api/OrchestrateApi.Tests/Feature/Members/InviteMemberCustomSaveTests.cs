using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Members;

namespace OrchestrateApi.Tests.Feature.Members
{
    public class InviteMemberCustomSaveTests : AutoRollbackTestHarness
    {
        private readonly InviteMemberCustomSave customSave = new(new MockMemberInviter());

        [Fact]
        public void EntityIsPreSaveValid()
        {
            Assert.False(this.customSave.EntityIsPreSaveValid(null));
            Assert.True(this.customSave.EntityIsPreSaveValid(new Member()));

        }

        [Fact]
        public async Task UserIsNotDefinedOnMemberDetail()
        {
            var member = await Context.Member
                .Include(e => e.Details!.User)
                .FirstAsync(e => e.Details!.UserId != null);

            var returnedEntities = await this.customSave.SaveLogicAsync(Context, member, default);

            var details = (MemberDetails)returnedEntities.First();
            Assert.NotNull(details.UserId);
            Assert.Null(details.User);
        }

        private sealed class MockMemberInviter : IMemberInviter
        {
            public Task<Member> InviteMember(Member member)
            {
                return Task.FromResult(member);
            }
        }
    }
}
