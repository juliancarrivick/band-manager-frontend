using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Contacts;

namespace OrchestrateApi.Tests.Feature.Contacts;

public class ContactsControllerTests : FullIntegrationTestHarness
{
    private readonly ContactsController controller;

    public ContactsControllerTests()
    {
        this.controller = ConstructController<ContactsController>();
    }

    [Theory]
    [InlineData(ContactCategory.Current, "1")]
    [InlineData(ContactCategory.Archived, "2", "3")]
    [InlineData(null, "1", "2", "3")]
    public async Task ContactsAreCorrectlyFilteredByCategory(
        ContactCategory? category,
        params string[] expectedNames)
    {
        Context.Add(new Contact { Name = "1" });
        Context.Add(new Contact { Name = "2", ArchivedOn = DateTime.UtcNow.AddSeconds(-1) });
        Context.Add(new Contact { Name = "3", ArchivedOn = DateTime.UtcNow.AddSeconds(1) });
        await Context.SaveChangesAsync();

        var contacts = await this.controller.GetAllContacts(category);

        Assert.Equal(expectedNames.Length, contacts.Count);
        foreach (var n in expectedNames)
        {
            Assert.Contains(n, contacts.Select(e => e.Name));
        }
    }

    [Fact]
    public async Task CreatedContactIsSanitised()
    {
        var contact = await this.controller.CreateContact(new CreateContactDto
        {
            Name = " 1 ",
            Email = "",
            PhoneNo = " ",
            Affiliation = null,
        });

        Assert.Equal("1", contact.Name);
        Assert.Null(contact.Email);
        Assert.Null(contact.PhoneNo);
        Assert.Null(contact.Affiliation);
    }

    [Fact]
    public async Task CreatedContactIsValidated()
    {
        var exception = await Assert.ThrowsAsync<ValidationException>(() => this.controller
            .CreateContact(new CreateContactDto
            {
                Name = "",
                Email = "invalidemail",
            }));

        Assert.Contains("Name", exception.Errors.Select(e => e.PropertyName));
        Assert.Contains("Email", exception.Errors.Select(e => e.PropertyName));
    }

    [Fact]
    public async Task UpdatedContactIsSanitised()
    {
        var entry = Context.Add(new Contact
        {
            Name = "1",
            Email = "e@m.com",
            PhoneNo = "123",
            Affiliation = "Comp",
        });
        await Context.SaveChangesAsync();
        var id = entry.Entity.Id;

        var result = await this.controller.UpdateContact(id, new UpdateContactDto
        {
            Name = " 2 ",
            Email = "",
            PhoneNo = " ",
            Affiliation = null,
        });

        Assert.NotNull(result.Value);
        Assert.Equal("2", result.Value.Name);
        Assert.Null(result.Value.Email);
        Assert.Null(result.Value.PhoneNo);
        Assert.Null(result.Value.Affiliation);
    }

    [Fact]
    public async Task UpdatedContactIsValidated()
    {
        var entry = Context.Add(new Contact { Name = "1" });
        await Context.SaveChangesAsync();

        var exception = await Assert.ThrowsAsync<ValidationException>(() => this.controller
            .UpdateContact(entry.Entity.Id, new UpdateContactDto
            {
                Name = "",
                Email = "invalidemail",
            }));

        Assert.Contains("Name", exception.Errors.Select(e => e.PropertyName));
        Assert.Contains("Email", exception.Errors.Select(e => e.PropertyName));
    }

    [Fact]
    public async Task UnknownContactReturnsNotFound()
    {
        var result = await this.controller.UpdateContact(-1, new UpdateContactDto());
        Assert.IsType<NotFoundResult>(result.Result);

        result = await this.controller.ArchiveContact(-1);
        Assert.IsType<NotFoundResult>(result.Result);

        result = await this.controller.UnarchiveContact(-1);
        Assert.IsType<NotFoundResult>(result.Result);

        var deleteResult = await this.controller.DeleteContact(-1);
        Assert.IsType<NotFoundResult>(deleteResult);
    }

    [Fact]
    public async Task ArchiveWorkflowWorksAsExpected()
    {
        var entry = Context.Add(new Contact { Name = "1" });
        await Context.SaveChangesAsync();
        Assert.Null(entry.Entity.ArchivedOn);

        var result = await this.controller.ArchiveContact(entry.Entity.Id);
        Assert.NotNull(result.Value?.ArchivedOn);
        var archivedOn = result.Value.ArchivedOn;

        result = await this.controller.ArchiveContact(entry.Entity.Id);
        Assert.Equal(archivedOn, result.Value?.ArchivedOn);

        result = await this.controller.UnarchiveContact(entry.Entity.Id);
        Assert.NotNull(result.Value);
        Assert.Null(result.Value.ArchivedOn);
    }

    [Fact]
    public async Task DeleteContactWorksAsExpected()
    {
        var entry = Context.Add(new Contact { Name = "1" });
        await Context.SaveChangesAsync();

        var result = await this.controller.DeleteContact(entry.Entity.Id);
        Assert.IsType<OkResult>(result);

        Assert.Equal(0, await Context.Contact.CountAsync(e => e.Id == entry.Entity.Id));
    }
}
