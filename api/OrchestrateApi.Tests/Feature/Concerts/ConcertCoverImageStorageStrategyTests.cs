using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Concerts;
using OrchestrateApi.Feature.MemberBilling;
using OrchestrateApi.Storage;
using OrchestrateApi.Tests.Storage;

namespace OrchestrateApi.Tests.Feature.Concerts;

public class ConcertCoverImageStorageStrategyTests : AutoRollbackTestHarness
{
    private ConcertCoverImageStorageStrategy StorageStrategy { get; } = new();

    [Fact]
    public async Task UploadedBlobMustBeAnImage()
    {
        var logoFile = new EmbeddedFile(typeof(IInvoiceRenderer), "Resources.GroupLogoPlaceholder.png");
        var result = await StorageStrategy.ValidateAsync(new FileAssemblyBlob(logoFile), default);
        Assert.True(result.IsValid);

        result = await StorageStrategy.ValidateAsync(new MockUploadedBlob("test", "text/plain", "contents"), default);
        Assert.False(result.IsValid);
    }

    [Fact]
    public async Task ValidBlobIdsAreAttachedToConcerts()
    {
        var a = GenerateConcertAndImage("A", hasBlob: true);
        var b = GenerateConcertAndImage("B", hasBlob: true);
        GenerateConcertAndImage("C", hasBlob: false);
        Context.Add(new OrchestrateBlob
        {
            Name = "test.txt",
            MimeType = "text/plain",
            StorageStrategy = new OrchestrateBlobStorageStrategyData
            {
                Name = StorageStrategy.Name,
                AccessData = JsonSerializer.SerializeToDocument("null"),
            },
        });
        await Context.SaveChangesAsync();

        var blobIds = await StorageStrategy.ValidBlobIds(Context).ToListAsync();
        Assert.Equal(2, blobIds.Count);
        Assert.Contains(a.CoverPhotoBlobId!.Value, blobIds);
        Assert.Contains(b.CoverPhotoBlobId!.Value, blobIds);

    }

    public Concert GenerateConcertAndImage(string concertName, bool hasBlob = true)
    {
        var concert = Context.Add(new Concert { Occasion = concertName }).Entity;

        if (hasBlob)
        {
            var blob = Context.Add(new OrchestrateBlob
            {
                Name = "test.txt",
                MimeType = "text/plain",
                StorageStrategy = new OrchestrateBlobStorageStrategyData
                {
                    Name = StorageStrategy.Name,
                    AccessData = JsonSerializer.SerializeToDocument("null"),
                },
            }).Entity;
            concert.CoverPhotoBlobId = blob.Id;
        }

        return concert;
    }

    private sealed class FileAssemblyBlob : IUploadedBlob
    {
        private readonly EmbeddedFile file;

        public FileAssemblyBlob(EmbeddedFile file)
        {
            this.file = file;
        }

        public string Name => "Mock";

        public string MimeType => "application/bytes";

        public Stream OpenReadStream() => this.file.OpenReadStream();
    }
}
