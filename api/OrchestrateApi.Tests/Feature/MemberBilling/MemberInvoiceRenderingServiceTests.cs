using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class MemberInvoiceRenderingServiceTests : AutoRollbackTestHarness
    {
        private readonly MockHostEnvironment mockHostEnvironment;
        private readonly MemberInvoiceRenderingService renderingService;

        public MemberInvoiceRenderingServiceTests()
        {
            this.mockHostEnvironment = new MockHostEnvironment();
            this.renderingService = new MemberInvoiceRenderingService(
                this.mockHostEnvironment,
                new SharpPdfInvoiceRenderer());
        }

        [Fact]
        public async Task MemberInvoiceGetsConvertedToInvoiceCorrectly()
        {
            var memberInvoice = await Context.MemberInvoice
                .Include(e => e.MemberBillingPeriod)
                .Include(e => e.Member)
                .Include(e => e.LineItems)
                .FirstAsync(e => e.Reference == "INV-000002");
            var tenant = await Context.Tenant.SingleAsync();
            var billingConfig = await Context.MemberBillingConfig.SingleAsync();
            var billingPeriod = await Context.MemberBillingPeriod.SingleAsync(e => e.Id == memberInvoice.MemberBillingPeriodId);
            var member = await Context.Member.SingleAsync(e => e.Id == memberInvoice.MemberId);

            var invoice = this.renderingService.ConvertToInvoice(tenant, billingConfig, memberInvoice);

            Assert.Equal($"*** TEST *** {billingPeriod.Name}", invoice.Title);
            Assert.Equal("This invoice has been generated in a test environment. It does not need to be paid.", invoice.Subtitle);
            Assert.Equal(memberInvoice.Reference, invoice.Reference);
            Assert.Equal(billingPeriod.Due.AddHours(8), invoice.Due); // +8 hours for UTC->AWST
            Assert.Equal(memberInvoice.Paid, invoice.Paid);
            Assert.Equal("Glenn Stevens", invoice.InvoicedTo);
            Assert.Equal(tenant.Name, invoice.ContactInformation.OrganisationName);
            Assert.Equal(tenant.Website, invoice.ContactInformation.Website);
            Assert.Equal(billingConfig.BankName, invoice.RemittanceAdvice.BankName);
            Assert.Equal(billingConfig.BankAccountName, invoice.RemittanceAdvice.BankAccountName);
            Assert.Equal(billingConfig.BankBsb, invoice.RemittanceAdvice.BankBsb);
            Assert.Equal(billingConfig.BankAccountNo, invoice.RemittanceAdvice.BankAccountNo);
            Assert.Equal(billingConfig.NotificationContactEmail, invoice.RemittanceAdvice.ContactEmail);

            Assert.Equal(2, invoice.LineItems.Count);
            var associationLineItem = invoice.LineItems.First(e => e.Description == "Association Fee");
            Assert.Equal(4000, associationLineItem.AmountCents);
            var bigBandLineItem = invoice.LineItems.First(e => e.Description == "Concert Band");
            Assert.Equal(9000, bigBandLineItem.AmountCents);
        }

        [Fact]
        public async Task TitleAndSubtitleDontGetAlteredIfInProdEnvironment()
        {
            this.mockHostEnvironment.EnvironmentName = Environments.Production;
            var memberInvoice = await Context.MemberInvoice
                .Include(e => e.MemberBillingPeriod)
                .Include(e => e.Member)
                .Include(e => e.LineItems)
                .SingleAsync(e => e.Reference == "INV-000002");
            var tenant = await Context.Tenant.SingleAsync();
            var billingConfig = await Context.MemberBillingConfig.SingleAsync();

            var invoice = this.renderingService.ConvertToInvoice(tenant, billingConfig, memberInvoice);

            Assert.Equal(memberInvoice.MemberBillingPeriod!.Name, invoice.Title);
            Assert.Null(invoice.Subtitle);
        }


        [Fact]
        public async Task WillAddEarlyBirdDiscountLineItemIfApplicable()
        {
            var openBillingPeriod = await Context.MemberBillingPeriod.FirstAsync(e => e.State == MemberBillingPeriodState.Open);
            openBillingPeriod.EarlyBirdDue = DateTime.UtcNow.AddDays(1);
            openBillingPeriod.Due = DateTime.UtcNow.AddDays(7);
            await Context.SaveChangesAsync();
            var memberInvoice = await Context.MemberInvoice.FirstAsync(e => !e.Paid.HasValue && e.MemberBillingPeriodId == openBillingPeriod.Id);

            var tenant = await Context.Tenant.SingleAsync();
            var billingConfig = await Context.MemberBillingConfig.SingleAsync();
            var billingPeriod = await Context.MemberBillingPeriod.SingleAsync(e => e.Id == memberInvoice.MemberBillingPeriodId);
            var member = await Context.Member.SingleAsync(e => e.Id == memberInvoice.MemberId);

            var invoice = this.renderingService.ConvertToInvoice(tenant, billingConfig, memberInvoice);

            var earlyBirdLineItem = invoice.LineItems.First(e => e.Description.Contains("Early Bird Discount"));
            Assert.Equal(-1000, earlyBirdLineItem.AmountCents);
        }

        // [Fact]
        [Fact(Skip = "Manually verify")]
        public async Task MemberInvoiceRendersSuccessfully()
        {
            var tenant = await Context.Tenant.SingleAsync();
            var billingConfig = await Context.MemberBillingConfig.SingleAsync();
            var memberInvoice = await Context.MemberInvoice
                .Include(e => e.MemberBillingPeriod)
                .Include(e => e.Member)
                .Include(e => e.LineItems)
                .SingleAsync(e => e.Reference == "INV-000001");
            memberInvoice.Paid = null;

            var outputPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            using var file = new FileStream($"{outputPath}/MemberInvoice.test.pdf", FileMode.Create);
            this.renderingService.RenderAsPdf(tenant, billingConfig, memberInvoice, file);
        }
    }
}
