using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class MemberInvoiceFactoryTests : AutoRollbackTestHarness, IAsyncLifetime
    {
        private Member member = null!;
        private readonly MemberBillingConfig billingConfig;
        private readonly MemberBillingPeriod billingPeriod;
        private readonly MemberInvoiceFactory invoiceFactory;

        public MemberInvoiceFactoryTests()
        {
            this.invoiceFactory = new MemberInvoiceFactory(Context);
            this.billingConfig = new MemberBillingConfig
            {
                BankBsb = string.Empty,
                BankAccountNo = string.Empty,
                NotificationContactName = string.Empty,
                NotificationContactEmail = string.Empty,
                ConcessionRate = 0.5M,
                AssociationPeriodFeeDollars = 40,
                EarlyBirdDiscountDollars = 10,
                InvoicePrefix = "INV",
                NextInvoiceNumber = 50,
            };
            this.billingPeriod = new MemberBillingPeriod
            {
                Name = "Test billing period",
                State = MemberBillingPeriodState.Created,
                EarlyBirdDue = DateTime.UtcNow.Date,
                Due = DateTime.UtcNow.AddDays(7),
            };
        }

        public async Task InitializeAsync()
        {
            this.member = await Context.Member
                .Include(e => e.Details)
                .FirstAsync(e => e.DateLeft == null);
            var membership = await Context.EnsembleMembership.Where(e => e.MemberId == member.Id).ToListAsync();
            Context.EnsembleMembership.RemoveRange(membership);

            var otherMembers = await Context.Member.Where(e => e.Id != member.Id).ToListAsync();
            Context.Member.RemoveRange(otherMembers);

            await Context.SaveChangesAsync();
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async Task WillOnlyBillAssociationFeeIfInNoEnsembles()
        {
            var invoice = await GenerateMemberInvoice();

            Assert.Single(invoice.LineItems);
            AssertLineItem(invoice, "Association Fee", 4000, 0);
        }

        [Fact]
        public async Task WillGenerateFullFees()
        {
            member.Details!.FeeClass = FeeClass.Full;

            var concertBand = await Context.Ensemble.FirstAsync(e => e.Name == "Concert Band");
            concertBand.MemberBillingPeriodFeeDollars = 100;
            await Context.EnsembleMembership.AddAsync(new EnsembleMembership
            {
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Member = member,
                Ensemble = concertBand,
            });

            var bigBand = await Context.Ensemble.FirstAsync(e => e.Name == "Big Band");
            bigBand.MemberBillingPeriodFeeDollars = 20;
            await Context.EnsembleMembership.AddAsync(new EnsembleMembership
            {
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Member = member,
                Ensemble = bigBand,
            });
            await Context.SaveChangesAsync();

            var invoice = await GenerateMemberInvoice();

            Assert.False(invoice.IsConcession);
            Assert.Equal(3, invoice.LineItems.Count);
            AssertLineItem(invoice, "Association Fee", 4000, 0);
            AssertLineItem(invoice, "Big Band Fee", 2000, 1);
            AssertLineItem(invoice, "Concert Band Fee", 10000, 2);
        }

        [Fact]
        public async Task WillGenerateConcessionFees()
        {
            this.member.Details!.FeeClass = FeeClass.Concession;

            var concertBand = await Context.Ensemble.FirstAsync(e => e.Name == "Concert Band");
            concertBand.MemberBillingPeriodFeeDollars = 100;
            await Context.EnsembleMembership.AddAsync(new EnsembleMembership
            {
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Member = member,
                Ensemble = concertBand,
            });

            var bigBand = await Context.Ensemble.FirstAsync(e => e.Name == "Big Band");
            bigBand.MemberBillingPeriodFeeDollars = 20;
            await Context.EnsembleMembership.AddAsync(new EnsembleMembership
            {
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Member = member,
                Ensemble = bigBand,
            });
            await Context.SaveChangesAsync();

            var invoice = await GenerateMemberInvoice();

            Assert.True(invoice.IsConcession);
            Assert.Equal(3, invoice.LineItems.Count);
            // Association fee is not affected by concession
            AssertLineItem(invoice, "Association Fee", 4000, 0);
            AssertLineItem(invoice, "Big Band Fee (Concession)", 1000, 1);
            AssertLineItem(invoice, "Concert Band Fee (Concession)", 5000, 2);
        }

        [Fact]
        public async Task WillNotInvoiceForInactiveEnsembleMembership()
        {
            this.member.Details!.FeeClass = FeeClass.Full;

            var concertBand = await Context.Ensemble.FirstAsync(e => e.Name == "Concert Band");
            concertBand.MemberBillingPeriodFeeDollars = 100;
            await Context.EnsembleMembership.AddAsync(new EnsembleMembership
            {
                DateJoined = DateTime.UtcNow.AddDays(-2),
                DateLeft = DateTime.UtcNow.AddDays(-1),
                Member = member,
                Ensemble = concertBand,
            });
            await Context.SaveChangesAsync();

            var invoice = await GenerateMemberInvoice();

            Assert.False(invoice.IsConcession);
            Assert.Single(invoice.LineItems);
            AssertLineItem(invoice, "Association Fee", 4000, 0);
        }

        [Fact]
        public async Task SpecialMembershipOfAnEnsembleIsFree()
        {
            member.Details!.FeeClass = FeeClass.Full;

            var concertBand = await Context.Ensemble.FirstAsync(e => e.Name == "Concert Band");
            concertBand.MemberBillingPeriodFeeDollars = 100;
            await Context.EnsembleMembership.AddAsync(new EnsembleMembership
            {
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Member = member,
                Ensemble = concertBand,
            });

            var bigBand = await Context.Ensemble.FirstAsync(e => e.Name == "Big Band");
            bigBand.MemberBillingPeriodFeeDollars = 20;
            await Context.EnsembleMembership.AddAsync(new EnsembleMembership
            {
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Member = member,
                Ensemble = bigBand,
                Type = MembershipType.Special,
            });
            await Context.SaveChangesAsync();

            var invoice = await GenerateMemberInvoice();

            Assert.Equal(3, invoice.LineItems.Count);
            AssertLineItem(invoice, "Association Fee", 4000, 0);
            AssertLineItem(invoice, "Big Band Fee (Special)", 0, 1);
            AssertLineItem(invoice, "Concert Band Fee", 10000, 2);
        }

        [Fact]
        public async Task WillCorrectlyGenerateReference()
        {
            var invoice = await GenerateMemberInvoice();

            Assert.NotNull(invoice);
            Assert.Equal("INV-000050", invoice.Reference);
            Assert.Equal(51, this.billingConfig.NextInvoiceNumber);
        }

        private async Task<MemberInvoice> GenerateMemberInvoice()
        {
            return await this.invoiceFactory.Build(this.billingConfig, this.billingPeriod, this.member);
        }

        private static void AssertLineItem(MemberInvoice invoice, string description, int amountCents, int ordinal)
        {
            var lineItem = invoice.LineItems.FirstOrDefault(e => e.Description == description);
            Assert.NotNull(lineItem);
            Assert.Equal(amountCents, lineItem.AmountCents);
            Assert.Equal(ordinal, lineItem.Ordinal);
        }
    }
}
