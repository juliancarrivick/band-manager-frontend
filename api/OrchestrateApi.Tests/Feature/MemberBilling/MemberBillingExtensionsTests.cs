using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class MemberBillingExtensionsTests
    {
        [Theory]
        [InlineData(EmailResultStatus.Success, MemberInvoiceSendStatus.Pending)]
        [InlineData(EmailResultStatus.Error, MemberInvoiceSendStatus.Failed)]
        [InlineData((EmailResultStatus)(-1), MemberInvoiceSendStatus.Failed)]
        public void EmailResultStatusMapsCorrectly(EmailResultStatus emailResultStatus, MemberInvoiceSendStatus sendResult)
        {
            Assert.Equal(sendResult, emailResultStatus.ToSendStatus());
        }

        [Theory]
        [InlineData(EmailEventStatus.Delivered, MemberInvoiceSendStatus.Delivered)]
        [InlineData(EmailEventStatus.Opened, MemberInvoiceSendStatus.Opened)]
        [InlineData(EmailEventStatus.Failed, MemberInvoiceSendStatus.Failed)]
        [InlineData((EmailEventStatus)(-1), MemberInvoiceSendStatus.Failed)]
        public void EmailEventStatusMapsCorrectly(EmailEventStatus emailEventStatus, MemberInvoiceSendStatus sendResult)
        {
            Assert.Equal(sendResult, emailEventStatus.ToSendStatus());
        }
    }
}
