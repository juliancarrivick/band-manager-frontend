using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class MockMemberInvoiceRenderingService : IMemberInvoiceRenderingService
    {
        public MemberInvoice? LastRenderedInvoice { get; set; }

        public byte[] RenderAsPdf(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice)
        {
            LastRenderedInvoice = memberInvoice;
            return Array.Empty<byte>();
        }

        public void RenderAsPdf(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice, Stream stream)
        {
            LastRenderedInvoice = memberInvoice;
        }
    }
}
