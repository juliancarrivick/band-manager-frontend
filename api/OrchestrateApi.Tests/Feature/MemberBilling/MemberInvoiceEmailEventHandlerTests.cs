using System.Globalization;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class MemberInvoiceEmailEventHandlerTests : AutoRollbackTestHarness
    {
        private readonly MemberInvoiceEmailEventHandler handler;

        public MemberInvoiceEmailEventHandlerTests()
        {
            this.handler = new MemberInvoiceEmailEventHandler(
                Context,
                TestHelper.BuildMockLogger<MemberInvoiceEmailEventHandler>());
        }

        [Theory]
        [InlineData(null, null)]
        [InlineData(null, "abc")]
        [InlineData(null, "1")]
        [InlineData("SomeOtherType", null)]
        [InlineData("SomeOtherType", "abc")]
        [InlineData("SomeOtherType", "1")]
        [InlineData(MemberInvoiceSender.MemberInvoiceEmailType, null)]
        [InlineData(MemberInvoiceSender.MemberInvoiceEmailType, "abc")]
        public async Task WillNotProcessInvalidEvents(string? type, string? id)
        {
            var e = new MockEmailEvent(type, id);

            var processed = await this.handler.HandleAsync(new[] { e });

            Assert.Equal(0, processed);
        }

        [Theory]
        [InlineData(EmailEventStatus.Delivered, MemberInvoiceSendStatus.Delivered)]
        [InlineData(EmailEventStatus.Opened, MemberInvoiceSendStatus.Opened)]
        [InlineData(EmailEventStatus.Failed, MemberInvoiceSendStatus.Failed)]
        public async Task WillUpdateInvoiceSendResultWithCorrectValue(EmailEventStatus emailEventStatus, MemberInvoiceSendStatus sendStatus)
        {
            var invoice = await Context.MemberInvoice.FirstAsync();
            var e = new MockEmailEvent(emailEventStatus, invoice.Id);

            var processed = await this.handler.HandleAsync(new[] { e });

            Assert.Equal(1, processed);
            Assert.Equal(sendStatus, invoice.SendStatus);
        }

        [Theory]
        [InlineData("the summary", "the details", "the summary (the details)")]
        [InlineData("the summary", "", "the summary")]
        public async Task WillUpdateInvoiceSendStatusMessage(string summary, string details, string message)
        {
            var invoice = await Context.MemberInvoice.FirstAsync();
            var e = new MockEmailEvent(summary, details, invoice.Id);

            var processed = await this.handler.HandleAsync(new[] { e });

            Assert.Equal(1, processed);
            Assert.Equal(DateTime.UnixEpoch, invoice.SendStatusUpdated);
            Assert.Equal(message, invoice.SendStatusMessage);
        }

        [Fact]
        public async Task WillUpdateStatusOfInvoiceInOtherTenant()
        {
            var tenantEntry = Context.Add(new Tenant
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                Abbreviation = "test",
                TimeZone = "Australia/Perth",
            });
            MemberInvoice invoice;
            using (var tenantOverride = TenantService.ForceOverrideTenantId(tenantEntry.Entity.Id))
            {
                var memberEntry = Context.Add(new Member
                {
                    FirstName = "a",
                    LastName = "b",
                    DateJoined = DateTime.UtcNow.AddDays(-1),
                });
                var billingPeriodEntry = Context.Add(new MemberBillingPeriod
                {
                    Name = "Test Billing Period",
                });
                var invoiceEntry = Context.Add(new MemberInvoice
                {
                    Reference = "TEST-INV",
                    MemberBillingPeriod = billingPeriodEntry.Entity,
                    Member = memberEntry.Entity,
                });
                await Context.SaveChangesAsync();

                invoice = invoiceEntry.Entity;
            }

            Assert.Null(invoice.SendStatus);

            var e = new MockEmailEvent(EmailEventStatus.Delivered, invoice.Id);
            var processed = await this.handler.HandleAsync(new[] { e });

            Assert.Equal(1, processed);
            Assert.Equal(MemberInvoiceSendStatus.Delivered, invoice.SendStatus);
        }

        private sealed class MockEmailEvent : EmailEvent
        {
            public MockEmailEvent(string? type, string? id)
            {
                Summary = string.Empty;
                Metadata[EmailService.EmailTypeKey] = type;
                Metadata[MemberInvoiceSender.MemberInvoiceIdKey] = id;
            }

            public MockEmailEvent(EmailEventStatus status, int id)
            {
                Status = status;
                Summary = string.Empty;
                Metadata[EmailService.EmailTypeKey] = MemberInvoiceSender.MemberInvoiceEmailType;
                Metadata[MemberInvoiceSender.MemberInvoiceIdKey] = id.ToString(CultureInfo.InvariantCulture);
            }

            public MockEmailEvent(string summary, string details, int id)
            {
                DateTime = DateTime.UnixEpoch;
                Summary = summary;
                Details = details;
                Metadata[EmailService.EmailTypeKey] = MemberInvoiceSender.MemberInvoiceEmailType;
                Metadata[MemberInvoiceSender.MemberInvoiceIdKey] = id.ToString(CultureInfo.InvariantCulture);
            }

            public override EmailEventStatus? Status { get; } = EmailEventStatus.Delivered;

            public override string Summary { get; }
        }
    }
}
