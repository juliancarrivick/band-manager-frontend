using OrchestrateApi.Common;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class InvoiceRendererTests
    {
        private readonly Invoice invoice;
        public InvoiceRendererTests()
        {
            this.invoice = new Invoice
            {
                Title = "2021 - Semester 1",
                Subtitle = "Unit test invoice",
                InvoicedTo = "Julian Carrivick",
                Reference = "INV-0001",
                Due = DateTime.Now.AddDays(3),
                Paid = null,
                Logo = Array.Empty<byte>(),
                ContactInformation = new()
                {
                    OrganisationName = "Leeming Area Community Bands",
                    PostalAddress = "1 Place",
                    StreetAddress = "2 Place",
                    Abn = "1234",
                    Website = "https://www.lacb.org.au",
                    PhoneNo = "+61 444 444 444",
                },
                RemittanceAdvice = new()
                {
                    BankAccountName = "Leeming Area Community Bands",
                    BankAccountNo = "123 456",
                    BankBsb = "111 111",
                    BankName = "St George's Bank",
                    ContactEmail = "test@gmail.com",
                    Remarks = "Test remarks",
                },
            }
            .AddLineItem(new InvoiceLineItem
            {
                Description = "Association Fee",
                AmountCents = 2000,
            })
            .AddLineItem(new InvoiceLineItem
            {
                Description = "Concert Band Fee",
                AmountCents = 10000,
            });

            var logoFile = new EmbeddedFile(typeof(IInvoiceRenderer), "Resources.GroupLogoPlaceholder.png");
            this.invoice.Logo = logoFile.AsByteArray();
        }

        // [Fact]
        [Fact(Skip = "Manually verify")]
        public void SharpPdfRendersSuccessfully()
        {
            var outputPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            using var file = new FileStream($"{outputPath}/{typeof(SharpPdfInvoiceRenderer).Name}.test.pdf", FileMode.Create);
            new SharpPdfInvoiceRenderer().RenderToStream(this.invoice, file);
        }
    }
}
