using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class OpenStateTests : BillingPeriodStateTestHarness
    {
        public OpenStateTests() : base()
        {
            AppConfig.BillingPeriodUpcomingActionNotificationInterval = TimeSpan.FromDays(3);
        }

        protected override MemberBillingPeriod BillingPeriod { get; } = new MemberBillingPeriod
        {
            Name = "",
            State = MemberBillingPeriodState.Open,
            Due = DateTime.UtcNow.Date.AddDays(-5),
        };

        [Fact]
        public async Task DoesntTransitionIfNotClosingBillingPeriod()
        {
            await StateContext.GenerateInvoices();
            Assert.Equal(MemberBillingPeriodState.Open, BillingPeriod.State);

            await StateContext.SendInvoices();
            Assert.Equal(MemberBillingPeriodState.Open, BillingPeriod.State);

            await StateContext.PerformTemporalTasks();
            Assert.Equal(MemberBillingPeriodState.Open, BillingPeriod.State);
        }

        [Fact]
        public async Task EmailsInvoicesIfNotPaidByEarlyBirdDate()
        {
            var unsentInvoice = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = await Context.Member.Include(e => e.Details).FirstAsync(),
                Reference = "",
                Sent = null, // Won't be sent a reminder if not sent at all
                Paid = null,
            };
            await Context.MemberInvoice.AddAsync(unsentInvoice);
            var sentBeforeEarlyBirdDueInvoice = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = await Context.Member.Include(e => e.Details).Skip(1).FirstAsync(),
                Reference = "",
                Sent = DateTime.UtcNow.AddDays(-2), // Sent before early bird due
                Paid = null,
            };
            await Context.MemberInvoice.AddAsync(sentBeforeEarlyBirdDueInvoice);
            BillingPeriod.EarlyBirdDue = DateTime.UtcNow.AddDays(-1);
            BillingPeriod.Due = DateTime.UtcNow.AddDays(6);
            await Context.SaveChangesAsync();

            await StateContext.PerformTemporalTasks();
            Assert.Equal(MemberBillingPeriodState.Open, BillingPeriod.State);
            Assert.Equal(2, MockEmailService.AllSentEmails.Count);
            Assert.DoesNotContain(MockEmailService.AllSentEmails, e => e.To?.Address == unsentInvoice.Member.Details!.Email);
            Assert.Single(MockEmailService.AllSentEmails, e => e.To?.Address == sentBeforeEarlyBirdDueInvoice.Member.Details!.Email);
            Assert.Single(MockEmailService.AllSentEmails, e => e is BillingPeriodEarlyBirdEndedNotificationEmail);
        }

        [Fact]
        public async Task DoesntEmailInvoicesIfPaidByEarlyBirdOrAlreadySent()
        {
            var paidInvoice = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = await Context.Member.FirstAsync(),
                Reference = "",
                Sent = DateTime.UtcNow.AddDays(-3), // Before early bird due
                Paid = DateTime.UtcNow.AddDays(-2), // but already paid
            };
            await Context.MemberInvoice.AddAsync(paidInvoice);
            var sentAfterEarlyBirdDueDateInvoice = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = await Context.Member.Skip(1).FirstAsync(),
                Reference = "",
                Sent = DateTime.UtcNow.AddDays(-1), // sent after early bird due
                Paid = null, // but not paid
            };
            await Context.MemberInvoice.AddAsync(sentAfterEarlyBirdDueDateInvoice);
            BillingPeriod.EarlyBirdDue = DateTime.UtcNow.AddDays(-2);
            BillingPeriod.Due = DateTime.UtcNow.AddDays(5);
            BillingPeriod.NotificationContactNotified = DateTime.UtcNow;
            await Context.SaveChangesAsync();

            await StateContext.PerformTemporalTasks();
            Assert.Equal(MemberBillingPeriodState.Open, BillingPeriod.State);
            Assert.Empty(MockEmailService.AllSentEmails);
        }

        [Fact]
        public async Task DoesntEmailOverdueNoticeForPaidInvoices()
        {
            var paidInvoice = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = await Context.Member.FirstAsync(),
                Reference = "",
                Sent = DateTime.UtcNow.AddDays(-3),
                Paid = DateTime.UtcNow.AddDays(-2),
            };
            await Context.MemberInvoice.AddAsync(paidInvoice);
            BillingPeriod.EarlyBirdDue = DateTime.UtcNow.AddDays(-2);
            BillingPeriod.Due = DateTime.UtcNow.AddDays(-1);
            BillingPeriod.NotificationContactNotified = DateTime.UtcNow;
            await Context.SaveChangesAsync();

            await StateContext.PerformTemporalTasks();
            Assert.Equal(MemberBillingPeriodState.Open, BillingPeriod.State);
            Assert.Empty(MockEmailService.AllSentEmails);
        }

        [Fact]
        public async Task EmailsInvoicesOverdueNoticeIfNotPaidByDueDate()
        {
            var unsentInvoice = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = await Context.Member.Include(e => e.Details).FirstAsync(),
                Reference = "",
                Sent = null, // Won't be sent a reminder if hasn't been sent at all
                Paid = null,
            };
            await Context.MemberInvoice.AddAsync(unsentInvoice);
            var sentBeforeDueInvoice = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = await Context.Member.Include(e => e.Details).Skip(1).FirstAsync(),
                Reference = "",
                Sent = DateTime.UtcNow.AddDays(-2),
                Paid = null,
            };
            await Context.MemberInvoice.AddAsync(sentBeforeDueInvoice);
            BillingPeriod.Due = DateTime.UtcNow.AddDays(-1);
            await Context.SaveChangesAsync();

            await StateContext.PerformTemporalTasks();
            Assert.Equal(MemberBillingPeriodState.Open, BillingPeriod.State);
            Assert.Equal(2, MockEmailService.AllSentEmails.Count);
            Assert.DoesNotContain(MockEmailService.AllSentEmails, e => e.To?.Address == unsentInvoice.Member.Details!.Email);
            Assert.Single(MockEmailService.AllSentEmails, e => e.To?.Address == sentBeforeDueInvoice.Member.Details!.Email);
            Assert.Single(MockEmailService.AllSentEmails, e => e is BillingPeriodOverdueNotificationEmail);
        }

        [Theory]
        // Valid scenarios here do due 7 days after early bird due
        [InlineData(4, 11, null)]
        [InlineData(1, 8, typeof(BillingPeriodEarlyBirdEndingSoonNotificationEmail))]
        [InlineData(-1, 6, typeof(BillingPeriodEarlyBirdEndedNotificationEmail))]
        [InlineData(-5, 2, typeof(BillingPeriodDueSoonNotificationEmail))]
        [InlineData(-8, -1, typeof(BillingPeriodOverdueNotificationEmail))]
        // Valid scenarios when no early bird due date
        [InlineData(null, 11, null)]
        [InlineData(null, 2, typeof(BillingPeriodDueSoonNotificationEmail))]
        [InlineData(null, -1, typeof(BillingPeriodOverdueNotificationEmail))]
        // Nonsensical situations, nothing should occur and it should be logged
        [InlineData(-1, 1, null)] // Less than NotificationInterval between early bird and due dates
        // Early bird due after due date
        [InlineData(-1, -3, null)] // Both in the past
        [InlineData(1, -1, typeof(BillingPeriodEarlyBirdEndingSoonNotificationEmail))] // EB in future but due in past
        [InlineData(5, 1, null)] // Both in the future
        public async Task NotificationEmailsGetSentWhenConditionsAreCorrect(int? earlyBirdDueInDays, int dueInDays, Type? emailType)
        {
            var now = DateTime.UtcNow;
            BillingPeriod.EarlyBirdDue = earlyBirdDueInDays.HasValue
                ? now.AddDays(earlyBirdDueInDays.Value)
                : (DateTime?)null;
            BillingPeriod.Due = now.AddDays(dueInDays);

            await StateContext.PerformTemporalTasks();

            if (emailType == null)
            {
                Assert.DoesNotContain(MockEmailService.AllSentEmails, e => e is BillingPeriodNotificationEmail);
            }
            else
            {
                Assert.Single(MockEmailService.AllSentEmails, e => e.GetType() == emailType);
            }
        }

        [Theory]
        [InlineData(true, -5, StateTransitionResult.Successful, MemberBillingPeriodState.Closed)]
        [InlineData(true, 5, StateTransitionResult.Successful, MemberBillingPeriodState.Closed)]
        [InlineData(false, -5, StateTransitionResult.Successful, MemberBillingPeriodState.Closed)]
        [InlineData(false, 5, StateTransitionResult.NotAllowed, MemberBillingPeriodState.Open)]
        public async Task DoesntAllowCloseIfNotDueYetAndAnyUnpaidInvoices(bool isPaid, int daysUntilDue, StateTransitionResult expectedResult, MemberBillingPeriodState expectedState)
        {
            var member = await Context.Member.FirstAsync();
            var invoice = new MemberInvoice
            {
                MemberBillingPeriod = BillingPeriod,
                Member = member,
                Reference = "",
                Paid = isPaid ? DateTime.UtcNow : (DateTime?)null,
            };
            await Context.MemberInvoice.AddAsync(invoice);
            BillingPeriod.Due = DateTime.UtcNow.AddDays(daysUntilDue);
            await Context.SaveChangesAsync();

            var closeResult = await StateContext.CloseBillingPeriod();
            Assert.Equal(expectedResult, closeResult);
            Assert.Equal(expectedState, BillingPeriod.State);
        }
    }
}
