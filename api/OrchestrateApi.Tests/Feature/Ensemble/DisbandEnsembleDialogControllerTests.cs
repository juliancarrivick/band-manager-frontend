using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Ensembles;

namespace OrchestrateApi.Tests.Feature.Ensembles;

public class DisbandEnsembleDialogControllerTests : AutoRollbackTestHarness
{
    private readonly DisbandEnsembleDialogController controller;
    public DisbandEnsembleDialogControllerTests()
    {
        this.controller = new(Context);
    }

    [Fact]
    public async Task NotFoundForUnknownEnsemble()
    {
        var impactResponse = await this.controller.GetImpact(-1);
        Assert.Equal(StatusCodes.Status404NotFound, impactResponse.GetStatusCode());

        var disbandResponse = await this.controller.DisbandEnsemble(-1);
        Assert.Equal(StatusCodes.Status404NotFound, disbandResponse.GetStatusCode());
    }

    [Fact]
    public async Task AlreadyDisbandedEnsembleIsRespondedToCorrectly()
    {
        var entry = Context.Add(new Ensemble { Name = "New", Status = EnsembleStatus.Disbanded });
        await Context.SaveChangesAsync();

        var impactResponse = await this.controller.GetImpact(entry.Entity.Id);
        Assert.Equal(StatusCodes.Status400BadRequest, impactResponse.GetStatusCode());

        var disbandResponse = await this.controller.DisbandEnsemble(entry.Entity.Id);
        Assert.Equal(StatusCodes.Status200OK, disbandResponse.GetStatusCode());
    }

    [Fact]
    public async Task EnsembleMembershipsAreEnded()
    {
        var ensemble = new Ensemble { Name = "Ensemble", Status = EnsembleStatus.Active };
        var membership = new EnsembleMembership
        {
            Ensemble = ensemble,
            Member = await Context.Member.FirstAsync(),
            DateJoined = DateTime.UtcNow.AddDays(-1),
        };
        var membership2 = new EnsembleMembership
        {
            Ensemble = ensemble,
            Member = await Context.Member.Skip(1).FirstAsync(),
            DateJoined = DateTime.UtcNow.AddDays(-2),
            DateLeft = DateTime.UtcNow.AddDays(-1),
        };
        ensemble.EnsembleMembership.Add(membership);
        ensemble.EnsembleMembership.Add(membership2);
        Context.Add(ensemble);
        await Context.SaveChangesAsync();

        var impactResponse = await this.controller.GetImpact(ensemble.Id);
        Assert.NotNull(impactResponse.Value);
        var endedName = Assert.Single(impactResponse.Value.MembershipEndedMemberNames);
        Assert.Equal($"{membership.Member.FirstName} {membership.Member.LastName}", endedName);

        var disbandResponse = await this.controller.DisbandEnsemble(ensemble.Id);
        Assert.Equal(StatusCodes.Status200OK, disbandResponse.GetStatusCode());
        Assert.Equal(EnsembleStatus.Disbanded, ensemble.Status);
        Assert.NotNull(membership.DateLeft);
        Assert.Equal(DateTime.UtcNow, membership.DateLeft.Value, TimeSpan.FromSeconds(1));
    }

    [Fact]
    public async Task MailingListReferencesAreRemoved()
    {
        var ensemble = new Ensemble { Name = "Ensemble" };
        var mailingListEntry = Context.Add(new MailingList { Name = "ML", Slug = "ml" });
        mailingListEntry.Entity.Ensembles.Add(ensemble);
        mailingListEntry.Entity.Ensembles.Add(new Ensemble { Name = "Ensemble2" });
        await Context.SaveChangesAsync();

        var impactResponse = await this.controller.GetImpact(ensemble.Id);
        Assert.NotNull(impactResponse.Value);
        var removedName = Assert.Single(impactResponse.Value.RemovedFromMailingLists);
        Assert.Equal(mailingListEntry.Entity.Name, removedName);

        var disbandResponse = await this.controller.DisbandEnsemble(ensemble.Id);
        Assert.Equal(StatusCodes.Status200OK, disbandResponse.GetStatusCode());
        Assert.Equal(EnsembleStatus.Disbanded, ensemble.Status);
        var mlEnsemble = Assert.Single(mailingListEntry.Entity.Ensembles);
        Assert.Equal("Ensemble2", mlEnsemble.Name);
    }
}
