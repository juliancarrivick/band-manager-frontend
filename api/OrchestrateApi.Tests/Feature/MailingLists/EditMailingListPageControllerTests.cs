using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.MailingLists;

namespace OrchestrateApi.Tests.Feature.MailingLists;

public class EditMailingListPageControllerTests : FullIntegrationTestHarness
{
    private readonly EditMailingListPageController controller;
    private readonly MockPostmarkApiService mockApiService;

    public EditMailingListPageControllerTests()
    {
        this.controller = ConstructController<EditMailingListPageController>();
        this.mockApiService = GetServiceAs<IPostmarkApiService, MockPostmarkApiService>();
    }

    [Fact]
    public async Task NotFoundForUnknownMailingList()
    {
        var dataResult = await this.controller.GetPageData(-1);
        Assert.Equal(StatusCodes.Status404NotFound, dataResult.GetStatusCode());

        var updateResult = await this.controller.UpdateMailingList(-1, new());
        Assert.Equal(StatusCodes.Status404NotFound, updateResult.GetStatusCode());
    }

    [Fact]
    public async Task GetPageDataReturnsCorrectInformation()
    {
        var mailingList = new MailingList { Name = "ML", Slug = "ml" };
        var ensemble = new Ensemble { Name = "Ensemble" };
        ensemble.EnsembleMembership.Add(new()
        {
            Member = new() { FirstName = "First", LastName = "Ensemble" }
        });
        mailingList.Ensembles.Add(ensemble);
        mailingList.Members.Add(new Member { FirstName = "First", LastName = "Member" });
        mailingList.Contacts.Add(new Contact { Name = "Contact" });
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        var result = await this.controller.GetPageData(mailingList.Id);
        Assert.Equal(StatusCodes.Status200OK, result.GetStatusCode());

        var data = result.Value!;
        Assert.Equal(mailingList.Ensembles.Select(e => e.Id), data.Recipients.EnsembleIds);
        Assert.Equal(mailingList.Members.Select(e => e.Id), data.Recipients.Members.Select(e => e.Id));
        Assert.Equal(mailingList.Contacts.Select(e => e.Id), data.Recipients.Contacts.Select(e => e.Id));
        Assert.Equal(OrchestrateSeed.GroupAbbr, data.ReferenceData.AddressParameters.TenantSlug);

        Assert.True(data.ReferenceData.Ensembles.Count > 0);
        var ensembleDto = Assert.Single(data.ReferenceData.Ensembles, e => e.Name == ensemble.Name);
        Assert.Equal(1, ensembleDto.NumRecipients);

        Assert.True(data.ReferenceData.Members.Count > 0);
        Assert.Single(data.ReferenceData.Members, e => e.Name.Contains("Ensemble"));
        Assert.Single(data.ReferenceData.Members, e => e.Name.Contains("Member"));

        Assert.True(data.ReferenceData.Contacts.Count > 0);
        Assert.Single(data.ReferenceData.Contacts, e => e.Name == "Contact");
    }

    [Fact]
    public async Task UpdateMailingList()
    {
        var mailingList = new MailingList { Name = "ML", Slug = "ml" };
        mailingList.Ensembles.Add(new Ensemble { Name = "Original Ensemble" });
        mailingList.Members.Add(new Member { FirstName = "Original", LastName = "Member" });
        mailingList.Contacts.Add(new Contact { Name = "Original Contact" });
        Context.Add(mailingList);
        var newEnsembleEntry = Context.Add(new Ensemble { Name = "New Ensemble" });
        var newMemberEntry = Context.Add(new Member { FirstName = "New", LastName = "Member" });
        var newContactEntry = Context.Add(new Contact { Name = "New Contact" });
        await Context.SaveChangesAsync();

        var result = await this.controller.UpdateMailingList(mailingList.Id, new UpdateMailingListAndRecipientsDto
        {
            MailingList = new UpdateMailingListDto { Name = "Mailing List", Slug = "mailing-list" },
            EnsembleIds = new int[] { newEnsembleEntry.Entity.Id },
            MemberIds = new int[] { newMemberEntry.Entity.Id },
            ContactIds = new int[] { newContactEntry.Entity.Id },
        });
        Assert.Equal(StatusCodes.Status200OK, result.GetStatusCode());

        Assert.Equal("Mailing List", mailingList.Name);
        Assert.Equal("mailing-list", mailingList.Slug);
        var ensemble = Assert.Single(mailingList.Ensembles);
        Assert.Equal("New Ensemble", ensemble.Name);
        var member = Assert.Single(mailingList.Members);
        Assert.Equal("New", member.FirstName);
        var contact = Assert.Single(mailingList.Contacts);
        Assert.Equal("New Contact", contact.Name);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public async Task OnlyRemovableSuppressionsAreRemoved(bool isRemovable)
    {
        var tenant = await Context.GetTenantAsync();
        Assert.NotNull(tenant);
        tenant.PostmarkServerToken = "token";

        var mailingList = new MailingList { Name = "ML", Slug = "ml", PostmarkStreamId = "id" };
        mailingList.Suppressions.Add(new MailingListSuppression
        {
            Email = "test@email.com",
            Reason = "Test reason",
            IsRemovable = isRemovable,
        });
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        var result = await this.controller.UpdateMailingList(mailingList.Id, new UpdateMailingListAndRecipientsDto
        {
            RemovedSuppressions = new List<string> { "test@email.com" },
        });
        Assert.Equal(StatusCodes.Status200OK, result.GetStatusCode());

        if (isRemovable)
        {
            Assert.Empty(mailingList.Suppressions);
            Assert.NotNull(mockApiService.MockAdapter.LastSuppressionDeletions);
            var lastDeleted = Assert.Single(mockApiService.MockAdapter.LastSuppressionDeletions);
            Assert.Equal("test@email.com", lastDeleted.EmailAddress);
        }
        else
        {
            var suppression = Assert.Single(mailingList.Suppressions);
            Assert.Equal("test@email.com", suppression.Email);
            Assert.Null(mockApiService.MockAdapter.LastSuppressionDeletions);
        }
    }

    [Fact]
    public async Task BuildRecipientListMapsDataCorrectly()
    {
        var mailingList = new MailingList { Name = "ML", Slug = "ml" };
        var ensemble = new Ensemble { Name = "Ensemble" };
        var dupMember = CreateMember("Dup1", "dup@email.com");
        ensemble.EnsembleMembership.Add(new() { Member = CreateMember("Ensemble", "ensemble@email.com") });
        ensemble.EnsembleMembership.Add(new() { Member = dupMember });
        mailingList.Ensembles.Add(ensemble);
        mailingList.Members.Add(CreateMember("Member", "member@email.com"));
        mailingList.Members.Add(dupMember);
        mailingList.Contacts.Add(new Contact { Name = "Contact", Email = "contact@email.com" });
        mailingList.Contacts.Add(new Contact { Name = "Dup2", Email = "dup@email.com" });
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        var recipientList = await this.controller.BuildRecipientList(new()
        {
            EnsembleIds = mailingList.Ensembles.Select(e => e.Id).ToList(),
            MemberIds = mailingList.Members.Select(e => e.Id).ToList(),
            ContactIds = mailingList.Contacts.Select(e => e.Id).ToList(),
            Suppressions = [],
        });

        Assert.All(recipientList, r => Assert.Null(r.DropReason));

        var ensembleRecp = Assert.Single(recipientList, r => r.Name.Contains("Ensemble"));
        Assert.Equal("ensemble@email.com", ensembleRecp.Email);
        Assert.Equal(RecipientSource.Active, ensembleRecp.Ensemble);
        Assert.Equal(RecipientSource.NotApplicable, ensembleRecp.Member);
        Assert.Equal(RecipientSource.NotApplicable, ensembleRecp.Contact);

        var memberRecp = Assert.Single(recipientList, r => r.Name.Contains("Member"));
        Assert.Equal("member@email.com", memberRecp.Email);
        Assert.Equal(RecipientSource.NotApplicable, memberRecp.Ensemble);
        Assert.Equal(RecipientSource.Active, memberRecp.Member);
        Assert.Equal(RecipientSource.NotApplicable, memberRecp.Contact);

        var contactRecp = Assert.Single(recipientList, r => r.Name.Contains("Contact"));
        Assert.Equal("contact@email.com", contactRecp.Email);
        Assert.Equal(RecipientSource.NotApplicable, contactRecp.Ensemble);
        Assert.Equal(RecipientSource.NotApplicable, contactRecp.Member);
        Assert.Equal(RecipientSource.Active, contactRecp.Contact);

        var dupRecp = Assert.Single(recipientList, r => r.Name.Contains("Dup"));
        Assert.Equal("dup@email.com", dupRecp.Email);
        Assert.Equal(RecipientSource.Active, dupRecp.Ensemble);
        Assert.Equal(RecipientSource.Active, dupRecp.Member);
        Assert.Equal(RecipientSource.Active, dupRecp.Contact);
    }

    [Fact]
    public async Task InactiveRecipientsHaveAppropriateDropReason()
    {
        var mailingList = new MailingList { Name = "ML", Slug = "ml" };
        var ensemble = new Ensemble { Name = "Ensemble", Status = EnsembleStatus.Disbanded };
        var inactiveMember = CreateMember("Inactive", "inactive@email.com");
        inactiveMember.DateLeft = DateTime.UtcNow.AddDays(-1);
        ensemble.EnsembleMembership.Add(new() { Member = CreateMember("Ensemble", "ensemble@email.com") });
        mailingList.Ensembles.Add(ensemble);
        mailingList.Members.Add(inactiveMember);
        mailingList.Contacts.Add(new Contact { Name = "Archived", ArchivedOn = DateTime.UtcNow.AddDays(-1) });
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        var recipientList = await this.controller.BuildRecipientList(new()
        {
            EnsembleIds = mailingList.Ensembles.Select(e => e.Id).ToList(),
            MemberIds = mailingList.Members.Select(e => e.Id).ToList(),
            ContactIds = mailingList.Contacts.Select(e => e.Id).ToList(),
            Suppressions = []
        });

        var ensembleRecp = Assert.Single(recipientList, r => r.Name.Contains("Ensemble"));
        Assert.Equal("ensemble@email.com", ensembleRecp.Email);
        Assert.Equal(RecipientSource.Inactive, ensembleRecp.Ensemble);
        Assert.Equal(RecipientSource.NotApplicable, ensembleRecp.Member);
        Assert.Equal(RecipientSource.NotApplicable, ensembleRecp.Contact);
        Assert.Contains("member of a disbanded ensemble", ensembleRecp.DropReason);

        var memberRecp = Assert.Single(recipientList, r => r.Name.Contains("Inactive"));
        Assert.Equal("inactive@email.com", memberRecp.Email);
        Assert.Equal(RecipientSource.NotApplicable, memberRecp.Ensemble);
        Assert.Equal(RecipientSource.Inactive, memberRecp.Member);
        Assert.Equal(RecipientSource.NotApplicable, memberRecp.Contact);
        Assert.Contains("inactive member", memberRecp.DropReason);

        var contactRecp = Assert.Single(recipientList, r => r.Name.Contains("Archived"));
        Assert.Null(contactRecp.Email);
        Assert.Equal(RecipientSource.NotApplicable, contactRecp.Ensemble);
        Assert.Equal(RecipientSource.NotApplicable, contactRecp.Member);
        Assert.Equal(RecipientSource.Inactive, contactRecp.Contact);
        Assert.Contains("don't have a valid email", contactRecp.DropReason);
        Assert.Contains("archived contact", contactRecp.DropReason);
    }
    [Fact]
    public async Task SuppressedRecipientsHaveAppropriateDropReason()
    {
        var mailingList = new MailingList { Name = "ML", Slug = "ml" };
        mailingList.Contacts.Add(new Contact { Name = "Suppressed", Email = "suppressed@email.com" });
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        var recipientList = await this.controller.BuildRecipientList(new()
        {
            EnsembleIds = [],
            MemberIds = [],
            ContactIds = mailingList.Contacts.Select(e => e.Id).ToList(),
            Suppressions = ["suppressed@email.com"]
        });

        var suppressRecp = Assert.Single(recipientList, r => r.Name.Contains("Suppressed"));
        Assert.Equal("suppressed@email.com", suppressRecp.Email);
        Assert.Equal(RecipientSource.NotApplicable, suppressRecp.Ensemble);
        Assert.Equal(RecipientSource.NotApplicable, suppressRecp.Member);
        Assert.Equal(RecipientSource.Active, suppressRecp.Contact);
        Assert.Contains("unsubscribed from emails", suppressRecp.DropReason);
    }

    [Fact]
    public async Task DeleteUnknownReturnsOk()
    {
        var result = await this.controller.DeleteMailingList(-1);
        Assert.Equal(StatusCodes.Status200OK, result.GetStatusCode());
    }

    [Fact]
    public async Task DeleteActsAccordingly()
    {
        var tenant = await Context.GetTenantAsync();
        Assert.NotNull(tenant);
        tenant.PostmarkServerToken = "abc";

        var entry = Context.Add(new MailingList { Name = "ML", Slug = "ml", PostmarkStreamId = "ml" });
        await Context.SaveChangesAsync();

        var result = await this.controller.DeleteMailingList(entry.Entity.Id);
        Assert.Equal(StatusCodes.Status200OK, result.GetStatusCode());
        Assert.Null(await Context.MailingList.SingleOrDefaultAsync(e => e.Id == entry.Entity.Id));
        Assert.Equal("ml", mockApiService.MockAdapter.LastArchival);
    }

    private static Member CreateMember(string firstName, string email) => new Member
    {
        FirstName = firstName,
        LastName = "LastName",
        Details = new() { Email = email, PhoneNo = ".", Address = "." },
    };
}
