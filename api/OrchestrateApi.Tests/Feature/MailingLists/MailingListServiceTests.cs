using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MailingLists;

namespace OrchestrateApi.Tests.Feature.MailingLists;

public class MailingListServiceTests
{
    private const string ApexDomain = "test.com.au";
    private readonly MailingListService service;

    public MailingListServiceTests()
    {
        this.service = new MailingListService(
            new MockAppConfiguration { MailingListApexDomain = ApexDomain },
            TestHelper.BuildMockLogger<MailingListService>());
    }

    [Fact]
    public void EmailAddressesAreGeneratedCorrectly()
    {
        Assert.Equal(ApexDomain, this.service.ApexDomain);

        var tenant = new Tenant { Abbreviation = "test" };
        Assert.Equal(
            new MailingListAddressParameters("test", ApexDomain),
            this.service.AddressParameters(tenant));

        var mailingList = new MailingList { Name = "Mailing", Slug = "mailing" };
        Assert.Equal(
            $"test#mailing@{ApexDomain}",
            this.service.MailingListEmail(tenant, mailingList));

        var expr = this.service.MailingListEmailExpr(tenant).Compile();
        Assert.Equal($"test#mailing@{ApexDomain}", expr(mailingList));
    }

    [Theory]
    [InlineData(ApexDomain, true)]
    [InlineData("other.domain.com", false)]
    public void ParsedIfApexDomainIsCorrect(string apexDomain, bool parses)
    {
        var succeeded = this.service.TryParseTenantAndSlug(
            $"abbr#slug@{apexDomain}",
            out var tenantAbbr,
            out var slug);
        Assert.Equal(parses, succeeded);
        if (parses)
        {
            Assert.Equal("abbr", tenantAbbr);
            Assert.Equal("slug", slug);
        }
    }

    [Theory]
    [InlineData("notanemail")]
    [InlineData("slug@unknown-domain.com")]
    [InlineData("tenant#slug@unknown-domain.com")]
    public void InvalidEmailsAndUnknownDomainsAreNotParsed(string email)
    {
        Assert.False(this.service.TryParseTenantAndSlug(email, out var abbr, out var slugs));
    }

    [Theory]
    [InlineData("tenant", false, "", "")]
    [InlineData("#tenant", false, "", "tenant")]
    [InlineData("tenant#", false, "tenant", "")]
    [InlineData("#tenant#", false, "", "tenant#")]
    [InlineData("tenant##slug", true, "tenant", "#slug")]
    [InlineData("tenant#slug", true, "tenant", "slug")]
    [InlineData("tenant#slug+discarded", true, "tenant", "slug")]
    [InlineData("tenant#+discarded", false, "tenant", "")]
    public void TryParseWorksAsExpected(
        string username,
        bool shouldSucceed,
        string expectedTenantAbbr,
        string expectedSlug)
    {
        var email = $"{username}@{ApexDomain}";

        Assert.Equal(shouldSucceed, this.service.TryParseTenantAndSlug(email, out var tenantAbbr, out var slug));
        Assert.Equal(expectedTenantAbbr, tenantAbbr);
        Assert.Equal(expectedSlug, slug);
    }
}
