using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MailingLists;

namespace OrchestrateApi.Tests.Feature.MailingLists;

public class MailingListsPageControllerTests : FullIntegrationTestHarness
{
    private readonly MailingListsPageController controller;

    public MailingListsPageControllerTests()
    {
        this.controller = ConstructController<MailingListsPageController>();
    }

    [Fact]
    public async Task CorrectSummaryIsGenerated()
    {
        var mailingList = new MailingList { Name = "ML", Slug = "ml" };
        mailingList.Contacts.Add(new Contact { Name = "contact" });
        mailingList.Messages.Add(SendMetadata(MailingListsPageController.UsageCutOffDaysAgo + 1));
        mailingList.Messages.Add(SendMetadata(12));
        mailingList.Messages.Add(SendMetadata(4.9));
        mailingList.Messages.Add(SendMetadata(5.0));
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        var summaryResult = await this.controller.GetSummary();
        Assert.Equal(StatusCodes.Status200OK, summaryResult.GetStatusCode());
        var summary = summaryResult.Value!;
        Assert.Equal(MailingListsPageController.UsageCutOffDaysAgo, summary.UsageCutOffDaysAgo);

        var mailingListSummary = Assert.Single(summary.MailingLists);
        Assert.Equal($"{OrchestrateSeed.GroupAbbr}#{mailingList.Slug}@test.domain.com", mailingListSummary.Email);
        Assert.Equal(SeedTimeZoneDaysAgoInUtc(4.9), mailingListSummary.LastUsed!.Value, TimeSpan.FromSeconds(1));
        Assert.Equal(1, mailingListSummary.RecipientCount);

        var usageSummary = summary.UsageHistory;
        Assert.Equal(2, usageSummary.Count);
        Assert.All(usageSummary, e => Assert.Equal(mailingList.Id, e.MailingListId));
        Assert.All(usageSummary, e => Assert.Equal(mailingList.Name, e.MailingListName));
        Assert.Equal([1, 2], usageSummary.Select(e => e.SendCount).ToHashSet());
    }

    [Fact]
    public async Task LastUsedNullIfNoHistory()
    {
        var mailingList = new MailingList { Name = "ML", Slug = "ml" };
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        var summaryResult = await this.controller.GetSummary();
        var mailingListSummary = Assert.Single(summaryResult.Value!.MailingLists);
        Assert.Null(mailingListSummary.LastUsed);
    }

    private static MailingListMessageMetadata SendMetadata(double daysAgo) => new()
    {
        Sent = SeedTimeZoneDaysAgoInUtc(daysAgo),
    };

    private static DateTime SeedTimeZoneDaysAgoInUtc(double daysAgo) => DateTime.UtcNow
        .InTimeZone(OrchestrateSeed.TimeZone)
        .Date.AddDays(-daysAgo)
        .ToUtcFromTimeZone(OrchestrateSeed.TimeZone);
}
