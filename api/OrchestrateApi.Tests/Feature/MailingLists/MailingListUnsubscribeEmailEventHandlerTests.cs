using System.Globalization;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.MailingLists;
using static OrchestrateApi.Feature.MailingLists.MailingListIncomingEmailHandler;

namespace OrchestrateApi.Tests.Feature.MailingLists;

public class MailingListUnsubscribeEmailEventHandlerTests : AutoRollbackTestHarness
{
    private readonly MailingListUnsubscribeEmailEventHandler handler;

    public MailingListUnsubscribeEmailEventHandlerTests()
    {
        this.handler = new(
            TenantService,
            Context,
            TestHelper.BuildMockLogger<MailingListUnsubscribeEmailEventHandler>());
    }

    [Fact]
    public async Task UnknownEventTypesAreDropped()
    {
        Assert.Equal(0, await HandleAsync(
            new UnknownEmailEvent(),
            new SubscriptionChangeEmailEvent()
            {
                IsSubscribed = false,
                IsChangable = false,
                Metadata =
                {
                    [EmailService.EmailTypeKey] = "Unknown",
                },
            }
        ));
    }

    [Theory]
    [MemberData(nameof(InvalidTenantIdMemberIdCombinations))]
    public async Task InvalidMailingListIdsAreDropped(string? tenantId, string? mailingListId)
    {
        Assert.Equal(0, await HandleAsync(new SubscriptionChangeEmailEvent()
        {
            IsSubscribed = false,
            IsChangable = false,
            Metadata = BuildSubscriptionChangeMetadata(tenantId, mailingListId),
        }));
    }

    public static TheoryData<string?, string?> InvalidTenantIdMemberIdCombinations()
    {
        var theoryData = new TheoryData<string?, string?>();
        foreach (var tenantId in new[] { null, "abc", Guid.NewGuid().ToString() })
        {
            foreach (var memberId in new[] { null, "", "notInt", "-1" })
            {
                theoryData.Add(tenantId, memberId);
            }
        }
        return theoryData;
    }

    [Fact]
    public async Task SuppressionIsRemovedIfIsSubscribed()
    {
        var entry = Context.Add(new MailingList
        {
            Name = "ML",
            Slug = "ml",
            Suppressions =
            {
                new() {Email = "suppressed@email.com", Reason = ""},
            },
        });
        await Context.SaveChangesAsync();

        Assert.Equal(1, await HandleAsync(new SubscriptionChangeEmailEvent
        {
            Recipient = "suppressed@email.com",
            IsSubscribed = true,
            IsChangable = true,
            Metadata = BuildSubscriptionChangeMetadata(entry.Entity.Id),
        }));

        Assert.Empty(entry.Entity.Suppressions);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public async Task SuppressionIsAddedIfNotSubscribed(bool isRemovable)
    {
        var entry = Context.Add(new MailingList { Name = "ML", Slug = "ml" });
        await Context.SaveChangesAsync();

        var now = DateTime.UtcNow;
        Assert.Equal(1, await HandleAsync(new SubscriptionChangeEmailEvent
        {
            IsSubscribed = false,
            IsChangable = isRemovable,
            Recipient = "suppressed@email.com",
            DateTime = now,
            Details = "Because",
            Metadata = BuildSubscriptionChangeMetadata(entry.Entity.Id),
        }));

        var suppression = Assert.Single(entry.Entity.Suppressions);
        Assert.Equal("suppressed@email.com", suppression.Email);
        Assert.Equal(now, suppression.AddedAt);
        Assert.Equal("Because", suppression.Reason);
        Assert.Equal(isRemovable, suppression.IsRemovable);
    }

    private async Task<int> HandleAsync(params EmailEvent[] events)
    {
        // Handler won't run in a tenant context so reset this for the tests
        using var tenantIdOverride = TenantService.ForceOverrideTenantId(null);
        return await this.handler.HandleAsync(events);
    }

    private static Dictionary<string, string?> BuildSubscriptionChangeMetadata(int? mailingListId)
        => BuildSubscriptionChangeMetadata(mailingListId?.ToString(CultureInfo.InvariantCulture));

    private static Dictionary<string, string?> BuildSubscriptionChangeMetadata(string? mailingListId)
        => BuildSubscriptionChangeMetadata(OrchestrateSeed.TenantId.ToString(), mailingListId);

    private static Dictionary<string, string?> BuildSubscriptionChangeMetadata(string? tenantId, string? mailingListId)
    {
        var metadata = new Dictionary<string, string?>
        {
            [EmailService.EmailTypeKey] = MailingListEmailType,
        };

        if (tenantId is not null)
        {
            metadata[EmailService.TenantIdKey] = tenantId;
        }

        if (mailingListId is not null)
        {
            metadata[MailingListIdMetadataKey] = mailingListId;
        }

        return metadata;
    }
}
