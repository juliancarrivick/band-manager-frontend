using FluentValidation;
using OrchestrateApi.Feature.MailingLists;

namespace OrchestrateApi.Tests.Feature.MailingLists;

public class GeneratedEmailValidatorTests : AutoRollbackTestHarness
{
    private readonly GeneratedEmailValidator<Dto> validator;
    private readonly MailingListService mailingListService;

    public GeneratedEmailValidatorTests()
    {
        this.mailingListService = new MailingListService(
                new MockAppConfiguration { MailingListApexDomain = "test.domain" },
                TestHelper.BuildMockLogger<MailingListService>());
        this.validator = new GeneratedEmailValidator<Dto>(
            this.mailingListService,
            Context);
    }

    [Theory]
    [InlineData(-1, true)]
    [InlineData(0, true)]
    [InlineData(1, false)]
    public async Task OffsetsFromMaxLengthAreValidatedCorrectly(int offsetLength, bool isValid)
    {
        var emptyAddr = this.mailingListService.MailingListEmail((await Context.GetTenantAsync())!, "");
        var lengthOverhead = emptyAddr.Length;
        var generatedLength = GeneratedEmailValidator<Dto>.MaxEmailLength - lengthOverhead + offsetLength;

        var dto = new Dto { Email = new string('e', generatedLength) };
        var validator = new DtoValidator();
        validator.RuleFor(e => e.Email).SetAsyncValidator(this.validator);
        Assert.Equal(isValid, (await validator.ValidateAsync(dto)).IsValid);

    }

    private sealed class DtoValidator : AbstractValidator<Dto>
    {
    }

    private sealed class Dto
    {
        public required string Email { get; set; }
    }
}
