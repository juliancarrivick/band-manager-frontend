using System.Diagnostics.CodeAnalysis;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MailingLists;

namespace OrchestrateApi.Tests.Feature.MailingLists;

[SuppressMessage("Specify IFormatProvider", "CA1305")]
public class RecipientListServiceTests : AutoRollbackTestHarness
{
    private readonly RecipientListService service;

    public RecipientListServiceTests()
    {
        this.service = new RecipientListService(Context);
    }

    [Fact]
    public async Task OnlyActiveEnsembleMembersAreIncluded()
    {
        var entry = Context.Add(CreateEnsemble(activeCount: 3, inactiveCount: 5));
        await Context.SaveChangesAsync();

        var recipients = await this.service.GetEnsembleRecipients(new[] { entry.Entity.Id });
        Assert.Equal(3, recipients.Count);
        Assert.All(recipients, r => Assert.False(string.IsNullOrWhiteSpace(r.Name)));
        Assert.All(recipients, r => Assert.False(string.IsNullOrWhiteSpace(r.Email)));
        Assert.All(recipients, r => Assert.True(r.IsActive));
    }

    [Fact]
    public async Task DisbandedEnsembleMembersAreInactive()
    {
        var ensemble = CreateEnsemble(activeCount: 3, inactiveCount: 5);
        ensemble.Status = EnsembleStatus.Disbanded;
        var entry = Context.Add(ensemble);
        await Context.SaveChangesAsync();

        var recipients = await this.service.GetEnsembleRecipients(new[] { ensemble.Id });
        Assert.Equal(3, recipients.Count);
        Assert.All(recipients, r => Assert.False(r.IsActive));
    }

    [Fact]
    public async Task InactiveMembersAreMarked()
    {
        var activeEntry = Context.Add(CreateMember("Active", "Member", "active@email.com"));
        var inactiveEntry = Context.Add(
            CreateMember("Inactive", "Member", "inactive@email.com", joinedDaysAgo: 2, leftDaysAgo: 1));
        await Context.SaveChangesAsync();

        var recipients = await this.service.GetMemberRecipients(new[] { activeEntry.Entity.Id, inactiveEntry.Entity.Id });
        Assert.Equal(2, recipients.Count);
        Assert.Equal(1, recipients.Count(r => r.Email == "active@email.com" && r.IsActive));
        Assert.Equal(1, recipients.Count(r => r.Email == "inactive@email.com" && !r.IsActive));
    }

    [Fact]
    public async Task ArchivedContactsAreMarked()
    {
        var activeEntry = Context.Add(CreateContact("Active", "active@email.com"));
        var inactiveEntry = Context.Add(CreateContact("Inactive", "inactive@email.com", 2));
        await Context.SaveChangesAsync();

        var recipients = await this.service.GetContactRecipients(new[] { activeEntry.Entity.Id, inactiveEntry.Entity.Id });
        Assert.Equal(2, recipients.Count);
        Assert.Single(recipients, r => r.Email == "active@email.com" && r.IsActive);
        Assert.Single(recipients, r => r.Email == "inactive@email.com" && !r.IsActive);
    }

    [Fact]
    public async Task GetDeliverableRecipientListFiltersOutMissingEmails()
    {
        var entry = Context.Add(CreateMailingList(
            members: new[]{
                CreateMember("Member", "Has", "member@email.com"),
                CreateMember("Member", "Hasn't", ""),
            },
            contacts: new[]{
                CreateContact("Contact Has", "contact@email.com"),
                CreateContact("Contact Hasn't", null),
            }
        ));
        await Context.SaveChangesAsync();

        var recipients = await this.service.GetDeliverableRecipientListAsync(entry.Entity);
        Assert.Equal(2, recipients.Count);
        Assert.Single(recipients, r => r.Name == "Member Has" && r.Email == "member@email.com");
        Assert.Single(recipients, r => r.Name == "Contact Has" && r.Email == "contact@email.com");
    }

    [Fact]
    public async Task GetDeliverableRecipientListFiltersOutInactiveRecipients()
    {
        var entry = Context.Add(CreateMailingList(
            members: new[]{
                CreateMember("Active", "Member", "active@member.com"),
                CreateMember("Inactive", "Member", "inactive@member.com", joinedDaysAgo: 2, leftDaysAgo: 1),
            },
            contacts: new[]{
                CreateContact("Active Contact", "active@contact.com"),
                CreateContact("Inactive Contact", "inactive@contact.com", archivedDaysAgo: 1),
            }
        ));
        await Context.SaveChangesAsync();

        var recipients = await this.service.GetDeliverableRecipientListAsync(entry.Entity);
        Assert.Equal(2, recipients.Count);
        Assert.Single(recipients, r => r.Name == "Active Member" && r.Email == "active@member.com");
        Assert.Single(recipients, r => r.Name == "Active Contact" && r.Email == "active@contact.com");
    }

    [Fact]
    public async Task GetDeliverableRecipientListDeduplicatesEmails()
    {
        var activeMember = CreateMember("Active", "Member", "active@member.com");
        var ensembleMember = CreateMember("Ensemble", "Member", "ensemble@member.com");
        var ensemble = CreateEnsemble();
        ensemble.EnsembleMembership.Add(new() { Member = activeMember, DateJoined = activeMember.DateJoined });
        ensemble.EnsembleMembership.Add(new() { Member = ensembleMember, DateJoined = ensembleMember.DateJoined });
        var entry = Context.Add(CreateMailingList(
            ensembles: new[] { ensemble },
            members: new[]{
                activeMember,
                CreateMember("Active2", "Member", "active2@email.com"),
                CreateMember("Only", "Member", "only@member.com"),
            },
            contacts: new[]{
                CreateContact("Only Contact", "only@contact.com"),
                CreateContact("Active2 Contact", "active2@email.com"),
            }
        ));
        await Context.SaveChangesAsync();

        var recipients = await this.service.GetDeliverableRecipientListAsync(entry.Entity);
        Assert.Equal(5, recipients.Count);
        Assert.Single(recipients, r => r.Name == "Active Member" && r.Email == "active@member.com");
        Assert.Single(recipients, r => r.Name == "Ensemble Member" && r.Email == "ensemble@member.com");
        Assert.Single(recipients, r => r.Name == "Active2 Member" && r.Email == "active2@email.com");
        Assert.Single(recipients, r => r.Name == "Only Member" && r.Email == "only@member.com");
        Assert.Single(recipients, r => r.Name == "Only Contact" && r.Email == "only@contact.com");
    }

    [Fact]
    public async Task GetDeliverableRecipientListDropsSuppressedEmails()
    {
        var ensemble = CreateEnsemble();
        ensemble.EnsembleMembership.Add(new()
        {
            Member = CreateMember("Suppressed", "Member", "suppressed@ensemble.com"),
        });
        var entry = Context.Add(CreateMailingList(
            ensembles: [ensemble],
            members: [CreateMember("Suppressed", "Member", "suppressed@member.com")],
            contacts: [CreateContact("Suppressed Contact", "suppressed@contact.com")],
            suppressions: [
                CreateSuppression("suppressed@ensemble.com"),
                CreateSuppression("suppressed@member.com"),
                CreateSuppression("suppressed@contact.com"),
            ]
        ));
        await Context.SaveChangesAsync();

        var recipients = await this.service.GetDeliverableRecipientListAsync(entry.Entity);
        Assert.Empty(recipients);
    }

    [Theory]
    [InlineData(EnsembleStatus.Disbanded, true, false, false)]
    [InlineData(EnsembleStatus.Active, false, false, false)]
    [InlineData(EnsembleStatus.Active, false, true, true)]
    [InlineData(EnsembleStatus.Active, true, false, true)]
    public async Task RecipientIsActiveInListExprIsConsistent(
        EnsembleStatus ensembleStatus,
        bool activeMembership,
        bool activeMember,
        bool expectedIsActive)
    {
        var member = CreateMember("A", "Member", "member@email.com", joinedDaysAgo: 5);
        if (!activeMember)
        {
            member.DateLeft = DateTime.UtcNow.AddDays(-1);
        }

        var ensemble = CreateEnsemble(status: ensembleStatus);
        ensemble.EnsembleMembership.Add(new EnsembleMembership
        {
            Member = member,
            DateJoined = DateTime.UtcNow.AddDays(-4),
            DateLeft = activeMembership ? null : DateTime.UtcNow.AddDays(-2),
        });

        var mailingList = new MailingList
        {
            Name = "ML",
            Slug = "ml",
            Ensembles = { ensemble },
            Members = { member },
        };
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        var activeMailingLists = await Context.MailingList
            .AsExpandable()
            .Where(service.RecipientIsActiveInListExpr(member.Id))
            .ToListAsync();
        if (expectedIsActive)
        {
            var activeMailingList = Assert.Single(activeMailingLists);
            Assert.Equal(mailingList.Id, activeMailingList.Id);
        }
        else
        {
            Assert.Empty(activeMailingLists);
        }
    }

    [Fact]
    public async Task SuppressedRecipientIsNotActiveInListExpr()
    {
        var member = CreateMember("A", "Member", "member@email.com", joinedDaysAgo: 5);
        var ensemble = CreateEnsemble();
        ensemble.EnsembleMembership.Add(new EnsembleMembership
        {
            Member = member,
            DateJoined = DateTime.UtcNow.AddDays(-4),
        });

        var mailingList = new MailingList
        {
            Name = "ML",
            Slug = "ml",
            Ensembles = { ensemble },
            Members = { member },
            Suppressions = [new MailingListSuppression { Email = "member@email.com", Reason = "" }]
        };
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        Assert.Empty(await Context.MailingList
            .AsExpandable()
            .Where(service.RecipientIsActiveInListExpr(member.Id))
            .ToListAsync());
    }

    [Fact]
    public async Task GetRecipientCountExprDeduplicatesAndCountsMissingEmails()
    {
        var activeMember = CreateMember("Active", "Member", "active@member.com");
        var ensembleMember = CreateMember("Ensemble", "Member", "ensemble@member.com");
        var ensemble = CreateEnsemble();
        ensemble.EnsembleMembership.Add(new() { Member = activeMember, DateJoined = activeMember.DateJoined });
        ensemble.EnsembleMembership.Add(new() { Member = ensembleMember, DateJoined = ensembleMember.DateJoined });
        var entry = Context.Add(CreateMailingList(
            ensembles: new[] { ensemble },
            members: new[]{
                activeMember,
                CreateMember("Active2", "Member", "active2@email.com"),
                CreateMember("Only", "Member", "only@member.com"),
                CreateMember("Inactive", "Member", "inactive@member.com", joinedDaysAgo: 2, leftDaysAgo: 1),
            },
            contacts: new[]{
                CreateContact("Only Contact", "only@contact.com"),
                CreateContact("Active2 Contact", "active2@email.com"),
                CreateContact("No Email", null),
                CreateContact("No Email2", null),
                CreateContact("Archived", "archived@contact.com", archivedDaysAgo: 2),
            }
        ));
        await Context.SaveChangesAsync();

        var expr = this.service.GetRecipientCountExpr();
        var recipientCount = await Context.MailingList
            .AsExpandable()
            .Where(e => e.Id == entry.Entity.Id)
            .Select(expr)
            .SingleAsync();
        Assert.Equal(9, recipientCount);
    }

    private static Ensemble CreateEnsemble(
        int activeCount = 0,
        int inactiveCount = 0,
        EnsembleStatus status = EnsembleStatus.Active)
    {
        var ensemble = new Ensemble { Name = "Ensemble", Status = status };
        for (var i = 0; i < activeCount; i++)
        {
            ensemble.EnsembleMembership.Add(new EnsembleMembership
            {
                Member = new Member
                {
                    FirstName = i.ToString(),
                    LastName = "Active",
                    Details = new MemberDetails { Email = $"{i}@m.com", PhoneNo = ".", Address = ".", }
                },
                DateJoined = DateTime.UtcNow.AddDays(-1),
            });
        }
        for (var i = 0; i < inactiveCount; i++)
        {
            ensemble.EnsembleMembership.Add(new EnsembleMembership
            {
                Member = new Member { FirstName = i.ToString(), LastName = "Inactive" },
                DateJoined = DateTime.UtcNow.AddDays(-2),
                DateLeft = DateTime.UtcNow.AddDays(-1),
            });
        }
        return ensemble;
    }

    private static Member CreateMember(
        string firstName,
        string lastName,
        string email,
        int joinedDaysAgo = 1,
        int? leftDaysAgo = null) => new Member
        {
            FirstName = firstName,
            LastName = lastName,
            DateJoined = DateTime.UtcNow.AddDays(-joinedDaysAgo),
            DateLeft = leftDaysAgo is { } d
            ? DateTime.UtcNow.AddDays(-d)
            : null,
            Details = new() { Email = email, PhoneNo = ".", Address = "." },
        };

    private static Contact CreateContact(string name, string? email, int? archivedDaysAgo = null) => new Contact
    {
        Name = name,
        Email = email,
        ArchivedOn = archivedDaysAgo is { } d ? DateTime.UtcNow.AddDays(-d) : null,
    };

    private static MailingListSuppression CreateSuppression(string email) => new()
    {
        Email = email,
        Reason = "Unsubscribe",
    };

    private static MailingList CreateMailingList(
        IList<Ensemble>? ensembles = null,
        IList<Member>? members = null,
        IList<Contact>? contacts = null,
        IList<MailingListSuppression>? suppressions = null)
    {
        return new MailingList
        {
            Name = "Mailing List",
            Slug = "mailing-list",
            Ensembles = ensembles ?? [],
            Members = members ?? [],
            Contacts = contacts ?? [],
            Suppressions = suppressions ?? [],
        };
    }
}
