
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Email.Postmark;
using Postmark.Model.MessageStreams;
using Postmark.Model.Suppressions;
using PostmarkDotNet;
using PostmarkDotNet.Exceptions;
using PostmarkDotNet.Model.Webhooks;

namespace OrchestrateApi.Tests;

public sealed class MockPostmarkApiService : IPostmarkApiService
{
    public MockPostmarkAdapter MockAdapter { get; } = new MockPostmarkAdapter();

    public IPostmarkAdapter? GetTenantClient(Tenant? tenant) => tenant?.PostmarkServerToken is not null
        ? MockAdapter
        : null;

    public Task<IPostmarkAdapter> GetOrCreateClientAsync(Tenant? tenant)
        => throw new NotImplementedException();

    public string? GetMailingListStreamId(MailingList mailingList) => mailingList.PostmarkStreamId;

    public Task<string> GetOrCreateMailingListStreamIdAsync(IPostmarkAdapter client, MailingList mailingList)
        => throw new NotImplementedException();

    public IList<MockPostmarkAdapter> TenantClients { get; set; } = [];

    public async Task<IEnumerable<IPostmarkAdapter>> GetAllClients()
        => await Task.FromResult(TenantClients.Concat([MockAdapter]));
}

public sealed class MockPostmarkAdapter : IPostmarkAdapter
{
    public Task<PostmarkMessageStream> CreateMessageStream(string id, MessageStreamType type, string name, string? description = null)
    {
        throw new NotImplementedException();
    }

    public IList<WebhookConfiguration> WebhookConfigurations { get; set; } = [];

    public Task<WebhookConfigurationListingResponse> GetWebhookConfigurationsAsync(string? messageStream = null)
    {
        return Task.FromResult(new WebhookConfigurationListingResponse
        {
            Webhooks = WebhookConfigurations
        });
    }

    public Task<WebhookConfiguration> CreateWebhookConfigurationAsync(string url, string? messageStream = null, HttpAuth? httpAuth = null, IEnumerable<HttpHeader>? httpHeaders = null, WebhookConfigurationTriggers? triggers = null)
    {
        throw new NotImplementedException();
    }

    public IList<WebhookConfiguration> EditedWebhookConfigurations { get; set; } = [];

    public Task<WebhookConfiguration> EditWebhookConfigurationAsync(long configurationId, string url, HttpAuth? httpAuth = null, IEnumerable<HttpHeader>? httpHeaders = null, WebhookConfigurationTriggers? triggers = null)
    {
        var configuration = new WebhookConfiguration
        {
            ID = configurationId,
            Url = url,
            HttpAuth = httpAuth,
            HttpHeaders = httpHeaders,
            Triggers = triggers,
        };
        EditedWebhookConfigurations.Add(configuration);
        return Task.FromResult(configuration);
    }

    public bool ThrowOnListSuppressions { get; set; }
    public IList<PostmarkSuppression> SuppressionList { get; set; } = [];

    public Task<PostmarkSuppressionListing> ListSuppressions(PostmarkSuppressionQuery query, string messageStream = "outbound")
    {
        if (ThrowOnListSuppressions)
        {
            throw new PostmarkValidationException(new PostmarkResponse());
        }

        return Task.FromResult(new PostmarkSuppressionListing { Suppressions = SuppressionList });
    }

    public bool ThrowValidationExceptionOnDeletion { get; set; }
    public IList<PostmarkSuppressionChangeRequest>? LastSuppressionDeletions { get; private set; }

    public Task<PostmarkBulkReactivationResult> DeleteSuppressions(IEnumerable<PostmarkSuppressionChangeRequest> suppressionChanges, string messageStream = "outbound")
    {
        LastSuppressionDeletions = suppressionChanges.ToList();

        if (ThrowValidationExceptionOnDeletion)
        {
            throw new PostmarkValidationException(new PostmarkResponse());
        }

        return Task.FromResult(new PostmarkBulkReactivationResult
        {
            Suppressions = LastSuppressionDeletions.Select(d => new PostmarkReactivationRequestResult
            {
                EmailAddress = d.EmailAddress,
                Status = PostmarkReactivationRequestStatus.Deleted,
                Message = string.Empty,
            }),
        });
    }

    public bool ThrowOnArchive { get; set; }
    public string? LastArchival { get; private set; }

    public Task<PostmarkMessageStreamArchivalConfirmation> ArchiveMessageStream(string id)
    {
        LastArchival = id;
        if (ThrowOnArchive)
        {
            throw new PostmarkValidationException(new PostmarkResponse());
        }
        return Task.FromResult(new PostmarkMessageStreamArchivalConfirmation
        {
            ID = id,
            ServerID = 1,
            ExpectedPurgeDate = DateTime.UtcNow.AddDays(90),
        });
    }

    public Task<IEnumerable<PostmarkResponse>> SendMessagesAsync(params PostmarkMessage[] messages)
    {
        throw new NotImplementedException();
    }
}
