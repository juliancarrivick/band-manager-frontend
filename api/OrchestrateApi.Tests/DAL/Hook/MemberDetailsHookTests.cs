using System.Linq.Expressions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Hook;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using EntityState = Breeze.Persistence.EntityState;

namespace OrchestrateApi.Tests.DAL.Hook
{
    public sealed class MemberDetailsHookTests : FullIntegrationTestHarness
    {
        private readonly MemberDetailHook hook;

        public MemberDetailsHookTests()
        {
            var testPersistenceManager = new OrchestratePersistenceManager(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId));
            this.hook = new()
            {
                PersistenceManager = testPersistenceManager,
                SaveMap = new(),
                UserService = GetService<UserService>(),
            };
        }

        [Fact]
        public async Task UpdatesUserEmailWhenDetailsUpdated()
        {
            var member = new Member
            {
                FirstName = "Bob",
                LastName = "The Builder",
                DateJoined = DateTime.UtcNow,
                Details = new MemberDetails
                {
                    Address = "abc",
                    Email = "a@email.com",
                    PhoneNo = "044444444",
                },
            };
            await Context.AddAsync(member);
            var user = new OrchestrateUser
            {
                UserName = member.Details.Email,
                Email = member.Details.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            member.Details.User = user;
            await GetService<UserManager<OrchestrateUser>>().CreateAsync(user);
            await Context.SaveChangesAsync();

            await this.hook.AfterSaveAsync(
                new[] { ModifiedMemberDetail(member.Details, e => e.Email, "new@email.com") },
                default);

            Assert.Equal("new@email.com", member.Details.User.UserName);
            Assert.Equal("new@email.com", member.Details.User.Email);
        }

        [Fact]
        public async Task DoesNothingIfEmailNotUpdated()
        {
            var member = new Member
            {
                FirstName = "Bob",
                LastName = "The Builder",
                DateJoined = DateTime.UtcNow,
                Details = new MemberDetails
                {
                    Address = "abc",
                    Email = "member@email.com",
                    PhoneNo = "044444444",
                },
            };
            await Context.AddAsync(member);
            var user = new OrchestrateUser
            {
                UserName = "user@email.com",
                Email = "user@email.com",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            member.Details.User = user;
            await GetService<UserManager<OrchestrateUser>>().CreateAsync(user);
            await Context.SaveChangesAsync();

            await this.hook.AfterSaveAsync(
                new[] { ModifiedMemberDetail(member.Details, e => e.Address, "def") },
                default);

            Assert.Equal("user@email.com", member.Details.User.UserName);
            Assert.Equal("user@email.com", member.Details.User.Email);
        }

        [Fact]
        public async Task DoesNothingIfNoUserLinked()
        {
            var member = new Member
            {
                FirstName = "Bob",
                LastName = "The Builder",
                DateJoined = DateTime.UtcNow,
                Details = new MemberDetails
                {
                    Address = "abc",
                    Email = "a@email.com",
                    PhoneNo = "044444444",
                },
            };
            await Context.AddAsync(member);
            await Context.SaveChangesAsync();

            await this.hook.AfterSaveAsync(
                new[] { ModifiedMemberDetail(member.Details, e => e.Email, "new@email.com") },
                default);

            Assert.Null(member.Details.UserId);
            Assert.Null(await Context.Users.FirstOrDefaultAsync(u => u.Email == "new@email.com"));
        }

        [Fact]
        public async Task ReturnsErrorIfMultipleMembersWithSameEmail()
        {
            var member = await Context.Member.Include(e => e.Details).FirstAsync();
            var otherMember = await Context.Member.Include(e => e.Details).Skip(1).FirstAsync();

            var errors = await this.hook.BeforeSaveAsync(
                new[] { ModifiedMemberDetail(member.Details!, e => e.Email, otherMember.Details!.Email) },
                default);

            Assert.Single(errors);
            Assert.Equal("ConflictingEmail", errors.First().ErrorName);
        }

        [Fact]
        public async Task ReturnsErrorIfExistingUserWithEmail()
        {
            var member = await Context.Member
                .Include(e => e.Details)
                .FirstAsync(e => e.Details!.UserId != null);
            var user = new OrchestrateUser
            {
                UserName = "other@email.com",
                Email = "other@email.com",
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await GetService<UserManager<OrchestrateUser>>().CreateAsync(user);

            var errors = await this.hook.AfterSaveAsync(
                new[] { ModifiedMemberDetail(member.Details!, e => e.Email, "other@email.com") },
                default);

            Assert.Single(errors);
            Assert.Equal("ConflictingEmail", errors.First().ErrorName);
        }

        private IEntityInfo<MemberDetails> ModifiedMemberDetail<T>(MemberDetails details, Expression<Func<MemberDetails, T>> propertyPath, T newValue)
        {
            var entityInfo = this.hook.PersistenceManager.CreateEntityInfo(details, EntityState.Modified);

            var entry = Context.Entry(details);
            var property = entry.Property(propertyPath);
            entityInfo.OriginalValuesMap = new() { [property.Metadata.Name] = property.CurrentValue };
            property.CurrentValue = newValue;

            return new EntityInfo<MemberDetails>(entityInfo);
        }
    }
}
