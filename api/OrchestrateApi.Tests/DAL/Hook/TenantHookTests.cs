using Breeze.Persistence;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Hook;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Tests.DAL.Hook
{
    public sealed class TenantHookTests : FullIntegrationTestHarness
    {
        private readonly TenantHook hook;

        public TenantHookTests()
        {
            PersistenceManager = GetService<OrchestratePersistenceManager>();
            this.hook = new()
            {
                PersistenceManager = PersistenceManager,
                SaveMap = new(),
            };
        }

        private OrchestratePersistenceManager PersistenceManager { get; }

        [Theory]
        [InlineData(new byte[] { })]
        [InlineData(new byte[] { 0xCA, 0xFE, 0xBA, 0xBE })]
        public void ErrorIsAddedForInvalidImages(byte[] bytes)
        {
            var entityInfo = CreateTenantInfo(bytes);
            var errors = this.hook.Validate(entityInfo);
            Assert.Single(errors);
            Assert.Equal(nameof(Tenant.Logo), errors.First().PropertyName);
        }

        [Fact]
        public void NullLogoIsValid()
        {
            var entityInfo = CreateTenantInfo(null);
            Assert.Empty(this.hook.Validate(entityInfo));
        }

        [Fact]
        public void ImageTypesAreAllowed()
        {
            foreach (var type in FileTypeDetector.Images.AvailableFileTypes)
            {
                if (type is SentinelFileType sentinalType)
                {
                    var image = sentinalType.Sentinel.Concat(new byte[] { 0xCA, 0xFE, 0xBA, 0xBE }).ToArray();
                    var entityInfo = CreateTenantInfo(image);
                    Assert.Empty(this.hook.Validate(entityInfo));
                }
                else
                {
                    throw new XunitException("Unknown file type");
                }
            }
        }

        private EntityInfo CreateTenantInfo(byte[]? logo)
        {
            var tenant = new Tenant { Name = "Test", Logo = logo };
            var entityInfo = PersistenceManager.CreateEntityInfo(tenant);
            entityInfo.OriginalValuesMap = new()
            {
                [nameof(Tenant.Logo)] = null,
            };

            return entityInfo;
        }
    }
}
