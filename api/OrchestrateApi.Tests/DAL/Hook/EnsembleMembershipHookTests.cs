using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Hook;
using OrchestrateApi.DAL.Model;
using EntityState = Breeze.Persistence.EntityState;

namespace OrchestrateApi.Tests.DAL.Hook
{
    public class EnsembleMembershipHookTests : AutoRollbackTestHarness
    {
        private readonly EnsembleMembershipHook hook;

        public EnsembleMembershipHookTests()
        {
            var testPersistenceManager = new OrchestratePersistenceManager(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId));
            hook = new()
            {
                PersistenceManager = testPersistenceManager,
                SaveMap = new(),
            };
        }

        [Fact]
        public async Task SetsDateLeftIfLastMembershipIsRemoved()
        {
            var member = await Context.Member.FirstAsync(m => m.EnsembleMembership.Any(e => e.DateLeft == null));
            Assert.Null(member.DateLeft);
            var endedMemberships = await Context.EnsembleMembership
                .Where(e => e.MemberId == member.Id && e.DateLeft == null)
                .ToListAsync();
            foreach (var membership in endedMemberships)
            {
                membership.DateLeft = DateTime.UtcNow.AddDays(-1);
            }
            await Context.SaveChangesAsync();

            await this.hook.AfterSaveAsync(
                endedMemberships.Select(m => new MockTypedEntityInfo<EnsembleMembership>(m, EntityState.Modified)),
                 default);
            await Context.SaveChangesAsync();

            Assert.NotNull(member.DateLeft);
            Assert.Equal(1, this.hook.SaveMap[typeof(Member)].Count(ei => ei.Entity == member));
        }

        [Fact]
        public async Task RemovesDateLeftIfNewActiveMembershipIsAdded()
        {
            var member = await Context.Member.FirstAsync(m => m.DateLeft != null);
            var ensemble = await Context.Ensemble.FirstAsync();
            var entry = await Context.AddAsync(new EnsembleMembership
            {
                Member = member,
                Ensemble = ensemble,
                DateJoined = DateTime.UtcNow,
            });
            await Context.SaveChangesAsync();

            await this.hook.AfterSaveAsync(
                new[] { new MockTypedEntityInfo<EnsembleMembership>(entry.Entity, EntityState.Added) },
                 default);
            await Context.SaveChangesAsync();

            Assert.Null(member.DateLeft);
            Assert.Equal(1, this.hook.SaveMap[typeof(Member)].Count(ei => ei.Entity == member));

        }
    }
}
