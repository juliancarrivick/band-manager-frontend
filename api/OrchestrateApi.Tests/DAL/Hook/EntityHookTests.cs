using Breeze.Persistence;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Hook;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Tests.DAL.Hook
{
    public class EntityHookTests : AutoRollbackTestHarness
    {
        [Fact]
        public void EntityGetsAddedIfNoneThere()
        {
            var hook = new TestEntityHook<Asset>
            {
                PersistenceManager = new OrchestratePersistenceManager(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId)),
                SaveMap = new(),
            };

            hook.ReturnToClient(new Asset(), EntityState.Added);

            Assert.Single(hook.SaveMap[typeof(Asset)]);
        }

        [Fact]
        public void EntityGetsAddedIfOthersWithDifferentKeys()
        {
            var persistenceManager = new OrchestratePersistenceManager(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId));
            var hook = new TestEntityHook<Asset>
            {
                PersistenceManager = persistenceManager,
                SaveMap = new()
                {
                    [typeof(Asset)] = new() { persistenceManager.CreateEntityInfo(new Asset { Id = 1 }) },
                },
            };

            hook.ReturnToClient(new Asset { Id = 2 }, EntityState.Added);

            Assert.Equal(2, hook.SaveMap[typeof(Asset)].Count);
        }

        // Better way to enumerate these?
        [Theory]
        [InlineData(EntityState.Added, EntityState.Added, false)]
        [InlineData(EntityState.Added, EntityState.Deleted, false)]
        [InlineData(EntityState.Added, EntityState.Detached, false)]
        [InlineData(EntityState.Added, EntityState.Modified, false)]
        [InlineData(EntityState.Added, EntityState.Unchanged, false)]
        [InlineData(EntityState.Deleted, EntityState.Added, false)]
        [InlineData(EntityState.Deleted, EntityState.Deleted, false)]
        [InlineData(EntityState.Deleted, EntityState.Detached, false)]
        [InlineData(EntityState.Deleted, EntityState.Modified, false)]
        [InlineData(EntityState.Deleted, EntityState.Unchanged, false)]
        [InlineData(EntityState.Detached, EntityState.Added, false)]
        [InlineData(EntityState.Detached, EntityState.Deleted, false)]
        [InlineData(EntityState.Detached, EntityState.Detached, false)]
        [InlineData(EntityState.Detached, EntityState.Modified, false)]
        [InlineData(EntityState.Detached, EntityState.Unchanged, false)]
        [InlineData(EntityState.Modified, EntityState.Added, false)]
        [InlineData(EntityState.Modified, EntityState.Deleted, false)]
        [InlineData(EntityState.Modified, EntityState.Detached, false)]
        [InlineData(EntityState.Modified, EntityState.Modified, true)]
        [InlineData(EntityState.Modified, EntityState.Unchanged, false)]
        [InlineData(EntityState.Unchanged, EntityState.Added, false)]
        [InlineData(EntityState.Unchanged, EntityState.Deleted, true)]
        [InlineData(EntityState.Unchanged, EntityState.Detached, false)]
        [InlineData(EntityState.Unchanged, EntityState.Modified, true)]
        [InlineData(EntityState.Unchanged, EntityState.Unchanged, false)]
        public void ExistingEntityIsUpdatedWhereAppropriate(EntityState existingState, EntityState targetState, bool isUpdated)
        {
            var persistenceManager = new OrchestratePersistenceManager(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId));
            var hook = new TestEntityHook<Asset>
            {
                PersistenceManager = persistenceManager,
                SaveMap = new()
                {
                    [typeof(Asset)] = new() { persistenceManager.CreateEntityInfo(new Asset { Id = 1 }, existingState) },
                },
            };

            if (isUpdated)
            {
                var updatedEntity = new Asset { Id = 1 };
                hook.ReturnToClient(updatedEntity, targetState);
                Assert.Single(hook.SaveMap[typeof(Asset)]);
                Assert.Equal(updatedEntity, hook.SaveMap[typeof(Asset)][0].Entity);
                Assert.Equal(targetState, hook.SaveMap[typeof(Asset)][0].EntityState);
            }
            else
            {
                Assert.Throws<InvalidOperationException>(() => hook.ReturnToClient(new Asset { Id = 1 }, targetState));
            }
        }

        [Fact]
        public void ThrowsIfAlreadyInSaveMapAndNonModelEntityUsed()
        {
            var hook = new TestEntityHook<MockEntity>
            {
                PersistenceManager = new OrchestratePersistenceManager(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId)),
                SaveMap = new()
                {
                    [typeof(MockEntity)] = new(),
                },
            };
            Assert.Throws<InvalidOperationException>(() => hook.ReturnToClient(new MockEntity(), EntityState.Added));
        }

        private sealed class MockEntity
        {
        }

        private sealed class TestEntityHook<T> : EntityHook<T>
        {
            public override IEnumerable<EntityError> Validate(IEntityInfo<T> entityInfo)
            {
                throw new NotImplementedException();
            }

            public override Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<T>> entityInfos, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public override Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<T>> entityInfos, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public new void ReturnToClient(object entity, EntityState entityState) => base.ReturnToClient(entity, entityState);
        }
    }
}
