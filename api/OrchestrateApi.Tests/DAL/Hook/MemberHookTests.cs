using System.Data;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Hook;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using EntityState = Breeze.Persistence.EntityState;

namespace OrchestrateApi.Tests.DAL.Hook
{
    public sealed class MemberHookTests : FullIntegrationTestHarness
    {
        private readonly MemberHook hook;

        public MemberHookTests()
        {
            PersistenceManager = new OrchestratePersistenceManager(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId));
            this.hook = new()
            {
                PersistenceManager = PersistenceManager,
                SaveMap = new(),
                UserService = GetService<UserService>(),
            };
        }

        private OrchestratePersistenceManager PersistenceManager { get; }

        [Fact]
        public async Task AlteringAnyRelatedEnsembleMembershipIsInvalid()
        {
            var member = await Context.Member
                .Include(m => m.EnsembleMembership)
                .FirstAsync(m => m.EnsembleMembership.Count > 0);
            this.hook.SaveMap[typeof(EnsembleMembership)] = new()
            {
                PersistenceManager.CreateEntityInfo(member.EnsembleMembership.First(), EntityState.Modified),
            };

            var memberEntityInfo = PersistenceManager.CreateEntityInfo(member, EntityState.Modified);
            memberEntityInfo.OriginalValuesMap = new()
            {
                [nameof(Member.DateLeft)] = null,
            };
            var errors = this.hook.Validate(memberEntityInfo);

            Assert.Single(errors);
            Assert.NotNull(errors.FirstOrDefault(e => e.PropertyName == nameof(Member.DateLeft)));
        }

        [Fact]
        public async Task AlteringUnrelatedEnsembleMembershipIsValid()
        {
            var member = await Context.Member
                .FirstAsync(m => m.EnsembleMembership.Count > 0);
            var otherMembership = await Context.EnsembleMembership.FirstAsync(m => m.MemberId != member.Id);
            this.hook.SaveMap[typeof(EnsembleMembership)] = new()
            {
                PersistenceManager.CreateEntityInfo(otherMembership, EntityState.Modified),
            };

            var memberEntityInfo = PersistenceManager.CreateEntityInfo(member, EntityState.Modified);
            memberEntityInfo.OriginalValuesMap = new()
            {
                [nameof(Member.DateLeft)] = null,
            };
            var errors = this.hook.Validate(memberEntityInfo);

            Assert.Empty(errors);
        }

        [Fact]
        public async Task AlteringRelatedEnsembleMembershipButNotDateLeftIsValid()
        {
            var member = await Context.Member
                .Include(m => m.EnsembleMembership)
                .FirstAsync(m => m.EnsembleMembership.Count > 0);
            this.hook.SaveMap[typeof(EnsembleMembership)] = new()
            {
                PersistenceManager.CreateEntityInfo(member.EnsembleMembership.First(), EntityState.Modified),
            };

            var memberEntityInfo = PersistenceManager.CreateEntityInfo(member, EntityState.Modified);
            memberEntityInfo.OriginalValuesMap = new()
            {
                [nameof(Member.FirstName)] = "name",
            };
            var errors = this.hook.Validate(memberEntityInfo);

            Assert.Empty(errors);
        }

        [Fact]
        public async Task IfDateLeftIsSetAllEnsembleMembershipIsEnded()
        {
            var member = await Context.Member.FirstAsync(m => m.DateLeft == null && m.EnsembleMembership.Any(m => m.DateLeft == null));
            member.DateLeft = DateTime.UtcNow;
            var entityInfo = new MockTypedEntityInfo<Member>(member, EntityState.Modified)
            {
                OriginalValuesMap = {
                    [nameof(Member.DateLeft)] = null,
                }
            };
            await Context.SaveChangesAsync();

            await this.hook.AfterSaveAsync(new[] { entityInfo }, default);
            await Context.SaveChangesAsync();

            Assert.Equal(0, await Context.EnsembleMembership.CountAsync(m => m.MemberId == member.Id && m.DateLeft == null));
        }

        [Fact]
        public async Task IfMemberIsDeletedUserIsNotDeletedToo()
        {
            var memberData = await Context.Member.Select(m => new { Member = m, m.Details!.UserId }).FirstAsync();
            var entityInfo = new MockTypedEntityInfo<Member>(memberData.Member, EntityState.Deleted);

            await this.hook.BeforeSaveAsync(new[] { entityInfo }, default); // Used to save userId
            Context.Remove(memberData.Member);
            await Context.SaveChangesAsync();
            await this.hook.AfterSaveAsync(new[] { entityInfo }, default); // Used to use saved userId
            await Context.SaveChangesAsync();

            Assert.NotNull(await Context.Users.SingleOrDefaultAsync(u => u.Id == memberData.UserId));
        }
    }
}
