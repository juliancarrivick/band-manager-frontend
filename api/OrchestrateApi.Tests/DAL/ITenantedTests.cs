using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Tests;

namespace OrchestrateApi.DAL
{
    public class ITenantedTests : AutoRollbackTestHarness
    {
        [Fact]
        public async Task TenantIdConcreteReturnsCorrectTenantId()
        {
            var asset = await Context.Asset.FirstAsync();
            Assert.Equal(OrchestrateSeed.TenantId, asset.TenantIdConcrete(Context));
        }

        [Fact]
        public async Task WhereInTenantFiltersAppropriately()
        {
            var tenant = new Tenant
            {
                Id = Guid.NewGuid(),
                Name = "Temp",
                Abbreviation = "temp",
                TimeZone = "Australia/Perth",
            };
            using (var tenantOverride = TenantService.ForceOverrideTenantId(tenant.Id))
            {
                Context.Add(tenant);
                Context.Add(new Asset { Description = "some stuff" });
                Context.Add(new Asset { Description = "some other stuff" });
                await Context.SaveChangesAsync();
            }

            Assert.Equal(3, await Context.Asset.CountAsync());
            Assert.Equal(5, await Context.Asset.IgnoreQueryFilters().CountAsync());
            Assert.Equal(2, await Context.Asset.IgnoreQueryFilters().WhereInTenant(tenant.Id).CountAsync());
        }
    }
}
