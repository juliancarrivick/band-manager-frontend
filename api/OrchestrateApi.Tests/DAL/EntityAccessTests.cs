using System.Collections;
using System.Data;
using System.Security.Claims;
using Breeze.Persistence;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using Xunit.Abstractions;

namespace OrchestrateApi.Tests.DAL
{
    public sealed class AssetControllerTests : FullIntegrationTestHarness
    {
        public AssetControllerTests()
        {
        }

        [Theory]
        [ClassData(typeof(EntityReadRows))]
        public void VerifyAuthorisedEntityReadCounts(EntityReadRow data)
        {
            var accessor = GetService<IHttpContextAccessor>();
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, "some@user.com"),
            };
            if (data.Role != null)
            {
                claims.Add(new Claim(ClaimTypes.Role, new TenantRole(OrchestrateSeed.TenantId, data.Role).Serialise()));
            }
            accessor.HttpContext = new DefaultHttpContext
            {
                User = new(new ClaimsIdentity(claims)),
            };

            var persistenceManager = GetService<OrchestratePersistenceManager>();
            var genericMethodInfo = ReflectionUtils.GetGenericMethodInfo(() => persistenceManager.AuthorisedEntitySet<Asset>());
            var methodInfo = genericMethodInfo.MakeGenericMethod(data.EntityType)!;
            var result = methodInfo.Invoke(persistenceManager, Array.Empty<object>())!;
            Assert.Equal(data.ExpectedCount, Queryable.Count((dynamic)result));
        }

        [Theory]
        [ClassData(typeof(EntityWriteRows))]
        public async Task VerifyAuthorisedEntityWriteAccess(EntityWriteRow data)
        {
            var accessor = GetService<IHttpContextAccessor>();
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, "some@user.com"),
            };
            if (data.Role != null)
            {
                claims.Add(new Claim(ClaimTypes.Role, new TenantRole(OrchestrateSeed.TenantId, data.Role).Serialise()));
            }
            accessor.HttpContext = new DefaultHttpContext
            {
                User = new(new ClaimsIdentity(claims)),
            };

            var persistenceManager = GetService<OrchestratePersistenceManager>();
            var saveMap = new Dictionary<Type, List<EntityInfo>>
            {
                [data.EntityType] = new List<EntityInfo>(),
            };

            var exception = await Record.ExceptionAsync(() => persistenceManager.StubbedSaveAsync(saveMap));
            Assert.Equal(data.CanWrite, exception == null);
            if (!data.CanWrite)
            {
                Assert.IsType<UnauthorisedSaveException>(exception);
            }
        }
    }

    public class EntityReadRows : IEnumerable<object[]>
    {
        public EntityReadRows()
        {
            var billingViewers = new[] { StandardRole.CommitteeMember, StandardRole.BillingAdministrator, StandardRole.Administrator };
            Rows = new List<EntityReadRow>()
                .AddAuthenticatedRead<Tenant>(1)
                .AddAuthenticatedRead<Asset>(3)
                .AddAuthenticatedRead<AssetLoan>(3)
                .AddAuthenticatedRead<Ensemble>(3)
                .AddAuthenticatedRead<EnsembleMembership>(4)
                .AddAuthenticatedRead<EnsembleRole>(0)
                .AddAuthenticatedRead<Score>(3)
                .AddRestrictedRead<MemberBillingPeriod>(billingViewers, 4)
                .AddRestrictedRead<MemberInvoice>(billingViewers, 8)
                .AddRestrictedRead<MemberBillingConfig>(billingViewers, 1)
                .AddRestrictedRead<MemberBillingPeriodConfig>(billingViewers, 2)
                .AddAuthenticatedRead<Member>(3)
                .AddRestrictedRead<MemberDetails>(StandardRole.StandardEditors, 3)
                .AddAuthenticatedRead<MemberInstrument>(3)
                .AddAuthenticatedRead<OrchestrateRole>(5);
        }

        private ICollection<EntityReadRow> Rows { get; }

        public IEnumerator<object[]> GetEnumerator() => Rows.Select(r => new object[] { r }).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class EntityReadRow : IXunitSerializable
    {
        public required Type EntityType { get; set; }
        public required string? Role { get; set; }
        public int ExpectedCount { get; set; }

        public void Serialize(IXunitSerializationInfo info)
        {
            info.AddValue(nameof(EntityType), EntityType);
            info.AddValue(nameof(Role), Role);
            info.AddValue(nameof(ExpectedCount), ExpectedCount);
        }

        public void Deserialize(IXunitSerializationInfo info)
        {
            EntityType = info.GetValue<Type>(nameof(EntityType));
            Role = info.GetValue<string>(nameof(Role));
            ExpectedCount = info.GetValue<int>(nameof(ExpectedCount));
        }
    }

    public static class EntityReadRowsExtensions
    {
        public static ICollection<EntityReadRow> AddAuthenticatedRead<T>(this ICollection<EntityReadRow> rows, int expectedCount)
        {
            rows.Add(new EntityReadRow { EntityType = typeof(T), Role = null, ExpectedCount = 0 });
            rows.Add(new EntityReadRow { EntityType = typeof(T), Role = StandardRole.Viewer, ExpectedCount = expectedCount });
            rows.Add(new EntityReadRow { EntityType = typeof(T), Role = StandardRole.Editor, ExpectedCount = expectedCount });
            rows.Add(new EntityReadRow { EntityType = typeof(T), Role = StandardRole.Administrator, ExpectedCount = expectedCount });
            rows.Add(new EntityReadRow { EntityType = typeof(T), Role = StandardRole.BillingAdministrator, ExpectedCount = expectedCount });
            rows.Add(new EntityReadRow { EntityType = typeof(T), Role = StandardRole.CommitteeMember, ExpectedCount = expectedCount });
            return rows;
        }

        public static ICollection<EntityReadRow> AddRestrictedRead<T>(this ICollection<EntityReadRow> rows, IReadOnlyCollection<string> roles, int expectedCount)
        {
            rows.Add(new EntityReadRow { EntityType = typeof(T), Role = null, ExpectedCount = 0 });

            foreach (var role in StandardRole.AllRoleNames.Except(roles))
            {
                rows.Add(new EntityReadRow { EntityType = typeof(T), Role = role, ExpectedCount = 0 });
            }

            foreach (var role in roles)
            {
                rows.Add(new EntityReadRow { EntityType = typeof(T), Role = role, ExpectedCount = expectedCount });
            }

            return rows;
        }
    }

    public class EntityWriteRows : IEnumerable<object[]>
    {

        public EntityWriteRows()
        {
            var adminAndBillingAdmin = new[] { StandardRole.Administrator, StandardRole.BillingAdministrator };
            Rows = new List<EntityWriteRow>()
                .AddRestrictedWrite<Tenant>(new[] { StandardRole.Administrator })
                .AddStandardEditors<Asset>()
                .AddStandardEditors<AssetLoan>()
                .AddRestrictedWrite<Ensemble>(StandardRole.StandardEditors.Union(new[] { StandardRole.BillingAdministrator }).ToList())
                .AddStandardEditors<EnsembleMembership>()
                .AddRestrictedWrite<EnsembleRole>(new[] { StandardRole.Administrator })
                .AddStandardEditors<Score>()
                .AddRestrictedWrite<MemberBillingPeriod>(adminAndBillingAdmin)
                .AddRestrictedWrite<MemberInvoice>(adminAndBillingAdmin)
                .AddRestrictedWrite<MemberBillingConfig>(adminAndBillingAdmin)
                .AddRestrictedWrite<MemberBillingPeriodConfig>(adminAndBillingAdmin)
                .AddStandardEditors<Member>()
                .AddStandardEditors<MemberDetails>()
                .AddStandardEditors<MemberInstrument>()
                .AddRestrictedWrite<OrchestrateRole>(Array.Empty<string>());
        }

        private ICollection<EntityWriteRow> Rows { get; }

        public IEnumerator<object[]> GetEnumerator() => Rows.Select(r => new object[] { r }).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class EntityWriteRow : IXunitSerializable
    {
        public required Type EntityType { get; set; }
        public required string? Role { get; set; }
        public bool CanWrite { get; set; }

        public void Serialize(IXunitSerializationInfo info)
        {
            info.AddValue(nameof(EntityType), EntityType);
            info.AddValue(nameof(Role), Role);
            info.AddValue(nameof(CanWrite), CanWrite);
        }

        public void Deserialize(IXunitSerializationInfo info)
        {
            EntityType = info.GetValue<Type>(nameof(EntityType));
            Role = info.GetValue<string>(nameof(Role));
            CanWrite = info.GetValue<bool>(nameof(CanWrite));
        }
    }

    public static class EntityWriteRowsExtensions
    {
        public static ICollection<EntityWriteRow> AddStandardEditors<T>(this ICollection<EntityWriteRow> rows)
        {
            return rows.AddRestrictedWrite<T>(StandardRole.StandardEditors);
        }

        public static ICollection<EntityWriteRow> AddRestrictedWrite<T>(this ICollection<EntityWriteRow> rows, IEnumerable<string> roles)
        {
            rows.Add(new EntityWriteRow { EntityType = typeof(T), Role = null, CanWrite = false });

            foreach (var role in StandardRole.AllRoleNames)
            {
                rows.Add(new EntityWriteRow { EntityType = typeof(T), Role = role, CanWrite = roles.Contains(role) });
            }

            return rows;
        }
    }
}
