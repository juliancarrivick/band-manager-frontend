using Breeze.Persistence;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using OrchestrateApi.Tests;
using EntityState = Breeze.Persistence.EntityState;

namespace OrchestrateApi.DAL.CustomSave
{
    public class SingleEntityCustomSaveTests : AutoRollbackTestHarness
    {
        public SingleEntityCustomSaveTests()
        {
            CustomSave = new();
            Manager = new(Context, null!, null!, null!, new StaticTenantService(OrchestrateSeed.TenantId));
        }

        private TestCustomSave CustomSave { get; }

        private OrchestratePersistenceManager Manager { get; }

        [Theory]
        [InlineData(true, typeof(Asset))]
        [InlineData(false)]
        [InlineData(false, typeof(Asset), typeof(Asset))]
        [InlineData(false, typeof(Asset), typeof(AssetLoan))]
        [InlineData(false, typeof(AssetLoan))]
        public void ThrowsIfNotSingleEntityOfStateUnchanged(bool isValid, params Type[] types)
        {
            var saveMap = new Dictionary<Type, List<EntityInfo>>();
            foreach (var type in types)
            {
                saveMap.Add(Manager.CreateEntityInfo(Activator.CreateInstance(type), EntityState.Unchanged));
            }

            if (isValid)
            {
                Assert.Same(saveMap, CustomSave.BeforeSave(saveMap));
            }
            else
            {
                Assert.Throws<InvalidSaveDataException>(() => CustomSave.BeforeSave(saveMap));
            }
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void ThrowsIfEntityIsNotInValidState(bool isValid)
        {
            var saveMap = CreateValidSaveMap(new Asset());
            CustomSave.IsPreSaveValid = isValid;

            if (isValid)
            {
                Assert.Same(saveMap, CustomSave.BeforeSave(saveMap));
            }
            else
            {
                Assert.Throws<InvalidSaveDataException>(() => CustomSave.BeforeSave(saveMap));
            }
        }

        [Fact]
        public async Task AttachesAndDetectsChangesToEntity()
        {
            var asset = await Context.Asset.FirstAsync();
            var originalDescription = asset.Description;
            CustomSave.DoModifyEntity = (context, asset, cancellationToken) =>
            {
                asset.Description = "ModifiedDescription";
                return Task.CompletedTask;
            };

            var saveMap = CreateValidSaveMap(asset);

            // Need to detatch after creating the savemap/entityinfo because otherwise
            // breeze will add it as a new entity (with a corresponding new entry)
            // even though a detached one already exists
            var entry = Context.Entry(asset);
            entry.State = Microsoft.EntityFrameworkCore.EntityState.Detached;

            await CustomSave.AfterSaveAsync(Manager, saveMap, default);

            // Entry seems to be immutable? Previous reference isn't updated
            entry = Context.Entry(asset);
            var descriptionProperty = entry.Property(e => e.Description);
            Assert.True(descriptionProperty.IsModified);
            Assert.Equal(originalDescription, descriptionProperty.OriginalValue);
            Assert.Equal("ModifiedDescription", descriptionProperty.CurrentValue);
            Assert.Equal("ModifiedDescription", asset.Description);

            var entityInfo = saveMap[typeof(Asset)][0];
            Assert.Equal(originalDescription, entityInfo.OriginalValuesMap[nameof(Asset.Description)]);
        }

        [Fact]
        public async Task ReturningEntitiesInSaveLogicAddsToSaveMap()
        {
            var asset = await Context.Asset.FirstAsync();
            var loanId = 0;
            CustomSave.DoSaveLogicAsync = async (context, asset, cancellationToken) =>
            {
                var loan = await context.AssetLoan.FirstAsync(e => e.AssetId == asset.Id, cancellationToken);
                loanId = loan.Id;
                return new[] { loan };
            };

            var saveMap = CreateValidSaveMap(asset);
            await CustomSave.AfterSaveAsync(Manager, saveMap, default);

            Assert.Single(saveMap[typeof(AssetLoan)]);
            var entityInfo = saveMap[typeof(AssetLoan)][0];
            Assert.Equal(EntityState.Modified, entityInfo.EntityState);
            Assert.Equal(loanId, ((AssetLoan)entityInfo.Entity).Id);
        }

        private Dictionary<Type, List<EntityInfo>> CreateValidSaveMap(Asset asset)
        {
            return new()
            {
                [typeof(Asset)] = new() { Manager.CreateEntityInfo(asset, EntityState.Unchanged) },
            };
        }

        private sealed class TestCustomSave : SingleEntityCustomSave<Asset>
        {
            public override FeaturePermission RequiredPermission => throw new NotImplementedException();

            public bool IsPreSaveValid { get; set; } = true;

            public Func<OrchestrateContext, Asset, CancellationToken, Task> DoModifyEntity { get; set; }
                = (context, asset, cancellationToken) => Task.CompletedTask;

            public Func<OrchestrateContext, Asset, CancellationToken, Task<IEnumerable<object>>> DoSaveLogicAsync { get; set; }
                = (context, entity, cancellationToken) => Task.FromResult(Enumerable.Empty<object>());

            internal override bool EntityIsPreSaveValid(Asset? entity) => IsPreSaveValid;

            internal override Task ModifyEntity(OrchestrateContext context, Asset entity, CancellationToken cancellationToken)
            {
                return DoModifyEntity(context, entity, cancellationToken);
            }

            internal override Task<IEnumerable<object>> SaveLogicAsync(OrchestrateContext context, Asset entity, CancellationToken cancellationToken)
            {
                return DoSaveLogicAsync(context, entity, cancellationToken);
            }
        }
    }
}
