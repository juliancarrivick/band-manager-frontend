using OrchestrateApi.DAL;

namespace OrchestrateApi.Tests.DAL
{
    public class TenantServiceTests
    {
        [Fact]
        public void StaticTenantServiceReturnsSpecifiedTenantId()
        {
            var tenantId = Guid.NewGuid();
            var tenantService = new StaticTenantService(tenantId);
            Assert.Equal(tenantId, tenantService.TenantId);

            var overrideTenantId = Guid.NewGuid();
            using (var tenantIdOverride = tenantService.ForceOverrideTenantId(overrideTenantId))
            {
                Assert.Equal(overrideTenantId, tenantService.TenantId);
            }

            Assert.Equal(tenantId, tenantService.TenantId);
        }

        [Fact]
        public void HttpContextTenantServiceReturnsSpecifiedTenantId()
        {
            var tenantService = new HttpContextTenantService(new MockHttpContextAccessor());
            Assert.Equal(MockHttpContextAccessor.TenantId, tenantService.TenantId);

            var overrideTenantId = Guid.NewGuid();
            using (var tenantIdOverride = tenantService.ForceOverrideTenantId(overrideTenantId))
            {
                Assert.Equal(overrideTenantId, tenantService.TenantId);
            }

            Assert.Equal(MockHttpContextAccessor.TenantId, tenantService.TenantId);
        }

        [Fact]
        public void HttpContextTenantServiceReturnsNullForNoContext()
        {
            var tenantService = new HttpContextTenantService(new MockHttpContextAccessor
            {
                HttpContext = null,
            });
            Assert.Null(tenantService.TenantId);
        }

        private sealed class MockHttpContextAccessor : IHttpContextAccessor
        {
            public static Guid TenantId { get; } = Guid.NewGuid();

            public HttpContext? HttpContext { get; set; } = new DefaultHttpContext
            {
                Request = {
                    Headers = {
                        [TenantedHttpContextExtensions.TenantIdHeader] = TenantId.ToString(),
                    }
                }
            };
        }
    }

    public class TenantedHttpContextExtensionsTests
    {
        [Fact]
        public void NullHttpContextReturnsNullAndFalse()
        {
            HttpContext? httpContext = null;
            Assert.Null(httpContext!.GetTenantId());
            Assert.False(httpContext!.TryGetTenantId(out var tenantId));
            Assert.Equal(Guid.Empty, tenantId);
        }

        [Fact]
        public void NoHeaderReturnsNullAndFalse()
        {
            var httpContext = new DefaultHttpContext();
            Assert.Null(httpContext.GetTenantId());
            Assert.False(httpContext.TryGetTenantId(out var tenantId));
            Assert.Equal(Guid.Empty, tenantId);
        }

        [Fact]
        public void InvalidGuidReturnsNullAndFalse()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[TenantedHttpContextExtensions.TenantIdHeader] = "abc";

            Assert.Null(httpContext.GetTenantId());
            Assert.False(httpContext.TryGetTenantId(out var tenantId));
            Assert.Equal(Guid.Empty, tenantId);
        }

        [Fact]
        public void ValidGuidComesThroughCorrectly()
        {
            var httpContext = new DefaultHttpContext();
            var guid = Guid.NewGuid();
            httpContext.Request.Headers[TenantedHttpContextExtensions.TenantIdHeader] = guid.ToString();

            Assert.Equal(guid, httpContext.GetTenantId());
            Assert.True(httpContext.TryGetTenantId(out var tenantId));
            Assert.Equal(guid, tenantId);
        }

        [Fact]
        public void OnlyFirstGuidComesThrough()
        {
            var httpContext = new DefaultHttpContext();
            var guid1 = Guid.NewGuid();
            var guid2 = Guid.NewGuid();
            httpContext.Request.Headers.Append(
                TenantedHttpContextExtensions.TenantIdHeader,
                new[] { guid1.ToString(), guid2.ToString() });

            Assert.Equal(guid1, httpContext.GetTenantId());
            Assert.True(httpContext.TryGetTenantId(out var tenantId));
            Assert.Equal(guid1, tenantId);
        }
    }
}
