using Microsoft.AspNetCore;
using Microsoft.AspNetCore.TestHost;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Tests
{
    public sealed class IntegrationEnvironment : IDisposable
    {
        private readonly IWebHost webHost;
        private readonly IServiceScope serviceScope;

        public IntegrationEnvironment(OrchestrateContext? context = null)
        {
            this.webHost = WebHost.CreateDefaultBuilder()
                .ConfigureAppConfiguration((builder) =>
                {
                    builder.ConfigureForTesting();
                })
                .UseStartup<Startup>()
                .ConfigureTestServices(services =>
                {
                    if (context != null)
                    {
                        SwapTransient(services, (services) => context);
                    }
                })
                .Build();
            this.serviceScope = this.webHost.Services.CreateScope();
        }

        public IntegrationEnvironment(Action<IServiceCollection> configureServices)
        {
            this.webHost = WebHost.CreateDefaultBuilder()
                .ConfigureAppConfiguration((builder) =>
                {
                    builder.ConfigureForTesting();
                })
                .UseStartup<Startup>()
                .ConfigureTestServices(configureServices)
                .Build();
            this.serviceScope = this.webHost.Services.CreateScope();
        }

        public T GetService<T>() where T : notnull
        {
            return this.serviceScope.ServiceProvider.GetRequiredService<T>();
        }

        public void Dispose()
        {
            this.serviceScope.Dispose();
            this.webHost.Dispose();
        }

        private static void SwapTransient<TImplementation>(
            IServiceCollection services,
            Func<IServiceProvider, TImplementation> factory)
            where TImplementation : notnull
        {
            var serviceDescriptors = services.Where(x => x.ServiceType == typeof(TImplementation) && x.Lifetime == ServiceLifetime.Transient).ToList();
            foreach (var serviceDescriptor in serviceDescriptors)
            {
                services.Remove(serviceDescriptor);
            }

            services.AddTransient(typeof(TImplementation), (services) => factory(services));
        }
    }
}
