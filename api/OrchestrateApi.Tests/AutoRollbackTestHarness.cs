using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Tests
{
    // TODO Update this to IAsyncDisposable when Xunit 3 is released
    [Collection("Auto rollback tests")] // Required to prevent deadlocks between connections
    public abstract class AutoRollbackTestHarness : IDisposable
    {
        protected AutoRollbackTestHarness()
        {
            TenantService = new StaticTenantService(OrchestrateSeed.TenantId);
            Context = SharedDatabaseFixture.BuildContext(TenantService);
            Transaction = Context.Database.BeginTransaction(IsolationLevel.Snapshot);
        }

        protected ITenantService TenantService { get; }
        protected OrchestrateContext Context { get; }
        protected IDbContextTransaction Transaction { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Transaction.Dispose();
                Context.Dispose();
            }
        }
    }
}
