using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Platform;
using OrchestrateApi.Security;
using OrchestrateApi.Validation.Validators;
using TenantDto = OrchestrateApi.Platform.TenantDto;
using UserDto = OrchestrateApi.Platform.UserDto;

namespace OrchestrateApi.Tests.Platform;

public class RegisterTenantControllerTests : FullIntegrationTestHarness
{
    private readonly RegisterTenantController controller;

    public RegisterTenantControllerTests()
    {
        this.controller = ConstructController<RegisterTenantController>();
        this.controller.VerifyCaptcha = false;
    }

    [Fact]
    public async Task TenantIsCreatedWithExpectedData()
    {
        var configuration = GetService<IConfiguration>();
        configuration["AppConfiguration:NotificationEmail"] = "notification@email.com";
        var result = await this.controller.RegisterTenant(new RegisterTenantDto
        {
            HcaptchaResponse = "unverified",
            RegisteringUser = new UserDto
            {
                Email = "unknown@email.com",
                FirstName = "unknown",
                LastName = "person",
            },
            Tenant = new TenantDto
            {
                Name = "My New Choir",
                Abbreviation = "choir",
                TimeZone = "Australia/Perth",
                Website = "abc.com.au",
            },
        });
        var resultDto = result.Value!;

        Assert.Equal(PostRegisterAction.CheckEmail, resultDto.PostRegisterAction);
        Assert.NotEqual(Guid.Empty, resultDto.Tenant.TenantId);
        Assert.Equal("My New Choir", resultDto.Tenant.Name);
        Assert.Equal("choir", resultDto.Tenant.Abbreviation);
        Assert.Null(resultDto.Tenant.Logo);

        var emailService = GetServiceAs<IEmailService, MockEmailService>();
        Assert.Equal(2, emailService.AllSentEmails.Count);
        var thankYouEmail = Assert.Single(emailService.AllSentEmails, e => e.To?.Address == "unknown@email.com");
        Assert.Equal("Thank you for registering with Orchestrate", thankYouEmail.Subject);
        Assert.Equal("unknown@email.com", thankYouEmail.To?.Address);
        var notificationEmail = Assert.Single(emailService.AllSentEmails, e => e.To?.Address == "notification@email.com");
        Assert.Contains("My New Choir", notificationEmail.Subject);
        Assert.Contains("choir", notificationEmail.HtmlContent);

        using var tenantOverride = TenantService.ForceOverrideTenantId(resultDto.Tenant.TenantId);

        var tenant = await Context.Tenant.SingleAsync();
        Assert.Equal("Australia/Perth", tenant.TimeZone);
        Assert.Equal("abc.com.au", tenant.Website);
        Assert.Equal("unknown@email.com", tenant.ContactAddress);

        Assert.Equal(DateTime.UtcNow, tenant.DateRegistered, TimeSpan.FromSeconds(1));
        Assert.True(tenant.Plan.PeriodEnd.HasValue);
        Assert.Equal(DateTime.UtcNow.AddDays(30), tenant.Plan.PeriodEnd.Value, TimeSpan.FromSeconds(1));

        var member = await Context.Member
            .Include(e => e.Details!.User)
            .SingleAsync();
        Assert.Equal("unknown", member.FirstName);
        Assert.Equal("person", member.LastName);
        Assert.Equal("unknown@email.com", member.Details!.Email);
        Assert.Equal("unknown@email.com", member.Details!.User!.Email);

        var userManager = GetService<UserManager<OrchestrateUser>>();
        Assert.True(await userManager.IsInRoleAsync(member.Details.User, StandardRole.Administrator));
    }
}

public class TenantDtoValidatorTests : AutoRollbackTestHarness
{
    private readonly TenantDtoValidator validator;

    public TenantDtoValidatorTests()
    {
        this.validator = new TenantDtoValidator(Context);
    }

    [Theory]
    [InlineData($" {OrchestrateSeed.GroupName} ", "CaseInsensitiveUniqueValidator")]
    [InlineData("", "NotEmptyValidator")]
    [InlineData(" ", "NotEmptyValidator")]
    public async Task ExistingTenantByNameIsDetected(string groupName, string errorCode)
    {
        using var tenantOverride = TenantService.ForceOverrideTenantId(Guid.NewGuid());

        var results = await this.validator.ValidateAsync(new TenantDto
        {
            Name = groupName,
            Abbreviation = "abc",
            TimeZone = "Australia/Perth",
        });

        var failure = Assert.Single(results.Errors);
        Assert.Equal(nameof(TenantDto.Name), failure.PropertyName);
        Assert.Equal(errorCode, failure.ErrorCode);
    }

    [Theory]
    [InlineData($" {OrchestrateSeed.GroupAbbr} ", "CaseInsensitiveUniqueValidator")]
    [InlineData("", "NotEmptyValidator")]
    [InlineData(" ", "NotEmptyValidator")]
    [InlineData("my abbreviation", "EmailAndUrlSlugValidator")]
    public async Task ExistingTenantByAbbreviationIsDetected(string groupAbbr, string errorCode)
    {
        using var tenantOverride = TenantService.ForceOverrideTenantId(Guid.NewGuid());

        var dto = new TenantDto
        {
            Name = "ABC",
            Abbreviation = groupAbbr,
            TimeZone = "Australia/Perth",
        };
        var results = await this.validator.ValidateAsync(dto);

        var failure = Assert.Single(results.Errors, e => e.ErrorCode == errorCode);
        Assert.Equal(nameof(TenantDto.Abbreviation), failure.PropertyName);
    }

    [Theory]
    [InlineData("Australia/Perth", true)]
    [InlineData("Australia/Invalid", false)]
    public async Task TimeZoneIsValidated(string timeZone, bool isValid)
    {
        var results = await this.validator.ValidateAsync(new TenantDto
        {
            Name = "ABC",
            Abbreviation = "abc",
            TimeZone = timeZone,
        });

        if (isValid)
        {
            Assert.True(results.IsValid);
            Assert.Empty(results.Errors);
        }
        else
        {
            var failure = Assert.Single(results.Errors);
            Assert.Equal(nameof(TenantDto.TimeZone), failure.PropertyName);
        }
    }

    [Fact]
    public async Task NullLogoIsValid()
    {
        var results = await this.validator.ValidateAsync(new TenantDto
        {
            Name = "ABC",
            Abbreviation = "abc",
            TimeZone = "Australia/Perth",
        });

        Assert.True(results.IsValid);
        Assert.Empty(results.Errors);
    }

    [Fact]
    public async Task LargeLogoIsInvalid()
    {
        var results = await this.validator.ValidateAsync(new TenantDto
        {
            Name = "ABC",
            Abbreviation = "abc",
            TimeZone = "Australia/Perth",
            Logo = new byte[TenantDtoValidator.MaxLogoSizeMegabytes * 1024 * 1024 + 1],
        });

        var failure = Assert.Single(results.Errors, e => e.ErrorCode == TenantDtoValidator.LogoTooLargeCode);
        Assert.Equal(nameof(TenantDto.Logo), failure.PropertyName);
    }

    [Fact]
    public async Task InvalidImageLogoIsInvalid()
    {
        var results = await this.validator.ValidateAsync(new TenantDto
        {
            Name = "ABC",
            Abbreviation = "abc",
            TimeZone = "Australia/Perth",
            Logo = new byte[] { 0x01, 0x02, 0x03, 0x04 },
        });

        var failure = Assert.Single(results.Errors);
        Assert.Equal(nameof(TenantDto.Logo), failure.PropertyName);
        Assert.Equal(TenantDtoValidator.LogoInvalidImageCode, failure.ErrorCode);
    }

    [Fact]
    public async Task ImageLogoIsValid()
    {
        var results = await this.validator.ValidateAsync(new TenantDto
        {
            Name = "ABC",
            Abbreviation = "abc",
            TimeZone = "Australia/Perth",
            Logo = TestHelper.TestImage.AsByteArray(),
        });

        Assert.True(results.IsValid);
        Assert.Empty(results.Errors);
    }
}
