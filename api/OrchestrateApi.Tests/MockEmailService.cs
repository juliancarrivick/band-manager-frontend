using OrchestrateApi.Email;

namespace OrchestrateApi.Tests
{
    public class MockEmailService : IEmailService
    {
        public IList<EmailMessage> AllSentEmails { get; } = new List<EmailMessage>();
        public EmailMessage? LastEmail { get; private set; }

        public EmailResultStatus NextResultStatus { get; set; } = EmailResultStatus.Success;
        public string NextResultMessage { get; set; } = string.Empty;

        public EmailMessageAddress DefaultFrom { get; } = new("mock@email.com");

        public IDisposable TemporarilyDisableEmail() => new MockOverride();

        public async Task<EmailResult> SendAsync(EmailMessage email)
        {
            LastEmail = email;
            AllSentEmails.Add(email);
            return await Task.FromResult(new EmailResult
            {
                Email = email,
                Status = NextResultStatus,
                StatusMessage = NextResultMessage,
            });
        }

        public async Task<IList<EmailResult>> SendAsync(IList<EmailMessage> emails)
        {
            foreach (var email in emails)
            {
                AllSentEmails.Add(email);
            }

            LastEmail = emails.LastOrDefault();

            var results = emails.Select(e => new EmailResult
            {
                Email = e,
                Status = NextResultStatus,
                StatusMessage = NextResultMessage,
            });
            return await Task.FromResult(results.ToList());
        }

        public Task ProcessEventsAsync(IList<EmailEvent> events)
        {
            return Task.CompletedTask;
        }

        private sealed class MockOverride : IDisposable
        {
            public void Dispose()
            {
                // Noop
            }
        }
    }
}
