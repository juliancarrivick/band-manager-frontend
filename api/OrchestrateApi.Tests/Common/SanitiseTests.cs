using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common;

public class SanitiserTests
{
    [Theory]
    [InlineData(null, "")]
    [InlineData("", "")]
    [InlineData(" ", "")]
    [InlineData("\r\n", "")]
    [InlineData(" test \r\n", "test")]
    public void TestStringToEmpty(string? input, string expected)
    {
        Assert.Equal(expected, Sanitiser.StringToEmpty(input));
    }

    [Theory]
    [InlineData("", "")]
    [InlineData(" ", "")]
    [InlineData("\r\n", "")]
    [InlineData(" test \r\n", "test")]
    public void TestOptionalStringToEmpty(string input, string expected)
    {
        Assert.Equal(new Optional<string>(), Sanitiser.StringToEmpty(new Optional<string>()));
        Assert.Equal(
            new Optional<string>(expected),
            Sanitiser.StringToEmpty(new Optional<string>(input)));
    }

    [Theory]
    [InlineData(null, null)]
    [InlineData("", null)]
    [InlineData(" ", null)]
    [InlineData("\r\n", null)]
    [InlineData(" test \r\n", "test")]
    public void TestStringToNull(string? input, string? expected)
    {
        Assert.Equal(expected, Sanitiser.StringToNull(input));
    }

    [Theory]
    [InlineData(null, null)]
    [InlineData("", null)]
    [InlineData(" ", null)]
    [InlineData("\r\n", null)]
    [InlineData(" test \r\n", "test")]
    public void TestOptionalStringToNull(string? input, string? expected)
    {
        Assert.Equal(new Optional<string?>(), Sanitiser.StringToNull(new Optional<string?>()));
        Assert.Equal(
            new Optional<string?>(expected),
            Sanitiser.StringToNull(new Optional<string?>(input)));
    }
}
