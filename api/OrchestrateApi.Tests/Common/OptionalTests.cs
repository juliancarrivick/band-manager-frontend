using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using System.Text.Json.Serialization.Metadata;
using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common;

public class OptionalTests
{
    [Fact]
    public void EqualityWorksAsExpected()
    {
        Assert.True(Equals(new Optional<int>(5), 5));
        Assert.False(Equals(5, new Optional<int>(5))); // Can't control wrapped type
        Assert.True(new Optional<int>(5) == 5);
        Assert.True(5 == new Optional<int>(5));
        Assert.False(new Optional<int>(5) != 5);
        Assert.False(5 != new Optional<int>(5));
        Assert.Equal(new Optional<int>(5).GetHashCode(), 5.GetHashCode());

        Assert.False(Equals(new Optional<int>(6), 5));
        Assert.False(Equals(5, new Optional<int>(6))); // Can't control wrapped type
        Assert.False(new Optional<int>(6) == 5);
        Assert.False(5 == new Optional<int>(6));
        Assert.True(new Optional<int>(6) != 5);
        Assert.True(5 != new Optional<int>(6));
        Assert.NotEqual(new Optional<int>(6).GetHashCode(), 5.GetHashCode());

        Assert.Equal(new Optional<int>(), new Optional<int>());
        Assert.False(Equals(new Optional<int>(), new Optional<string>()));
    }

    [Fact]
    public void AccessIsCorrectlyGatedByHasValue()
    {
        Assert.True(new Optional<int>(5).HasValue);
        Assert.False(new Optional<int>().HasValue);
        Assert.Equal(5, new Optional<int>(5).Value);
        Assert.Throws<InvalidOperationException>(() => new Optional<int>().Value);
    }

    [Fact]
    public void UnboxedValuesAreImplicitlyConverted()
    {
        Optional<int> v = 5;
        Assert.True(v.HasValue);
        Assert.Equal(5, v.Value);
    }
}

public class OptionalJsonConverterTests
{
    private readonly JsonSerializerOptions options = new JsonSerializerOptions
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        Converters = { new OptionalJsonConverter() },
        TypeInfoResolver = new DefaultJsonTypeInfoResolver
        {
            Modifiers = { OptionalJsonConverter.IgnoreUnsetOptionalValues },
        },
    };

    [Fact]
    public void OptionalValueIsCorrectlyParsed()
    {
        Assert.Equal(new Optional<int>(5), Deserialise<int>("5"));
        Assert.Throws<JsonException>(() => Deserialise<int>("null"));
        Assert.Equal(new Optional<int?>(null), Deserialise<int?>("null"));

        Assert.Equal(new Optional<string>("test"), Deserialise<string>("\"test\""));
        Assert.Null(Deserialise<string>("null").Value); // Nullable reference types are only enforced at compile time
        Assert.Equal(new Optional<string?>(null), Deserialise<string?>("null"));
    }

    [Fact]
    public void OptionalPropertiesForValueTypesAreCorrectlyParsed()
    {
        var dto1 = JsonSerializer.Deserialize<Dto<int>>("""{"prop": 5}""", options);
        Assert.NotNull(dto1);
        Assert.True(dto1.Prop.HasValue);
        Assert.Equal(5, dto1.Prop.Value);
        dto1 = JsonSerializer.Deserialize<Dto<int>>("{}", options);
        Assert.NotNull(dto1);
        Assert.False(dto1.Prop.HasValue);

        var dto2 = JsonSerializer.Deserialize<Dto<int?>>("""{"prop": null}""", options);
        Assert.NotNull(dto2);
        Assert.True(dto2.Prop.HasValue);
        Assert.Null(dto2.Prop.Value);
        dto2 = JsonSerializer.Deserialize<Dto<int?>>("{}", options);
        Assert.NotNull(dto2);
        Assert.False(dto1.Prop.HasValue);
    }

    [Fact]
    public void OptionalPropertiesForReferenceTypesAreCorrectlyParsed()
    {
        var dto1 = JsonSerializer.Deserialize<Dto<string>>("""{"prop": "test"}""", options);
        Assert.NotNull(dto1);
        Assert.True(dto1.Prop.HasValue);
        Assert.Equal("test", dto1.Prop.Value);
        dto1 = JsonSerializer.Deserialize<Dto<string>>("""{"prop": null}""", options);
        Assert.NotNull(dto1);
        Assert.True(dto1.Prop.HasValue);
        Assert.Null(dto1.Prop.Value); // NRTs are only enforced at compile time
        dto1 = JsonSerializer.Deserialize<Dto<string>>("{}", options);
        Assert.NotNull(dto1);
        Assert.False(dto1.Prop.HasValue);

        var dto2 = JsonSerializer.Deserialize<Dto<string?>>("""{"prop": "test"}""", options);
        Assert.NotNull(dto2);
        Assert.True(dto2.Prop.HasValue);
        Assert.Equal("test", dto2.Prop.Value);
        dto2 = JsonSerializer.Deserialize<Dto<string?>>("""{"prop": null}""", options);
        Assert.NotNull(dto2);
        Assert.True(dto2.Prop.HasValue);
        Assert.Null(dto2.Prop.Value); // NRTs are only enforced at compile time
        dto2 = JsonSerializer.Deserialize<Dto<string?>>("{}", options);
        Assert.NotNull(dto2);
        Assert.False(dto2.Prop.HasValue);
    }

    [Fact]
    public void UnsetOptionalValuesShouldNotBeSerialised()
    {
        Assert.Equal("{}", JsonSerializer.Serialize(new Dto<int>(), options));
        Assert.Equal("""{"prop":5}""", JsonSerializer.Serialize(new Dto<int> { Prop = 5 }, options));
    }

    [Fact]
    public void JsonConverterWithoutTypeInfoResolverModifierThrows()
    {
        Assert.Throws<InvalidOperationException>(() =>
        {
            JsonSerializer.Serialize(new Dto<int>(), new JsonSerializerOptions
            {
                Converters = { new OptionalJsonConverter() },
            });
        });
    }

    private Optional<T> Deserialise<T>(string value) => JsonSerializer.Deserialize<Optional<T>>(value, options);

    [SuppressMessage("", "CA1812")]
    private sealed class Dto<T>
    {
        public Optional<T> Prop { get; set; }
    }
}

public class OptionalMapperTests
{
    private readonly AutoMapper.MapperConfiguration configuration;
    private readonly AutoMapper.IMapper mapper;

    public OptionalMapperTests()
    {
        this.configuration = new(cfg =>
        {
            cfg.CreateMap<Optional<int>, int>().ConvertUsing(new OptionalMapper<int>());
            cfg.CreateMap<Optional<string>, string>().ConvertUsing(new OptionalMapper<string>());
            cfg.CreateMap(typeof(Dto<>), typeof(Entity<>));
        });
        this.mapper = this.configuration.CreateMapper();
    }

    [Fact]
    public void ValueIsExtractedIfSet()
    {
        var i = this.mapper.Map(
            new Dto<int> { Prop = new Optional<int>(5) },
            new Entity<int> { Prop = 6 });
        Assert.Equal(5, i.Prop);

        i = this.mapper.Map(
            new Dto<int>(),
            new Entity<int> { Prop = 6 });
        Assert.Equal(6, i.Prop);

        var s = this.mapper.Map(
            new Dto<string> { Prop = new Optional<string>("opt") },
            new Entity<string> { Prop = "default" });
        Assert.Equal("opt", s.Prop);

        s = this.mapper.Map(
            new Dto<string>(),
            new Entity<string> { Prop = "default" });
        Assert.Equal("default", s.Prop);

        var os = this.mapper.Map(
            new Dto<string?> { Prop = new Optional<string?>(null) },
            new Entity<string?> { Prop = "default" });
        Assert.Null(os.Prop);

        os = this.mapper.Map(
            new Dto<string?>(),
            new Entity<string?> { Prop = "default" });
        Assert.Equal("default", os.Prop);
    }

    private sealed class Entity<T>
    {
        public T Prop { get; set; } = default!;
    }

    private sealed class Dto<T>
    {
        public Optional<T> Prop { get; set; }
    }
}
