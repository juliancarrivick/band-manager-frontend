using System.Security.Claims;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Common
{
    public class ClaimsPrincipalExtensionsTests
    {
        private readonly ClaimsPrincipal user = CreateUserWithClaims(
            new Claim(ClaimTypes.Role, new TenantRole(Guid.Empty, "Viewer").Serialise()),
            new Claim(ClaimTypes.Role, new TenantRole(Guid.Empty, "Editor").Serialise()),
            new Claim(ClaimTypes.NameIdentifier, "some@user.com"));

        [Theory]
        [InlineData(FeaturePermission.PlatformAccess, true)]
        [InlineData(FeaturePermission.AssetRead, true)]
        [InlineData(FeaturePermission.AssetWrite, true)]
        [InlineData(FeaturePermission.MemberBillingWrite, false)]
        [InlineData(FeaturePermission.PlatformSecurityAccess, false)]
        public void HasPermissionInTenentWorksAsExpected(FeaturePermission permission, bool hasPermission)
        {
            Assert.Equal(hasPermission, user.HasPermissionInTenant(Guid.Empty, permission));
        }

        [Fact]
        public void RolesAreReturnedAsExpected()
        {
            var roles = this.user.RolesInTenant(Guid.Empty);
            Assert.Equal(2, roles.Count);
            Assert.Contains("Viewer", roles);
            Assert.Contains("Editor", roles);
        }

        [Fact]
        public void GlobalAdminGetsAdministratorRoleEvenIfGivenDirectRolesByTenant()
        {
            var user = CreateUserWithClaims(
                new Claim(ClaimsPrincipalExtensions.GlobalAdminClaimName, bool.TrueString),
                new Claim(ClaimTypes.Role, new TenantRole(Guid.Empty, "Viewer").Serialise()),
                new Claim(ClaimTypes.Role, new TenantRole(Guid.Empty, "Editor").Serialise()),
                new Claim(ClaimTypes.NameIdentifier, "some@user.com"));
            Assert.Contains(StandardRole.Administrator, user.RolesInTenant(Guid.Empty));
        }

        [Fact]
        public void RolesWithNullUserReturnsNothing()
        {
            ClaimsPrincipal? user = null;
            Assert.Empty(user!.RolesInTenant(Guid.Empty));
        }

        [Fact]
        public void NullUserHasAccessToNoTenants()
        {
            ClaimsPrincipal? user = null;
            Assert.Empty(user!.AccessibleTenants());
        }

        [Fact]
        public void SingleAccessibleTenantIsListed()
        {
            Assert.Single(this.user.AccessibleTenants());
            Assert.Equal(Guid.Empty, this.user.AccessibleTenants().First());
        }

        [Fact]
        public void MultipleAccessibleTenantsAreListed()
        {
            var tenantId1 = Guid.NewGuid();
            var tenantId2 = Guid.NewGuid();
            var user = CreateUserWithClaims(
                new Claim(ClaimTypes.Role, new TenantRole(tenantId1, "Viewer").Serialise()),
                new Claim(ClaimTypes.Role, new TenantRole(tenantId2, "Viewer").Serialise()),
                new Claim(ClaimTypes.NameIdentifier, "some@user.com"));

            var tenants = user.AccessibleTenants().ToList();
            Assert.Equal(2, tenants.Count);
            Assert.Contains(tenantId1, tenants);
            Assert.Contains(tenantId2, tenants);
        }

        [Fact]
        public void NullUserIsNotGlobalAdmin()
        {
            ClaimsPrincipal? user = null;
            Assert.False(user!.IsGlobalAdmin());
        }

        [Theory]
        [InlineData("False", false)] // bool.FalseString
        [InlineData("Garbage", false)]
        [InlineData(null, false)]
        [InlineData("True", true)] // bool.TrueString
        public void OnlyTrueStringAllowsGlobalAdmin(string? claimValue, bool isGlobalAdmin)
        {
            var claims = claimValue != null
                ? new[] { new Claim(ClaimsPrincipalExtensions.GlobalAdminClaimName, claimValue) }
                : Array.Empty<Claim>();
            var user = CreateUserWithClaims(claims);
            Assert.Equal(isGlobalAdmin, user.IsGlobalAdmin());
        }

        [Fact]
        public void UserIdIsExpected()
        {
            var user = CreateUserWithClaims();
            Assert.Throws<InvalidOperationException>(() => user.UserId());

            var id = Guid.NewGuid();
            user = CreateUserWithClaims(new Claim(ClaimTypes.NameIdentifier, id.ToString()));
            Assert.Equal(id, user.UserId());
        }

        [Fact]
        public void UserNameIsExpected()
        {
            var user = CreateUserWithClaims();
            Assert.Throws<InvalidOperationException>(() => user.Username());

            user = CreateUserWithClaims(new Claim(ClaimTypes.Name, "abc"));
            Assert.Equal("abc", user.Username());
        }

        [Theory]
        [InlineData("A", "B", "A B")]
        [InlineData("A", null, "A")]
        [InlineData("A", "", "A")]
        [InlineData(null, "B", "B")]
        [InlineData("", "B", "B")]
        [InlineData(null, null, "abc")]
        [InlineData("", "", "abc")]
        public void DisplayNameGeneratesAndFallsBackToUsername(string? first, string? last, string expected)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "abc"),
            };

            if (first != null)
            {
                claims.Add(new Claim(ClaimTypes.GivenName, first));
            }

            if (last != null)
            {
                claims.Add(new Claim(ClaimTypes.Surname, last));
            }

            var user = CreateUserWithClaims(claims.ToArray());
            Assert.Equal(expected, user.DisplayName());
        }

        private static ClaimsPrincipal CreateUserWithClaims(params Claim[] claims)
        {
            return new ClaimsPrincipal(new ClaimsIdentity(claims));
        }
    }
}
