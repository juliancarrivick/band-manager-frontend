using System.Text;
using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common
{
    public class EmbeddedFileTests
    {
        private const string TestFileContents = "Embedded File Contents\n";

        [Fact]
        public void AsStringRetrievesCorrectly()
        {
            var embeddedFile = new EmbeddedFile(typeof(EmbeddedFileTests), "Resources.EmbeddedFile.txt");
            Assert.Equal(TestFileContents, embeddedFile.AsString());
        }

        [Fact]
        public void AsByteArrayRetrievesCorrectly()
        {
            var embeddedFile = new EmbeddedFile(typeof(EmbeddedFileTests), "Resources.EmbeddedFile.txt");
            Assert.Equal(Encoding.UTF8.GetBytes(TestFileContents), embeddedFile.AsByteArray());
        }
    }
}
