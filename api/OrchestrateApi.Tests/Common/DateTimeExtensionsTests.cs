using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common
{
    public class DateTimeExtensionsTests
    {
        [Fact]
        public void ConvertsToTimeZoneCorrectly()
        {
            var source = new DateTime(2021, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            Assert.Equal(new DateTime(2021, 1, 1, 8, 0, 0, DateTimeKind.Local), source.InTimeZone("Australia/Perth"));
            Assert.Equal(new DateTime(2021, 1, 1, 10, 0, 0, DateTimeKind.Local), source.InTimeZone("Australia/Brisbane"));
            Assert.Equal(new DateTime(2021, 1, 1, 11, 0, 0, DateTimeKind.Local), source.InTimeZone("Australia/Melbourne"));
        }

        [Fact]
        public void ThrowsForUnknownTimezone()
        {
            Assert.Throws<TimeZoneNotFoundException>(() => DateTime.UtcNow.InTimeZone("Unknown/Location"));
            Assert.Throws<TimeZoneNotFoundException>(() => DateTime.Now.ToUtcFromTimeZone("Unknown/Location"));
        }

        [Fact]
        public void ThrowsForNonUtcDateTime()
        {
            Assert.Throws<ArgumentException>(() => DateTime.Now.InTimeZone("Europe/Berlin"));
            Assert.Throws<ArgumentException>(() => new DateTime(2021, 1, 1, 0, 0, 0, DateTimeKind.Unspecified).InTimeZone("Europe/Berlin"));
        }

        [Fact]
        public void ConvertsToUtcCorrectly()
        {
            var source = new DateTime(2021, 1, 1, 11, 0, 0, DateTimeKind.Unspecified);
            Assert.Equal(new DateTime(2021, 1, 1, 3, 0, 0, DateTimeKind.Utc), source.ToUtcFromTimeZone("Australia/Perth"));
            Assert.Equal(new DateTime(2021, 1, 1, 1, 0, 0, DateTimeKind.Utc), source.ToUtcFromTimeZone("Australia/Brisbane"));
            Assert.Equal(new DateTime(2021, 1, 1, 0, 0, 0, DateTimeKind.Utc), source.ToUtcFromTimeZone("Australia/Melbourne"));
        }

        [Fact]
        public void ThrowsForSpecifiedKindOfDateTime()
        {
            Assert.Throws<ArgumentException>(() => DateTime.Now.ToUtcFromTimeZone("Europe/Berlin"));
            Assert.Throws<ArgumentException>(() => new DateTime(2021, 1, 1, 0, 0, 0, DateTimeKind.Utc).ToUtcFromTimeZone("Europe/Berlin"));
        }

        [Fact]
        public void FromUnixTimestampBuildsCorrectly()
        {
            Assert.Equal(new DateTime(1969, 12, 31, 23, 59, 50), DateTimeExtensions.FromUnixTimestamp(-10));
            Assert.Equal(DateTime.UnixEpoch, DateTimeExtensions.FromUnixTimestamp(0));
            Assert.Equal(new DateTime(2021, 06, 26, 4, 25, 28), DateTimeExtensions.FromUnixTimestamp(1624681528));

        }
    }
}
