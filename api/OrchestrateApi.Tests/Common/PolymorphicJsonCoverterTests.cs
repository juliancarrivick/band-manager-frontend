using System.Text.Json;
using System.Text.Json.Serialization;
using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common;

[System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1812")]
public class PolymorphicJsonConverterTests
{
    [Theory]
    [InlineData(""" [] """)]
    [InlineData(""" {type} """)]
    [InlineData(""" {"type"} """)]
    [InlineData(""" {"type": 1} """)]
    public void InvalidJsonWillThrow(string json)
    {
        Assert.Throws<JsonException>(() => JsonSerializer.Deserialize<Base>(json));
    }

    [Fact]
    public void NullIsAllowed()
    {
        Assert.Null(JsonSerializer.Deserialize<Base>("null"));
    }

    [Fact]
    public void DescriminatorThatRefersToBaseTypeShouldThrow()
    {
        var json = """ {"type":"base"} """;
        Assert.Throws<InvalidOperationException>(() => JsonSerializer.Deserialize<Base2>(json));
    }

    [Theory]
    [InlineData("a", "A")]
    [InlineData("c", "C")]
    [InlineData("unknown", "D")]
    public void DeserialisesToTheCorrectSubType(string type, string property)
    {
        var json = $$""" {"type":"{{type}}"} """;
        var obj = JsonSerializer.Deserialize<Base>(json);
        Assert.NotNull(obj);
        Assert.Equal(type, obj.Type);
        Assert.Equal(property, obj!.Property);
    }

    [Fact]
    public void SelectTargetTypeThatReturnsNonInheritedTypeThrows()
    {
        Assert.Throws<InvalidOperationException>(() => JsonSerializer.Deserialize<Base>(""" {"type":"b"} """)); ;
    }

    [Fact]
    public void SelectTargetTypeThatReturnsBaseTypeThrows()
    {
        Assert.Throws<InvalidOperationException>(() => JsonSerializer.Deserialize<Base2>(""" {"type":"base"} """));
    }

    private sealed class TestConverter : PolymorphicJsonConverter<Base>
    {
        protected override string DiscriminatorProperty => "type";

        protected override Type SelectTargetType(string? discriminator)
        {
            return discriminator switch
            {
                "a" => typeof(A),
                "b" => typeof(B),
                "c" => typeof(C),
                _ => typeof(D),
            };
        }
    }

    [JsonConverter(typeof(TestConverter))]
    private abstract class Base
    {
        [JsonPropertyName("type")]
        public string Type { get; set; } = null!;

        public virtual string Property => "Base";
    }

    private sealed class A : Base
    {
        public override string Property => "A";
    }

    private sealed class B { }

    private sealed class C : Base
    {
        public override string Property => "C";
    }

    private sealed class D : Base
    {
        public override string Property => "D";
    }

    [JsonConverter(typeof(InvalidConverter))]
    private abstract class Base2
    {
        [JsonPropertyName("type")]
        public string Type { get; set; } = null!;

        public virtual string Property => "Base2";
    }

    private sealed class InvalidConverter : PolymorphicJsonConverter<Base2>
    {
        protected override string DiscriminatorProperty => "type";

        protected override Type SelectTargetType(string? discriminator) => typeof(Base2);
    }
}
