using FluentValidation;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Validation;
using OrchestrateApi.Validation.Validators;

namespace OrchestrateApi.Tests.Validation;

public class CaseInsensitiveUniqueValidatorTests : AutoRollbackTestHarness
{
    [Theory]
    [InlineData(" Test Asset ", false)]
    [InlineData("TeSt AsSeT", false)]
    [InlineData("test asset", false)]
    [InlineData("TEST ASSET", false)]
    [InlineData("Another asset", true)]
    public async Task CorrectlyValidatesUnique(string value, bool isValid)
    {
        Context.Add(new Asset { Description = "Test Asset" });
        await Context.SaveChangesAsync();

        var v = new AssetDtoValidator();
        v.RuleFor(e => e.Description).CaseInsensitiveUnique(
            Context.Asset, e => e.Description, "Custom Message");

        var result = await v.ValidateAsync(new AssetDto { Description = value });
        Assert.Equal(isValid, result.IsValid);
        if (!isValid)
        {
            var error = Assert.Single(result.Errors);
            Assert.Equal("CaseInsensitiveUniqueValidator", error.ErrorCode);
            Assert.Equal("Custom Message", error.ErrorMessage);
        }
    }

    [Fact]
    public async Task IdSpecifiedWontBeConsidered()
    {
        var entry = Context.Add(new Asset { Description = "Asset 1" });
        Context.Add(new Asset { Description = "Asset 2" });
        await Context.SaveChangesAsync();

        var v = new AssetDtoValidator();
        v.RuleFor(e => e.Description).SetAsyncValidator(new CaseInsensitiveUniqueValidator<AssetDto, Asset>(
            entry.Entity.Id, Context.Asset, e => e.Id, e => e.Description));

        Assert.True((await v.ValidateAsync(new AssetDto { Description = "Asset 1" })).IsValid);
        Assert.False((await v.ValidateAsync(new AssetDto { Description = "Asset 2" })).IsValid);
    }

    private sealed class AssetDto
    {
        public required string Description { get; set; }
    }

    private sealed class AssetDtoValidator : AbstractValidator<AssetDto>
    {
    }
}
