using FluentValidation;
using OrchestrateApi.Validation;

namespace OrchestrateApi.Tests.Validation;

public class EmailAndUrlSlugValidatorTests
{
    [Theory]
    [InlineData(" test-slug ", false)]
    [InlineData("test#slug", false)]
    [InlineData("test+slug", false)]
    [InlineData("test-slug", true)]
    [InlineData("testslug2", true)]
    public void CorrectlyValidatesUnique(string value, bool isValid)
    {
        var v = new AssetDtoValidator();
        v.RuleFor(e => e.Description).EmailAndUrlSlug();

        var result = v.Validate(new AssetDto { Description = value });
        Assert.Equal(isValid, result.IsValid);
        if (!isValid)
        {
            var error = Assert.Single(result.Errors);
            Assert.Equal("EmailAndUrlSlugValidator", error.ErrorCode);
        }
    }

    private sealed class AssetDto
    {
        public required string Description { get; set; }
    }

    private sealed class AssetDtoValidator : AbstractValidator<AssetDto>
    {
    }
}
