using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace OrchestrateApi.Tests;

public static class ActionResultExtensions
{
    public static int? GetStatusCode<T>(this ActionResult<T> actionResult)
    {
        IConvertToActionResult convert = actionResult;
        var statusCodeResult = convert.Convert() as IStatusCodeActionResult;
        return statusCodeResult?.StatusCode;
    }

    public static int? GetStatusCode(this IActionResult actionResult)
    {
        var statusCodeResult = actionResult as IStatusCodeActionResult;
        return statusCodeResult?.StatusCode;
    }
}
