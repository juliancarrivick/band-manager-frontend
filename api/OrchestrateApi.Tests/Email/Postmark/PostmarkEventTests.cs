using OrchestrateApi.Email;
using OrchestrateApi.Email.Postmark;

using static OrchestrateApi.Email.Postmark.PostmarkService;

namespace OrchestrateApi.Tests.Email.Postmark;

public class PostmarkEventTests
{
    [Theory]
    [InlineData(typeof(PostmarkDeliveryEvent), typeof(DeliveredEmailEvent))]
    [InlineData(typeof(PostmarkOpenEvent), typeof(OpenedEmailEvent))]
    [InlineData(typeof(PostmarkBounceEvent), typeof(UndeliverableEmailEvent))]
    [InlineData(typeof(PostmarkSpamComplaintEvent), typeof(UndeliverableEmailEvent))]
    [InlineData(typeof(PostmarkSubscriptionChangeEvent), typeof(SubscriptionChangeEmailEvent))]
    [InlineData(typeof(PostmarkUnknownEvent), typeof(UnknownEmailEvent))]
    public void ConvertsToEmailEventCorrectly(Type postmarkEventType, Type emailEventType)
    {
        var postmarkEvent = (PostmarkEvent)postmarkEventType
            .GetConstructor(Type.EmptyTypes)!
            .Invoke([]);
        var emailEvent = postmarkEvent.ToEmailEvent();
        Assert.IsType(emailEventType, emailEvent);
    }

    [Fact]
    public void CorrectlyBuildsDeliveredEmailEvent()
    {
        var now = DateTime.UtcNow;
        var postmarkEvent = new PostmarkDeliveryEvent
        {
            Recipient = "john@example.com",
            DeliveredAt = now,
            Metadata = {
                ["key"] = "value",
            }
        };

        var emailEvent = postmarkEvent.ToEmailEvent();

        Assert.Same(postmarkEvent, emailEvent.OriginalEvent);
        Assert.Equal("john@example.com", emailEvent.Recipient);
        Assert.Equal(now, emailEvent.DateTime);
        Assert.Equal("value", emailEvent.Metadata["key"]);
    }

    [Fact]
    public void CorrectlyBuildsOpenedEmailEvent()
    {
        var now = DateTime.UtcNow;
        var postmarkEvent = new PostmarkOpenEvent
        {
            Recipient = "john@example.com",
            ReceivedAt = now,
            Metadata = {
                ["key"] = "value",
            }
        };

        var emailEvent = postmarkEvent.ToEmailEvent();

        Assert.Same(postmarkEvent, emailEvent.OriginalEvent);
        Assert.Equal("john@example.com", emailEvent.Recipient);
        Assert.Equal(now, emailEvent.DateTime);
        Assert.Equal("value", emailEvent.Metadata["key"]);
    }

    [Fact]
    public void CorrectlyBuildsEmailEventFromBounceEvent()
    {
        var now = DateTime.UtcNow;
        var postmarkEvent = new PostmarkBounceEvent
        {
            Type = "Fake",
            Name = "Fake",
            Email = "john@example.com",
            BouncedAt = now,
            Description = "Some bounce description",
            Details = "Bounce details",
            Metadata = {
                ["key"] = "value",
            }
        };

        var emailEvent = postmarkEvent.ToEmailEvent();

        Assert.Same(postmarkEvent, emailEvent.OriginalEvent);
        Assert.Equal("john@example.com", emailEvent.Recipient);
        Assert.Equal(now, emailEvent.DateTime);
        Assert.Equal("Some bounce description - Bounce details", emailEvent.Details);
        Assert.Equal("value", emailEvent.Metadata["key"]);
    }

    [Fact]
    public void CorrectlyBuildsEmailEventFromSpamEvent()
    {
        var now = DateTime.UtcNow;
        var postmarkEvent = new PostmarkSpamComplaintEvent
        {
            Type = "Spam",
            Name = "Spam",
            Email = "john@example.com",
            BouncedAt = now,
            Description = "Some spam description",
            Details = "Spam details",
            Metadata = {
                ["key"] = "value",
            }
        };

        var emailEvent = postmarkEvent.ToEmailEvent();

        Assert.Same(postmarkEvent, emailEvent.OriginalEvent);
        Assert.Equal("john@example.com", emailEvent.Recipient);
        Assert.Equal(now, emailEvent.DateTime);
        Assert.Equal("Some spam description - Spam details", emailEvent.Details);
        Assert.Equal("value", emailEvent.Metadata["key"]);
    }

    [Theory]
    [InlineData(HardBounceSuppressionReason, true, "An email to this recipient failed to be delivered")]
    [InlineData(SpamComplaintSuppressionReason, false, "The recipient marked an email from this mailing list as spam")]
    [InlineData(ManualSuppressionReason, true, "The recipient unsubscribed from this mailing list")]
    [InlineData("unknown reason", true, "The recipient unsubscribed from this mailing list")]
    [InlineData(null, true, "")]
    public void CorrectlyBuildsEmailEventFromSubscriptionChangeEvent(
        string? suppressionReason, bool isChangable, string details)
    {
        var now = DateTime.UtcNow;
        var postmarkEvent = new PostmarkSubscriptionChangeEvent
        {
            Recipient = "john@example.com",
            ChangedAt = now,
            SuppressSending = true,
            SuppressionReason = suppressionReason,
            Metadata = {
                ["key"] = "value",
            }
        };

        var emailEvent = postmarkEvent.ToEmailEvent();

        var subChangeEvent = Assert.IsType<SubscriptionChangeEmailEvent>(emailEvent);
        Assert.Same(postmarkEvent, subChangeEvent.OriginalEvent);
        Assert.Equal("john@example.com", subChangeEvent.Recipient);
        Assert.Equal(now, subChangeEvent.DateTime);
        Assert.False(subChangeEvent.IsSubscribed);
        Assert.Equal(isChangable, subChangeEvent.IsChangable);
        Assert.Equal("value", subChangeEvent.Metadata["key"]);
        Assert.Equal(details, subChangeEvent.Details);
    }

    [Fact]
    public void CorrectlyBuildsUnknownEmailEvent()
    {
        var now = DateTime.UtcNow;
        var postmarkEvent = new PostmarkUnknownEvent
        {
            Metadata = {
                ["key"] = "value",
            }
        };

        var emailEvent = postmarkEvent.ToEmailEvent();

        Assert.Same(postmarkEvent, emailEvent.OriginalEvent);
        Assert.Equal(string.Empty, emailEvent.Recipient);
        Assert.Equal(now, emailEvent.DateTime, TimeSpan.FromSeconds(1));
        Assert.Equal("value", emailEvent.Metadata["key"]);
    }
}
