using System.Globalization;
using System.Text.Json;
using OrchestrateApi.Common;
using OrchestrateApi.Email.Postmark;

namespace OrchestrateApi.Tests.Email.Postmark;

public class PostmarkEventJsonConverterTests
{
    // These files adapted from https://postmarkapp.com/developer/webhooks/webhooks-overview
    [Theory]
    [InlineData("PostmarkDeliveryEvent.json", typeof(PostmarkDeliveryEvent))]
    [InlineData("PostmarkOpenEvent.json", typeof(PostmarkOpenEvent))]
    [InlineData("PostmarkClickEvent.json", typeof(PostmarkUnknownEvent))]
    [InlineData("PostmarkBounceEvent.json", typeof(PostmarkBounceEvent))]
    [InlineData("PostmarkSpamEvent.json", typeof(PostmarkSpamComplaintEvent))]
    [InlineData("PostmarkSubscriptionChangeEvent.json", typeof(PostmarkSubscriptionChangeEvent))]
    public void CorrectlyConvertsToTheRightType(string jsonFileName, Type expectedType)
    {
        var jsonFile = new EmbeddedFile(typeof(PostmarkEventJsonConverterTests), $"Resources.{jsonFileName}");
        var parsedEvent = JsonSerializer.Deserialize<PostmarkEvent>(jsonFile.AsString());
        Assert.IsType(expectedType, parsedEvent);
    }

    [Fact]
    public void CorrectlyMapsDeliveryEvent()
    {
        var sentJsonFile = new EmbeddedFile(typeof(PostmarkEventJsonConverterTests), "Resources.PostmarkDeliveryEvent.json");
        var parsedEvent = JsonSerializer.Deserialize<PostmarkDeliveryEvent>(sentJsonFile.AsString())!;

        AssertStandardProperties(parsedEvent);
        Assert.Equal("john@example.com", parsedEvent.Recipient);
        Assert.Equal(ParseToUtc("2019-11-05T16:33:54.9070259Z"), parsedEvent.DeliveredAt);
    }

    [Fact]
    public void CorrectlyMapsOpenEvent()
    {
        var sentJsonFile = new EmbeddedFile(typeof(PostmarkEventJsonConverterTests), "Resources.PostmarkOpenEvent.json");
        var parsedEvent = JsonSerializer.Deserialize<PostmarkOpenEvent>(sentJsonFile.AsString())!;

        AssertStandardProperties(parsedEvent);
        Assert.Equal("john@example.com", parsedEvent.Recipient);
        Assert.Equal(ParseToUtc("2019-11-05T16:33:54.9070259Z"), parsedEvent.ReceivedAt);
    }

    [Fact]
    public void CorrectlyMapsBounceEvent()
    {
        var sentJsonFile = new EmbeddedFile(typeof(PostmarkEventJsonConverterTests), "Resources.PostmarkBounceEvent.json");
        var parsedEvent = JsonSerializer.Deserialize<PostmarkBounceEvent>(sentJsonFile.AsString())!;

        AssertStandardProperties(parsedEvent);
        Assert.Equal("HardBounce", parsedEvent.Type);
        Assert.Equal("Hard bounce", parsedEvent.Name);
        Assert.Equal("The server was unable to deliver your message (ex: unknown user, mailbox not found).", parsedEvent.Description);
        Assert.Equal("john@example.com", parsedEvent.Email);
        Assert.Equal(ParseToUtc("2019-11-05T16:33:54.9070259Z"), parsedEvent.BouncedAt);
    }

    [Fact]
    public void CorrectlyMapsSpamEvent()
    {
        var sentJsonFile = new EmbeddedFile(typeof(PostmarkEventJsonConverterTests), "Resources.PostmarkSpamEvent.json");
        var parsedEvent = JsonSerializer.Deserialize<PostmarkSpamComplaintEvent>(sentJsonFile.AsString())!;

        AssertStandardProperties(parsedEvent);
        Assert.Equal("SpamComplaint", parsedEvent.Type);
        Assert.Equal("Spam complaint", parsedEvent.Name);
        Assert.Equal("The subscriber explicitly marked this message as spam.", parsedEvent.Description);
        Assert.Equal("Test spam complaint details", parsedEvent.Details);
        Assert.Equal("john@example.com", parsedEvent.Email);
        Assert.Equal(ParseToUtc("2019-11-05T16:33:54.9070259Z"), parsedEvent.BouncedAt);
    }

    [Fact]
    public void CorrectlyMapsClickEvent()
    {
        var sentJsonFile = new EmbeddedFile(typeof(PostmarkEventJsonConverterTests), "Resources.PostmarkClickEvent.json");
        var parsedEvent = JsonSerializer.Deserialize<PostmarkUnknownEvent>(sentJsonFile.AsString())!;

        AssertStandardProperties(parsedEvent);
    }

    [Fact]
    public void CorrectlyMapsSubscriptionChangedEvent()
    {
        var sentJsonFile = new EmbeddedFile(typeof(PostmarkEventJsonConverterTests), "Resources.PostmarkSubscriptionChangeEvent.json");
        var parsedEvent = JsonSerializer.Deserialize<PostmarkUnknownEvent>(sentJsonFile.AsString())!;

        AssertStandardProperties(parsedEvent);
    }

    private static DateTime ParseToUtc(string datetime)
    {
        return DateTime.Parse(datetime, CultureInfo.InvariantCulture).ToUniversalTime();
    }

    private static void AssertStandardProperties(PostmarkEvent e)
    {
        Assert.Equal(new Guid("883953f4-6105-42a2-a16a-77a8eac79483"), e.MessageId);
        Assert.Equal("a_value", e.Metadata["a_key"]);
        Assert.Equal("b_value", e.Metadata["b_key"]);
    }
}

