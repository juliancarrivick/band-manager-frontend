using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Email.Postmark;
using OrchestrateApi.Security;
using Postmark.Model.MessageStreams;
using PostmarkDotNet;

namespace OrchestrateApi.Tests.Email.Postmark;

public class NonProductionPostmarkApiServiceTests : AutoRollbackTestHarness
{
    private readonly PostmarkApiService service;
    private readonly MockAppConfiguration mockAppConfiguration;
    private readonly MockHostEnvironment mockHostEnvironment;

    public NonProductionPostmarkApiServiceTests()
    {
        this.mockHostEnvironment = new MockHostEnvironment();
        this.mockAppConfiguration = new MockAppConfiguration
        {
            PostmarkAccountToken = "abc",
            PostmarkServerToken = "def",
        };
        this.service = new(
            Context,
            this.mockAppConfiguration,
            this.mockHostEnvironment,
            TestHelper.BuildMockLogger<PostmarkApiService>());
    }

    [Theory]
    [InlineData("Development")]
    [InlineData("Staging")]
    public async Task DefaultServerWillBeUsedInNonProduction(string environmentName)
    {
        this.mockHostEnvironment.EnvironmentName = environmentName;

        Assert.Null(this.service.GetTenantClient(null));

        IEnumerable<IPostmarkAdapter?> expectedDefaultClients = [
            this.service.GetTenantClient(new Tenant()),
            await this.service.GetOrCreateClientAsync(null),
            await this.service.GetOrCreateClientAsync(new Tenant()),
        ];
        foreach (var client in expectedDefaultClients)
        {
            var auth = Assert.IsAssignableFrom<IPostmarkAuth>(client);
            Assert.Equal("def", auth.Token);
        }
    }

    [Theory]
    [InlineData("Development")]
    [InlineData("Staging")]
    public async Task DefaultBroadcastStreamWillBeUsedInNonProduction(string environmentName)
    {
        this.mockHostEnvironment.EnvironmentName = environmentName;

        var streamId = await this.service.GetOrCreateMailingListStreamIdAsync(
            new MockPostmarkAdapter(),
            new MailingList { Name = "ML", Slug = "ml" });
        Assert.Equal("broadcast", streamId);
    }

    [Fact]
    public async Task OnlyDefaultClientWillBeProvidedInNonProduction()
    {
        var clients = await this.service.GetAllClients();
        var client = Assert.Single(clients);
        var auth = Assert.IsAssignableFrom<IPostmarkAuth>(client);
        Assert.Equal("def", auth.Token);
    }
}

public class LivePostmarkApiServiceTests : AutoRollbackTestHarness, IClassFixture<TestServerFixture>
{
    public const string SkipText = "Don't hit the API during normal testing!";

    public static readonly IEnumerable<string?> Skips = [
        CreateServerSkip,
        CreateStreamSkip,
    ];

    private readonly TestServerFixture fixture;
    private readonly PostmarkApiService service;
    private readonly MockHostEnvironment mockHostEnvironment;

    public LivePostmarkApiServiceTests(TestServerFixture fixture)
    {
        this.mockHostEnvironment = new MockHostEnvironment { EnvironmentName = Environments.Production };
        this.fixture = fixture;
        this.service = new(
            Context,
            TestHelper.AppConfig,
            this.mockHostEnvironment,
            TestHelper.BuildMockLogger<PostmarkApiService>())
        {
            ServerDeliveryType = "Sandbox",
        };
    }

    public const string? CreateServerSkip = SkipText;
    [Theory(Skip = CreateServerSkip)]
    [InlineData(null, "Development")]
    [InlineData("Tenant", "Tenant")]
    public async Task ServerIsCreatedSuccessfully(string? tenantName, string expectedServerName)
    {
        var tenant = tenantName is not null ? new Tenant { Name = tenantName } : null;
        var client = await service.GetOrCreateClientAsync(tenant);
        var concreteClient = Assert.IsAssignableFrom<PostmarkClient>(client);

        var server = await concreteClient.GetServerAsync();
        Assert.Equal(expectedServerName, server.Name);

        if (tenantName is not null)
        {
            // This will fail, we don't have permissions to delete right now
            await fixture.AdminClient.DeleteServerAsync(server.ID);
        }
    }

    public const string? CreateStreamSkip = SkipText;
    [Fact(Skip = CreateStreamSkip)]
    public async Task MessageStreamAndWebhookGetCreated()
    {
        var mailingList = new MailingList { Name = "ML", Slug = "ml" };
        var streamId = await service.GetOrCreateMailingListStreamIdAsync(fixture.Client, mailingList);

        Assert.Equal(mailingList.Slug, mailingList.PostmarkStreamId);
        Assert.Equal(mailingList.Slug, streamId);

        var stream = await fixture.Client.GetMessageStream(streamId);
        Assert.Equal(MessageStreamType.Broadcasts, stream.MessageStreamType);

        var response = await fixture.Client.GetWebhookConfigurationsAsync(stream.ID);
        var webhook = Assert.Single(response.Webhooks.ToList());
        Assert.Equal(UserService.ApiUsername, webhook.HttpAuth.Username);
        Assert.True(webhook.Triggers.Bounce.Enabled);
        Assert.True(webhook.Triggers.SpamComplaint.Enabled);
        Assert.True(webhook.Triggers.SubscriptionChange.Enabled);
    }
}

public sealed class TestServerFixture : IAsyncLifetime
{
    public string ApiToken { get; private set; } = null!;
    public PostmarkAdapter Client { get; private set; } = null!;
    public PostmarkAdminClient AdminClient { get; private set; } = null!;
    private int ServerId { get; set; }

    public async Task InitializeAsync()
    {
        if (!LivePostmarkApiServiceTests.Skips.Any(s => s is null))
        {
            return;
        }

        AdminClient = new(TestHelper.AppConfig.PostmarkAccountToken);

        var server = await AdminClient.CreateServerAsync($"UnitTest{Guid.NewGuid()}", deliveryType: "Sandbox");
        ApiToken = server.ApiTokens.First();
        Client = new(ApiToken);
        ServerId = server.ID;
    }

    public async Task DisposeAsync()
    {
        if (AdminClient is not null)
        {
            // This will fail, we don't have permissions to delete right now
            await AdminClient.DeleteServerAsync(ServerId);
        }
    }
}
