using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email.Postmark;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Email.Postmark;

public class PostmarkServiceTests : AutoRollbackTestHarness
{
    private readonly PostmarkService service;
    private readonly MockPostmarkApiService mockApiService;

    public PostmarkServiceTests()
    {
        this.mockApiService = new MockPostmarkApiService();
        this.service = new(Context, this.mockApiService, TestHelper.BuildMockLogger<PostmarkService>());
    }

    [Fact]
    public async Task MissingPostmarkSuppressionsAreAddedToMailingListSuppressions()
    {
        var mailingList = await SetupForPostmark();
        mailingList.Suppressions.Add(new MailingListSuppression
        {
            Email = "existing@email.com",
            Reason = "Unsubscribe",
        });
        await Context.SaveChangesAsync();

        // Postmark parses as Local so make sure we can handle that
        var now = DateTime.Now;
        this.mockApiService.MockAdapter.SuppressionList = [new()
        {
            EmailAddress = "new@email.com",
            CreatedAt = now,
            SuppressionReason = PostmarkService.SpamComplaintSuppressionReason,
        }];
        await this.service.AddAllMissingSuppressions();

        Assert.Equal(2, mailingList.Suppressions.Count);
        Assert.Single(mailingList.Suppressions, e => e.Email == "existing@email.com");
        var suppression = Assert.Single(mailingList.Suppressions, e => e.Email == "new@email.com");
        Assert.Equal(now.ToUniversalTime(), suppression.AddedAt);
        Assert.False(suppression.IsRemovable);
        Assert.Contains("spam", suppression.Reason);
    }

    [Fact]
    public async Task ThrowInListSuppressionsDoesntCrash()
    {
        var mailingList = await SetupForPostmark();

        this.mockApiService.MockAdapter.ThrowOnListSuppressions = true;
        this.mockApiService.MockAdapter.SuppressionList = [new()
        {
            EmailAddress = "new@email.com",
            CreatedAt = DateTime.Now,
            SuppressionReason = PostmarkService.SpamComplaintSuppressionReason,
        }];
        await this.service.AddAllMissingSuppressions();

        Assert.Empty(mailingList.Suppressions);
    }

    [Fact]
    public async Task NotFoundStreamDoesntRaiseException()
    {
        this.mockApiService.MockAdapter.ThrowValidationExceptionOnDeletion = true;
        var tenant = await Context.GetTenantAsync();
        Assert.NotNull(tenant);
        tenant.PostmarkServerToken = "abc";
        await Context.SaveChangesAsync();

        await this.service.RemoveSuppressions(
            new MailingList { Name = "ML", Slug = "ml", PostmarkStreamId = "def" },
            ["test@email.com"]);

        Assert.NotNull(this.mockApiService.MockAdapter.LastSuppressionDeletions);
        Assert.Equal(
            ["test@email.com"],
            this.mockApiService.MockAdapter.LastSuppressionDeletions.Select(e => e.EmailAddress));
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public async Task DeleteMailingListIsHandledAppropriately(bool archiveThrows)
    {
        this.mockApiService.MockAdapter.ThrowOnArchive = archiveThrows;

        var mailingList = await SetupForPostmark();
        await service.DeleteMailingListStreamAsync(mailingList);

        Assert.Null(mailingList.PostmarkStreamId);
    }

    [Fact]
    public async Task UpdateWebhookApiUserPasswordsIteratesThroughClients()
    {
        this.mockApiService.MockAdapter.WebhookConfigurations = [
            new() { ID = 1, HttpAuth = new() { Username = UserService.ApiUsername, Password = "abc" } },
            new() { ID = 2, HttpAuth = new() { Username = "not-api", Password = "def" } },
            new() { ID = 3, HttpAuth = null },
        ];
        this.mockApiService.TenantClients = [new MockPostmarkAdapter
        {
            WebhookConfigurations = [
                new() { ID = 4, HttpAuth = new() { Username = UserService.ApiUsername, Password = "abc" } },
            ],
        }];

        await service.UpdateWebhookApiUserPasswords("xyz");

        var editedConfig = Assert.Single(this.mockApiService.MockAdapter.EditedWebhookConfigurations);
        Assert.Equal(1, editedConfig.ID);
        Assert.Equal("xyz", editedConfig.HttpAuth.Password);

        editedConfig = Assert.Single(this.mockApiService.TenantClients[0].EditedWebhookConfigurations);
        Assert.Equal(4, editedConfig.ID);
        Assert.Equal("xyz", editedConfig.HttpAuth.Password);
    }

    private async Task<MailingList> SetupForPostmark(string streamId = "ml")
    {
        var tenant = await Context.GetTenantAsync();
        Assert.NotNull(tenant);

        tenant.PostmarkServerToken = "abc";
        var mailingList = new MailingList { Name = "ML", Slug = "ml", PostmarkStreamId = streamId };
        Context.Add(mailingList);
        await Context.SaveChangesAsync();

        return mailingList;
    }
}
