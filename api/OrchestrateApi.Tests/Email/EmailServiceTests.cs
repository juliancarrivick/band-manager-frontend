using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;

namespace OrchestrateApi.Tests.Email
{
    public class EmailServiceTests : AutoRollbackTestHarness
    {
        private readonly MockHostEnvironment mockHostEnvironment;
        private readonly MockAppConfiguration mockAppConfiguration;
        private readonly MockEmailProvider mockEmailProvider;
        private readonly List<MockEmailEventHandler> mockEventHandlers;
        private readonly EmailService emailService;

        public EmailServiceTests()
        {
            this.mockHostEnvironment = new MockHostEnvironment();
            this.mockEmailProvider = new MockEmailProvider { NextSendStatus = EmailResultStatus.Success };
            this.mockAppConfiguration = new MockAppConfiguration { EmailProvider = this.mockEmailProvider.Name };
            this.mockEventHandlers = new List<MockEmailEventHandler> { new MockEmailEventHandler() };
            this.emailService = new EmailService(
                this.mockHostEnvironment,
                this.mockAppConfiguration,
                Context,
                new[] { this.mockEmailProvider },
                this.mockEventHandlers,
                TestHelper.BuildMockLogger<EmailService>())
            {
                // We've overridden the provider, so we know no emails will be sent, and we need
                // to exercise all the other logic for tests!
                DropNonProdEmailsWithoutOverrideOrAllowlist = false,
                DropDemoEmails = false,
            };
        }

        [Theory]
        [InlineData("Mock", "Mock")]
        [InlineData("MOCK", "Mock")]
        [InlineData(" Mock ", "Mock")]
        [InlineData("Stub", "Stub")]
        [InlineData("STUB", "Stub")]
        [InlineData(" Stub ", "Stub")]
        [InlineData("Unknown", null)]
        public void WillThrowIfUnableToFindAppropriateProvider(string nameToCheck, string? provider)
        {
            var config = new MockAppConfiguration { EmailProvider = nameToCheck };
            var buildEmailService = () => new EmailService(
                this.mockHostEnvironment,
                config,
                Context,
                new IEmailProvider[] { new MockEmailProvider(), new StubEmailProvider() },
                Array.Empty<IEmailEventHandler>(),
                TestHelper.BuildMockLogger<EmailService>());

            if (provider != null)
            {
                var emailService = buildEmailService();
                Assert.Equal(provider, emailService.ActiveProvider);
            }
            else
            {
                Assert.Throws<InvalidOperationException>(buildEmailService);
            }
        }

        [Fact]
        public async Task WillOverrideToAddressIfSetInConfig()
        {
            this.mockAppConfiguration.ToEmailOverride = "test@email.com";

            var email = new EmailMessage();
            var result = await this.emailService.SendAsync(email);

            Assert.Equal(this.mockAppConfiguration.ToEmailOverride, result.Email.To?.Address);
        }

        [Theory]
        [InlineData(false, null, EmailResultStatus.Success)]
        [InlineData(false, "test.other@to.com", EmailResultStatus.Success)]
        [InlineData(true, "", EmailResultStatus.Error)]
        [InlineData(true, "test.other@to.com", EmailResultStatus.Error)]
        [InlineData(true, "test@to.com", EmailResultStatus.Success)]
        [InlineData(true, " test@to.com ", EmailResultStatus.Success)]
        public async Task WillClearEmailIfNotOnWhitelist(bool whitelistEnabled, string? whitelist, EmailResultStatus expectedSendResult)
        {
            var email = ValidMessage("test@to.com");

            this.mockAppConfiguration.EmailWhitelistEnabled = whitelistEnabled;
            this.mockAppConfiguration.EmailWhitelist = string.IsNullOrEmpty(whitelist)
                ? []
                : [whitelist];
            var result = await this.emailService.SendAsync(email);
            Assert.Equal(expectedSendResult, result.Status);
            if (expectedSendResult == EmailResultStatus.Error)
            {
                Assert.Contains("list of allowed", result.StatusMessage);
            }
        }

        [Fact]
        public async Task WillNotSendEmailIfToAddressIsNotSet()
        {
            var email = new EmailMessage { From = new("test-from@email.com"), TextContent = "Test" };
            var result = await this.emailService.SendAsync(email);

            Assert.Equal(EmailResultStatus.Error, result.Status);
            Assert.Contains("no to address", result.StatusMessage);
            Assert.Empty(this.mockEmailProvider.LastSentEmails);
        }

        [Fact]
        public async Task WillNotFillOutFromNameFromConfigIfAddressSet()
        {
            this.mockAppConfiguration.DefaultFromName = "Test Name";

            var email = new EmailMessage { From = new("test-from@email.com"), To = new("test-to@email.com") };
            await this.emailService.SendAsync(email);

            Assert.Null(email.From.Name);
        }

        [Fact]
        public async Task WillFillOutFromAddressFromConfigIfNotSet()
        {
            this.mockAppConfiguration.DefaultFromEmail = "test-from@email.com";

            var email = new EmailMessage { To = new("test-to@email.com") };
            var result = await this.emailService.SendAsync(email);

            Assert.Equal(this.mockAppConfiguration.DefaultFromEmail, result.Email.From?.Address);
        }

        [Fact]
        public async Task WillNotSentEmailIfFromAddressIsNotSet()
        {
            var email = new EmailMessage { To = new("test-to@email.com"), TextContent = "test" };
            var result = await this.emailService.SendAsync(email);

            Assert.Equal(EmailResultStatus.Error, result.Status);
            Assert.Contains("no from address", result.StatusMessage);
            Assert.Empty(this.mockEmailProvider.LastSentEmails);
        }

        [Fact]
        public async Task SkippedEmailsAreNotAttemptedToBeSentButValidOnesAreStillSent()
        {
            var emails = new List<EmailMessage>
            {
                new EmailMessage{From = new("test-from@email.com"), TextContent = "Test"},
                new EmailMessage{To = new("test-to@email.com"), TextContent = "Test"},
                ValidMessage(),
            };

            var results = await this.emailService.SendAsync(emails);

            var noToResult = results.First(e => e.Email == emails[0]);
            Assert.Equal(EmailResultStatus.Error, noToResult.Status);
            var noFromResult = results.First(e => e.Email == emails[1]);
            Assert.Equal(EmailResultStatus.Error, noFromResult.Status);
            var sentResult = results.First(e => e.Email == emails[2]);
            Assert.Equal(EmailResultStatus.Success, sentResult.Status);

            Assert.Single(this.mockEmailProvider.LastSentEmails);
            Assert.Equal(this.mockEmailProvider.LastSentEmails[0], emails[2]);
        }

        [Fact]
        public async Task WillPropogateFailuresFromEmailProviderCorrectly()
        {
            this.mockEmailProvider.NextSendStatus = EmailResultStatus.Error;

            var email = ValidMessage();
            var result = await this.emailService.SendAsync(email);

            Assert.Equal(EmailResultStatus.Error, result.Status);
            Assert.Single(this.mockEmailProvider.LastSentEmails);
            Assert.Equal(this.mockEmailProvider.LastSentEmails[0], email);
        }

        [Fact]
        public async Task IfNoEmailsPassedWontCallProvider()
        {
            var result = await this.emailService.SendAsync(Array.Empty<EmailMessage>());
            Assert.Empty(result);
            Assert.False(this.mockEmailProvider.SendCalled);
        }

        [Theory]
        [InlineData("Production", false)]
        [InlineData("Staging", true)]
        [InlineData("Development", true)]
        [InlineData("Other", true)]
        public async Task AdjustsEmailsInNonProdEnvironment(string environment, bool adjusts)
        {
            var email = new EmailMessage
            {
                Subject = "Test Subject",
                TextContent = "Test text content",
                HtmlContent = "<p>Test HTML content</p>",
            };
            this.mockHostEnvironment.EnvironmentName = environment;

            await this.emailService.SendAsync(email);

            if (adjusts)
            {
                Assert.Equal($"<{environment}> Test Subject", email.Subject);
                var expectedTextContent = $"*** THIS EMAIL IS FROM A TEST ENVIRONMENT ***\n"
                            + "If you are not expecting this email, please ignore its contents.\n"
                            + "---\n\n"
                            + "Test text content";
                Assert.Equal(expectedTextContent, email.TextContent);
                var expectedHtmlContent = $"<p><strong>THIS EMAIL IS FROM A TEST ENVIRONMENT</strong><br/>"
                            + "If you are not expecting this email, please ignore its contents.</p><hr/>"
                            + "<p>Test HTML content</p>";
                Assert.Equal(expectedHtmlContent, email.HtmlContent);
            }
            else
            {
                Assert.Equal("Test Subject", email.Subject);
                Assert.Equal("Test text content", email.TextContent);
                Assert.Equal("<p>Test HTML content</p>", email.HtmlContent);
            }
        }

        [Fact]
        public async Task SendFailsIfNoContent()
        {
            var email = new EmailMessage
            {
                From = new("from@email.com"),
                To = new("to@email.com"),
            };

            var result = await this.emailService.SendAsync(email);

            Assert.Equal(EmailResultStatus.Error, result.Status);
            Assert.Contains("no content", result.StatusMessage);
        }

        [Fact]
        public async Task AddsStandardMetadataToEmail()
        {
            this.mockHostEnvironment.EnvironmentName = "Unit Test";
            var email = new EmailMessage { Type = "Unit Test Email" };

            await this.emailService.SendAsync(email);

            Assert.Equal("Unit Test", email.Metadata[EmailService.EmailEnvironmentMetadataKey]);
            Assert.Equal("Unit Test Email", email.Metadata[EmailService.EmailTypeKey]);
            Assert.Equal(OrchestrateSeed.TenantId.ToString(), email.Metadata[EmailService.TenantIdKey]);
            Assert.Equal("Motley Crue", email.Metadata[EmailService.TenantNameKey]);
        }

        [Fact]
        public async Task DoesntAddTenantMetadataToEmailWithNoTenant()
        {
            using var tenantOverride = TenantService.ForceOverrideTenantId(null);

            var email = new EmailMessage { Type = "Unit Test Email" };
            await this.emailService.SendAsync(email);

            Assert.False(email.Metadata.ContainsKey(EmailService.TenantIdKey));
            Assert.False(email.Metadata.ContainsKey(EmailService.TenantNameKey));
        }

        [Fact]
        public async Task WillFilterEventsThatDontMatchCurrentEnvironment()
        {
            this.mockHostEnvironment.EnvironmentName = "UnitTest";
            var unitTestEvent = BuildEventFromEnvironment("UnitTest");
            var events = new[]
            {
                BuildEventFromEnvironment(null),
                BuildEventFromEnvironment("Development"),
                BuildEventFromEnvironment("Staging"),
                BuildEventFromEnvironment("Production"),
                unitTestEvent,
            };

            await this.emailService.ProcessEventsAsync(events);

            var handler = this.mockEventHandlers[0];
            Assert.NotNull(handler.LastEvents);
            Assert.Single(handler.LastEvents);
            Assert.Single(handler.LastEvents, unitTestEvent);
        }

        [Fact]
        public async Task WillFilterUnknownEvents()
        {
            var unknownEmailEvent = new UnknownEmailEvent();
            await this.emailService.ProcessEventsAsync(new[] { unknownEmailEvent });
            Assert.False(this.mockEventHandlers[0].WasCalled);
            Assert.Null(this.mockEventHandlers[0].LastEvents);

            var knownEmailEvents = typeof(EmailEvent).Assembly
                .GetTypes()
                .Where(t => !t.IsAbstract && typeof(EmailEvent).IsAssignableFrom(t))
                .Where(t => t != typeof(UnknownEmailEvent))
                .Select(t => t.GetConstructor(Type.EmptyTypes))
                .Select(c => (EmailEvent)c!.Invoke(Array.Empty<object>()))
                .ToList();
            foreach (var e in knownEmailEvents)
            {
                e.Metadata[EmailService.EmailEnvironmentMetadataKey] = MockHostEnvironment.UnitTestEnvironmentName;
            }

            await this.emailService.ProcessEventsAsync(knownEmailEvents);
            Assert.True(this.mockEventHandlers[0].WasCalled);
            Assert.Equal(knownEmailEvents, this.mockEventHandlers[0].LastEvents);
        }

        [Fact]
        public async Task WontCallHandlersIfNoApplicableEvents()
        {
            await this.emailService.ProcessEventsAsync(Array.Empty<EmailEvent>());
            Assert.False(this.mockEventHandlers[0].WasCalled);

            this.mockHostEnvironment.EnvironmentName = "UnitTest";
            var unitTestEvent = BuildEventFromEnvironment("NotUnitTest");
            await this.emailService.ProcessEventsAsync(new[] { unitTestEvent });
            Assert.False(this.mockEventHandlers[0].WasCalled);
        }

        [Fact]
        public async Task PreventsOneHandlerExceptionFromAffectingTheRest()
        {
            var firstNonThrowingHandler = this.mockEventHandlers[0];
            var throwingHandler = new MockEmailEventHandler { WillThrow = true };
            this.mockEventHandlers.Add(throwingHandler);
            var secondNonThrowingHandler = new MockEmailEventHandler();
            this.mockEventHandlers.Add(secondNonThrowingHandler);

            this.mockHostEnvironment.EnvironmentName = "UnitTest";
            var unitTestEvent = BuildEventFromEnvironment("UnitTest");

            await this.emailService.ProcessEventsAsync(new[] { unitTestEvent });

            Assert.NotNull(firstNonThrowingHandler.LastEvents);
            Assert.Single(firstNonThrowingHandler.LastEvents);
            Assert.Single(firstNonThrowingHandler.LastEvents, unitTestEvent);

            Assert.True(throwingHandler.WasCalled);
            Assert.Null(throwingHandler.LastEvents);

            Assert.NotNull(secondNonThrowingHandler.LastEvents);
            Assert.Single(secondNonThrowingHandler.LastEvents);
            Assert.Single(secondNonThrowingHandler.LastEvents, unitTestEvent);
        }

        [Theory]
        [InlineData("Production", null, null, EmailResultStatus.Success)]
        [InlineData("Staging", null, null, EmailResultStatus.Error)]
        [InlineData("Development", null, null, EmailResultStatus.Error)]
        [InlineData(MockHostEnvironment.UnitTestEnvironmentName, null, null, EmailResultStatus.Error)]
        [InlineData("Staging", "override@email.com", null, EmailResultStatus.Success)]
        [InlineData("Staging", null, "original@email.com", EmailResultStatus.Success)]
        [InlineData("Staging", "override@email.com", "override@email.com", EmailResultStatus.Success)]
        public async Task NonProductionEmailsAreDroppedWithNoOverrides(
            string envName, string? overrideEmail, string? allowEmail, EmailResultStatus expectedResult)
        {
            this.emailService.DropNonProdEmailsWithoutOverrideOrAllowlist = true;
            this.mockHostEnvironment.EnvironmentName = envName;
            if (!string.IsNullOrWhiteSpace(overrideEmail))
            {
                this.mockAppConfiguration.ToEmailOverride = overrideEmail;
            }
            if (!string.IsNullOrWhiteSpace(allowEmail))
            {
                this.mockAppConfiguration.EmailWhitelistEnabled = true;
                this.mockAppConfiguration.EmailWhitelist = new[] { allowEmail };
            }

            var email = ValidMessage("original@email.com");
            var result = await this.emailService.SendAsync(email);
            Assert.Equal(expectedResult, result.Status);

            if (expectedResult == EmailResultStatus.Success)
            {
                Assert.True(this.mockEmailProvider.SendCalled);
                Assert.Single(this.mockEmailProvider.LastSentEmails, email);
            }
            else if (expectedResult == EmailResultStatus.Error)
            {
                Assert.Contains("non-production", result.StatusMessage);
                Assert.False(this.mockEmailProvider.SendCalled);
            }
            else
            {
                Assert.Fail("Unknown result status");
            }
        }

        [Theory]
        [InlineData(true, PlanType.Demo, EmailResultStatus.Error)]
        [InlineData(true, PlanType.Free, EmailResultStatus.Success)]
        [InlineData(false, PlanType.Demo, EmailResultStatus.Success)]
        [InlineData(false, PlanType.Free, EmailResultStatus.Success)]
        public async Task DemoTenantsHaveTheirEmailsDropped(
            bool dropDemoEmails, PlanType planType, EmailResultStatus expectedResult)
        {
            this.emailService.DropDemoEmails = dropDemoEmails;
            var tenant = await Context.GetTenantAsync();
            tenant!.Plan.Type = planType;
            await Context.SaveChangesAsync();

            var email = ValidMessage();
            var result = await this.emailService.SendAsync(email);
            Assert.Equal(expectedResult, result.Status);

            if (expectedResult == EmailResultStatus.Success)
            {
                Assert.True(this.mockEmailProvider.SendCalled);
                Assert.Single(this.mockEmailProvider.LastSentEmails, email);
            }
            else if (expectedResult == EmailResultStatus.Error)
            {
                Assert.Contains("demo tenant", result.StatusMessage);
                Assert.False(this.mockEmailProvider.SendCalled);
            }
            else
            {
                Assert.Fail("Unknown result status");
            }
        }

        private static EmailMessage ValidMessage(string to = "to@email.com") => new EmailMessage
        {
            From = new("from@email.com"),
            To = new(to),
            TextContent = "Some content",
        };

        private static EmailEvent BuildEventFromEnvironment(string? environmentName)
        {
            return new DeliveredEmailEvent
            {
                Metadata = {
                    [EmailService.EmailEnvironmentMetadataKey] = environmentName,
                }
            };
        }

        private sealed class MockEmailProvider : IEmailProvider
        {
            public string Name => "Mock";

            public EmailResultStatus NextSendStatus { get; set; }
            public IList<EmailMessage> LastSentEmails { get; } = new List<EmailMessage>();
            public bool SendCalled { get; set; }

            public async Task<IList<EmailResult>> SendAsync(IList<EmailMessage> emails)
            {
                SendCalled = true;
                foreach (var email in emails)
                {
                    LastSentEmails.Add(email);
                }

                var results = emails.Select(e => new EmailResult
                {
                    Email = e,
                    Status = NextSendStatus,
                    StatusMessage = string.Empty,
                });
                return await Task.FromResult(results.ToList());
            }
        }

        private sealed class MockEmailEventHandler : IEmailEventHandler
        {
            public bool WillThrow { get; set; }
            public IList<EmailEvent>? LastEvents { get; set; }
            public bool WasCalled { get; private set; }

            public Task<int> HandleAsync(IList<EmailEvent> events)
            {
                WasCalled = true;

                if (WillThrow)
                {
                    throw new InvalidOperationException();
                }

                LastEvents = events;
                return Task.FromResult(events.Count);
            }
        }

        private sealed class StubEmailProvider : IEmailProvider
        {
            public string Name => "Stub";

            public Task<IList<EmailResult>> SendAsync(IList<EmailMessage> emails)
            {
                throw new NotImplementedException();
            }
        }
    }
}
