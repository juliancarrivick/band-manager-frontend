using OrchestrateApi.Email;

namespace OrchestrateApi.Tests.Email;

public class SpamComplainEmailEventHanderTests
{
    private readonly MockLogger logger;
    private readonly SpamComplainEmailEventHander handler;

    public SpamComplainEmailEventHanderTests()
    {
        this.logger = new MockLogger();
        this.handler = new SpamComplainEmailEventHander(this.logger);
    }

    [Theory]
    [InlineData(typeof(DeliveredEmailEvent), false)]
    [InlineData(typeof(OpenedEmailEvent), false)]
    [InlineData(typeof(UndeliverableEmailEvent), false)]
    [InlineData(typeof(SpamComplaintEmailEvent), true)]
    [InlineData(typeof(UnknownEmailEvent), false)]
    public async Task OnlyHandlesSpamComplaintEvent(Type emailEventType, bool isHandled)
    {
        var eventConstructor = emailEventType.GetConstructor(Type.EmptyTypes)!;
        var @event = (EmailEvent)eventConstructor.Invoke(Array.Empty<object>());
        @event.Recipient = "test@email.com";

        var numHandled = await this.handler.HandleAsync(new[] { @event });

        if (isHandled)
        {
            Assert.Equal(1, numHandled);
            Assert.Equal(LogLevel.Error, this.logger.LastLog!.Value);
        }
        else
        {
            Assert.Equal(0, numHandled);
            Assert.Null(this.logger.LastLog);
        }
    }

    private sealed class MockLogger : ILogger<SpamComplainEmailEventHander>
    {
        public LogLevel? LastLog { get; set; }

        public IDisposable? BeginScope<TState>(TState state) where TState : notnull => throw new NotImplementedException();

        public bool IsEnabled(LogLevel logLevel) => throw new NotImplementedException();

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
            LastLog = logLevel;
        }
    }
}
