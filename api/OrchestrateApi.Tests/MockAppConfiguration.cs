using OrchestrateApi.Common;

namespace OrchestrateApi.Tests
{
    public class MockAppConfiguration : IAppConfiguration
    {
        public string EmailProvider { get; set; } = string.Empty;
        public string PostmarkAccountToken { get; set; } = string.Empty;
        public string PostmarkServerToken { get; set; } = string.Empty;
        public string DefaultFromEmail { get; set; } = string.Empty;
        public string DefaultFromName { get; set; } = string.Empty;
        public Uri FrontEndUri { get; set; } = new Uri("http://localhost:5200");
        public string NotificationEmail { get; set; } = string.Empty;
        public string ToEmailOverride { get; set; } = string.Empty;
        public bool EmailWhitelistEnabled { get; set; }
        public IEnumerable<string> EmailWhitelist { get; set; } = Enumerable.Empty<string>();
        public TimeSpan TokenLifetime { get; set; }
        public string JwtIssuer { get; set; } = string.Empty;
        public string JwtSecret { get; set; } = string.Empty;
        public string ConnectionString { get; set; } = string.Empty;
        public string StripeSecretKey { get; set; } = string.Empty;
        public string StripeWebhookSecret { get; set; } = string.Empty;
        public string StripeNotForProfitCouponId { get; set; } = string.Empty;
        public bool PeriodicJobCheckEnabled { get; set; }
        public TimeSpan PeriodicJobCheckInterval { get; set; }
        public TimeSpan PeriodicJobMaxExpectedTime { get; set; }
        public TimeSpan BillingPeriodTransitionCheckInterval { get; set; }
        public TimeSpan BillingPeriodUpcomingActionNotificationInterval { get; set; }
        public TimeSpan OrphanedBlobCleanupInterval { get; set; }
        public TimeSpan TrialTimeSpan { get; set; }
        public TimeSpan TrialEndCheckInterval { get; set; }
        public string HcaptchaSiteKey { get; set; } = string.Empty;
        public string HcaptchaSecret { get; set; } = string.Empty;
        public string MailingListApexDomain { get; set; } = string.Empty;
        public int MaxMailingListCount { get; set; }
        public TimeSpan PostmarkSuppressionSyncInterval { get; set; }
        public int DbMaxTransientFailureRetryCount { get; set; }
        public TimeSpan DbTransientFailureTrackingAgeOutPeriod { get; set; }
    }
}
