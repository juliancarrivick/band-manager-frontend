using System.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using OrchestrateApi.DAL;
using OrchestrateApi.Email;
using OrchestrateApi.PaymentProcessor;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests
{
    [Collection("Full integration tests")]
    public abstract class FullIntegrationTestHarness : IDisposable
    {
        private static readonly CustomWebApplicationFactory fixture = new();

        private readonly IServiceScope scope;
        private readonly IDbContextTransaction transaction;

        protected FullIntegrationTestHarness()
        {
            this.scope = fixture.Services.CreateScope();
            Context = this.scope.ServiceProvider.GetRequiredService<OrchestrateContext>();
            this.transaction = Context.Database.BeginTransaction(IsolationLevel.Snapshot);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.transaction.Dispose();
                this.scope.Dispose();
            }
        }

        protected OrchestrateContext Context { get; }

        protected ITenantService TenantService => GetService<ITenantService>();

        protected T GetService<T>() where T : notnull => this.scope.ServiceProvider.GetRequiredService<T>();

        protected TAs GetServiceAs<T, TAs>() where TAs : T where T : notnull => (TAs)GetService<T>();

        protected TImpl GetSpecificService<T, TImpl>() where TImpl : T where T : notnull
            => (TImpl)this.scope.ServiceProvider.GetServices<T>().Single(e => e is TImpl);

        protected T ConstructController<T>() where T : ControllerBase
        {
            var constructor = typeof(T).GetConstructors()[0];
            var parameters = constructor.GetParameters();
            var args = parameters.Select(p => p.ParameterType)
                .Select(this.scope.ServiceProvider.GetRequiredService)
                .ToArray();
            var controller = (T)constructor.Invoke(args);
            return controller;
        }
    }

    // Based off https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-5.0#customize-webapplicationfactory
    public class CustomWebApplicationFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration(builder =>
                {
                    builder.AddConfiguration(TestHelper.RawConfiguration);
                })
                .ConfigureServices(services =>
                {
                    RemoveService(services, typeof(DbContextOptions<OrchestrateContext>));

                    RemoveService(services, typeof(EmailService));
                    services.AddScoped<IEmailService, MockEmailService>();

                    RemoveService(services, typeof(IPostmarkApiService));
                    services.AddScoped<IPostmarkApiService, MockPostmarkApiService>();

                    RemoveServices(services, typeof(IStorageProvider));
                    services.AddScoped<IStorageProvider, MockStorageProvider>();
                    // Don't remove other storage strategies (because we may want to test them)
                    // But add this one for when we want to have mocked behaviour
                    services.AddScoped<IBlobStorageStrategy, MockStorageStrategy>();
                    services.AddScoped<IBlobStorageStrategy, MockLinkedStorageStrategy>();
                    services.AddScoped<IBlobStorageStrategy, MockValidatingStorageStrategy>();

                    RemoveService(services, typeof(HttpContextTenantService));
                    services.AddSingleton<ITenantService>(new StaticTenantService(OrchestrateSeed.TenantId));

                    RemoveService(services, typeof(IPaymentProcessor));
                    services.AddScoped<IPaymentProcessor, MockPaymentProcessor>();

                    services.AddDbContext<OrchestrateContext>(options =>
                    {
                        options.UseNpgsql(TestHelper.AppConfig.ConnectionString, dbOpt =>
                        {
                            dbOpt.ExecutionStrategy(deps => new NonRetryingExecutionStrategy(deps));
                        });
                    });

                    using var scope = services.BuildServiceProvider().CreateScope();
                    var context = scope.ServiceProvider.GetRequiredService<OrchestrateContext>();
                    SharedDatabaseFixture.Seed(context);
                });
        }

        private static void RemoveService(IServiceCollection services, Type serviceType)
        {
            var descriptor = services.SingleOrDefault(d => d.ServiceType == serviceType);
            if (descriptor != null)
            {
                services.Remove(descriptor);
            }
        }

        private static void RemoveServices(IServiceCollection services, Type serviceType)
        {
            var descriptors = services.Where(d => d.ServiceType == serviceType).ToList();
            foreach (var descriptor in descriptors)
            {
                services.Remove(descriptor);
            }
        }
    }
}
