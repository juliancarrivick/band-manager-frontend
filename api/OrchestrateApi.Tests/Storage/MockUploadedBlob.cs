using System.Text;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests.Storage;

public class MockUploadedBlob : IUploadedBlob
{
    public MockUploadedBlob(string name, string mimeType, string contents)
    {
        Name = name;
        MimeType = mimeType;
        Contents = contents;
    }

    public string Name { get; }

    public string MimeType { get; }

    public Stream OpenReadStream() => new MemoryStream(Encoding.UTF8.GetBytes(Contents));

    private string Contents { get; }
}
