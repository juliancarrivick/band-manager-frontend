// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.dev.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// Placeholder configuration file. You can copy and fill out the contents yourself for each
// environment that you desire.

export const environment = {
    production: false,
};
