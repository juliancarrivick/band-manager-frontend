#!/bin/bash

cat > $1 << EOF
{
    "environmentName": "$ENVIRONMENT_NAME",
    "serverEndpoint": "$SERVER_ENDPOINT",
    "sentryDsn": "$SENTRY_DSN",
    "heapAppId": "$HEAP_APP_ID",
    "hcaptchaSiteKey": "$HCAPTCHA_SITEKEY"
}
EOF
