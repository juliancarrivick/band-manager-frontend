import { CommonModule } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { SaveErrorComponent } from "app/common-ux/save-cancel-buttons/save-error.component";
import { AppConfig } from "app/common/app-config";
import { catchErrorOfType } from "app/common/rxjs-utilities";
import { EMPTY, of, switchMap, tap } from "rxjs";

@Component({
    selector: "app-api-user",
    template: `
        <h2>Reset API User Password</h2>
        <form>
            <mat-form-field>
                <mat-label>Password</mat-label>
                <input
                    matInput
                    name="password"
                    type="password"
                    [(ngModel)]="newPassword"
                />
            </mat-form-field>
            <button
                mat-raised-button
                type="submit"
                color="primary"
                [disabled]="!newPassword"
                [asyncClick]="resetPassword"
            >
                Reset Password
            </button>
            <app-save-error
                *ngIf="error"
                [saveError]="error"
            />
        </form>
    `,
    styles: `
        button {
            display: block;
        }

        app-save-error {
            display: block;
            margin-top: 1rem;
        }
    `,
    imports: [
        CommonModule,
        AppButtonModule,
        AppFormModule,
        SaveErrorComponent,
    ],
})
export class ApiUserComponent {
    protected newPassword?: string;
    protected error?: Error;

    public constructor(
        private appConfig: AppConfig,
        private httpClient: HttpClient,
    ) {}

    protected resetPassword = () => {
        this.error = undefined;

        if (!this.newPassword) {
            return of({});
        }

        return this.appConfig.serverEndpoint("administration/reset-api-user-password").pipe(
            switchMap((url) => this.httpClient.post(url, { password: this.newPassword })),
            catchErrorOfType(Error, (e) => {
                this.error = e;
                return EMPTY;
            }),
            tap(() => {
                this.newPassword = undefined;
            }),
        );
    };
}
