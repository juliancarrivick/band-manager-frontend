import { inject } from "@angular/core";
import { CanActivateChildFn, Router } from "@angular/router";
import { AuthenticationService } from "app/auth/authentication.service";
import { AppRoutes } from "app/common/app-routing-types";
import { map } from "rxjs";
import { AdminPageComponent } from "./admin-page/admin-page.component";

const restrictedToGlobalAdminGuard: CanActivateChildFn = (route, state) => {
    const router = inject(Router);
    const authenticationService = inject(AuthenticationService);

    return authenticationService.user$.pipe(
        map((user) => {
            if (!user) {
                return router.parseUrl("/login");
            }

            if (!user.isGlobalAdmin) {
                return router.parseUrl("/group");
            }

            return true;
        }),
    );
};

export const adminRoutes: AppRoutes = [
    {
        path: "",
        component: AdminPageComponent,
        canActivate: [
            restrictedToGlobalAdminGuard,
        ],
    },
];
