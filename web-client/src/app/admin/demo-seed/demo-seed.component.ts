import { CommonModule } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { SaveErrorComponent } from "app/common-ux/save-cancel-buttons/save-error.component";
import { AppConfig } from "app/common/app-config";
import { catchErrorOfType } from "app/common/rxjs-utilities";
import { EMPTY, switchMap, tap } from "rxjs";

interface SeedInfoDto {
    id: string;
    abbreviation: string;
    name: string;
}

@Component({
    selector: "app-demo-seed",
    template: `
        <h2>Single Ensemble Demo Seed</h2>
        <button
            mat-raised-button
            color="primary"
            [asyncClick]="singleEnsembleSeed"
        >
            Seed
        </button>
        <app-save-error
            *ngIf="error"
            [saveError]="error"
        />
        <dl *ngIf="singleEnsembleInfo">
            <dt>Id</dt>
            <dd>{{ singleEnsembleInfo.id }}</dd>
            <dt>Name</dt>
            <dd>
                <a [routerLink]="['../group', singleEnsembleInfo.abbreviation, 'home']">
                    {{ singleEnsembleInfo.name }}
                </a>
            </dd>
        </dl>
    `,
    styles: [
        `
            @use "dl";
            dl {
                @include dl.mobile-friendly-dl();
            }
        `,
    ],
    imports: [
        CommonModule,
        RouterModule,
        AppButtonModule,
        SaveErrorComponent,
    ],
})
export class DemoSeedComponent {
    public singleEnsembleInfo?: SeedInfoDto;
    public error?: Error;

    public constructor(
        private appConfig: AppConfig,
        private httpClient: HttpClient,
    ) {}

    public singleEnsembleSeed = () => {
        this.error = undefined;
        this.singleEnsembleInfo = undefined;

        return this.appConfig.serverEndpoint("demo-seed/single-ensemble").pipe(
            switchMap((url) => this.httpClient.post<SeedInfoDto>(url, {})),
            tap((info) => (this.singleEnsembleInfo = info)),
            catchErrorOfType(Error, (e) => {
                this.error = e;
                return EMPTY;
            }),
        );
    };
}
