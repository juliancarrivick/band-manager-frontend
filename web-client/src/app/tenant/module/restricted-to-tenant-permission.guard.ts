import { inject } from "@angular/core";
import { CanActivateChildFn, Router } from "@angular/router";
import { AppRouteData } from "app/common/app-routing-types";
import { TenantService } from "app/tenant/module/tenant.service";
import { map } from "rxjs/operators";
import { AuthenticationService } from "../../auth/authentication.service";

export const restrictedToTenantPermissionGuard: CanActivateChildFn = (route, state) => {
    const router = inject(Router);
    const authenticationService = inject(AuthenticationService);
    const tenantService = inject(TenantService);

    return authenticationService.user$.pipe(
        map((user) => {
            if (!user) {
                return router.parseUrl("/login");
            }

            const data: AppRouteData = route.data;
            if (!data.restrictedToPermission) {
                return true;
            }

            const tenantAbbreviation = tenantService.getTenantAbbreviationForRoute(route);
            if (!tenantAbbreviation) {
                return tenantService.getDefaultUrlForUser(user);
            }

            const roles = user.getImpliedTenantRolesByAbbreviation(tenantAbbreviation);
            const restrictedToPermission = Array.isArray(data.restrictedToPermission)
                ? data.restrictedToPermission
                : [data.restrictedToPermission];
            if (restrictedToPermission.some((p) => roles.hasPermission(p))) {
                if (route.url.some((segment) => segment.path.includes("login"))) {
                    return router.createUrlTree([tenantAbbreviation, "home"]);
                }

                return true;
            }

            return router.createUrlTree([tenantAbbreviation, "home"]);
        }),
    );
};
