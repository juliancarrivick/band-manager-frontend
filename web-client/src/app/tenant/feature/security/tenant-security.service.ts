import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RoleName } from "app/auth/role.enum";
import { AppConfig } from "app/common/app-config";
import { Observable } from "rxjs";
import { switchMap } from "rxjs/operators";

export enum UserStatus {
    Active = "Active",
    Pending = "Pending",
    NoAccess = "NoAccess",
}

export interface TenantUserDto {
    userId: string;
    username: string;
    memberId: number;
    name: string;
    status: UserStatus;
    roles: RoleName[];
    ensembleRoles: EnsembleRolesDto[];
}

export interface EnsembleRolesDto {
    ensembleId: number;
    ensembleName: string;
    roles: RoleName[];
}

@Injectable({
    providedIn: "root",
})
export class TenantSecurityService {
    private usersEndpoint$: Observable<string>;

    public constructor(
        appConfig: AppConfig,
        private http: HttpClient,
    ) {
        this.usersEndpoint$ = appConfig.serverEndpoint("users");
    }

    public getAllUsers() {
        return this.usersEndpoint$.pipe(
            switchMap((entityEndpoint) => this.http.get<TenantUserDto[]>(entityEndpoint)),
        );
    }

    public addUserToRole(userId: string, roleId: string) {
        return this.usersEndpoint$.pipe(
            switchMap((entityEndpoint) =>
                this.http.post<void>(`${entityEndpoint}/${userId}/role/${roleId}`, ""),
            ),
        );
    }

    public removeUserFromRole(userId: string, roleId: string) {
        return this.usersEndpoint$.pipe(
            switchMap((entityEndpoint) =>
                this.http.delete<void>(`${entityEndpoint}/${userId}/role/${roleId}`),
            ),
        );
    }
}
