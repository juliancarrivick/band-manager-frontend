import { Component, ViewChild } from "@angular/core";
import { ColDef } from "ag-grid-community";
import { roleLabel } from "app/auth/role";
import { RoleName } from "app/auth/role.enum";
import { DataGridComponent } from "app/common-ux/data-grid/data-grid.component";
import { buildEditColumn } from "app/common-ux/data-grid/grid-edit-column";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { filterForDefinedValue } from "app/common/rxjs-utilities";
import { Observable, ReplaySubject, delay, map } from "rxjs";
import { TenantSecurityService, TenantUserDto, UserStatus } from "../tenant-security.service";
import { UserDetailsDialogComponent } from "../user-details-dialog/user-details-dialog.component";
import { UserIsActiveIconRenderer } from "./user-is-active-icon-renderer";

@Component({
    templateUrl: "./users-and-security-page.component.html",
    styleUrls: ["./users-and-security-page.component.scss"],
    standalone: false,
})
export class UsersAndSecurityPageComponent {
    public users$: Observable<TenantUserDto[]>;
    public columns$: Observable<ColDef<TenantUserDto>[]>;

    @ViewChild(DataGridComponent)
    public set dataGrid(v: DataGridComponent<TenantUserDto>) {
        this.dataGrid$.next(v);
    }
    private dataGrid$ = new ReplaySubject<DataGridComponent<TenantUserDto> | null>(1);

    constructor(
        tenantSecurityService: TenantSecurityService,
        private dialogService: DialogService,
    ) {
        this.users$ = tenantSecurityService.getAllUsers();
        this.columns$ = this.dataGrid$.pipe(
            delay(0), // Hack to fix ExpressionChangedAfterCheck
            filterForDefinedValue(),
            map((dataGrid) => this.getColumns(dataGrid)),
        );
    }

    public editUser = (user: TenantUserDto) => {
        return this.dialogService.open(UserDetailsDialogComponent, user, {
            autoFocus: false,
        });
    };

    public getColumns(dataGrid: DataGridComponent<TenantUserDto>): ColDef<TenantUserDto>[] {
        return [
            {
                headerName: "",
                field: "status",
                cellRenderer: UserIsActiveIconRenderer,
                sort: "asc",
                sortIndex: 1,
                comparator: (a: UserStatus, b: UserStatus) => {
                    if (a === UserStatus.NoAccess && b !== UserStatus.NoAccess) {
                        return 1;
                    } else if (a !== UserStatus.NoAccess && b === UserStatus.NoAccess) {
                        return -1;
                    } else {
                        return 0;
                    }
                },
                width: 48,
                cellStyle: { padding: "0", "padding-left": "1rem" },
            },
            {
                field: "name",
                sort: "asc",
                sortIndex: 2,
                flex: 1,
                minWidth: 200,
            },
            {
                headerName: "Email",
                field: "username",
                width: 250,
            },
            {
                headerName: "Roles",
                valueGetter: (params) => {
                    if (!params.data) {
                        return "";
                    }

                    const user: TenantUserDto = params.data;
                    const roles = new Set<RoleName>();
                    for (const role of user.roles) {
                        roles.add(role);
                    }
                    for (const ensemble of user.ensembleRoles) {
                        for (const role of ensemble.roles) {
                            roles.add(role);
                        }
                    }
                    return roles.size > 0
                        ? [...roles]
                              .sort((a, b) => a.localeCompare(b))
                              .map((r) => roleLabel[r])
                              .join(", ")
                        : "No access";
                },
                ...wrapTextColDef(),
            },
            buildEditColumn(dataGrid, [
                {
                    icon: "policy",
                    tooltip: "Details",
                    onClick: this.editUser,
                },
            ]),
        ];
    }
}
