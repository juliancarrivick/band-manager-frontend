import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { UsersAndSecurityPageComponent } from "app/tenant/feature/security/users-and-security-page/users-and-security-page.component";

const userRoutes: AppRoutes = [
    {
        path: "",
        component: UsersAndSecurityPageComponent,
        data: {
            title: "Users & Security",
            icon: "security",
            restrictedToPermission: FeaturePermission.PlatformSecurityAccess,
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(userRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppSecurityRoutingModule {}
