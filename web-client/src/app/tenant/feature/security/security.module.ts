import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatDividerModule } from "@angular/material/divider";
import { AppAuthModule } from "app/auth/auth.module";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { EditRolesComponent } from "./edit-roles/edit-roles.component";
import { AppSecurityRoutingModule } from "./security-routing.module";
import { UserDetailsDialogComponent } from "./user-details-dialog/user-details-dialog.component";
import { UsersAndSecurityPageComponent } from "./users-and-security-page/users-and-security-page.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatCardModule,
        MatDividerModule,
        AppSecurityRoutingModule,
        AppAuthModule,
        AppPageWrapperModule,
        AppDataGridModule,
        AppDialogModule,
        AppFormModule,
        AppButtonModule,
        AppFormModule, // Only really need tag box
    ],
    declarations: [
        UsersAndSecurityPageComponent,
        UserDetailsDialogComponent,
        EditRolesComponent,
    ],
})
export class AppSecurityModule {}
