import { Concert } from "app/dal/model/concert";
import { Ensemble } from "app/dal/model/ensemble";

export class MemberConcert {
    public constructor(
        public readonly concert: Concert,
        public readonly ensembles: Ensemble[],
    ) {}

    public compare(rhs: MemberConcert) {
        return this.concert.date.getTime() - rhs.concert.date.getTime();
    }

    public toPerformanceString() {
        let formattedConcertWithPerformances = this.concert.formattedOccasion;
        formattedConcertWithPerformances += " playing in: ";
        formattedConcertWithPerformances += this.ensembles.map((e) => e.name).join(", ");

        return formattedConcertWithPerformances;
    }
}
