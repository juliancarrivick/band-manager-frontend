import { Component, EventEmitter, Input, OnChanges, Output } from "@angular/core";
import { ColDef, IRowNode } from "ag-grid-community";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { undefinedWhileLoading } from "app/common/rxjs-utilities";
import { areSameArray } from "app/common/utils";
import { AllMemberCategoryMetadata, Member, MemberCategory } from "app/dal/model/member";
import {
    BehaviorSubject,
    Observable,
    ReplaySubject,
    combineLatest,
    distinctUntilChanged,
    map,
} from "rxjs";
import { MemberCategoryIconRenderer } from "../member-table/member-category-icon-renderer";
import { MemberService } from "../member.service";

interface CategorySelection {
    icon: string;
    label: string;
    category: MemberCategory;
    isSelected: boolean;
}

@Component({
    selector: "app-select-members",
    templateUrl: "./select-members.component.html",
    standalone: false,
})
export class SelectMembersComponent implements OnChanges {
    @Input() public selectedMembers?: Member[];
    private selectedMembers$ = new ReplaySubject<Member[]>(1);
    @Output() public selectedMembersChange = new EventEmitter<Member[]>();

    public members$: Observable<Member[] | undefined>;
    public columns = this.getColumns();
    public categorySelection: CategorySelection[];
    private selectedCategories$: BehaviorSubject<MemberCategory[]>;

    public constructor(memberService: MemberService) {
        this.categorySelection = AllMemberCategoryMetadata.map((m) => ({
            icon: m.icon,
            label: m.label,
            category: m.category,
            isSelected: m.category === MemberCategory.Current,
        }));
        this.selectedCategories$ = new BehaviorSubject(this.selectedCategories);

        const membersWithCategory$ = this.selectedCategories$.pipe(
            undefinedWhileLoading((c) => memberService.getMembersByCategoryWithInstruments(c)),
        );
        const otherMembers$ = combineLatest([
            this.selectedCategories$,
            this.selectedMembers$,
        ]).pipe(
            map(([categories, members]) => {
                return categories.length > 0
                    ? members.filter((m) => !categories.includes(m.category))
                    : [];
            }),
            distinctUntilChanged((p, c) => areSameArray(p, c)),
            undefinedWhileLoading((members) =>
                memberService.getMembersByIdWithInstruments(members.map((m) => m.id)),
            ),
        );
        this.members$ = combineLatest([
            membersWithCategory$,
            otherMembers$,
        ]).pipe(
            map(([categoryMembers, otherMembers]) => {
                return categoryMembers && otherMembers
                    ? categoryMembers.concat(otherMembers)
                    : undefined;
            }),
        );
    }

    public ngOnChanges(): void {
        this.selectedMembers$.next(this.selectedMembers ?? []);
    }

    public get selectedCategories() {
        return this.categorySelection.filter((c) => c.isSelected).map((c) => c.category);
    }

    public toggleSelection(category: CategorySelection) {
        category.isSelected = !category.isSelected;
        this.selectedCategories$.next(this.selectedCategories);
    }

    public memberToId(member: Member) {
        return member.id;
    }

    private getColumns(): ColDef[] {
        return [
            {
                cellRenderer: MemberCategoryIconRenderer,
                headerName: "",
                field: "category",
                width: 88,
                cellStyle: { padding: "0", "padding-left": "1rem" },
            },
            {
                headerName: "Name",
                field: "fullName",
                sort: "asc",
                comparator: (
                    nameA: string,
                    nameB: string,
                    nodeA: IRowNode<Member>,
                    nodeB: IRowNode<Member>,
                ) => {
                    const aIsSelected = this.selectedMembers!.includes(nodeA.data!);
                    const bIsSelected = this.selectedMembers!.includes(nodeB.data!);

                    if (aIsSelected && !bIsSelected) {
                        return -1;
                    } else if (bIsSelected && !aIsSelected) {
                        return 1;
                    } else {
                        return nameA.localeCompare(nameB);
                    }
                },
            },
            {
                headerName: "Instruments",
                field: "instrumentList",
                flex: 1,
                minWidth: 200,
                ...wrapTextColDef(),
            },
        ];
    }
}
