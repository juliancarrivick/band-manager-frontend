import { Injectable } from "@angular/core";
import { groupBy } from "app/common/utils";
import { BreezeEntityService } from "app/dal/breeze-domain.service";
import {
    Ensemble,
    EnsembleBreezeModel,
    EnsembleMembershipBreezeModel,
    EnsembleStatus,
} from "app/dal/model/ensemble";
import { EnsembleMembership, EnsembleMembershipType } from "app/dal/model/ensemble-membership";
import {
    Member,
    MemberBreezeModel,
    MemberCategory,
    MemberDetails,
    MemberInstrumentBreezeModel,
} from "app/dal/model/member";
import { PerformanceMember, PerformanceMemberBreezeModel } from "app/dal/model/performance";
import { Predicate } from "breeze-client";
import { of } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { MemberConcert } from "./member-concert";

export interface MemberSummary {
    id: number;
    firstName: string;
    lastName: string;
    performanceCount?: number;

    instruments: string[];
    ensembles: string[];
}

@Injectable({
    providedIn: "root",
})
export class MemberService extends BreezeEntityService<Member> {
    public readonly breezeModel = MemberBreezeModel;

    private readonly entityEndpoint$ = this.apiEndpoint$.pipe(map((e) => `${e}/member`));

    public create() {
        return this.breezeService.create(MemberBreezeModel, {
            dateJoined: new Date(),
            details: new MemberDetails(),
        });
    }

    public createInstrument(member: Member, instrumentName: string) {
        return this.breezeService.create(MemberInstrumentBreezeModel, {
            memberId: member.id,
            instrumentName,
        });
    }

    public getAllActiveEnsembles() {
        return this.breezeService.getByPredicate(
            EnsembleBreezeModel,
            new Predicate("status", "==", EnsembleStatus.Active),
        );
    }

    public getMembershipForMember(member: Member) {
        const query = this.breezeService
            .createQuery(EnsembleMembershipBreezeModel)
            .where(new Predicate("memberId", "==", member.id))
            .expand("ensemble");
        return this.breezeService.executeQuery<EnsembleMembership>(query);
    }

    public addMembership(
        member: Member,
        ensemble: Ensemble,
        properties?: Partial<Pick<EnsembleMembership, "dateJoined" | "dateLeft" | "type">>,
    ) {
        return this.breezeService.create(EnsembleMembershipBreezeModel, {
            memberId: member.id,
            ensembleId: ensemble.id,
            dateJoined: new Date(),
            type: EnsembleMembershipType.Regular,
            ...properties,
        });
    }

    public getCurrentMembers() {
        return this.breezeService.getByPredicate(
            MemberBreezeModel,
            new Predicate("dateLeft", "==", null).or(new Predicate("dateLeft", ">", new Date())),
        );
    }

    public getMembersByCategoryWithInstruments(categories: MemberCategory[]) {
        let query = this.breezeService.createQuery(MemberBreezeModel).expand("memberInstrument");

        const predicate = this.getPredicateForCategories(...categories);
        if (predicate) {
            query = query.where(predicate);
        }

        return this.breezeService.executeQuery<Member>(query);
    }

    public getMembersByIdWithInstruments(otherMemberIds: number[]) {
        if (otherMemberIds.length === 0) {
            return of([]);
        }

        let predicate: Predicate | undefined;
        for (const memberId of otherMemberIds) {
            if (predicate) {
                predicate = predicate.or(new Predicate("id", "==", memberId));
            } else {
                predicate = new Predicate("id", "==", memberId);
            }
        }

        const query = this.breezeService
            .createQuery(MemberBreezeModel)
            .where(predicate)
            .expand("memberInstrument");
        return this.breezeService.executeQuery<Member>(query);
    }

    public getPrimedMembersByCategory(...categories: MemberCategory[]) {
        let query = this.breezeService
            .createQuery(MemberBreezeModel)
            .expand(["details", "memberInstrument", "ensembleMembership.ensemble"]);

        const categoryPredicate = this.getPredicateForCategories(...categories);
        if (categoryPredicate) {
            query = query.where(categoryPredicate);
        }

        return this.breezeService.executeQuery<Member>(query);
    }

    public getMemberSummary() {
        return this.entityEndpoint$.pipe(
            switchMap((entityEndpoint) =>
                this.http.get<MemberSummary[]>(`${entityEndpoint}/summary`),
            ),
        );
    }

    public getCurrentMemberSummary() {
        return this.entityEndpoint$.pipe(
            switchMap((entityEndpoint) =>
                this.http.get<MemberSummary[]>(`${entityEndpoint}/current/summary`),
            ),
        );
    }

    public invite(member: Member) {
        return this.breezeService.customSave({ controller: "member", resource: "invite" }, member);
    }

    public getMemberConcerts(memberId: number) {
        const query = this.breezeService
            .createQuery(PerformanceMemberBreezeModel)
            .where(new Predicate("memberId", "==", memberId))
            .expand("performance.concert, performance.ensemble");
        return this.breezeService.executeQuery<PerformanceMember>(query).pipe(
            map((entities) => groupBy(entities, (e) => e.performance!.concert!)),
            map((tuples) =>
                tuples
                    .map(
                        (t) =>
                            new MemberConcert(
                                t[0],
                                t[1].map((pm) => pm.performance!.ensemble!),
                            ),
                    )
                    .sort((a, b) => b.compare(a)),
            ),
        );
    }

    private getPredicateForCategories(...categories: MemberCategory[]) {
        const now = new Date();
        const categoryQueries: Record<MemberCategory, Predicate> = {
            [MemberCategory.Current]: new Predicate("dateLeft", "==", null).or(
                new Predicate("dateLeft", ">", now),
            ),
            [MemberCategory.Past]: new Predicate("dateLeft", "<", now),
        };

        let predicate: Predicate | undefined;

        if (categories.length !== Object.keys(MemberCategory).length) {
            for (const c of categories) {
                if (predicate) {
                    predicate = predicate.or(categoryQueries[c]);
                } else {
                    predicate = categoryQueries[c];
                }
            }
        }

        return predicate;
    }
}
