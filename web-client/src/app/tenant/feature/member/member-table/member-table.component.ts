import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ColDef, ValueGetterParams } from "ag-grid-community";
import { DataGridComponent } from "app/common-ux/data-grid/data-grid.component";
import { dateColDef } from "app/common-ux/data-grid/grid-date-column";
import { buildEditColumn } from "app/common-ux/data-grid/grid-edit-column";
import { GridEmailLinkRenderer } from "app/common-ux/data-grid/grid-email-link-renderer";
import { columnExportColDef } from "app/common-ux/data-grid/grid-export-options";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { undefinedWhileLoading } from "app/common/rxjs-utilities";
import { filterOutNullOrUndefined, valueIfConditionMet } from "app/common/utils";
import { Member, MemberCategory } from "app/dal/model/member";
import { BehaviorSubject, Observable, ReplaySubject, combineLatest } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { EditMemberDialogComponent } from "../edit-member-dialog/edit-member-dialog.component";
import { MemberService } from "../member.service";
import { MemberCategoryIconRenderer } from "./member-category-icon-renderer";

@Component({
    selector: "app-member-table",
    templateUrl: "./member-table.component.html",
    standalone: false,
})
export class MemberTableComponent {
    @Input() public set allowEditing(value: boolean | null) {
        this.allowEditing$.next(!!value);
    }
    public get allowEditing() {
        return this.allowEditing$.value;
    }
    private allowEditing$ = new BehaviorSubject<boolean>(false);
    @Input() public set categories(value: MemberCategory[] | undefined) {
        this.categories$.next(value);
    }
    public categories$ = new BehaviorSubject<MemberCategory[] | undefined>(undefined);
    @Output() public totalCountChange = new EventEmitter<number>();

    @ViewChild(DataGridComponent)
    public set dataGrid(v: DataGridComponent<Member>) {
        this.dataGrid$.next(v);
    }
    private dataGrid$ = new ReplaySubject<DataGridComponent<Member>>(1);

    public members$: Observable<Member[] | undefined>;
    public columns$: Observable<ColDef<Member>[]>;

    constructor(
        private memberService: MemberService,
        private dialogService: DialogService,
    ) {
        this.members$ = this.categories$.pipe(
            undefinedWhileLoading((categories) => {
                return categories
                    ? this.memberService.getPrimedMembersByCategory(...categories)
                    : this.memberService.getAll();
            }),
            tap((members) => {
                if (members) {
                    this.totalCountChange.emit(members.length);
                }
            }),
        );
        const includesPastMembers$ = this.categories$.pipe(
            map((categories) => !categories || categories.includes(MemberCategory.Past)),
        );
        this.columns$ = combineLatest([
            this.dataGrid$,
            includesPastMembers$,
            this.allowEditing$,
        ]).pipe(
            map(([dataGrid, includesPastMembers, isEditing]) =>
                this.getColumns(dataGrid, isEditing, includesPastMembers),
            ),
        );
    }

    public addMember = () =>
        this.memberService
            .create()
            .pipe(switchMap((m) => this.dialogService.open(EditMemberDialogComponent, m)));

    public editMember = (m: Member) => this.dialogService.open(EditMemberDialogComponent, m);

    public deleteMember = (m: Member) => this.memberService.deleteAndSave(m);

    public inviteMember(member: Member) {
        this.dialogService
            .openConfirmation(
                `Would you like to send ${member.fullName} an invitation email?`,
                () => this.memberService.invite(member),
            )
            .subscribe({
                next: () => this.dialogService.success("Invitation email sent successfully"),
                error: () => this.dialogService.danger("Failed to send invitation email"),
            });
    }

    private getColumns(
        dataGrid: DataGridComponent<Member>,
        isEditing: boolean,
        hasPastMembers: boolean,
    ): ColDef[] {
        return filterOutNullOrUndefined([
            // Just for exporting
            {
                field: "firstName",
                hide: true,
            },
            {
                field: "lastName",
                hide: true,
            },
            {
                cellRenderer: MemberCategoryIconRenderer,
                headerName: "",
                field: "category",
                pinned: "left",
                width: 48,
                cellStyle: { padding: "0", "padding-left": "1rem" },
                ...columnExportColDef({ skipColumn: true }),
            },
            {
                headerName: "Name",
                field: "fullName",
                pinned: "left",
                sortable: true,
                sort: "asc",
                width: 150,
                headerClass: "ag-no-left-header-padding",
                cellStyle: { "padding-left": "0" },
                ...columnExportColDef({ skipColumn: true }),
            },
            {
                // TODO Format phone no
                headerName: "Phone No",
                field: "details.phoneNo",
                width: 120,
            },
            {
                headerName: "Address",
                field: "details.address",
                ...wrapTextColDef(),
            },
            {
                headerName: "Email",
                field: "details.email",
                cellRenderer: GridEmailLinkRenderer,
                width: 220,
                ...wrapTextColDef(),
            },
            {
                headerName: "Fee Class",
                field: "details.feeClass",
                width: 100,
            },
            {
                field: "dateJoined",
                width: 120,
                ...dateColDef(),
            },
            valueIfConditionMet(hasPastMembers, {
                field: "dateLeft",
                width: 120,
                ...dateColDef(),
            }),
            {
                headerName: "Instruments",
                width: 140,
                valueGetter: (params: ValueGetterParams) => {
                    const member: Member = params.data;
                    return member.instrumentList;
                },
                filter: "true",
                ...wrapTextColDef(),
            },
            {
                headerName: "Ensembles",
                valueGetter: (params: ValueGetterParams) => {
                    const member: Member = params.data;
                    const activeEnsembles = member.ensembleMembership
                        .filter((e) => !e.dateLeft || e.dateLeft > new Date())
                        .map((e) => e.ensemble);

                    if (activeEnsembles.length === 0) {
                        return "None";
                    }

                    return activeEnsembles.map((e) => e?.name).join(", ");
                },
                filter: "true",
                ...wrapTextColDef(),
            },
            {
                headerName: "Notes",
                field: "details.notes",
                minWidth: 300,
                flex: 1,
                ...wrapTextColDef(),
            },
            valueIfConditionMet(
                isEditing,
                buildEditColumn<any>(dataGrid, [
                    {
                        icon: "send",
                        tooltip: "Send an invitation email",
                        onClick: this.inviteMember.bind(this),
                    },
                ]),
            ),
        ]);
    }
}
