import { Component } from "@angular/core";
import { FeaturePermission } from "app/auth/feature-permission";
import { Chip } from "app/common-ux/chip-selection/chip-selection.component";
import { addRowRef } from "app/common-ux/data-grid/data-grid-consts";
import { findElementByRef } from "app/common/utils";
import { AllMemberCategoryMetadata, Member, MemberCategory } from "app/dal/model/member";
import { HelpDataHooks, HelpablePage } from "app/help/help-broker.service";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { BehaviorSubject, map } from "rxjs";
import { membersPageId } from "../member-consts";

@Component({
    selector: "app-all-members",
    templateUrl: "./all-members-page.component.html",
    styleUrls: ["./all-members-page.component.scss"],
    standalone: false,
})
export class AllMembersPageComponent implements HelpablePage {
    public readonly pageId = membersPageId;
    public allowEditing$ = this.authorisationService.hasPermission(FeaturePermission.MemberWrite);
    public memberCount$ = new BehaviorSubject<number | undefined>(undefined);
    public chips: Chip<MemberCategory>[];
    public selectedCategories: MemberCategory[];
    public dataHooks: HelpDataHooks;

    constructor(private authorisationService: TenantAuthorisationService) {
        this.chips = AllMemberCategoryMetadata.map((cm) => ({
            text: cm.label,
            icon: cm.icon,
            tooltip: cm.pluralText,
            value: cm.category,
        }));
        this.selectedCategories = [MemberCategory.Current];
        this.dataHooks = {
            hasNoData$: this.memberCount$.pipe(
                map((count) => typeof count !== "number" || count < 2),
            ),
            addDataPermission: FeaturePermission.MemberWrite,
            addData: () => findElementByRef(addRowRef)?.click(),
        };
    }

    public get title() {
        const memberCountText =
            typeof this.memberCount$.value === "number"
                ? ` (${this.memberCount$.value} shown)`
                : "";
        return `Members${memberCountText}`;
    }

    public updateSelectedIndex(member: Member) {
        const categoryAlreadySelected = this.selectedCategories.find((t) => t === member.category);
        if (!categoryAlreadySelected) {
            this.selectedCategories = [...this.selectedCategories, member.category];
        }
    }
}
