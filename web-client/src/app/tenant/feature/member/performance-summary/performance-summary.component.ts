import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from "@angular/core";
import { undefinedWhileLoading } from "app/common/rxjs-utilities";
import { Observable, ReplaySubject, map, tap } from "rxjs";
import { MemberConcert } from "../member-concert";
import { MemberService, MemberSummary } from "../member.service";

@Component({
    selector: "app-performance-summary",
    templateUrl: "./performance-summary.component.html",
    styleUrls: ["./performance-summary.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: false,
})
export class PerformanceSummaryComponent {
    @Input() public set member(value: MemberSummary) {
        this.member$.next(value);
    }
    public member$ = new ReplaySubject<MemberSummary>(1);

    public instrumentList$: Observable<string>;
    public ensembleList$: Observable<string>;
    public memberConcerts$: Observable<MemberConcert[] | undefined>;

    constructor(memberService: MemberService, changeDetectorRef: ChangeDetectorRef) {
        this.instrumentList$ = this.member$.pipe(map((m) => m.instruments.join(", ")));
        this.ensembleList$ = this.member$.pipe(map((m) => m.ensembles.join(", ")));
        this.memberConcerts$ = this.member$.pipe(
            undefinedWhileLoading((m) => memberService.getMemberConcerts(m.id)),
            tap(() => changeDetectorRef.markForCheck()),
        );
    }
}
