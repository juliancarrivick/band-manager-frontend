import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BaseBreezeDialog } from "app/common-ux/dialog/base-breeze-dialog";
import { cacheLatest } from "app/common/rxjs-utilities";
import { EntityTracker } from "app/dal/entity-tracker";
import { Ensemble } from "app/dal/model/ensemble";
import {
    EnsembleMembership,
    EnsembleMembershipType,
    EnsembleMembershipTypeMetadata,
} from "app/dal/model/ensemble-membership";
import { Member } from "app/dal/model/member";
import { BehaviorSubject, combineLatest, Observable } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { MemberService } from "../member.service";

interface EnsembleMembershipHistory {
    ensemble: Ensemble;
    history: EnsembleMembership[];
    hasActiveMembership: boolean;
}

@Component({
    templateUrl: "./edit-member-ensembles-dialog.component.html",
    styleUrls: ["./edit-member-ensembles-dialog.component.scss"],
    standalone: false,
})
export class EditMemberEnsemblesDialogComponent extends BaseBreezeDialog<Member> {
    public readonly dialogName = "EditMemberEnsembles";

    public entityTracker: EntityTracker;
    public saveError?: Error;

    public availableEnsembles$: Observable<Ensemble[]>;
    public selectedEnsemble?: Ensemble;
    public selectedMembershipType = EnsembleMembershipType.Regular;
    public ensembleMembershipHistory$: Observable<EnsembleMembershipHistory[]>;
    private refreshMembership$ = new BehaviorSubject<void>(undefined);
    public ensemblePanelOpen = new Map<Ensemble, boolean>();

    constructor(
        dialogRef: MatDialogRef<EditMemberEnsemblesDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public member: Member,
        private memberService: MemberService,
    ) {
        super(dialogRef);
        this.entityTracker = new EntityTracker(memberService);

        const membershipHistory$ = memberService.getMembershipForMember(member).pipe(
            tap((membership) => membership.forEach((m) => this.entityTracker.add(m))),
            cacheLatest(),
            switchMap(() => this.refreshMembership$),
            map(() => member.ensembleMembership),
        );
        this.availableEnsembles$ = combineLatest([
            memberService.getAllActiveEnsembles(),
            membershipHistory$,
        ]).pipe(
            map(([ensembles, membershipHistory]) => {
                const activeMembership = membershipHistory.filter(
                    (m) => !m.dateLeft || m.dateLeft > new Date(),
                );
                return ensembles.filter(
                    (e) => !activeMembership.find((m) => m.ensembleId === e.id),
                );
            }),
        );
        this.ensembleMembershipHistory$ = membershipHistory$.pipe(
            map((membershipHistory) => {
                const history: EnsembleMembershipHistory[] = [];

                for (const membership of membershipHistory) {
                    let ensembleHistory = history.find((h) => h.ensemble === membership.ensemble);
                    if (!ensembleHistory) {
                        ensembleHistory = {
                            ensemble: membership.ensemble!,
                            history: [],
                            hasActiveMembership: false,
                        };
                        history.push(ensembleHistory);
                    }

                    ensembleHistory.history.push(membership);
                    if (!membership.dateLeft) {
                        ensembleHistory.hasActiveMembership = true;
                    }
                }

                for (const ensembleHistory of history) {
                    ensembleHistory.history.sort(
                        (a, b) => b.dateJoined.getDate() - a.dateJoined.getDate(),
                    );
                }

                return history.sort((a, b) => {
                    return a.ensemble.name.localeCompare(b.ensemble.name);
                });
            }),
        );
    }

    public joinEnsembleOnClick(ensemble: Ensemble) {
        this.memberService
            .addMembership(this.member, ensemble, {
                type: this.selectedMembershipType,
            })
            .subscribe((membership) => {
                this.selectedEnsemble = undefined;
                this.selectedMembershipType = EnsembleMembershipType.Regular;

                this.entityTracker.add(membership);
                this.ensemblePanelOpen.set(ensemble, true);
                this.membershipUpdated();
            });
    }

    public endMembershipOnClick(membership: EnsembleMembership) {
        membership.dateLeft = new Date();
        this.membershipUpdated();
    }

    public deleteMembership(membership: EnsembleMembership) {
        this.memberService.delete(membership);
        this.membershipUpdated();
    }

    public membershipUpdated() {
        this.refreshMembership$.next();
    }

    public membershipTypeMetadata(membership: EnsembleMembership) {
        return EnsembleMembershipTypeMetadata[membership.type];
    }
}
