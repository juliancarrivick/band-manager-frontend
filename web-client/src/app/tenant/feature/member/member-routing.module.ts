import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { helpPages } from "app/help/route/help-list";
import { AllMembersPageComponent } from "./all-members-page/all-members-page.component";
import { SummaryPageComponent } from "./summary-page/summary-page.component";

const memberRoutes: AppRoutes = [
    {
        path: "",
        component: AllMembersPageComponent,
        data: {
            title: "Members",
            icon: "face",
            restrictedToPermission: FeaturePermission.MemberWrite,
            help: [helpPages.members],
        },
    },
    {
        path: "dashboard",
        component: SummaryPageComponent,
        data: {
            title: "Member Dashboard",
            icon: "dashboard",
            restrictedToPermission: FeaturePermission.MemberRead,
            help: [helpPages.members],
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(memberRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppMemberRoutingModule {}
