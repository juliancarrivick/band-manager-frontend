import { COMMA, ENTER, SEMICOLON } from "@angular/cdk/keycodes";
import { Component, Inject } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import { MatChipInputEvent } from "@angular/material/chips";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BaseBreezeDialog } from "app/common-ux/dialog/base-breeze-dialog";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { BreezeFormGroup } from "app/common-ux/form/breeze-form-control";
import { EntityTracker } from "app/dal/entity-tracker";
import { Ensemble } from "app/dal/model/ensemble";
import { EnsembleMembership } from "app/dal/model/ensemble-membership";
import { FeeClass, Member, MemberInstrument } from "app/dal/model/member";
import { BehaviorSubject, combineLatest, Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { EditMemberEnsemblesDialogComponent } from "../edit-member-ensembles-dialog/edit-member-ensembles-dialog.component";
import { MemberService } from "../member.service";

@Component({
    templateUrl: "./edit-member-dialog.component.html",
    styleUrls: ["./edit-member-dialog.component.scss"],
    standalone: false,
})
export class EditMemberDialogComponent extends BaseBreezeDialog<Member> {
    public readonly dialogName = "EditMember";

    public isNewMember: boolean;
    public title: string;
    public feeClassOptions: { label: string; value: FeeClass; hint: string }[];
    public hintText$: Observable<string | undefined>;
    public saveError?: Error;

    public readonly seperatorKeyCodes = [ENTER, COMMA, SEMICOLON];
    public entityTracker: EntityTracker;

    public ensembleCtrl = new FormControl<string | Ensemble>("");
    private refreshMembership$ = new BehaviorSubject<void>(undefined);
    public currentMembership$: Observable<EnsembleMembership[]>;
    public availableEnsembles$: Observable<Ensemble[]>;

    // Need to update the error messages as well to be more user friendly
    public form = new FormGroup({
        member: new BreezeFormGroup(
            this.member,
            [
                "firstName",
                "lastName",
                "dateJoined",
                "dateLeft",
            ],
            this.dialogRef.afterClosed(),
        ),
        details: new BreezeFormGroup(
            this.member.details!,
            [
                "phoneNo",
                "email",
                "address",
                "feeClass",
                "notes",
            ],
            this.dialogRef.afterClosed(),
        ),
    });

    public constructor(
        dialogRef: MatDialogRef<EditMemberDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public member: Member,
        private memberService: MemberService,
        private dialogService: DialogService,
    ) {
        super(dialogRef);
        if (!member.details) {
            throw new Error("Member details must be populated");
        }

        this.entityTracker = new EntityTracker(memberService);
        this.entityTracker.add(member);
        this.entityTracker.add(member.details);
        member.memberInstrument.forEach((i) => this.entityTracker.add(i));
        member.ensembleMembership.forEach((m) => this.entityTracker.add(m));

        this.isNewMember = member.entityAspect.entityState.isAdded();
        this.title = this.isNewMember ? "Add Member" : "Edit Member";
        this.feeClassOptions = [
            {
                label: "Full",
                value: FeeClass.Full,
                hint: "Member will pay full ensemble fees",
            },
            {
                label: "Concession",
                value: FeeClass.Concession,
                hint: "Member will have discounted ensemble fees",
            },
            {
                label: "Special",
                value: FeeClass.Special,
                hint: "Member will not pay fees",
            },
        ];

        this.currentMembership$ = this.refreshMembership$.pipe(
            map(() =>
                member.ensembleMembership.filter((m) => !m.dateLeft || m.dateLeft > new Date()),
            ),
        );
        this.availableEnsembles$ = combineLatest([
            memberService.getAllActiveEnsembles(),
            this.currentMembership$,
            this.ensembleCtrl.valueChanges.pipe(startWith("")),
        ]).pipe(
            map(([ensembles, membership, inputText]) => {
                const lowerInputText =
                    typeof inputText === "string" ? inputText.toLocaleLowerCase() : "";
                return ensembles
                    .filter((e) => !membership.find((m) => m.ensembleId === e.id))
                    .filter((e) => e.name.toLocaleLowerCase().includes(lowerInputText));
            }),
        );
        this.hintText$ = this.form.controls.details.controls.feeClass.value$.pipe(
            map((feeClass) => this.feeClassOptions.find((e) => e.value === feeClass)?.hint),
        );
    }

    public addInstrument(event: MatChipInputEvent) {
        const name = event.value.trim();
        if (!name) {
            event.chipInput.clear();
            return;
        }

        this.memberService.createInstrument(this.member, name).subscribe((instrument) => {
            this.entityTracker.add(instrument);
            event.chipInput.clear();
        });
    }

    public removeInstrument(instrument: MemberInstrument) {
        this.memberService.delete(instrument);
    }

    public addMembership(event: MatAutocompleteSelectedEvent) {
        if (event.option.value instanceof Ensemble) {
            this.memberService.addMembership(this.member, event.option.value).subscribe((m) => {
                this.entityTracker.add(m);
                this.refreshMembership$.next();
                this.ensembleCtrl.setValue("");
            });
        }
    }

    public removeMembership(membership: EnsembleMembership) {
        if (membership.entityAspect.entityState.isAdded()) {
            this.memberService.delete(membership);
        } else {
            membership.dateLeft = new Date();
        }

        this.refreshMembership$.next();
    }

    public cleanEnsembleInput(event: MatChipInputEvent) {
        event.chipInput.clear();
    }

    public openMembershipHistoryDialog() {
        this.dialogService.open(EditMemberEnsemblesDialogComponent, this.member).subscribe(() => {
            this.refreshMembership$.next();
        });
    }
}
