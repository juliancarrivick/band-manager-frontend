import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Member } from "app/dal/model/member";
import { combineLatest, Observable, ObservableInput, of } from "rxjs";
import { map, startWith, switchMap } from "rxjs/operators";
import { MemberService } from "../member.service";

@Component({
    selector: "app-select-member",
    template: `
        <mat-form-field>
            <mat-label>{{ label }}</mat-label>
            <input
                type="text"
                matInput
                [formControl]="memberFormControl"
                [matAutocomplete]="autocomplete"
            />
            <mat-autocomplete
                #autocomplete="matAutocomplete"
                [displayWith]="displayMember"
            >
                <mat-option
                    *ngFor="let m of filteredMembers$ | async"
                    [value]="m"
                >
                    {{ m.fullName }}
                </mat-option>
            </mat-autocomplete>
        </mat-form-field>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }
        `,
    ],
    standalone: false,
})
export class SelectMemberComponent implements OnChanges {
    @Input() public member?: Member;
    @Output() public memberChange = new EventEmitter<Member | undefined>();

    @Input() public label = "Member";
    @Input() public filterAllowedMembers: (members: Member[]) => ObservableInput<Member[]>;

    public memberFormControl = new FormControl<string | Member>("");
    public filteredMembers$: Observable<Member[]>;

    public constructor(memberService: MemberService) {
        this.filterAllowedMembers = (members: Member[]) => of(members);

        this.filteredMembers$ = combineLatest([
            memberService
                .getCurrentMembers()
                .pipe(switchMap((members) => this.filterAllowedMembers(members))),
            this.memberFormControl.valueChanges.pipe(startWith("")),
        ]).pipe(
            map(([currentMembers, value]) => {
                const v = typeof value === "string" ? value.trim().toLowerCase() : "";
                return currentMembers
                    .filter((m) => m.fullName.toLowerCase().includes(v))
                    .sort((a, b) => a.compare(b));
            }),
        );
        this.memberFormControl.valueChanges
            .pipe(map((v) => (v instanceof Member ? v : undefined)))
            .subscribe((m) => {
                this.member = m;
                this.memberChange.emit(m);
            });
    }

    public ngOnChanges(changes: SimpleChanges) {
        if (changes.member) {
            this.memberFormControl.setValue(changes.member.currentValue);
        }
    }

    public displayMember(member?: Member) {
        return member?.fullName ?? "";
    }
}
