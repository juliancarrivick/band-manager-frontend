import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { HelpBrokerService } from "app/help/help-broker.service";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";
import { allScoresPageId } from "../score-consts";

@Component({
    templateUrl: "./music-library-help.component.html",
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class MusicLibraryHelpComponent {
    public helpData$ = this.helpBroker.helpablePageDataHooks(allScoresPageId);

    public constructor(private helpBroker: HelpBrokerService) {}
}
