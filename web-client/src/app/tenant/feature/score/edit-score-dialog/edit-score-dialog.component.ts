import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BaseBreezeDialog } from "app/common-ux/dialog/base-breeze-dialog";
import { EntityTracker } from "app/dal/entity-tracker";
import { AllScoreCategoryMetadata, Score, ScoreCategory } from "app/dal/model/score";
import { ValidationError } from "breeze-client";
import { ScoreService } from "../score.service";

const categoryNotSelectedError = new ValidationError(null, {}, "Select a category");
const scoreCategoryOptions = AllScoreCategoryMetadata.map((cm) => ({
    label: cm.label,
    value: cm.category,
    hint: cm.singularText,
}));

@Component({
    templateUrl: "./edit-score-dialog.component.html",
    styleUrls: ["./edit-score-dialog.component.scss"],
    standalone: false,
})
export class EditScoreDialogComponent extends BaseBreezeDialog<Score> {
    public readonly dialogName = "EditScore";

    public title: string;
    public entityTracker: EntityTracker;
    public saveError?: Error;

    public category?: ScoreCategory;
    public categoryOptions = scoreCategoryOptions;
    public hintText?: string;

    public ScoreCategory = ScoreCategory;

    public constructor(
        dialogRef: MatDialogRef<EditScoreDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public score: Score,
        scoreService: ScoreService,
    ) {
        super(dialogRef);
        this.title = score.entityAspect.entityState.isAdded() ? "Add Score" : "Edit Score";

        this.entityTracker = new EntityTracker(scoreService);
        this.entityTracker.add(score);

        if (score.entityAspect.entityState.isAdded()) {
            delete (score as any).inLibrary;
            score.entityAspect.addValidationError(categoryNotSelectedError);
        } else {
            this.updateCategorySelection(score.category);
        }
    }

    public updateCategorySelection(category: ScoreCategory) {
        this.category = category;
        this.score.category = category;

        if (this.score.entityAspect.entityState.isAdded()) {
            this.score.isOwned = this.score.inLibrary;
            this.score.entityAspect.removeValidationError(categoryNotSelectedError);
        }

        const option = this.categoryOptions.find((e) => e.value === category);
        this.hintText = option?.hint;
    }
}
