import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ColDef, ValueFormatterParams, ValueGetterParams } from "ag-grid-community";
import { DataGridComponent } from "app/common-ux/data-grid/data-grid.component";
import { currencyColDef } from "app/common-ux/data-grid/grid-currency-column";
import { dateColDef } from "app/common-ux/data-grid/grid-date-column";
import { ColumnAction, buildEditColumn } from "app/common-ux/data-grid/grid-edit-column";
import { columnExportColDef } from "app/common-ux/data-grid/grid-export-options";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { cacheLatest, undefinedWhileLoading } from "app/common/rxjs-utilities";
import { filterOutNullOrUndefined } from "app/common/utils";
import { Score, ScoreCategory, ScoreCategoryMetadata } from "app/dal/model/score";
import { buildGridAttachmentActions } from "app/storage/attachment-grid-utils";
import { BehaviorSubject, Observable, ReplaySubject, combineLatest } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { EditScoreDialogComponent } from "../edit-score-dialog/edit-score-dialog.component";
import { ScoreAttachmentsDialogComponent } from "../score-attachments-dialog/score-attachments-dialog.component";
import { ScoreMetadataDto, ScoreService } from "../score.service";
import { ScorePlayCountRendererComponent } from "./play-count-renderer.component";
import { ScoreCategoryIconRenderer } from "./score-category-icon-renderer";
import { ScoreIsOwnedRenderer } from "./score-is-owned-renderer";

@Component({
    selector: "app-score-table",
    templateUrl: "./score-table.component.html",
    standalone: false,
})
export class ScoreTableComponent {
    @Input() public allowEditing: boolean | null = false;
    @Input() public set category(value: ScoreCategory[] | undefined) {
        this.category$.next(value);
    }
    public get category() {
        return this.category$.value;
    }
    private category$ = new BehaviorSubject<ScoreCategory[] | undefined>(undefined);

    @Output() public scoreAddedOrEdited = new EventEmitter<Score>();
    @Output() public scoreCount = new EventEmitter<number>();

    @ViewChild(DataGridComponent)
    public set dataGrid(v: DataGridComponent<ScoreMetadataDto>) {
        this.dataGrid$.next(v);
    }
    private dataGrid$ = new ReplaySubject<DataGridComponent<ScoreMetadataDto>>(1);

    public hasLibraryScores$: Observable<boolean>;
    public scores$: Observable<ScoreMetadataDto[] | undefined>;
    public columns$: Observable<ColDef<ScoreMetadataDto>[]>;
    public ScoreCategory = ScoreCategory;

    constructor(
        private scoreService: ScoreService,
        private dialogService: DialogService,
    ) {
        this.hasLibraryScores$ = this.category$.pipe(
            map((c) => !c || c.includes(ScoreCategory.MusicLibrary)),
        );
        this.scores$ = this.category$.pipe(
            undefinedWhileLoading((c) => {
                return typeof c === "undefined"
                    ? scoreService.getAllWithMetadata()
                    : scoreService.getScoreByCategoryWithMetadata(c);
            }),
            tap((scores) => {
                if (typeof scores !== "undefined") {
                    this.scoreCount.emit(scores.length);
                }
            }),
            cacheLatest(),
        );
        const additionalActions$ = buildGridAttachmentActions({
            source: this.scores$,
            getAttachmentCount: (scoreWithMetadata) => scoreWithMetadata.attachmentCount,
            setAttachmentCount: (scoreWithMetadata, count) =>
                (scoreWithMetadata.attachmentCount = count),
            allowEditing: () => !!this.allowEditing,
            openAttachmentDialog: (scoreWithMetadata) =>
                this.dialogService.open(ScoreAttachmentsDialogComponent, scoreWithMetadata.score, {
                    autoFocus: false,
                }),
        });
        this.columns$ = combineLatest([
            this.dataGrid$,
            this.hasLibraryScores$,
            additionalActions$,
        ]).pipe(
            map(([dataGrid, hasLibraryScores, additionalActions]) =>
                this.getColumns(dataGrid, hasLibraryScores, additionalActions),
            ),
        );
    }

    public addScore = () =>
        this.scoreService.create().pipe(
            switchMap((score) => this.dialogService.open(EditScoreDialogComponent, score)),
            tap((score) => this.scoreAddedOrEdited.emit(score)),
            // TODO Allow uploads during creation
            map((score): ScoreMetadataDto => ({ score, playCount: 0, attachmentCount: 0 })),
        );

    public editScore = (s: ScoreMetadataDto) =>
        this.dialogService.open(EditScoreDialogComponent, s.score).pipe(
            tap((score) => this.scoreAddedOrEdited.emit(score)),
            map(() => s),
        );

    public deleteScore = (s: ScoreMetadataDto) => this.scoreService.deleteAndSave(s.score);

    private getColumns(
        dataGrid: DataGridComponent<ScoreMetadataDto>,
        hasLibraryScores: boolean,
        additionalActions: ColumnAction<ScoreMetadataDto>[],
    ): ColDef[] {
        return filterOutNullOrUndefined([
            // Just for exporting
            {
                field: "score.title",
                headerName: "Title",
                hide: true,
            },
            {
                field: "score.composer",
                headerName: "Composer",
                hide: true,
            },
            {
                field: "score.arranger",
                headerName: "Arranger",
                hide: true,
            },
            {
                cellRenderer: ScoreCategoryIconRenderer,
                headerName: "",
                field: "score.category",
                pinned: "left",
                width: 48,
                cellStyle: { padding: "0", "padding-left": "1rem" },
                ...columnExportColDef({
                    processHeaderCallback: () => "Category",
                    preprocessCellValue: (params) => {
                        const category: ScoreCategory = params.value;
                        return ScoreCategoryMetadata[category].label;
                    },
                }),
            },
            {
                headerName: "Title",
                valueGetter: (params: ValueGetterParams<ScoreMetadataDto>) =>
                    params.data?.score.formattedName,
                filter: "true",
                pinned: "left",
                sortable: true,
                sort: "asc",
                width: 300,
                ...wrapTextColDef(),
                ...columnExportColDef({ skipColumn: true }),
            },
            {
                headerName: "Is Owned",
                hide: !hasLibraryScores,
                valueGetter: (params: ValueGetterParams<ScoreMetadataDto>) => {
                    if (!params.data) {
                        return "";
                    }

                    const s: ScoreMetadataDto = params.data;
                    // Hack to ensure column gets rerendered when either of these fields change
                    if (s.score.inLibrary) {
                        return String(s.score.isOwned).toUpperCase();
                    } else {
                        return String(s.score.isOwned);
                    }
                },
                cellRenderer: ScoreIsOwnedRenderer,
                width: 100,
                ...columnExportColDef({
                    preprocessCellValue: (params) => {
                        const hackedIsOwned: string = params.value;
                        // Undo above hack
                        return hackedIsOwned.toLowerCase();
                    },
                }),
            },
            {
                field: "playCount",
                width: 100,
                sortable: true,
                cellRenderer: ScorePlayCountRendererComponent,
            },
            {
                headerName: "Duration",
                field: "score.duration",
                valueFormatter: (params: ValueFormatterParams) => {
                    const s: ScoreMetadataDto = params.data;
                    return s.score.formattedDuration;
                },
                width: 120,
                sortable: true,
                ...columnExportColDef({
                    processHeaderCallback: () => "Duration (seconds)",
                }),
            },
            {
                headerName: "Grade",
                field: "score.grade",
                width: 80,
            },
            {
                headerName: "Genre",
                field: "score.genre",
                filter: true,
            },
            {
                headerName: "Date Purchased",
                field: "score.datePurchased",
                ...dateColDef(),
            },
            {
                headerName: "Value Paid",
                field: "score.valuePaid",
                ...currencyColDef(),
            },
            {
                headerName: "Location",
                field: "score.location",
            },
            {
                headerName: "Notes",
                field: "score.notes",
                minWidth: 400,
                flex: 1,
                ...wrapTextColDef(),
            },
            buildEditColumn<any>(dataGrid, additionalActions),
        ]);
    }
}
