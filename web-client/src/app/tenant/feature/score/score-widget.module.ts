import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppCommonUxModule } from "app/common-ux/common-ux.module";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppRouterModule } from "app/common-ux/router/router.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { AppStorageModule } from "app/storage/storage.module";
import { EditScoreDialogComponent } from "./edit-score-dialog/edit-score-dialog.component";
import { ScoreAttachmentsDialogComponent } from "./score-attachments-dialog/score-attachments-dialog.component";
import { ScorePerformanceListDialogComponent } from "./score-performance-list-dialog/score-performance-list-dialog.component";
import { ScorePlayCountRendererComponent } from "./score-table/play-count-renderer.component";
import { ScoreTableComponent } from "./score-table/score-table.component";
import { SelectScoresComponent } from "./select-scores/select-scores.component";

@NgModule({
    imports: [
        CommonModule,

        MatIconModule,
        MatListModule,

        AppCommonUxModule,
        AppDataGridModule,
        AppDialogModule,
        AppFormModule,
        AppButtonModule,
        AppSaveCancelButtonsModule,
        AppStorageModule,

        AppRouterModule,
    ],
    exports: [
        ScoreTableComponent,
        SelectScoresComponent,
    ],
    declarations: [
        ScoreTableComponent,
        EditScoreDialogComponent,
        ScoreAttachmentsDialogComponent,
        SelectScoresComponent,
        ScorePerformanceListDialogComponent,
        ScorePlayCountRendererComponent,
    ],
})
export class AppScoreWidgetModule {}
