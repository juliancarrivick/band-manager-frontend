import { Injectable } from "@angular/core";
import { BreezeEntityService } from "app/dal/breeze-domain.service";
import { Concert, ConcertBreezeModel } from "app/dal/model/concert";
import { Ensemble, EnsembleBreezeModel, EnsembleStatus } from "app/dal/model/ensemble";
import { Performance, PerformanceBreezeModel } from "app/dal/model/performance";
import { Predicate } from "breeze-client";
import { map, switchMap } from "rxjs";

export interface ConcertStats {
    totalMemberCount: number;
    totalScoreCount: number;
}

@Injectable()
export class ConcertService extends BreezeEntityService<Concert> {
    protected readonly breezeModel = ConcertBreezeModel;

    public create() {
        return this.breezeService.create(ConcertBreezeModel, {
            date: new Date(),
        });
    }

    public getAllWithEnsembles() {
        const query = this.breezeService
            .createQuery(ConcertBreezeModel)
            .expand("performance.ensemble");
        return this.breezeService.executeQuery<Concert>(query);
    }

    public getWithPerformanceEnsemblesMembersAndScores(concertId: number) {
        const query = this.breezeService
            .createQuery(ConcertBreezeModel)
            .where("id", "==", concertId)
            .expand(
                "performance.ensemble,performance.performanceMembers.member,performance.performanceScores.score",
            );
        return this.breezeService.executeQuery<Concert>(query).pipe(map((data) => data[0]));
    }

    public getPerformancesWithMembersAndScores(concert: Concert) {
        const query = this.breezeService
            .createQuery(PerformanceBreezeModel)
            .where("concertId", "==", concert.id)
            .expand("ensemble,performanceMembers.member,performanceScores.score");
        return this.breezeService.executeQuery<Performance>(query);
    }

    public getConcertStats(concert: Concert) {
        return this.apiEndpoint$.pipe(
            switchMap((endpoint) =>
                this.http.get<ConcertStats>(`${endpoint}/concert/${concert.id}/stats`),
            ),
        );
    }

    public getCurrentEnsemblesWithMembership() {
        const query = this.breezeService
            .createQuery(EnsembleBreezeModel)
            .where(new Predicate("status", "==", EnsembleStatus.Active))
            .expand("ensembleMembership.member");
        return this.breezeService.executeQuery<Ensemble>(query);
    }
}
