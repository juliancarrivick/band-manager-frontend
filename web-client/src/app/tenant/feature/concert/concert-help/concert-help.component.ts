import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { HelpBrokerService } from "app/help/help-broker.service";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";
import { concertsPageId, editConcertPageId } from "../concert-consts";

@Component({
    templateUrl: "./concert-help.component.html",
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class ConcertHelpComponent {
    public concertsDataHooks$ = this.helpBroker.helpablePageDataHooks(concertsPageId);
    public editConcertDataHooks$ = this.helpBroker.helpablePageDataHooks(editConcertPageId);

    public constructor(private helpBroker: HelpBrokerService) {}
}
