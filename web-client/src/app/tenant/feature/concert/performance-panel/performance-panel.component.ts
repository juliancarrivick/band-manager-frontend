import { Component, Input, OnChanges } from "@angular/core";
import { Member } from "app/dal/model/member";
import { Performance } from "app/dal/model/performance";
import { Score } from "app/dal/model/score";
import { TenantService } from "app/tenant/module/tenant.service";
import { map, Observable } from "rxjs";

@Component({
    selector: "app-performance-panel",
    templateUrl: "./performance-panel.component.html",
    styleUrls: [
        "../../../../common-ux/wrapped-expansion-panel.scss",
        "./performance-panel.component.scss",
    ],
    standalone: false,
})
export class PerformancePanelComponent implements OnChanges {
    @Input() public performance?: Performance;
    public scores: Score[] = [];
    public members: Member[] = [];

    @Input() public expanded = true;

    public tenantAbbr$: Observable<string | undefined>;

    public constructor(tenantService: TenantService) {
        this.tenantAbbr$ = tenantService.currentTenant$.pipe(map((t) => t?.abbreviation));
    }

    public ngOnChanges() {
        this.scores =
            this.performance?.performanceScores
                .map((e) => e.score)
                .filter((e): e is Score => !!e) ?? [];
        this.members =
            this.performance?.performanceMembers
                .map((e) => e.member)
                .filter((e): e is Member => !!e)
                .sort((a, b) => a.compare(b)) ?? [];
    }
}
