import { ActivatedRoute, Router } from "@angular/router";
import { ICellRendererComp, ICellRendererParams } from "ag-grid-community";
import { Concert } from "app/dal/model/concert";
import { fromEvent, Subscription, switchMap, tap } from "rxjs";
import { isLinkRendererContext } from "./link-renderer-context";

export class ConcertLinkRenderer implements ICellRendererComp {
    private gui = document.createElement("a");
    private router!: Router;
    private route!: ActivatedRoute;
    private subscription?: Subscription;

    public init(params: ICellRendererParams) {
        if (!isLinkRendererContext(params.context)) {
            throw new Error("Invalid context passed to ConcertLinkRenderer");
        }
        this.router = params.context.router;
        this.route = params.context.route;
        this.setUrl(params.data);
    }

    public refresh(params: ICellRendererParams) {
        this.setUrl(params.data);
        return true;
    }

    private setUrl(concert: Concert) {
        this.subscription?.unsubscribe();

        this.gui.innerText = concert.occasion;

        const urlTree = this.router.createUrlTree([concert.id], { relativeTo: this.route });
        this.gui.href = this.router.serializeUrl(urlTree);

        this.subscription = fromEvent(this.gui, "click")
            .pipe(
                // Need to do this so that we don't get a refresh on the link click
                // (navigation has to be done by Angular Router)
                tap((e) => e.preventDefault()),
                switchMap(async () => {
                    const url = new URL(this.gui.href);
                    await this.router.navigateByUrl(url.pathname);
                }),
            )
            .subscribe();
    }

    public getGui() {
        return this.gui;
    }

    public destroy() {
        this.subscription?.unsubscribe();
    }
}
