import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { numberRouteParamFunc } from "app/common/routing-utils";
import { filterForDefinedValue, propagateMap, propagateSwitchMap } from "app/common/rxjs-utilities";
import { Concert } from "app/dal/model/concert";
import { Performance } from "app/dal/model/performance";
import { EntityAttachmentSensitivity } from "app/storage/entity-attachment-metadata";
import { EntityAttachmentService } from "app/storage/entity-attachment.service";
import { StorageService } from "app/storage/storage.service";
import { Observable, map, switchMap } from "rxjs";
import { ConcertService, ConcertStats } from "../concert.service";

@Component({
    templateUrl: "./concert-page.component.html",
    styleUrls: ["./concert-page.component.scss"],
    standalone: false,
})
export class ConcertPageComponent {
    public readonly FeaturePermission = FeaturePermission;

    public concert$: Observable<Concert | undefined>;
    public stats$: Observable<ConcertStats>;
    public performances$: Observable<Performance[]>;
    public focussedPerformanceId$: Observable<number | undefined>;
    public attachmentService$: Observable<EntityAttachmentService | undefined>;

    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        private concertService: ConcertService,
        private dialogService: DialogService,
        storageService: StorageService,
    ) {
        this.concert$ = route.paramMap.pipe(
            map(numberRouteParamFunc("concertId")),
            propagateSwitchMap((concertId) => concertService.getById(concertId)),
        );
        this.stats$ = this.concert$.pipe(
            filterForDefinedValue(),
            switchMap((concert) => concertService.getConcertStats(concert)),
        );
        this.performances$ = this.concert$.pipe(
            filterForDefinedValue(),
            switchMap((concert) => concertService.getPerformancesWithMembersAndScores(concert)),
        );
        this.focussedPerformanceId$ = route.paramMap.pipe(
            map((params) => params.get("focussedPerformance")),
            map((id) => (id !== null ? Number(id) : undefined)),
        );
        this.attachmentService$ = this.concert$.pipe(
            propagateMap((concert) =>
                storageService.buildEntityAttachmentService(
                    "ConcertAttachment",
                    concert.id,
                    EntityAttachmentSensitivity.Secured,
                ),
            ),
        );
    }

    public isExpanded(performance: Performance, focussedId: number | null | undefined) {
        return performance.id === focussedId || focussedId === null || focussedId === undefined;
    }

    public deleteConcert(concert: Concert) {
        const entities = [
            concert,
            ...concert.performance,
            ...concert.performance.flatMap((p) => p.performanceMembers),
            ...concert.performance.flatMap((p) => p.performanceScores),
        ];
        this.dialogService
            .openConfirmation("Are you sure you would like to delete this concert?", () =>
                this.concertService.deleteAndSave(...entities),
            )
            .pipe(
                switchMap(() =>
                    this.router.navigate([".."], {
                        relativeTo: this.route,
                    }),
                ),
            )
            .subscribe();
    }
}
