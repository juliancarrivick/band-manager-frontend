import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { SaveErrorDialogComponent } from "app/common-ux/save-cancel-buttons/save-error-dialog.component";
import { numberRouteParamFunc } from "app/common/routing-utils";
import {
    cacheLatest,
    filterForDefinedValue,
    propagateMap,
    propagateSwitchMap,
} from "app/common/rxjs-utilities";
import { EntityTracker } from "app/dal/entity-tracker";
import { Concert } from "app/dal/model/concert";
import { Performance } from "app/dal/model/performance";
import { HelpDataHooks, HelpablePage } from "app/help/help-broker.service";
import { EntityAttachmentSensitivity } from "app/storage/entity-attachment-metadata";
import { EntityAttachmentService } from "app/storage/entity-attachment.service";
import { StorageService, UploadBlobDto } from "app/storage/storage.service";
import { Observable, first, map, switchMap, tap } from "rxjs";
import { editConcertPageId } from "../concert-consts";
import { ConcertService } from "../concert.service";
import { CreatePerformanceDialogComponent } from "../create-performance-dialog/create-performance-dialog.component";
import { PerformanceService } from "../performance.service";

@Component({
    templateUrl: "./edit-concert-page.component.html",
    styleUrls: ["./edit-concert-page.component.scss"],
    standalone: false,
})
export class EditConcertPageComponent implements HelpablePage {
    public readonly pageId = editConcertPageId;
    public entityTracker: EntityTracker;
    public concert$: Observable<Concert | undefined>;
    public selectedPerformance?: Performance;
    public attachmentService$: Observable<EntityAttachmentService | undefined>;
    public readonly uploadCoverImageDto: UploadBlobDto = {
        storageStrategyName: "ConcertCoverImage",
        accessData: undefined,
        linkData: undefined,
    };
    public dataHooks: HelpDataHooks;

    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        private concertService: ConcertService,
        private performanceService: PerformanceService,
        private dialogService: DialogService,
        storageService: StorageService,
    ) {
        this.entityTracker = new EntityTracker(concertService);

        this.concert$ = route.paramMap.pipe(
            map(numberRouteParamFunc("concertId")),
            propagateSwitchMap((concertId) => {
                return concertService.getWithPerformanceEnsemblesMembersAndScores(concertId);
            }),
            tap((concert) => {
                if (concert) {
                    this.selectedPerformance = concert.performance[0];
                    this.entityTracker.add(concert);
                }
            }),
            cacheLatest(),
        );
        this.attachmentService$ = this.concert$.pipe(
            propagateMap((concert) =>
                storageService.buildEntityAttachmentService(
                    "ConcertAttachment",
                    concert.id,
                    EntityAttachmentSensitivity.Secured,
                ),
            ),
        );
        this.dataHooks = {
            addDataPermission: FeaturePermission.ConcertWrite,
            hasNoData$: this.concert$.pipe(
                map((concert) => concert?.performance.length ?? 0),
                map((performanceCount) => performanceCount === 0),
            ),
            addData: () =>
                this.concert$.pipe(filterForDefinedValue(), first()).subscribe((concert) => {
                    this.addPerformance(concert);
                }),
        };
    }

    public addPerformance(concert: Concert) {
        this.performanceService
            .create(concert)
            .pipe(
                switchMap((performance) =>
                    this.dialogService.open(CreatePerformanceDialogComponent, performance),
                ),
            )
            .subscribe((performance) => {
                this.entityTracker.add(
                    performance,
                    ...performance.performanceMembers,
                    ...performance.performanceScores,
                );
                this.selectedPerformance = performance;
            });
    }

    public deletePerformance(performance: Performance, concert: Concert) {
        this.dialogService
            .openConfirmation("Are you sure you would like to delete this performance?")
            .subscribe(() => {
                let currIdx = concert.performance.indexOf(performance);

                // Add before delete so we track the members and scores too
                this.entityTracker.add(
                    performance,
                    ...performance.performanceMembers,
                    ...performance.performanceScores,
                );
                this.concertService.delete(performance);

                if (currIdx >= concert.performance.length) {
                    currIdx = concert.performance.length - 1;
                }
                this.selectedPerformance = concert.performance[currIdx];
            });
    }

    public async goToViewPage() {
        await this.router.navigate([".."], { relativeTo: this.route });
    }

    public showErrorDialog(error?: Error) {
        if (!error) {
            return;
        }

        this.dialogService.open(SaveErrorDialogComponent, error).subscribe();
    }
}
