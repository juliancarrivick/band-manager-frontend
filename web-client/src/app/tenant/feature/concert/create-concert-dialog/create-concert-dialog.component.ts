import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { BaseBreezeDialog } from "app/common-ux/dialog/base-breeze-dialog";
import { EntityTracker } from "app/dal/entity-tracker";
import { Concert } from "app/dal/model/concert";
import { ConcertService } from "../concert.service";

@Component({
    templateUrl: "./create-concert-dialog.component.html",
    standalone: false,
})
export class CreateConcertDialogComponent extends BaseBreezeDialog<Concert> {
    public readonly dialogName = "CreateConcert";

    public entityTracker: EntityTracker;
    public error?: Error;

    public constructor(
        dialogRef: MatDialogRef<CreateConcertDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public concert: Concert,
        concertService: ConcertService,
    ) {
        super(dialogRef);
        this.entityTracker = new EntityTracker(concertService);
        this.entityTracker.add(concert);
    }
}
