import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { Ensemble } from "app/dal/model/ensemble";
import { Member } from "app/dal/model/member";
import { Performance } from "app/dal/model/performance";
import { lastValueFrom, Observable } from "rxjs";
import { ConcertService } from "../concert.service";
import { PerformanceService } from "../performance.service";

@Component({
    selector: "app-select-ensemble",
    templateUrl: "./create-performance-dialog.component.html",
    styleUrls: ["./create-performance-dialog.component.scss"],
    standalone: false,
})
export class CreatePerformanceDialogComponent extends BaseDialog<Performance> {
    public readonly dialogName = "SelectPerformanceEnsemble";

    public ensembles$: Observable<Ensemble[]>;

    public autoSelectMembers = true;
    public ensembleMembers: Member[] = [];

    public constructor(
        dialogRef: MatDialogRef<CreatePerformanceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public performance: Performance,
        concertService: ConcertService,
        private performanceService: PerformanceService,
    ) {
        super(dialogRef);
        this.ensembles$ = concertService.getCurrentEnsemblesWithMembership();
    }

    public updatePerformanceEnsemble(ensemble: Ensemble) {
        this.performance.ensemble = ensemble;
        this.ensembleMembers = ensemble.ensembleMembership
            .filter((m) => m.dateLeft == null)
            .map((m) => m.member!) // Should be defined from getEnsemblesWithMembership()
            .sort((a, b) => a.compare(b));
    }

    public async resolveWithPerformanceAndMembers() {
        if (this.autoSelectMembers) {
            await Promise.all(
                this.ensembleMembers.map((m) =>
                    lastValueFrom(
                        this.performanceService.createPerformanceMember(this.performance, m.id),
                    ),
                ),
            );
        }

        this.resolve(this.performance);
    }

    public cancel() {
        // Normally would be done by app-save-cancel-buttons
        this.performanceService.delete(this.performance);
        super.cancel();
    }
}
