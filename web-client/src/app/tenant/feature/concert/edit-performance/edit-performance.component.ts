import { Component, EventEmitter, Input, OnChanges, Output } from "@angular/core";
import { getArrayDifference } from "app/common/utils";
import { BreezeEntity } from "app/dal/breeze-entity";
import { Member } from "app/dal/model/member";
import { Performance, PerformanceMember, PerformanceScore } from "app/dal/model/performance";
import { lastValueFrom } from "rxjs";
import { PerformanceService } from "../performance.service";

@Component({
    selector: "app-edit-performance",
    templateUrl: "./edit-performance.component.html",
    styleUrls: ["./edit-performance.component.scss"],
    standalone: false,
})
export class EditPerformanceComponent implements OnChanges {
    @Input() public performance!: Performance;
    @Output() public entityModified = new EventEmitter<BreezeEntity>();

    public selectedMembers: Member[] = [];
    public selectedScoreIds: number[] = [];

    public constructor(private performanceService: PerformanceService) {}

    public ngOnChanges() {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        if (!this.performance) {
            throw new Error("Performance must be defined");
        }
        if (!this.performance.ensemble) {
            throw new Error("Performance must be primed (ensemble, members AND scores");
        }

        this.selectedMembers = this.performance.performanceMembers.map((e) => e.member!);
        this.selectedScoreIds = this.performance.performanceScores.map((e) => e.scoreId);
    }

    public async updateMembership(selectedMembers: Member[]) {
        const memberDifference = getArrayDifference(
            this.performance.performanceMembers.map((e) => e.member!),
            selectedMembers,
        );

        const changedEntities = [
            ...(await Promise.all(
                memberDifference.added.map((member) => {
                    return lastValueFrom(
                        this.performanceService.createPerformanceMember(
                            this.performance,
                            member.id,
                        ),
                    );
                }),
            )),
            ...memberDifference.removed
                .map((member) =>
                    this.performance.performanceMembers.find((pm) => pm.memberId === member.id),
                )
                .filter((pm): pm is PerformanceMember => !!pm)
                .map((pm) => {
                    this.performanceService.delete(pm);
                    return pm;
                }),
        ];
        for (const entity of changedEntities) {
            this.entityModified.emit(entity);
        }

        this.selectedMembers = selectedMembers;
    }

    public async updateScores(selectedScoreIds: number[]) {
        const scoreIdDifference = getArrayDifference(
            this.performance.performanceScores.map((e) => e.scoreId),
            selectedScoreIds,
        );

        const changedEntities = [
            ...(await Promise.all(
                scoreIdDifference.added.map((id) => {
                    return lastValueFrom(
                        this.performanceService.createPerformanceScore(this.performance, id),
                    );
                }),
            )),
            ...scoreIdDifference.removed
                .map((id) => this.performance.performanceScores.find((ps) => ps.scoreId === id))
                .filter((ps): ps is PerformanceScore => !!ps)
                .map((ps) => {
                    this.performanceService.delete(ps);
                    return ps;
                }),
        ];
        for (const entity of changedEntities) {
            this.entityModified.emit(entity);
        }

        this.selectedScoreIds = selectedScoreIds;
    }
}
