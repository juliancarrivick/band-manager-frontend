import { GridIconRenderer } from "app/common-ux/data-grid/grid-icon-renderer";
import { ContactCategory, ContactCategoryMetadata } from "../contact";

export class ContactCategoryIconRenderer extends GridIconRenderer<ContactCategory> {
    protected icon(value: ContactCategory): string {
        return ContactCategoryMetadata[value].icon;
    }

    protected tooltip(value: ContactCategory): string {
        return ContactCategoryMetadata[value].label;
    }
}
