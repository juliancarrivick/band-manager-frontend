import { Component, ViewChild } from "@angular/core";
import { ColDef } from "ag-grid-community";
import { FeaturePermission } from "app/auth/feature-permission";
import { Chip } from "app/common-ux/chip-selection/chip-selection.component";
import { DataGridComponent } from "app/common-ux/data-grid/data-grid.component";
import { dateColDef } from "app/common-ux/data-grid/grid-date-column";
import { buildEditColumn } from "app/common-ux/data-grid/grid-edit-column";
import { GridEmailLinkRenderer } from "app/common-ux/data-grid/grid-email-link-renderer";
import { columnExportColDef } from "app/common-ux/data-grid/grid-export-options";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { undefinedWhileLoading } from "app/common/rxjs-utilities";
import { HelpablePage } from "app/help/help-broker.service";
import { BehaviorSubject, Observable, ReplaySubject, combineLatest, first, map } from "rxjs";
import { Contact, ContactCategory, ContactCategoryMetadata, contactCategory } from "../contact";
import { ContactService } from "../contact.service";
import { contactsPageId } from "../contacts-consts";
import { EditContactDialogComponent } from "../edit-contact-dialog/edit-contact-dialog.component";
import { ContactCategoryIconRenderer } from "./contact-category-icon-renderer";

@Component({
    selector: "app-contacts-page",
    templateUrl: "./contacts-page.component.html",
    styles: [
        `
            app-chip-selection {
                display: flex;
                justify-content: center;
                padding: 0.5rem 0;
            }

            app-data-grid {
                display: block;
                margin: 1rem;
                margin-top: 0;
            }
        `,
    ],
    standalone: false,
})
export class ContactsPageComponent implements HelpablePage {
    public readonly pageId = contactsPageId;

    public chips: Chip<ContactCategory>[];
    public contacts$: Observable<Contact[] | undefined>;
    public columns$: Observable<ColDef<Contact>[]>;
    public selectedCategory$ = new BehaviorSubject<ContactCategory | undefined>(
        ContactCategory.Current,
    );

    @ViewChild(DataGridComponent)
    public set dataGrid(v: DataGridComponent<Contact>) {
        this.dataGrid$.next(v);
    }
    private dataGrid$ = new ReplaySubject<DataGridComponent<Contact>>(1);

    public dataHooks = {
        addDataPermission: FeaturePermission.ContactsWrite,
        hasNoData$: new ReplaySubject<boolean>(1),
        addData: () => this.dataGrid$.pipe(first()).subscribe((grid) => grid.addRow()),
    };

    public constructor(
        private contactService: ContactService,
        private dialogService: DialogService,
    ) {
        this.chips = Object.values(ContactCategoryMetadata).map((m) => ({
            value: m.category,
            icon: m.icon,
            text: m.label,
        }));
        this.contacts$ = this.selectedCategory$.pipe(
            undefinedWhileLoading((status) => contactService.getAll(status)),
        );
        this.columns$ = combineLatest([this.selectedCategory$, this.dataGrid$]).pipe(
            map(([currentCategory, dataGrid]) => {
                const visibleCategories =
                    typeof currentCategory === "undefined"
                        ? Object.values(ContactCategory)
                        : [currentCategory];
                return this.getColumns(visibleCategories, dataGrid);
            }),
        );
    }

    public addContact = () => {
        return this.dialogService.open(EditContactDialogComponent, undefined);
    };

    public editContact = (contact: Contact) => {
        return this.dialogService.open(EditContactDialogComponent, contact);
    };

    public deleteContact = (contact: Contact) => {
        return this.contactService.delete(contact.id);
    };

    protected getColumns(
        visibleCategories: ContactCategory[],
        dataGrid: DataGridComponent<Contact>,
    ): ColDef<Contact>[] {
        return [
            {
                cellRenderer: ContactCategoryIconRenderer,
                headerName: "",
                pinned: "left",
                width: 48,
                cellStyle: {
                    padding: 0,
                    "padding-left": "1rem",
                },
                valueGetter: (params) =>
                    params.data ? contactCategory(params.data) : ContactCategory.Current,
                ...columnExportColDef({
                    processHeaderCallback: () => "Category",
                    preprocessCellValue(params) {
                        const category: ContactCategory = params.value;
                        return ContactCategoryMetadata[category].label;
                    },
                }),
            },
            {
                field: "name",
                pinned: "left",
                cellStyle: {
                    "padding-left": 0,
                },
            },
            {
                field: "email",
                width: 280,
                cellRenderer: GridEmailLinkRenderer,
                ...wrapTextColDef(),
            },
            {
                field: "phoneNo",
                ...wrapTextColDef(),
            },
            {
                field: "affiliation",
                ...wrapTextColDef(),
            },
            {
                field: "archivedOn",
                hide: !visibleCategories.includes(ContactCategory.Archived),
                ...dateColDef(),
            },
            {
                field: "notes",
                flex: 1,
                minWidth: 400,
                ...wrapTextColDef(),
            },
            buildEditColumn(dataGrid, [
                {
                    icon: (contact) => (contact.archivedOn ? "unarchive" : "archive"),
                    tooltip: (contact) => (contact.archivedOn ? "Unarchive" : "Archive"),
                    onClick: (contact) => {
                        if (contact.archivedOn) {
                            return this.dialogService.openConfirmation(
                                "Are you sure you want to unarchive this contact?",
                                () => this.contactService.unarchive(contact),
                            );
                        } else {
                            return this.dialogService.openConfirmation(
                                "Are you sure you want to archive this contact?",
                                () => this.contactService.archive(contact),
                            );
                        }
                    },
                },
            ]),
        ];
    }
}
