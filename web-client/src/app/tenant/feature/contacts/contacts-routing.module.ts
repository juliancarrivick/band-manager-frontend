import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { ContactsPageComponent } from "./contacts-page/contacts-page.component";
import { helpPages } from "app/help/route/help-list";

const contactsRoutes: AppRoutes = [
    {
        path: "",
        component: ContactsPageComponent,
        data: {
            title: "Contacts",
            icon: "contacts",
            restrictedToPermission: FeaturePermission.ContactsRead,
            help: [helpPages.contacts],
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(contactsRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppContactsRoutingModule {}
