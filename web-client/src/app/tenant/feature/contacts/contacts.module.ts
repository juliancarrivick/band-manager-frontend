import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppChipSelectionModule } from "app/common-ux/chip-selection/chip-selection.module";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { ContactService } from "./contact.service";
import { ContactsPageComponent } from "./contacts-page/contacts-page.component";
import { AppContactsRoutingModule } from "./contacts-routing.module";
import { EditContactDialogComponent } from "./edit-contact-dialog/edit-contact-dialog.component";

@NgModule({
    imports: [
        CommonModule,

        AppContactsRoutingModule,
        AppPageWrapperModule,
        AppDataGridModule,
        AppLoadingModule,
        AppDialogModule,
        AppButtonModule,
        AppFormModule,
        AppChipSelectionModule,
        AppSaveCancelButtonsModule,
    ],
    declarations: [
        ContactsPageComponent,
        EditContactDialogComponent,
    ],
    providers: [
        ContactService,
    ],
})
export class AppContactsModule {}
