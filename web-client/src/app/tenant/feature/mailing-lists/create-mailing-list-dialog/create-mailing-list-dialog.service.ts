import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { switchMap } from "rxjs";
import { MailingList, MailingListAddressTemplateParameters } from "../mailing-list";

export type CreateMailingListDto = Omit<MailingList, "id">;

export interface CreateMailingListDialogDataDto {
    addressParameters: MailingListAddressTemplateParameters;
}

@Injectable()
export class CreateMailingListDialogService {
    public constructor(
        private appConfig: AppConfig,
        private httpClient: HttpClient,
    ) {}

    public getData() {
        return this.appConfig
            .serverEndpoint(`create-mailing-list-dialog`)
            .pipe(switchMap((url) => this.httpClient.get<CreateMailingListDialogDataDto>(url)));
    }

    public create(dto: CreateMailingListDto) {
        return this.appConfig
            .serverEndpoint(`create-mailing-list-dialog`)
            .pipe(switchMap((url) => this.httpClient.post<MailingList>(url, dto)));
    }
}
