import { GridIconRenderer } from "app/common-ux/data-grid/grid-icon-renderer";

export class RecipientSendStatusIconRenderer extends GridIconRenderer<string> {
    protected icon(dropReason: string | undefined): string {
        return dropReason ? "block" : "check";
    }
    protected tooltip(dropReason: string | undefined): string {
        return dropReason ?? "Recipient will receive emails";
    }
}
