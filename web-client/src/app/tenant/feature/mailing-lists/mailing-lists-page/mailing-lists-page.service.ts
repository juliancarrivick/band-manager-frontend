import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { switchMap } from "rxjs";

export interface MailingListsSummaryDto {
    mailingLists: MailingListSummaryDto[];
    usageHistory: UsageDatum[];
    usageCutOffDaysAgo: number;
    maxMailingListCount: number;
}

export interface MailingListSummaryDto {
    id: number;
    name: string;
    email: string;
    recipientCount: number;
    lastUsed?: Date;
}

export interface UsageDatum {
    date: Date;
    mailingListId: number;
    mailingListName: string;
    sendCount: number;
}

@Injectable()
export class MailingListsPageService {
    public constructor(
        private appConfig: AppConfig,
        private httpClient: HttpClient,
    ) {}

    public getSummary() {
        return this.appConfig
            .serverEndpoint("mailing-lists-page")
            .pipe(switchMap((url) => this.httpClient.get<MailingListsSummaryDto>(url)));
    }
}
