import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { helpPages } from "app/help/route/help-list";
import { EditMailingListPageComponent } from "./edit-mailing-list-page/edit-mailing-list-page.component";
import { MailingListsPageComponent } from "./mailing-lists-page/mailing-lists-page.component";

export const mailingListsRoutes: AppRoutes = [
    {
        path: "",
        component: MailingListsPageComponent,
        data: {
            title: "Mailing Lists",
            icon: "email",
            restrictedToPermission: FeaturePermission.MailingListRead,
            help: [helpPages.mailingLists],
        },
    },
    {
        path: ":mailingListId/edit",
        component: EditMailingListPageComponent,
        data: {
            icon: "email",
            restrictedToPermission: FeaturePermission.MailingListWrite,
        },
    },
];
