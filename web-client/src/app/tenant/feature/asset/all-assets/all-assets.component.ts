import { Component } from "@angular/core";
import { FeaturePermission } from "app/auth/feature-permission";
import { addRowRef } from "app/common-ux/data-grid/data-grid-consts";
import { findElementByRef } from "app/common/utils";
import { HelpablePage } from "app/help/help-broker.service";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { BehaviorSubject } from "rxjs";
import { allAssetsPageId } from "../asset-consts";

@Component({
    selector: "app-all-assets",
    templateUrl: "./all-assets.component.html",
    standalone: false,
})
export class AllAssetsComponent implements HelpablePage {
    public readonly pageId = allAssetsPageId;
    public hasEditPermissions$ = this.authorisationService.hasPermission(
        FeaturePermission.AssetWrite,
    );
    public dataHooks = {
        addDataPermission: FeaturePermission.AssetWrite,
        hasNoData$: new BehaviorSubject<boolean>(false),
        addData: () => findElementByRef(addRowRef)?.click(),
    };

    constructor(private authorisationService: TenantAuthorisationService) {}
}
