import { Injectable } from "@angular/core";
import { generatePatchDocument } from "app/common/utils";
import { BreezeEntityService } from "app/dal/breeze-domain.service";
import { Asset, AssetBreezeModel } from "app/dal/model/asset";
import { AssetLoan, AssetLoanBreezeModel } from "app/dal/model/asset-loan";
import { EntityQuery, Predicate } from "breeze-client";
import { lastValueFrom } from "rxjs";
import { map, switchMap } from "rxjs/operators";

export interface AssetMetadataDto {
    asset: Asset;
    attachmentCount: number;
}

@Injectable({
    providedIn: "root",
})
export class AssetService extends BreezeEntityService<Asset> {
    protected readonly breezeModel = AssetBreezeModel;

    private get entityEndpoint$() {
        return this.apiEndpoint$.pipe(map((apiEndpoint) => `${apiEndpoint}/asset`));
    }

    public getAllWithMetadata() {
        return this.breezeService.entityManager$.pipe(
            switchMap(async (em) => {
                const query = new EntityQuery("AssetsWithMetadata");
                const queryResult = await em.executeQuery(query);
                return queryResult.results as AssetMetadataDto[];
            }),
        );
    }

    public getCurrentLoans() {
        const query = this.breezeService
            .createQuery(AssetLoanBreezeModel)
            .where(new Predicate("dateReturned", "==", null))
            .expand("member");
        return this.breezeService.executeQuery<AssetLoan>(query);
    }

    public create() {
        return this.breezeService.create(AssetBreezeModel, {
            quantity: 1,
            dateAcquired: new Date(),
        });
    }

    public async getLoanHistoryByAssetId(assetId: number) {
        return lastValueFrom(
            this.entityEndpoint$.pipe(
                switchMap((entityEndpoint) => this.http.get(`${entityEndpoint}/${assetId}/loan`)),
                map(this.toLoanArray),
            ),
        );
    }

    public getCurrentLoanByAssetId(assetId: number): Promise<AssetLoan | undefined> {
        return lastValueFrom(
            this.entityEndpoint$.pipe(
                switchMap((entityEndpoint) =>
                    this.http.get(`${entityEndpoint}/${assetId}/loan/current`),
                ),
                map(this.toLoan),
            ),
        );
    }

    public returnLoanByAssetId(assetId: number) {
        return lastValueFrom(
            this.entityEndpoint$.pipe(
                switchMap((entityEndpoint) =>
                    this.http.post(`${entityEndpoint}/${assetId}/loan/return`, ""),
                ),
                map(this.toLoan),
            ),
        );
    }

    public loanAssetToMember(assetId: number, memberId: number) {
        return lastValueFrom(
            this.entityEndpoint$.pipe(
                map((entityEndpoint) => `${entityEndpoint}/${assetId}/loan/to/member/${memberId}`),
                switchMap((endpoint) => this.http.post(endpoint, "")),
                map(this.toLoan),
            ),
        );
    }

    public updateLoan(assetLoanId: number, updatedData: Partial<AssetLoan>) {
        const patchDocument = generatePatchDocument(updatedData);
        return lastValueFrom(
            this.apiEndpoint$.pipe(
                switchMap((apiEndpoint) =>
                    this.http.patch<void>(
                        `${apiEndpoint}/asset_loan/${assetLoanId}`,
                        patchDocument,
                    ),
                ),
            ),
        );
    }

    public deleteLoan(assetLoanId: number) {
        return lastValueFrom(
            this.apiEndpoint$.pipe(
                switchMap((apiEndpoint) =>
                    this.http.delete(`${apiEndpoint}/asset_loan/${assetLoanId}`),
                ),
            ),
        );
    }

    private toLoanArray(this: void, response: unknown) {
        if (!Array.isArray(response)) {
            return [];
        }

        return response.map((l: unknown) => {
            const loan = new AssetLoan();
            Object.assign(loan, l);
            return loan;
        });
    }

    private toLoan(this: void, response: unknown) {
        if (!response) {
            return undefined;
        }

        const loan = new AssetLoan();
        Object.assign(loan, response);
        return loan;
    }
}
