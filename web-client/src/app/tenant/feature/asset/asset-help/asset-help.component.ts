import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { HelpBrokerService } from "app/help/help-broker.service";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";
import { allAssetsPageId } from "../asset-consts";

@Component({
    templateUrl: "./asset-help.component.html",
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class AssetHelpComponent {
    public assetHooks$ = this.helpBroker.helpablePageDataHooks(allAssetsPageId);

    public constructor(private helpBroker: HelpBrokerService) {}
}
