import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BaseBreezeDialog } from "app/common-ux/dialog/base-breeze-dialog";
import { EntityTracker } from "app/dal/entity-tracker";
import { Asset } from "app/dal/model/asset";
import { AssetService } from "../asset.service";

@Component({
    templateUrl: "./edit-asset-dialog.component.html",
    styleUrls: ["./edit-asset-dialog.component.scss"],
    standalone: false,
})
export class EditAssetDialogComponent extends BaseBreezeDialog<Asset> {
    public readonly dialogName = "EditAsset";

    public title: string;
    public entityTracker: EntityTracker;
    public saveError?: Error;

    public constructor(
        dialogRef: MatDialogRef<EditAssetDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public asset: Asset,
        assetService: AssetService,
    ) {
        super(dialogRef);
        this.title = asset.entityAspect.entityState.isAdded() ? "Add Asset" : "Edit Asset";

        this.entityTracker = new EntityTracker(assetService);
        this.entityTracker.add(asset);
    }
}
