import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { FeaturePermission } from "app/auth/feature-permission";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { Asset } from "app/dal/model/asset";
import {
    EntityAttachmentMetadata,
    EntityAttachmentSensitivity,
} from "app/storage/entity-attachment-metadata";
import { EntityAttachmentsDialogOptions } from "app/storage/entity-attachments-dialog/entity-attachments-dialog.component";

@Component({
    selector: "app-asset-attachments-dialog",
    template: `<app-entity-attachments-dialog [options]="options"></app-entity-attachments-dialog>`,
    standalone: false,
})
export class AssetAttachmentsDialogComponent extends BaseDialog<Asset, EntityAttachmentMetadata[]> {
    public readonly dialogName = "AssetAttachmentDialog";

    public options: EntityAttachmentsDialogOptions;

    public constructor(
        dialogRef: MatDialogRef<AssetAttachmentsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) asset: Asset,
    ) {
        super(dialogRef);
        this.options = {
            storageStrategyName: "AssetAttachment",
            entityId: asset.id,
            defaultSensitivity: EntityAttachmentSensitivity.Sensitive,
            entityFormattedText: asset.description,
            editPermission: FeaturePermission.AssetWrite,
            dialogClose$: this.dialogClose$,
            setDisableClose: (isDisabled) => (dialogRef.disableClose = isDisabled),
            resolve: (a) => this.resolve(a),
        };
    }
}
