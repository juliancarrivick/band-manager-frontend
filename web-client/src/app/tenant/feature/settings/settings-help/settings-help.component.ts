import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";

@Component({
    templateUrl: "./settings-help.component.html",
    styles: [
        `
            .stripe-link {
                display: block;
                margin-bottom: 0.25rem;
                max-width: 150px;
            }
        `,
    ],
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class SettingsHelpComponent {}
