import { Component, Injector, Type } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { deleteArrayElement } from "app/common/utils";
import { forkJoin, map, Observable, ObservableInput, of, startWith } from "rxjs";
import { GeneralSettingsComponent } from "../general-settings/general-settings.component";
import { MemberBillingSettingsComponent } from "../member-billing-settings/member-billing-settings.component";
import { SettingsService } from "../setting.service";
import { SettingsComponent } from "../settings-component.interface";
import { SubscriptionSettingsComponent } from "../subscription-settings/subscription-settings.component";

interface SettingsTab {
    icon: string;
    title: string;
    slug: string;
    content: Type<SettingsComponent>;
}

@Component({
    templateUrl: "./settings-page.component.html",
    styleUrls: ["./settings-page.component.scss"],
    providers: [
        SettingsService,
    ],
    standalone: false,
})
export class SettingsPageComponent {
    public tabs: SettingsTab[];
    private tabComponents: SettingsComponent[] = [];
    public selectedTabIndex$: Observable<number>;

    public constructor(
        private router: Router,
        private route: ActivatedRoute,
        public injector: Injector,
        _settingsService: SettingsService,
    ) {
        this.tabs = [
            {
                icon: "workspaces",
                title: "General",
                slug: "general",
                content: GeneralSettingsComponent,
            },
            {
                icon: "payment",
                title: "Subscription",
                slug: "subscription",
                content: SubscriptionSettingsComponent,
            },
            {
                icon: "monetization_on",
                title: "Member Billing",
                slug: "member-billing",
                content: MemberBillingSettingsComponent,
            },
        ];
        this.selectedTabIndex$ = route.paramMap.pipe(
            startWith(route.snapshot.paramMap),
            map((params) => params.get("section")),
            map((section) => this.tabs.findIndex((t) => t.slug === section)),
            map((index) => (index < 0 ? 0 : index)),
        );
    }

    public postSaveAction = () => {
        const actions = this.tabComponents
            .map((c) => c.postSaveAction?.())
            .filter((a): a is ObservableInput<unknown> => !!a);
        return actions.length > 0 ? forkJoin(actions) : of(undefined);
    };

    public addTabComponentInstance(component: SettingsComponent) {
        this.tabComponents.push(component);
    }

    public removeTabComponentInstance(component: SettingsComponent) {
        deleteArrayElement(this.tabComponents, component);
    }

    public refreshTabs() {
        for (const component of this.tabComponents) {
            component.refreshData();
        }
    }

    public async updateSelectedTab(index: number) {
        const tab = this.tabs[index];
        if (!tab) {
            return;
        }

        await this.router.navigate([{ section: tab.slug }], {
            relativeTo: this.route,
            replaceUrl: true,
        });
    }
}
