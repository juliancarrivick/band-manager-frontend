import { Component, ElementRef, ViewChild } from "@angular/core";
import { filterForDefinedValue } from "app/common/rxjs-utilities";
import { fromBreezeEvent } from "app/dal/breeze-rxjs-utilities";
import { Tenant } from "app/dal/model/tenant";
import { TenantService } from "app/tenant/module/tenant.service";
import { BehaviorSubject, Observable, combineLatest, map, switchMap } from "rxjs";
import { SettingsService } from "../setting.service";
import { SettingsComponent } from "../settings-component.interface";

@Component({
    templateUrl: "./general-settings.component.html",
    styleUrls: ["./general-settings.component.scss"],
    standalone: false,
})
export class GeneralSettingsComponent implements SettingsComponent {
    public tenant$: Observable<Tenant>;
    public uploadedLogo$ = new BehaviorSubject<string | undefined>(undefined);
    public logo$: Observable<string>;
    public now = new Date();

    @ViewChild("fileInput") public fileInput?: ElementRef<HTMLInputElement>;
    public logoValidationError$: Observable<string | undefined>;

    public constructor(
        settingsService: SettingsService,
        private tenantService: TenantService,
    ) {
        this.tenant$ = settingsService.getTenantEntity().pipe(filterForDefinedValue());
        this.logo$ = combineLatest([
            tenantService.currentTenant$,
            this.uploadedLogo$,
        ]).pipe(
            map(([tenantDtoLogo, uploadedLogo]) => {
                return uploadedLogo ?? tenantDtoLogo?.logo ?? "/assets/group_placeholder.svg";
            }),
        );
        this.logoValidationError$ = this.tenant$.pipe(
            switchMap((t) => fromBreezeEvent(t.entityAspect.validationErrorsChanged)),
            map((t) => t.entity.entityAspect.getValidationErrors("logo")),
            map((errors) => errors.map((e) => e.errorMessage)),
            map((errors) => (errors.length > 0 ? errors.join(", ") : undefined)),
        );
    }

    public postSaveAction() {
        return this.tenantService.forceTenantRefresh();
    }

    public refreshData(): void {
        this.uploadedLogo$.next(undefined);

        if (this.fileInput?.nativeElement) {
            this.fileInput.nativeElement.value = "";
        }
    }

    public async onFileSelected(tenant: Tenant) {
        const input = this.fileInput?.nativeElement;
        if (!input) {
            return;
        }

        try {
            if (!input.files || input.files.length !== 1) {
                return;
            }

            const file = input.files[0];
            const newLogo = await this.getFileAsDataUrl(file!);

            this.uploadedLogo$.next(newLogo);
            tenant.logo = this.getBase64TextFromDataUrl(newLogo) ?? null;
        } catch (e) {
            this.uploadedLogo$.next(undefined);
        }
    }

    private getFileAsDataUrl(file: File) {
        return new Promise<string>((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = (e) => {
                if (reader.result) {
                    resolve(reader.result as string);
                } else {
                    reject(new Error("No data in file"));
                }
            };
            reader.onerror = (e) => {
                reject(new Error("Error while reading"));
            };
            reader.onabort = (e) => {
                reject(new Error("Reading aborted"));
            };

            reader.readAsDataURL(file);
        });
    }

    private getBase64TextFromDataUrl(dataUrl: string) {
        const partitionedDataUrl = dataUrl.split(";base64,");
        return partitionedDataUrl[1];
    }
}
