import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { helpPages } from "app/help/route/help-list";
import { SettingsPageComponent } from "./settings-page/settings-page.component";

const configRoutes: AppRoutes = [
    {
        path: "",
        component: SettingsPageComponent,
        data: {
            title: "Settings",
            icon: "settings",
            restrictedToPermission: [
                FeaturePermission.SettingsWrite,
                FeaturePermission.MemberBillingWrite,
            ],
            help: [helpPages.settings],
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(configRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppConfigRoutingModule {}
