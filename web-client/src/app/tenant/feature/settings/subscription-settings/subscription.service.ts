import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { map, Observable, switchMap } from "rxjs";

export enum PlanType {
    Trial = "Trial",
    Free = "Free",
    Small = "Small",
    Medium = "Medium",
    Large = "Large",
    Unlimited = "Unlimited",
    Demo = "Demo",
}

export interface SubscriptionDetailsDto {
    currentPlan: PlanType;
    nextPlan: PlanType;
    isAllowedToActivatePaidPlan: boolean;
    monthlyFeeDollars?: number;
    isNotForProfit: boolean;
    maximumMemberCount?: number;
    dateRegistered: Date;
    accessEndsDate?: Date;
    canAccessCustomerPortal: boolean;
    contactDetails?: {
        name: string;
        email: string;
        address?: string;
    };
    paymentMethod?: {
        type: string;
        preview: string;
    };
    upcomingInvoice?: UpcomingInvoiceDto;
    previousInvoices: PreviousInvoiceDto[];
}

export interface UpcomingInvoiceDto {
    billingDate: Date;
    description: string;
    startDate: Date;
    endDate: Date;
    totalDollars: number;
}

export interface PreviousInvoiceDto {
    description: string;
    startDate: Date;
    endDate: Date;
    totalDollars: number;
    url?: string;
}

export interface PaidPlanDto {
    planType: PlanType;
    monthlyFeeDollars: number;
    nfpMonthlyFeeDollars: number;
    maxMemberCount?: number;
}

@Injectable()
export class SubscriptionService {
    public constructor(
        private appConfig: AppConfig,
        private httpClient: HttpClient,
    ) {}

    public getSubscriptionDetails(): Observable<SubscriptionDetailsDto> {
        return this.appConfig
            .serverEndpoint(`settings/subscription/details`)
            .pipe(switchMap((url) => this.httpClient.get<SubscriptionDetailsDto>(url)));
    }

    public getPaidPlans() {
        return this.appConfig.serverEndpoint(`settings/subscription/paid-plans`).pipe(
            switchMap((url) => this.httpClient.get<PaidPlanDto[]>(url)),
            map((plans) => plans.sort((a, b) => a.monthlyFeeDollars - b.monthlyFeeDollars)),
        );
    }

    public getCustomerPortalUrl(): Observable<string> {
        return this.appConfig.serverEndpoint(`settings/subscription/customer-portal-url`).pipe(
            switchMap((endpoint) => {
                return this.httpClient.get(endpoint, {
                    responseType: "text",
                    params: {
                        returnUrl: location.href,
                    },
                });
            }),
            map((buffer) => String(buffer)),
        );
    }

    public getSubscriptionCheckoutUrl(planType: PlanType, promoCode?: string): Observable<string> {
        return this.appConfig
            .serverEndpoint(`settings/subscription/subscription-checkout-url`)
            .pipe(
                switchMap((endpoint) => {
                    const params = new HttpParams();
                    params.set("planType", planType);
                    params.set("returnUrl", location.href);
                    if (promoCode) {
                        params.set("promoCode", promoCode);
                    }

                    return this.httpClient.get(endpoint, {
                        responseType: "text",
                        params,
                    });
                }),
                map((buffer) => String(buffer)),
            );
    }
}
