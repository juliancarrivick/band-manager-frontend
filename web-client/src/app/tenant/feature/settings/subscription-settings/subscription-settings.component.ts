import { Component } from "@angular/core";
import { getErrorMessage } from "app/common/error";
import { undefinedWhileLoading } from "app/common/rxjs-utilities";
import { BehaviorSubject, catchError, NEVER, Observable, of, switchMap, tap } from "rxjs";
import { SettingsComponent } from "../settings-component.interface";
import {
    SubscriptionDetailsDto,
    SubscriptionService,
    PlanType,
    PaidPlanDto,
} from "./subscription.service";

@Component({
    selector: "app-subscription-settings",
    templateUrl: "./subscription-settings.component.html",
    styleUrls: ["./subscription-settings.component.scss"],
    providers: [
        SubscriptionService,
    ],
    standalone: false,
})
export class SubscriptionSettingsComponent implements SettingsComponent {
    public subscription$: Observable<SubscriptionDetailsDto | undefined>;
    public paidPlans$: Observable<PaidPlanDto[]>;
    public selectedPlan?: PlanType;
    public promoCode?: string;
    public checkoutError?: string;

    public PlanType = PlanType;
    private refreshData$ = new BehaviorSubject<void>(undefined);

    public paidPlanIcons: Partial<Record<PlanType, string>> = {
        [PlanType.Small]: "assets/notation_icons/quaver.svg",
        [PlanType.Medium]: "assets/notation_icons/crotchet.svg",
        [PlanType.Large]: "assets/notation_icons/minim.svg",
    };

    public constructor(private subscriptionService: SubscriptionService) {
        this.subscription$ = this.refreshData$.pipe(
            undefinedWhileLoading(() => subscriptionService.getSubscriptionDetails()),
        );
        this.paidPlans$ = this.subscriptionService.getPaidPlans();
    }

    public refreshData(): void {
        this.refreshData$.next();
    }

    public openCustomerPortal = () => {
        return this.subscriptionService.getCustomerPortalUrl().pipe(
            tap((url) => (window.location.href = url)),
            switchMap(() => NEVER), // So the button doesn't stop spinning
        );
    };

    public openSubscriptionCheckout = (planType?: PlanType) => {
        if (!planType) {
            return of({});
        }

        return this.subscriptionService.getSubscriptionCheckoutUrl(planType, this.promoCode).pipe(
            tap((url) => (window.location.href = url)),
            switchMap(() => NEVER), // So the button doesn't stop spinning
            catchError((e) => (this.checkoutError = getErrorMessage(e))),
        );
    };
}
