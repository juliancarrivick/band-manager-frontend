import { ObservableInput } from "rxjs";

export interface SettingsComponent {
    postSaveAction?(): ObservableInput<unknown>;
    refreshData(): void;
}
