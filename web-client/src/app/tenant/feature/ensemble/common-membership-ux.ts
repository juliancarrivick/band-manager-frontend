import { pastelPalette } from "app/common-ux/palette";
import { FeeClass } from "app/dal/model/member";
import { HslColour } from "../../../common-ux/colours";

export interface MembershipCategory {
    feeClass: FeeClass;
    label: string;
    fill: HslColour;
    stroke: HslColour;
}

export const fullMemberCategory: MembershipCategory = {
    feeClass: FeeClass.Full,
    label: "Full",
    fill: pastelPalette.darkGreen,
    stroke: pastelPalette.darkGreen.addLightness(-10),
};
export const concessionMemberCategory: MembershipCategory = {
    feeClass: FeeClass.Concession,
    label: "Concession",
    fill: pastelPalette.darkBlue,
    stroke: pastelPalette.darkBlue.addLightness(-10),
};
export const specialMemberCategory: MembershipCategory = {
    feeClass: FeeClass.Special,
    label: "Special",
    fill: pastelPalette.darkPurple,
    stroke: pastelPalette.darkPurple.addLightness(-10),
};

export const membershipCategories: readonly MembershipCategory[] = [
    fullMemberCategory,
    concessionMemberCategory,
    specialMemberCategory,
];
