import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { HelpBrokerService, HelpDataHooks } from "app/help/help-broker.service";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";
import { Observable } from "rxjs";
import { ensemblesPageId } from "../ensemble-consts";

@Component({
    templateUrl: "./ensemble-help.component.html",
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class EnsembleHelpComponent {
    public ensemblesPageDataHooks$: Observable<HelpDataHooks | undefined>;

    public constructor(helpBroker: HelpBrokerService) {
        this.ensemblesPageDataHooks$ = helpBroker.helpablePageDataHooks(ensemblesPageId);
    }
}
