import { Component, Input } from "@angular/core";
import { filterForDefinedValue } from "app/common/rxjs-utilities";
import { Member } from "app/dal/model/member";
import { Performance } from "app/dal/model/performance";
import { Score } from "app/dal/model/score";
import { TenantService } from "app/tenant/module/tenant.service";
import { map, Observable, ReplaySubject, switchMap, zip } from "rxjs";
import { EnsembleService } from "../ensemble.service";

interface PerformanceDetails {
    scores: Score[];
    members: Member[];
}

@Component({
    selector: "app-ensemble-performance-panel",
    templateUrl: "./ensemble-performance-panel.component.html",
    styleUrls: [
        "../../../../common-ux/wrapped-expansion-panel.scss",
        "./ensemble-performance-panel.component.scss",
    ],
    standalone: false,
})
export class EnsemblePerformancePanelComponent {
    @Input() public set performance(value: Performance) {
        this.performance$.next(value);
    }
    public performance$ = new ReplaySubject<Performance>(1);
    public details$: Observable<PerformanceDetails>;
    public tenantAbbr$: Observable<string>;

    public constructor(ensembleService: EnsembleService, tenantService: TenantService) {
        this.details$ = zip([
            this.performance$.pipe(
                switchMap((p) => ensembleService.getPerformanceScores(p)),
                map((scores) =>
                    scores.sort((a, b) => a.formattedName.localeCompare(b.formattedName)),
                ),
            ),
            this.performance$.pipe(
                switchMap((p) => ensembleService.getPerformanceMembers(p)),
                map((members) => members.sort((a, b) => a.compare(b))),
            ),
        ]).pipe(map(([scores, members]) => ({ scores, members })));
        this.tenantAbbr$ = tenantService.currentTenant$.pipe(
            filterForDefinedValue(),
            map((t) => t.abbreviation),
        );
    }
}
