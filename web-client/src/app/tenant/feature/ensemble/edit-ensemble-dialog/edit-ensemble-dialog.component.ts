import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FeaturePermission } from "app/auth/feature-permission";
import { roleLabel } from "app/auth/role";
import { RoleName } from "app/auth/role.enum";
import { BaseBreezeDialog } from "app/common-ux/dialog/base-breeze-dialog";
import { BreezeFormGroup } from "app/common-ux/form/breeze-form-control";
import { EntityTracker } from "app/dal/entity-tracker";
import { Ensemble } from "app/dal/model/ensemble";
import { Role } from "app/dal/model/role";
import { Observable, map } from "rxjs";
import { EnsembleService } from "../ensemble.service";

@Component({
    templateUrl: "./edit-ensemble-dialog.component.html",
    styleUrls: ["./edit-ensemble-dialog.component.scss"],
    standalone: false,
})
export class EditEnsembleDialogComponent extends BaseBreezeDialog<Ensemble> {
    public readonly dialogName = "EditEnsemble";
    public readonly FeaturePermission = FeaturePermission;

    public title: string;
    public entityTracker: EntityTracker;
    public form: BreezeFormGroup<Ensemble, ["name"]>;
    public saveError?: Error;

    public roles$: Observable<Role[]>;
    public selectedRoles$: Observable<Role[]>;

    public constructor(
        dialogRef: MatDialogRef<EditEnsembleDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public ensemble: Ensemble,
        private ensembleService: EnsembleService,
    ) {
        super(dialogRef);

        this.title = ensemble.entityAspect.entityState.isAdded() ? "Add Ensemble" : "Edit Ensemble";
        this.entityTracker = new EntityTracker(ensembleService);
        this.entityTracker.add(ensemble);

        this.form = new BreezeFormGroup(ensemble, ["name"], dialogRef.afterClosed());

        this.roles$ = ensembleService.getAllRoles();
        this.selectedRoles$ = ensembleService
            .getEnsembleRolesWithRole(ensemble)
            .pipe(map((ensembleRoles) => ensembleRoles.map((e) => e.role!)));
    }

    public addRole(role: Role) {
        this.ensembleService.addEnsembleRole(this.ensemble, role).subscribe((e) => {
            this.entityTracker.add(e);
        });
    }

    public removeRole(role: Role) {
        const ensembleRole = this.ensemble.ensembleRole.find((e) => e.role === role);
        if (ensembleRole) {
            this.entityTracker.add(ensembleRole);
            this.ensembleService.delete(ensembleRole);
        }
    }

    public getRoleName(role: Role) {
        return (roleLabel[role.name as RoleName] as string | undefined) ?? "Unknown";
    }
}
