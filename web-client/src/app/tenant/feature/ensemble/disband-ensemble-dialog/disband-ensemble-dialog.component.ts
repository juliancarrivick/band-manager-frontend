import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { cacheLatest } from "app/common/rxjs-utilities";
import { Ensemble } from "app/dal/model/ensemble";
import { Observable, catchError, map, of, tap } from "rxjs";
import { DisbandEnsembleDialogService, DisbandImpactDto } from "./disband-ensemble-dialog.service";

@Component({
    templateUrl: "./disband-ensemble-dialog.component.html",
    styleUrls: ["./disband-ensemble-dialog.component.scss"],
    providers: [DisbandEnsembleDialogService],
    standalone: false,
})
export class DisbandEnsembleDialogComponent extends BaseDialog<Ensemble> {
    public readonly dialogName = "DisbandEnsemble";

    public saveError?: Error;
    public confirmed = false;

    public impact$: Observable<DisbandImpactDto>;
    public removedMailingLists$: Observable<string | undefined>;
    public isOpen = false;
    public listMaxHeight?: number;

    public constructor(
        dialogRef: MatDialogRef<DisbandEnsembleDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public ensemble: Ensemble,
        private disbandService: DisbandEnsembleDialogService,
    ) {
        super(dialogRef);

        this.impact$ = disbandService.getImpact(ensemble).pipe(cacheLatest());
        this.removedMailingLists$ = this.impact$.pipe(
            map((i) => {
                const mailingLists = i.removedFromMailingLists;

                if (mailingLists.length === 0) {
                    return undefined;
                } else if (mailingLists.length === 1) {
                    return `${mailingLists[0]} mailing list`;
                } else if (mailingLists.length === 2) {
                    return `${mailingLists.join(" and ")} mailing lists`;
                }

                const lastTwo = mailingLists.slice(-2).join(" and ");
                const remaining = mailingLists.slice(0, -2);
                return `${[...remaining, lastTwo].join(", ")} mailing lists`;
            }),
        );
    }

    public toggleOpen() {
        this.isOpen = !this.isOpen;
        if (!this.isOpen) {
            delete this.listMaxHeight;
        }
    }

    public calculateMemberListHeight(event: TransitionEvent) {
        if (this.isOpen && event.target instanceof HTMLElement) {
            this.listMaxHeight = event.target.clientHeight;
        }
    }

    public disbandEnsemble = () => {
        return this.disbandService.disbandEnsemble(this.ensemble).pipe(
            tap(() => {
                this.resolve(this.ensemble);
            }),
            catchError((e) => {
                this.saveError = e;
                return of(undefined);
            }),
        );
    };
}
