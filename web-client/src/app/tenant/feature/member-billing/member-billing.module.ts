import { TextFieldModule } from "@angular/cdk/text-field";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatCardModule } from "@angular/material/card";
import { MatDividerModule } from "@angular/material/divider";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatMenuModule } from "@angular/material/menu";
import { MatSelectModule } from "@angular/material/select";
import { MatTooltipModule } from "@angular/material/tooltip";
import { AgChartsModule } from "ag-charts-angular";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppChipSelectionModule } from "app/common-ux/chip-selection/chip-selection.module";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { AppDateModule } from "app/common-ux/date/date.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { PrimaryRouterLinkDirective } from "app/common-ux/router/primary-router-link.directive";
import { AppSelectMemberModule } from "app/tenant/feature/member/select-member/select-member.module";
import { BillingPeriodCardComponent } from "./billing-period-card/billing-period-card.component";
import { BillingPeriodPageComponent } from "./billing-period-page/billing-period-page.component";
import { BillingPeriodStateBadgeComponent } from "./billing-period-state-badge/billing-period-state-badge.component";
import { BillingPeriodsPageComponent } from "./billing-periods-page/billing-periods-page.component";
import { EditInvoiceLineColumnRendererComponent } from "./edit-invoice-line-column-renderer/edit-invoice-line-column-renderer.component";
import { MarkInvoicePaidDialogComponent } from "./mark-invoice-paid-dialog/mark-invoice-paid-dialog.component";
import { AppMemberBillingRoutingModule } from "./member-billing-routing.module";
import { MemberBillingService } from "./member-billing.service";
import { MemberInvoiceLineItemsGridComponent } from "./member-invoice-line-items-grid/member-invoice-line-items-grid.component";
import { MemberInvoicePanelComponent } from "./member-invoice-panel/member-invoice-panel.component";
import { MemberInvoiceStateBadgeComponent } from "./member-invoice-state-badge/member-invoice-state-badge.component";

@NgModule({
    imports: [
        CommonModule,

        TextFieldModule,

        MatCardModule,
        MatExpansionModule,
        MatDividerModule,
        MatTooltipModule,
        MatSelectModule,
        MatMenuModule,

        AgChartsModule,

        AppButtonModule,
        AppPageWrapperModule,
        AppBadgeModule,
        AppDataGridModule,
        AppDialogModule,
        AppMemberBillingRoutingModule,
        AppFormModule,
        AppLoadingModule,
        AppDateModule,
        AppSelectMemberModule,
        AppChipSelectionModule,

        BillingPeriodStateBadgeComponent,
        MemberInvoiceStateBadgeComponent,
        PrimaryRouterLinkDirective,
    ],
    declarations: [
        BillingPeriodsPageComponent,
        BillingPeriodCardComponent,
        BillingPeriodPageComponent,
        MemberInvoicePanelComponent,
        EditInvoiceLineColumnRendererComponent,
        MarkInvoicePaidDialogComponent,
        MemberInvoiceLineItemsGridComponent,
    ],
    providers: [
        MemberBillingService,
    ],
})
export class AppMemberBillingModule {}
