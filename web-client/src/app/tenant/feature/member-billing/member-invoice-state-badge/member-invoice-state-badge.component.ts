import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import {
    billingPeriodStateCompare,
    MemberBillingPeriodState,
} from "app/dal/model/member-billing-period";
import { BehaviorSubject, combineLatest, map, Observable, ReplaySubject, switchMap } from "rxjs";
import {
    paidEarlyCategory,
    paidLateCategory,
    paidOnTimeCategory,
    unpaidCategory,
} from "../common-member-billing-ux";

export interface InvoiceShape {
    paid?: Date | null;
    sent?: Date | null;
}

export interface BillingPeriodShape {
    earlyBirdDue?: Date | null;
    due: Date;
    state: MemberBillingPeriodState;
}

@Component({
    selector: "app-member-invoice-state-badge",
    templateUrl: "./member-invoice-state-badge.component.html",
    imports: [
        CommonModule,
        AppBadgeModule,
    ],
})
export class MemberInvoiceStateBadgeComponent {
    @Input() public set invoice(value: InvoiceShape | undefined | null) {
        this.invoice$.next(value);
    }
    private invoice$ = new ReplaySubject<InvoiceShape | undefined | null>(1);
    @Input() public set billingPeriod(value: BillingPeriodShape | undefined | null) {
        this.billingPeriod$.next(value);
    }
    private billingPeriod$ = new ReplaySubject<BillingPeriodShape | undefined | null>(1);

    public badge$: Observable<{ icon: string; text: string; colour: string }>;
    private refresh$ = new BehaviorSubject<void>(undefined);

    public constructor() {
        // TODO tests for this - it's complicated!
        this.badge$ = this.refresh$.pipe(
            switchMap(() => combineLatest([this.invoice$, this.billingPeriod$])),
            map(([invoice, billingPeriod]) => {
                if (!invoice || !billingPeriod) {
                    return { icon: "", text: "", colour: "" };
                }

                const paymentScheduleCategory = this.findAppropriatePaymentScheduleCategory(
                    invoice,
                    billingPeriod,
                );
                if (!paymentScheduleCategory) {
                    return { icon: "edit_note", text: "Draft", colour: "#e0e0e0" };
                }

                return {
                    icon: paymentScheduleCategory.icon,
                    text: paymentScheduleCategory.label,
                    colour: paymentScheduleCategory.fill
                        .addSaturation(10)
                        .addLightness(15)
                        .toCssString(),
                };
            }),
        );
    }

    public refresh() {
        this.refresh$.next();
    }

    private findAppropriatePaymentScheduleCategory(
        invoice: InvoiceShape,
        billingPeriod: BillingPeriodShape,
    ) {
        if (invoice.paid) {
            if (billingPeriod.earlyBirdDue && invoice.paid < billingPeriod.earlyBirdDue) {
                return paidEarlyCategory;
            } else if (invoice.paid > billingPeriod.due) {
                return paidLateCategory;
            } else {
                return paidOnTimeCategory;
            }
        } else if (
            billingPeriodStateCompare(billingPeriod.state, MemberBillingPeriodState.Draft) > 0 ||
            invoice.sent
        ) {
            return unpaidCategory;
        }
    }
}
