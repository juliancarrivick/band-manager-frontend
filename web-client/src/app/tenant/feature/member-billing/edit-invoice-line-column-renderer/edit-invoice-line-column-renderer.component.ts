import { ChangeDetectorRef, Component, OnDestroy } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { AgEvent, ICellRendererParams } from "ag-grid-community";
import { fromGridEvent } from "app/common-ux/data-grid/grid-utils";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { BreezeService } from "app/dal/breeze.service";
import { MemberInvoiceLineItem } from "app/dal/model/member-invoice";
import { Subscription, merge, of } from "rxjs";
import { filter, map, tap } from "rxjs/operators";

export class LineItemRowDeletedEvent implements AgEvent<"appRowDeleted"> {
    public readonly type = "appRowDeleted";

    public constructor(public readonly lineItem: MemberInvoiceLineItem) {}
}

export class LineItemRowEditedEvent implements AgEvent<"appRowEdited"> {
    public readonly type = "appRowEdited";

    public constructor(public readonly lineItem: MemberInvoiceLineItem) {}
}

@Component({
    template: `
        <ng-container *ngIf="isEditing$ | async; else notEditingTemplate">
            <button
                mat-icon-button
                aria-label="Done"
                (click)="saveChanges()"
            >
                <mat-icon>done</mat-icon>
            </button>
            <button
                mat-icon-button
                aria-label="Cancel"
                (click)="cancelChanges()"
            >
                <mat-icon>cancel</mat-icon>
            </button>
        </ng-container>

        <ng-template #notEditingTemplate>
            <button
                mat-icon-button
                aria-label="Edit"
                (click)="editRow()"
            >
                <mat-icon>edit</mat-icon>
            </button>
            <button
                mat-icon-button
                aria-label="Delete"
                (click)="deleteRow()"
            >
                <mat-icon>delete</mat-icon>
            </button>
        </ng-template>
    `,
    standalone: false,
})
export class EditInvoiceLineColumnRendererComponent implements ICellRendererAngularComp, OnDestroy {
    public isEditing$ = of(false);
    private params?: ICellRendererParams;
    private row?: MemberInvoiceLineItem;
    private subscription?: Subscription;

    public constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private breezeService: BreezeService,
        private dialogService: DialogService,
    ) {}

    agInit(params: ICellRendererParams): void {
        this.params = params;
        this.row = params.data;

        const rowEditingStarted$ = fromGridEvent(params, "rowEditingStarted").pipe(
            filter((e) => e.rowIndex === params.node.rowIndex),
            map(() => true),
        );
        const rowEditingStopped$ = fromGridEvent(params, "rowEditingStopped").pipe(
            filter((e) => e.rowIndex === params.node.rowIndex),
            map(() => false),
        );
        this.isEditing$ = merge(rowEditingStarted$, rowEditingStopped$).pipe(
            // Hack to fix view not changing on first click in row (seems to be bug in ag-grid)
            tap(() => setTimeout(() => this.changeDetectorRef.detectChanges())),
        );

        this.subscription = fromGridEvent(params, "rowValueChanged").subscribe((e) => {
            if (e.rowIndex === params.node.rowIndex) {
                params.api.dispatchEvent(new LineItemRowEditedEvent(this.row!));
            }
        });
    }

    public ngOnDestroy() {
        this.subscription?.unsubscribe();
    }

    public refresh(): boolean {
        return false;
    }

    public editRow() {
        if (!this.params) {
            return;
        }

        const column = this.params.api.getAllDisplayedColumns()[0];
        if (!column) {
            return;
        }

        this.params.api.setFocusedCell(this.params.node.rowIndex!, column);
        this.params.api.startEditingCell({
            rowIndex: this.params.node.rowIndex!,
            colKey: column,
        });
    }

    public deleteRow() {
        if (!this.params || !this.row) {
            return;
        }

        this.dialogService
            .openConfirmation("Are you sure you want to delete this line item?")
            .subscribe(() => {
                this.breezeService.delete(this.row!);
                this.params!.api.applyTransaction({ remove: [this.row!] });
                this.params?.api.dispatchEvent(new LineItemRowDeletedEvent(this.row!));
            });
    }

    public saveChanges() {
        if (!this.params) {
            return;
        }

        this.params.api.stopEditing();
    }

    public cancelChanges() {
        if (!this.params) {
            return;
        }

        const cancel = true;
        this.params.api.stopEditing(cancel);
    }
}
