import { pastelPalette } from "app/common-ux/palette";
import { HslColour } from "../../../common-ux/colours";
import { BillingPeriodStats } from "./member-billing.service";

export interface PaymentScheduleCategory {
    statProperty: keyof BillingPeriodStats;
    icon: string;
    label: string;
    fill: HslColour;
    stroke: HslColour;
}

export const paidEarlyCategory: PaymentScheduleCategory = {
    statProperty: "paidEarly",
    icon: "check",
    label: "Paid early",
    fill: pastelPalette.darkGreen,
    stroke: pastelPalette.darkGreen.addLightness(-10),
};
export const paidOnTimeCategory: PaymentScheduleCategory = {
    statProperty: "paidOnTime",
    icon: "check",
    label: "Paid on time",
    fill: pastelPalette.darkBlue,
    stroke: pastelPalette.darkBlue.addLightness(-10),
};
export const paidLateCategory: PaymentScheduleCategory = {
    statProperty: "paidLate",
    icon: "check",
    label: "Paid late",
    fill: pastelPalette.darkOrange,
    stroke: pastelPalette.darkOrange.addLightness(-10),
};
export const unpaidCategory: PaymentScheduleCategory = {
    statProperty: "unpaid",
    icon: "close",
    label: "Unpaid",
    fill: pastelPalette.darkRed,
    stroke: pastelPalette.darkRed.addLightness(-10),
};

export const paymentScheduleCategories: readonly PaymentScheduleCategory[] = [
    paidEarlyCategory,
    paidOnTimeCategory,
    paidLateCategory,
    unpaidCategory,
];
