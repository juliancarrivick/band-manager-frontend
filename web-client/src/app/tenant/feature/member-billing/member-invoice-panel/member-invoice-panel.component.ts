import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewChild,
} from "@angular/core";
import { FeaturePermission } from "app/auth/feature-permission";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { filterForDefinedValue } from "app/common/rxjs-utilities";
import { EntityTracker } from "app/dal/entity-tracker";
import { MemberBillingPeriodState } from "app/dal/model/member-billing-period";
import {
    MemberInvoice,
    MemberInvoiceLineItem,
    MemberInvoiceSendStatus,
    MemberInvoiceSendStatusMetadata,
} from "app/dal/model/member-invoice";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { BehaviorSubject, Observable, combineLatest, lastValueFrom } from "rxjs";
import { finalize, first, map, switchMap, tap } from "rxjs/operators";
import { MarkInvoicePaidDialogComponent } from "../mark-invoice-paid-dialog/mark-invoice-paid-dialog.component";
import { MemberBillingService } from "../member-billing.service";
import { MemberInvoiceLineItemsGridComponent } from "../member-invoice-line-items-grid/member-invoice-line-items-grid.component";
import { MemberInvoiceStateBadgeComponent } from "../member-invoice-state-badge/member-invoice-state-badge.component";

@Component({
    selector: "app-member-invoice-panel",
    templateUrl: "./member-invoice-panel.component.html",
    styleUrls: [
        "../../../../common-ux/wrapped-expansion-panel.scss",
        "./member-invoice-panel.component.scss",
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: false,
})
export class MemberInvoicePanelComponent implements OnInit, OnDestroy {
    @Input() public set memberInvoice(value: MemberInvoice | undefined) {
        this.memberInvoice$.next(value);
        if (value) {
            this.entityTracker.add(value, ...value.lineItems);
        }
    }
    public get memberInvoice() {
        return this.memberInvoice$.value;
    }

    public memberInvoice$ = new BehaviorSubject<MemberInvoice | undefined>(undefined);
    public billingPeriod$ = this.memberInvoice$.pipe(map((e) => e?.memberBillingPeriod));

    @Output() public init = new EventEmitter<MemberInvoicePanelComponent>();
    @Output() public statusChange = new EventEmitter<MemberInvoice>();
    @Output() public invoiceDeleted = new EventEmitter<MemberInvoice>();

    @ViewChild(MemberInvoiceLineItemsGridComponent)
    public lineItemsGridComponent?: MemberInvoiceLineItemsGridComponent;
    @ViewChild(MemberInvoiceStateBadgeComponent)
    public invoiceBadgeComponent?: MemberInvoiceStateBadgeComponent;

    public hasWritePermissions$: Observable<boolean>;
    public canEdit$: Observable<boolean>;
    public status$: Observable<
        { value: MemberInvoiceSendStatus; icon: string; tooltip: string } | undefined
    >;
    public entityTracker = new EntityTracker(this.memberBillingService);
    public expanded = false;

    public BillingPeriodState = MemberBillingPeriodState;
    public SendStatus = MemberInvoiceSendStatus;

    public constructor(
        private elementRef: ElementRef<HTMLElement>,
        public changeDetectorRef: ChangeDetectorRef,
        private memberBillingService: MemberBillingService,
        private dialogService: DialogService,
        authorisationService: TenantAuthorisationService,
    ) {
        this.hasWritePermissions$ = authorisationService.tenantRoles$.pipe(
            map((roles) => roles.hasPermission(FeaturePermission.MemberBillingWrite)),
        );
        this.canEdit$ = combineLatest([
            this.hasWritePermissions$,
            this.billingPeriod$,
            this.memberInvoice$,
        ]).pipe(
            map(([isBillingAdmin, billingPeriod, memberInvoice]) => {
                return (
                    isBillingAdmin &&
                    billingPeriod?.state !== MemberBillingPeriodState.Closed &&
                    !memberInvoice?.paid
                );
            }),
        );

        this.status$ = this.memberInvoice$.pipe(
            map((invoice) => {
                if (!invoice?.sendStatus) {
                    return undefined;
                }

                const metadata = MemberInvoiceSendStatusMetadata[invoice.sendStatus];
                return {
                    value: invoice.sendStatus,
                    icon: metadata.icon,
                    tooltip: metadata.description + " - click to see details",
                };
            }),
        );
    }

    public ngOnInit() {
        this.init.emit(this);
    }

    public ngOnDestroy() {
        this.entityTracker.cancelChanges().subscribe();
    }

    public saveChanges = () => {
        const hasModifiedLineItem = this.entityTracker.validEntities.some(
            (e) => e instanceof MemberInvoiceLineItem && !e.entityAspect.entityState.isUnchanged(),
        );

        return this.entityTracker.saveChanges().pipe(
            finalize(() => {
                this.changeDetectorRef.markForCheck();

                if (
                    !hasModifiedLineItem ||
                    !this.memberInvoice?.memberBillingPeriod?.isAcceptingPayments
                ) {
                    return;
                }

                this.dialogService
                    .openConfirmation(
                        "Would you like to send the updated invoice to the member?",
                        () =>
                            this.memberInvoice$.pipe(
                                first(),
                                filterForDefinedValue(),
                                switchMap((e) => this.memberBillingService.sendInvoice(e)),
                            ),
                    )
                    .subscribe(() => {
                        this.invoiceBadgeComponent?.refresh();
                        this.changeDetectorRef.markForCheck();
                    });
            }),
        );
    };

    public cancelChanges = async () => {
        await lastValueFrom(this.entityTracker.cancelChanges());
        this.lineItemsGridComponent?.updateData();
    };

    public refreshDetails() {
        if (this.memberInvoice$.value) {
            this.memberInvoice$.next(this.memberInvoice$.value);
        }
    }

    public scrollIntoView() {
        this.elementRef.nativeElement.scrollIntoView({
            behavior: "smooth",
            block: "start",
        });
    }

    public sendInvoice(memberInvoice: MemberInvoice) {
        const message = memberInvoice.paid
            ? "Are you sure you want to send a receipt of this invoice to the member?"
            : "Are you sure you want to send this invoice to the member?";
        const action = this.memberBillingService.sendInvoice(memberInvoice);
        this.dialogService
            .openConfirmation(message, () => action)
            .subscribe(() => {
                // Trigger refresh
                this.memberInvoice = memberInvoice;
                this.changeDetectorRef.markForCheck();
            });
    }

    public downloadInvoice = () => {
        return this.memberInvoice$.pipe(
            first(),
            filterForDefinedValue(),
            switchMap((e) => this.memberBillingService.downloadInvoicePdf(e)),
            tap(() => this.changeDetectorRef.markForCheck()),
        );
    };

    public markInvoiceAsPaid(memberInvoice: MemberInvoice) {
        this.dialogService
            .open(MarkInvoicePaidDialogComponent, memberInvoice)
            .pipe(finalize(() => this.changeDetectorRef.markForCheck()))
            .subscribe(() => {
                // Trigger refresh
                this.memberInvoice = memberInvoice;
                this.statusChange.next(memberInvoice);
                this.lineItemsGridComponent?.updateData();
                this.invoiceBadgeComponent?.refresh();
            });
    }

    public deleteInvoice(memberInvoice: MemberInvoice) {
        this.dialogService
            .openConfirmation(
                "Are you sure you want to delete this invoice? You can manually recreate it on the left side of this page.",
                () => {
                    this.memberInvoice = undefined;
                    return this.memberBillingService.deleteAndSave(memberInvoice);
                },
            )
            .subscribe(() => {
                this.invoiceDeleted.emit(memberInvoice);
            });
    }
}
