import { Injectable } from "@angular/core";
import { BreezeDomainService } from "app/dal/breeze-domain.service";
import { BreezeEntity } from "app/dal/breeze-entity";
import { Member } from "app/dal/model/member";
import {
    MemberBillingConfig,
    MemberBillingConfigBreezeModel,
} from "app/dal/model/member-billing-config";
import {
    MemberBillingPeriod,
    MemberBillingPeriodBreezeModel,
} from "app/dal/model/member-billing-period";
import { MemberBillingPeriodConfigBreezeModel } from "app/dal/model/member-billing-period-config";
import {
    MemberInvoice,
    MemberInvoiceBreezeModel,
    MemberInvoiceLineItem,
    MemberInvoiceLineItemBreezeModel,
} from "app/dal/model/member-invoice";
import { Predicate } from "breeze-client";
import { Observable } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";

export interface BillingPeriodStats {
    name: string;
    paidEarly: number;
    paidOnTime: number;
    paidLate: number;
    unpaid: number;
    numConcession: number;
    incomeDollars: number;
    totalValueDollars: number;
}

@Injectable()
export class MemberBillingService extends BreezeDomainService {
    public getBillingConfig(): Observable<MemberBillingConfig | undefined> {
        return this.breezeService
            .getAll(MemberBillingConfigBreezeModel)
            .pipe(map((configs) => configs[0]));
    }

    public getBillingPeriodConfigs() {
        return this.breezeService.getAll(MemberBillingPeriodConfigBreezeModel);
    }

    public getAllBillingPeriods() {
        return this.breezeService.getAll(MemberBillingPeriodBreezeModel);
    }

    public getBillingPeriodById(billingPeriodId: number) {
        return this.breezeService.getById(MemberBillingPeriodBreezeModel, billingPeriodId);
    }

    public getMemberInvoicesForBillingPeriod(billingPeriodId: number) {
        const query = this.breezeService
            .createQuery(MemberInvoiceBreezeModel)
            .expand("member, lineItems")
            .where(new Predicate("memberBillingPeriodId", "==", billingPeriodId));
        return this.breezeService.executeQuery<MemberInvoice>(query).pipe(
            tap((invoices) => {
                for (const invoice of invoices) {
                    // Ensure most recently created line items are last
                    invoice.lineItems.reverse();
                }
            }),
        );
    }

    public getMemberInvoice(invoiceId: number) {
        return this.breezeService.getById(MemberInvoiceBreezeModel, invoiceId);
    }

    public generateInvoiceForMember(billingPeriod: MemberBillingPeriod, member: Member) {
        const endpointPart = `member-billing/billing-period/${billingPeriod.id}/generate-invoice-for-member/${member.id}`;
        return this.appConfig
            .serverEndpoint(endpointPart)
            .pipe(switchMap((endpoint) => this.http.post<number>(endpoint, "")));
    }

    public getBillingHistoryStats() {
        return this.appConfig
            .serverEndpoint(`member-billing/history-stats`)
            .pipe(switchMap((endpoint) => this.http.get<BillingPeriodStats[]>(endpoint)));
    }

    public getBillingPeriodStats(billingPeriodId: number) {
        return this.appConfig
            .serverEndpoint(`member-billing/billing-period/${billingPeriodId}/stats`)
            .pipe(switchMap((endpoint) => this.http.get<BillingPeriodStats>(endpoint)));
    }

    public generateNextBillingPeriod() {
        return this.appConfig
            .serverEndpoint(`member-billing/generate-next-billing-period`)
            .pipe(switchMap((endpoint) => this.http.post(endpoint, "")));
    }

    public autoAdvanceBillingPeriods() {
        return this.appConfig
            .serverEndpoint(`member-billing/auto-advance-billing-periods`)
            .pipe(switchMap((endpoint) => this.http.post(endpoint, "")));
    }

    public sendBillingPeriodInvoices(billingPeriod: MemberBillingPeriod) {
        return this.appConfig
            .serverEndpoint(`member-billing/billing-period/${billingPeriod.id}/send`)
            .pipe(switchMap((endpoint) => this.http.post(endpoint, "")));
    }

    public deleteBillingPeriod(billingPeriod: MemberBillingPeriod) {
        return this.deleteAndSave(billingPeriod);
    }

    public closeBillingPeriod(billingPeriod: MemberBillingPeriod) {
        return this.appConfig
            .serverEndpoint(`member-billing/billing-period/${billingPeriod.id}/close`)
            .pipe(switchMap((endpoint) => this.http.post(endpoint, "")));
    }

    public createInvoiceLineItem(invoice: MemberInvoice, data?: Partial<MemberInvoiceLineItem>) {
        return this.breezeService.create(MemberInvoiceLineItemBreezeModel, {
            memberInvoice: invoice,
            ordinal: invoice.lineItems.length,
            ...data,
        });
    }

    public sendInvoice(invoice: MemberInvoice) {
        return this.customBreezeSave("send-invoice", invoice);
    }

    public downloadInvoicePdf(invoice: MemberInvoice) {
        return this.appConfig.serverEndpoint(`member-billing/invoice/${invoice.id}/pdf`).pipe(
            switchMap((endpoint) =>
                this.http.get(endpoint, {
                    responseType: "blob",
                }),
            ),
            map((res) => {
                const a = document.createElement("a");
                a.href = URL.createObjectURL(res);
                a.download = `${invoice.reference}.pdf`;
                a.click();

                URL.revokeObjectURL(a.href);
            }),
        );
    }

    public markInvoicePaid(invoice: MemberInvoice) {
        return this.customBreezeSave("mark-invoice-paid", invoice);
    }

    private customBreezeSave(endpoint: string, entity: BreezeEntity) {
        return this.breezeService.customSave(
            { controller: "member-billing", resource: endpoint },
            entity,
        );
    }
}
