import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AgPolarChartOptions } from "ag-charts-community";
import { ColDef } from "ag-grid-community";
import { FeaturePermission } from "app/auth/feature-permission";
import { orchestrateAgChartTheme } from "app/common-ux/chart-theme";
import { Chip } from "app/common-ux/chip-selection/chip-selection.component";
import { CheckboxRenderer } from "app/common-ux/data-grid/grid-checkbox-renderer";
import { currencyColDef } from "app/common-ux/data-grid/grid-currency-column";
import { dateColDef } from "app/common-ux/data-grid/grid-date-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import {
    cacheLatest,
    filterForDefinedValue,
    propagateMap,
    undefinedWhileLoading,
} from "app/common/rxjs-utilities";
import { Member } from "app/dal/model/member";
import { MemberBillingPeriod, MemberBillingPeriodState } from "app/dal/model/member-billing-period";
import { MemberInvoice } from "app/dal/model/member-invoice";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { BehaviorSubject, Observable, combineLatest, forkJoin, of } from "rxjs";
import { debounceTime, filter, first, map, switchMap, tap } from "rxjs/operators";
import { paymentScheduleCategories } from "../common-member-billing-ux";
import { BillingPeriodStats, MemberBillingService } from "../member-billing.service";
import { MemberInvoicePanelComponent } from "../member-invoice-panel/member-invoice-panel.component";

enum BillingPeriodView {
    List,
    Table,
}

@Component({
    templateUrl: "./billing-period-page.component.html",
    styleUrls: ["./billing-period-page.component.scss"],
    standalone: false,
})
export class BillingPeriodPageComponent {
    // Probably will need this when many of the endpoints become part of breeze
    // @ViewChildren(MemberInvoicePanelComponent) public invoiceCards?: QueryList<MemberInvoicePanelComponent>;

    public hasWritePermissions$: Observable<boolean>;

    public refreshBillingPeriod$ = new BehaviorSubject<void>(undefined);
    public refreshInvoices$ = new BehaviorSubject<void>(undefined);
    public billingPeriod$: Observable<MemberBillingPeriod | undefined>;
    public isNotDraftState$: Observable<boolean>;
    public billingPeriodStats$: Observable<BillingPeriodStats | undefined>;
    public canCloseBillingPeriod$: Observable<boolean>;
    public numberOfPaidInvoices$: Observable<number | undefined>;
    public numberOfInvoices$: Observable<number | undefined>;
    public memberInvoices$: Observable<MemberInvoice[] | undefined>;
    public filteredMemberInvoices$: Observable<MemberInvoice[] | undefined>;
    private filterValue$ = new BehaviorSubject<string | undefined>(undefined);
    public invoiceStatusChartOptions$: Observable<AgPolarChartOptions | undefined>;
    public invoiceListColumns = this.getColumns();
    public view = BillingPeriodView.List;
    public chips: Chip<BillingPeriodView>[];

    public BillingPeriodView = BillingPeriodView;
    public BillingPeriodState = MemberBillingPeriodState;
    public now = new Date();
    public memberToGenerate?: Member;
    public scrollToInvoiceId?: number;

    public constructor(
        private memberBillingService: MemberBillingService,
        private dialogService: DialogService,
        authorisationService: TenantAuthorisationService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.hasWritePermissions$ = authorisationService.tenantRoles$.pipe(
            map((roles) => roles.hasPermission(FeaturePermission.MemberBillingWrite)),
        );

        // TODO A more efficient way of doing this would be to do custom breeze save with
        // send/close so the updated entities get returned at we just render at that point
        const billingPeriodId$ = route.params.pipe(map((params) => Number(params.billingPeriodId)));
        this.billingPeriod$ = billingPeriodId$.pipe(
            switchMap((id) => this.refreshBillingPeriod$.pipe(map(() => id))),
            switchMap((billingPeriodId) =>
                memberBillingService.getBillingPeriodById(billingPeriodId),
            ),
            cacheLatest(),
        );
        this.isNotDraftState$ = this.billingPeriod$.pipe(
            map((billingPeriod) => {
                return (
                    billingPeriod?.state !== MemberBillingPeriodState.Created &&
                    billingPeriod?.state !== MemberBillingPeriodState.Draft
                );
            }),
        );
        this.billingPeriodStats$ = billingPeriodId$.pipe(
            switchMap((id) => this.refreshBillingPeriod$.pipe(map(() => id))),
            undefinedWhileLoading((id) => memberBillingService.getBillingPeriodStats(id)),
            cacheLatest(),
        );
        this.numberOfInvoices$ = this.billingPeriodStats$.pipe(
            propagateMap((s) => s.paidEarly + s.paidOnTime + s.paidLate + s.unpaid),
        );
        this.numberOfPaidInvoices$ = this.billingPeriodStats$.pipe(
            propagateMap((s) => s.paidEarly + s.paidOnTime + s.paidLate),
        );

        this.memberInvoices$ = billingPeriodId$.pipe(
            switchMap((id) => this.refreshInvoices$.pipe(map(() => id))),
            undefinedWhileLoading((billingPeriodId) => {
                return memberBillingService
                    .getMemberInvoicesForBillingPeriod(billingPeriodId)
                    .pipe(
                        map((invoices) =>
                            invoices.sort((a, b) => a.reference.localeCompare(b.reference)),
                        ),
                    );
            }),
            cacheLatest(),
        );

        const filterValue$ = this.filterValue$.pipe(debounceTime(200));
        this.filteredMemberInvoices$ = combineLatest([this.memberInvoices$, filterValue$]).pipe(
            map(([memberInvoices, filterValue]) => {
                if (!filterValue) {
                    return memberInvoices;
                }

                return memberInvoices?.filter((i) => {
                    const text = `${i.reference}\t${i.member?.firstName ?? ""} ${
                        i.member?.lastName ?? ""
                    }`;
                    return text.toLowerCase().includes(filterValue);
                });
            }),
        );

        this.canCloseBillingPeriod$ = combineLatest([
            this.billingPeriod$,
            this.memberInvoices$,
        ]).pipe(
            map(([billingPeriod, invoices]) => {
                return (
                    (!!billingPeriod?.due && billingPeriod.due < new Date()) ||
                    !!invoices?.every((i) => !!i.paid)
                );
            }),
        );

        this.invoiceStatusChartOptions$ = this.billingPeriodStats$.pipe(
            propagateMap((stats) =>
                paymentScheduleCategories.map((e) => ({
                    label: e.label,
                    value: stats[e.statProperty],
                })),
            ),
            propagateMap((data) => ({
                theme: orchestrateAgChartTheme,
                data,
                series: [
                    {
                        type: "pie",
                        angleKey: "value",
                        calloutLabelKey: "label",
                        fills: paymentScheduleCategories.map((e) =>
                            e.fill.toRgbColour().toCssString(),
                        ),
                        strokes: paymentScheduleCategories.map((e) =>
                            e.stroke.toRgbColour().toCssString(),
                        ),
                    },
                ],
                legend: {
                    position: "bottom",
                },
            })),
        );

        this.chips = [
            {
                value: BillingPeriodView.List,
                icon: "view_agenda",
                text: "List",
            },
            {
                value: BillingPeriodView.Table,
                icon: "view_headline",
                text: "Table",
            },
        ];
    }

    public generateInvoice = () => {
        if (!this.memberToGenerate) {
            return of({});
        }

        return this.billingPeriod$.pipe(
            filterForDefinedValue(),
            first(),
            switchMap((billingPeriod) =>
                this.memberBillingService.generateInvoiceForMember(
                    billingPeriod,
                    this.memberToGenerate!,
                ),
            ),
            tap((newInvoiceId) => {
                this.scrollToInvoiceId = newInvoiceId;
                this.memberToGenerate = undefined;
                this.refreshBillingPeriod$.next();
                this.refreshInvoices$.next();
                // this.invoiceCards?.forEach((c) => c.refreshDetails());

                this.billingPeriod$
                    .pipe(
                        first(),
                        filterForDefinedValue(),
                        filter((e) => e.isAcceptingPayments),
                        switchMap(() =>
                            this.dialogService.openConfirmation(
                                "Would you like to send the generated invoice to the member?",
                                () =>
                                    this.memberBillingService.getMemberInvoice(newInvoiceId).pipe(
                                        filterForDefinedValue(),
                                        switchMap((e) => this.memberBillingService.sendInvoice(e)),
                                    ),
                            ),
                        ),
                    )
                    .subscribe();
            }),
        );
    };

    public onInvoicePanelInit(invoicePanelComponent: MemberInvoicePanelComponent) {
        if (
            this.scrollToInvoiceId &&
            this.scrollToInvoiceId === invoicePanelComponent.memberInvoice?.id
        ) {
            invoicePanelComponent.scrollIntoView();
            this.scrollToInvoiceId = undefined;
        }
    }

    public sendInvoices() {
        this.billingPeriod$
            .pipe(
                filterForDefinedValue(),
                first(),
                switchMap((e) =>
                    this.dialogService.openConfirmation(
                        "Are you sure you want to send all invoices to members? This could take a few minutes.",
                        () => this.memberBillingService.sendBillingPeriodInvoices(e),
                    ),
                ),
            )
            .subscribe(() => {
                this.refreshBillingPeriod$.next();
                this.refreshInvoices$.next();
                // this.invoiceCards?.forEach((c) => c.refreshDetails());
            });
    }

    public deleteBillingPeriod() {
        forkJoin([
            this.billingPeriod$.pipe(filterForDefinedValue(), first()),
            this.memberInvoices$.pipe(filterForDefinedValue(), first()),
        ])
            .pipe(
                switchMap(([billingPeriod, invoices]) =>
                    this.dialogService.openConfirmation(
                        "Are you sure you want to delete this billing period and ALL generated invoices? This could take a while.",
                        () =>
                            this.memberBillingService.deleteAndSave(
                                billingPeriod,
                                ...invoices,
                                ...invoices.flatMap((i) => i.lineItems),
                            ),
                    ),
                ),
                switchMap(() => this.router.navigate(["../../"], { relativeTo: this.route })),
            )
            .subscribe();
    }

    public closeBillingPeriod() {
        this.billingPeriod$
            .pipe(
                filterForDefinedValue(),
                first(),
                switchMap((e) =>
                    this.dialogService.openConfirmation(
                        "Are you sure you want to close this billing period and prevent any further changes?",
                        () => this.memberBillingService.closeBillingPeriod(e),
                    ),
                ),
            )
            .subscribe(() => {
                this.refreshBillingPeriod$.next();
                this.refreshInvoices$.next();
                // this.invoiceCards?.forEach((c) => c.refreshDetails());
            });
    }

    public applyFilter(filterText: string) {
        this.filterValue$.next(filterText.toLowerCase());
    }

    public filterForGeneratableMembers = (members: Member[]) => {
        return this.memberInvoices$.pipe(
            map((invoices) => {
                if (!invoices) {
                    return [];
                }

                return members.filter((m) => !invoices.some((i) => i.memberId === m.id));
            }),
        );
    };

    public updateView(view?: BillingPeriodView) {
        if (typeof view !== "undefined") {
            this.view = view;
        }
    }

    private getColumns(): ColDef[] {
        return [
            {
                field: "reference",
                width: 120,
                sortable: true,
                sort: "asc",
            },
            {
                field: "member.fullName",
                headerName: "Name",
                flex: 1,
                minWidth: 150,
                sortable: true,
            },
            {
                field: "isConcession",
                headerName: "Concession",
                width: 100,
                sortable: true,
                resizable: true,
                cellRenderer: CheckboxRenderer,
            },
            {
                field: "total",
                width: 90,
                sortable: true,
                ...currencyColDef(),
            },
            {
                field: "paid",
                width: 120,
                sortable: true,
                ...dateColDef(),
            },
        ];
    }
}
