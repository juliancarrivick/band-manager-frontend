import { formatCurrency } from "@angular/common";
import {
    AfterViewInit,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    Output,
    ViewChild,
} from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { ColDef, RowEditingStartedEvent, RowEditingStoppedEvent } from "ag-grid-community";
import { GridCurrencyCellEditorComponent } from "app/common-ux/data-grid/grid-currency-cell-editor";
import { GridTextCellEditorComponent } from "app/common-ux/data-grid/grid-text-cell-editor";
import { fromCustomGridEvent } from "app/common-ux/data-grid/grid-utils";
import { filterForDefinedValue } from "app/common/rxjs-utilities";
import { MemberInvoice, MemberInvoiceLineItem } from "app/dal/model/member-invoice";
import {
    BehaviorSubject,
    Observable,
    ReplaySubject,
    Subscription,
    combineLatest,
    merge,
} from "rxjs";
import { first, map, switchMap, tap } from "rxjs/operators";
import {
    EditInvoiceLineColumnRendererComponent,
    LineItemRowDeletedEvent,
    LineItemRowEditedEvent,
} from "../edit-invoice-line-column-renderer/edit-invoice-line-column-renderer.component";
import { MemberBillingService } from "../member-billing.service";

@Component({
    selector: "app-member-invoice-line-items-grid",
    templateUrl: "./member-invoice-line-items-grid.component.html",
    styleUrls: ["./member-invoice-line-items-grid.component.scss"],
    standalone: false,
})
export class MemberInvoiceLineItemsGridComponent implements AfterViewInit, OnDestroy {
    @Input() public set memberInvoice(value: MemberInvoice | undefined) {
        this.memberInvoice$.next(value);
    }
    public memberInvoice$ = new BehaviorSubject<MemberInvoice | undefined>(undefined);
    public lineItems$: Observable<MemberInvoiceLineItem[] | undefined>;

    @Input() public set canEdit(value: boolean | null) {
        this.canEdit$.next(!!value);
    }
    public canEdit$ = new BehaviorSubject<boolean>(false);

    @Output() public lineItemAdded = new EventEmitter<MemberInvoiceLineItem>();
    @Output() public lineItemEdited = new EventEmitter<MemberInvoiceLineItem>();
    @Output() public lineItemDeleted = new EventEmitter<MemberInvoiceLineItem>();

    public columnDefs$: Observable<ColDef[]>;
    public updateTotal$ = new BehaviorSubject<void>(undefined);
    public invoiceTotal$: Observable<number>;

    @ViewChild(AgGridAngular) public invoiceItemGrid?: AgGridAngular<MemberInvoiceLineItem>;
    private invoiceItemGrid$ = new ReplaySubject<AgGridAngular<MemberInvoiceLineItem>>(1);
    private subscription: Subscription;

    public constructor(private memberBillingService: MemberBillingService) {
        this.lineItems$ = this.memberInvoice$.pipe(
            map((invoice) => invoice?.lineItems.sort((a, b) => a.ordinal - b.ordinal)),
        );
        this.invoiceTotal$ = combineLatest([this.memberInvoice$, this.updateTotal$]).pipe(
            map(([invoice, _]) => {
                if (!invoice) {
                    return 0;
                }

                const totalCents = invoice.lineItems
                    .map((e) => e.amountCents)
                    .reduce((prev, curr) => prev + curr, 0);
                return totalCents / 100;
            }),
        );

        this.columnDefs$ = this.canEdit$.pipe(map((canEdit) => this.buildColumnDefs(canEdit)));

        this.subscription = this.invoiceItemGrid$
            .pipe(
                switchMap((g) =>
                    merge(
                        fromCustomGridEvent<LineItemRowEditedEvent>(g, "appRowEdited").pipe(
                            tap((e) => this.lineItemEdited.emit(e.lineItem)),
                        ),
                        fromCustomGridEvent<LineItemRowDeletedEvent>(g, "appRowDeleted").pipe(
                            tap((e) => this.lineItemDeleted.emit(e.lineItem)),
                        ),
                    ),
                ),
            )
            .subscribe((_e) => {
                this.updateTotal$.next();
            });
    }

    public ngAfterViewInit() {
        if (!this.invoiceItemGrid) {
            throw new Error("Grid should be initialised");
        }

        this.invoiceItemGrid$.next(this.invoiceItemGrid);
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public updateData() {
        this.updateTotal$.next();

        const lineItems = this.memberInvoice$.value?.lineItems;
        if (lineItems) {
            this.invoiceItemGrid?.api.updateGridOptions({
                rowData: lineItems,
            });
        }
    }

    public createInvoiceItem() {
        if (!this.invoiceItemGrid?.api) {
            return;
        }

        this.memberInvoice$
            .pipe(
                filterForDefinedValue(),
                first(),
                switchMap((memberInvoice) =>
                    this.memberBillingService.createInvoiceLineItem(memberInvoice),
                ),
            )
            .subscribe((invoiceLine) => {
                const transactionResult = this.invoiceItemGrid?.api.applyTransaction({
                    add: [invoiceLine],
                });
                const addedRowNode = transactionResult?.add[0];

                const columns = this.invoiceItemGrid?.api.getAllDisplayedColumns();
                if (addedRowNode?.rowIndex && columns) {
                    const firstColumn = columns[0];
                    if (!firstColumn) {
                        return;
                    }

                    this.invoiceItemGrid?.api.setFocusedCell(addedRowNode.rowIndex, firstColumn);
                    this.invoiceItemGrid?.api.startEditingCell({
                        rowIndex: addedRowNode.rowIndex,
                        colKey: firstColumn,
                    });
                }

                this.lineItemAdded.emit(invoiceLine);
            });
    }

    public updateRowHeight(
        event: RowEditingStartedEvent | RowEditingStoppedEvent,
        height: number | null,
    ) {
        event.node.setRowHeight(height);
        event.api.onRowHeightChanged();
    }

    private buildColumnDefs(canEdit: boolean) {
        const columnDefs: ColDef[] = [
            {
                field: "description",
                editable: canEdit,
                flex: 1,
                cellEditor: GridTextCellEditorComponent,
                minWidth: 240,
            },
            {
                type: "rightAligned",
                field: "amount",
                editable: canEdit,
                valueGetter: (params) => {
                    const invoiceLine: MemberInvoiceLineItem = params.data;
                    return invoiceLine.amountCents / 100;
                },
                valueSetter: (params) => {
                    const lineItem: MemberInvoiceLineItem = params.data;

                    const newValue: number = params.newValue * 100;
                    if (newValue === lineItem.amountCents) {
                        return false;
                    }

                    lineItem.amountCents = newValue;
                    return true;
                },
                valueFormatter: (params) => formatCurrency(params.value, "en-AU", "$"),
                cellEditor: GridCurrencyCellEditorComponent,
                width: 150,
            },
        ];

        if (canEdit) {
            columnDefs.push({
                headerName: "Edit",
                cellStyle: {
                    "padding-left": "4px",
                    "padding-right": "4px",
                },
                width: 108,
                cellRenderer: EditInvoiceLineColumnRendererComponent,
            });
        }

        return columnDefs;
    }
}
