import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { helpPages } from "app/help/route/help-list";
import { BillingPeriodPageComponent } from "./billing-period-page/billing-period-page.component";
import { BillingPeriodsPageComponent } from "./billing-periods-page/billing-periods-page.component";

const memberBillingRoutes: AppRoutes = [
    {
        path: "",
        component: BillingPeriodsPageComponent,
        data: {
            title: "Member Billing",
            icon: "monetization_on",
            restrictedToPermission: FeaturePermission.MemberBillingRead,
            help: [helpPages.memberBilling],
        },
    },
    {
        path: "period/:billingPeriodId",
        component: BillingPeriodPageComponent,
        data: {
            icon: "receipt",
            restrictedToPermission: FeaturePermission.MemberBillingRead,
            help: [helpPages.memberBilling],
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(memberBillingRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppMemberBillingRoutingModule {}
