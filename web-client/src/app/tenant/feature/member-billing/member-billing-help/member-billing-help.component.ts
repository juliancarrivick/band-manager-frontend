import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { HelpBrokerService } from "app/help/help-broker.service";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";
import { memberBillingSettingsPageId } from "../../settings/settings-consts";
import { billingPeriodsPageId } from "../member-billing-consts";

@Component({
    selector: "app-member-billing-help",
    templateUrl: "./member-billing-help.component.html",
    styles: [
        `
            ul {
                text-align: justify;
            }
        `,
    ],
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class MemberBillingHelpComponent {
    public memberBillingPageHooks$ = this.helpBroker.helpablePageDataHooks(billingPeriodsPageId);
    public memberBillingSettingsHooks$ = this.helpBroker.helpablePageDataHooks(
        memberBillingSettingsPageId,
    );

    public constructor(private helpBroker: HelpBrokerService) {}
}
