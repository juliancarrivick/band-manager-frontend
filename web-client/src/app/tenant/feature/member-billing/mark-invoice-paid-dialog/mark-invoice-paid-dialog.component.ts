import { Component, Inject, ViewChild } from "@angular/core";
import { NgModel } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { cacheLatest, filterForDefinedValue } from "app/common/rxjs-utilities";
import { addDays } from "app/common/utils";
import { EntityTracker } from "app/dal/entity-tracker";
import { MemberBillingPeriod } from "app/dal/model/member-billing-period";
import { MemberInvoice, MemberInvoiceLineItem } from "app/dal/model/member-invoice";
import { lastValueFrom, Observable } from "rxjs";
import { finalize, first, map, switchMap } from "rxjs/operators";
import { MemberBillingService } from "../member-billing.service";
import { MemberInvoiceLineItemsGridComponent } from "../member-invoice-line-items-grid/member-invoice-line-items-grid.component";

enum PaymentType {
    EarlyBird,
    Full,
}

@Component({
    templateUrl: "./mark-invoice-paid-dialog.component.html",
    styleUrls: ["./mark-invoice-paid-dialog.component.scss"],
    standalone: false,
})
export class MarkInvoicePaidDialogComponent extends BaseDialog<MemberInvoice> {
    public readonly dialogName = "MarkInvoicePaid";

    public title: string;
    public billingPeriod: MemberBillingPeriod;
    public paymentType?: PaymentType;
    public loading = false;
    public confirmed = false;

    public minPaidDate?: Date;
    public minPaidErrorText = "Paid date must be after invoices were sent out";
    public maxPaidDate?: Date;
    public readonly maxPaidErrorText = "Paid date must be before the early bird period ended";

    public entityTracker: EntityTracker;
    private earlyBirdLineItem?: MemberInvoiceLineItem;
    private earlyBirdDiscountValueCents$: Observable<number>;

    @ViewChild("datePaidInput") public datePaidInput?: NgModel;
    @ViewChild(MemberInvoiceLineItemsGridComponent)
    public lineItemsGrid?: MemberInvoiceLineItemsGridComponent;

    public paymentTypes = [
        {
            type: PaymentType.EarlyBird,
            label: "Early Bird Payment",
        },
        {
            type: PaymentType.Full,
            label: "Full Payment",
        },
    ];

    public constructor(
        dialogRef: MatDialogRef<MarkInvoicePaidDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public invoice: MemberInvoice,
        private memberBillingService: MemberBillingService,
    ) {
        super(dialogRef);
        if (!invoice.memberBillingPeriod) {
            throw new Error("memberBillingPeriod must be primed");
        }

        this.entityTracker = new EntityTracker(memberBillingService);
        this.entityTracker.add(invoice);

        this.title = `Mark ${invoice.reference} as paid`;
        this.billingPeriod = invoice.memberBillingPeriod;

        const now = new Date();
        if (this.billingPeriod.earlyBirdDue) {
            if (this.billingPeriod.earlyBirdDue > now) {
                this.paymentType = PaymentType.EarlyBird;
            } else if (addDays(this.billingPeriod.earlyBirdDue, 7) < now) {
                // I think this is OK to hard code ^^, we'll probably remove the grace period from the config
                this.paymentType = PaymentType.Full;
            }
        }

        this.earlyBirdDiscountValueCents$ = memberBillingService.getBillingConfig().pipe(
            filterForDefinedValue(),
            map((c) => c.earlyBirdDiscountDollars * -100),
            cacheLatest(),
        );

        this.setPaidDateRange();
        this.updateLineItems();
        this.invoice.paid = now;
    }

    public onPaymentTypeChange(paymentType: PaymentType) {
        this.paymentType = paymentType;
        this.setPaidDateRange();
        this.updateLineItems();

        this.datePaidInput?.control.markAsTouched();

        // Hack, really should be using a reactive form here to do this
        setTimeout(() => {
            if (this.datePaidInput?.invalid) {
                this.confirmed = false;
            }
        });
    }

    public setPaidDateRange() {
        this.minPaidDate = this.billingPeriod.sent ?? undefined;
        this.minPaidErrorText = "Paid date must be after invoices were sent out";
        this.maxPaidDate = new Date();

        if (typeof this.paymentType === "undefined") {
            return;
        }

        if (this.paymentType === PaymentType.Full) {
            this.minPaidDate = this.billingPeriod.earlyBirdDue
                ? addDays(this.billingPeriod.earlyBirdDue, 1)
                : undefined;
            this.minPaidErrorText = "Paid date must be after the early bird period ended";
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        } else if (this.paymentType === PaymentType.EarlyBird) {
            this.maxPaidDate = this.billingPeriod.earlyBirdDue ?? new Date();
        }
    }

    public updateLineItems() {
        if (this.paymentType === PaymentType.EarlyBird) {
            if (this.earlyBirdLineItem) {
                return;
            }

            this.loading = true;
            this.dialogRef.disableClose = true;
            this.earlyBirdDiscountValueCents$
                .pipe(
                    first(),
                    switchMap((v) =>
                        this.memberBillingService.createInvoiceLineItem(this.invoice, {
                            description: "Early Bird Discount",
                            amountCents: v,
                        }),
                    ),
                )
                .subscribe((lineItem) => {
                    this.earlyBirdLineItem = lineItem;
                    this.entityTracker.add(lineItem);
                    this.lineItemsGrid?.updateData();
                    this.loading = false;
                    this.dialogRef.disableClose = false;
                });
        } else if (this.earlyBirdLineItem) {
            this.memberBillingService.delete(this.earlyBirdLineItem);
            this.earlyBirdLineItem = undefined;
            this.lineItemsGrid?.updateData();
        }
    }

    public handleDeletedLineItem(lineItem: MemberInvoiceLineItem) {
        this.entityTracker.add(lineItem);

        if (this.earlyBirdLineItem === lineItem) {
            this.earlyBirdLineItem = undefined;
        }
    }

    public saveAndClose = async () => {
        await lastValueFrom(this.entityTracker.saveChanges());
        this.resolve(this.invoice);
    };

    public saveAndSend = () => {
        return this.entityTracker.saveChanges().pipe(
            switchMap(() => this.memberBillingService.sendInvoice(this.invoice)),
            finalize(() => this.resolve(this.invoice)),
        );
    };

    public cancelChanges = async () => {
        await lastValueFrom(this.entityTracker.cancelChanges());
        this.cancel();
    };
}
