import { Component } from "@angular/core";
import { AgBarSeriesOptions, AgCartesianChartOptions } from "ag-charts-community";
import { FeaturePermission } from "app/auth/feature-permission";
import { orchestrateAgChartTheme } from "app/common-ux/chart-theme";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { cacheLatest } from "app/common/rxjs-utilities";
import { isArrayWithData } from "app/common/utils";
import { MemberBillingConfig } from "app/dal/model/member-billing-config";
import { MemberBillingPeriod, MemberBillingPeriodState } from "app/dal/model/member-billing-period";
import { MemberBillingPeriodConfig } from "app/dal/model/member-billing-period-config";
import { HelpDataHooks, HelpablePage } from "app/help/help-broker.service";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { BehaviorSubject, Observable, combineLatest } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { paymentScheduleCategories } from "../common-member-billing-ux";
import { billingPeriodsPageId } from "../member-billing-consts";
import { BillingPeriodStats, MemberBillingService } from "../member-billing.service";

interface NextBillingPeriod {
    name: string;
    generationDate: Date;
}

@Component({
    templateUrl: "./billing-periods-page.component.html",
    styleUrls: ["./billing-periods-page.component.scss"],
    standalone: false,
})
export class BillingPeriodsPageComponent implements HelpablePage {
    public pageId = billingPeriodsPageId;
    public canEdit$: Observable<boolean>;
    public billingConfig$: Observable<MemberBillingConfig | undefined>;
    public billingIsEnabled$: Observable<boolean>;
    public billingIsDisabled$: Observable<boolean>;
    public openBillingPeriods$: Observable<MemberBillingPeriod[]>;
    public closedBillingPeriods$: Observable<MemberBillingPeriod[]>;
    public hasDraftBillingPeriod$: Observable<boolean>;
    public nextBillingPeriod$: Observable<NextBillingPeriod | undefined>;
    private fetchBillingPeriods$ = new BehaviorSubject<void>(undefined);
    public billingPeriodsChartOptions$: Observable<AgCartesianChartOptions>;
    public settingsCommands = ["settings", { section: "member-billing" }];
    public dataHooks: HelpDataHooks;

    public constructor(
        private memberBillingService: MemberBillingService,
        private dialogService: DialogService,
        authService: TenantAuthorisationService,
    ) {
        this.canEdit$ = authService.hasPermission(FeaturePermission.MemberBillingWrite);
        this.billingConfig$ = memberBillingService.getBillingConfig().pipe(cacheLatest());
        this.billingIsEnabled$ = this.billingConfig$.pipe(map((c) => !!c?.isEnabled));
        this.billingIsDisabled$ = this.billingIsEnabled$.pipe(map((e) => !e));
        const billingPeriods$ = this.fetchBillingPeriods$.pipe(
            switchMap(() => memberBillingService.getAllBillingPeriods()),
            map((periods) => periods.sort((a, b) => b.due.getTime() - a.due.getTime())),
            cacheLatest(),
        );
        this.openBillingPeriods$ = billingPeriods$.pipe(
            map((billingPeriods) =>
                billingPeriods.filter((p) => p.state !== MemberBillingPeriodState.Closed),
            ),
        );
        this.closedBillingPeriods$ = billingPeriods$.pipe(
            map((billingPeriods) =>
                billingPeriods.filter((p) => p.state === MemberBillingPeriodState.Closed),
            ),
        );

        this.hasDraftBillingPeriod$ = this.openBillingPeriods$.pipe(
            map(
                (e) =>
                    !!e.find(
                        (p) =>
                            p.state === MemberBillingPeriodState.Created ||
                            p.state === MemberBillingPeriodState.Draft,
                    ),
            ),
        );
        this.nextBillingPeriod$ = combineLatest([
            memberBillingService.getBillingPeriodConfigs(),
            billingPeriods$,
        ]).pipe(
            map(([billingPeriodConfigs, billingPeriods]) =>
                this.calculateNextBillingPeriod(billingPeriodConfigs, billingPeriods),
            ),
        );

        this.billingPeriodsChartOptions$ = memberBillingService
            .getBillingHistoryStats()
            .pipe(map((stats) => this.buildChartOptions(stats)));

        this.dataHooks = {
            addDataPermission: FeaturePermission.MemberBillingWrite,
            hasNoData$: this.billingConfig$.pipe(map((config) => !config)),
            addData: () => this.settingsCommands,
        };
    }

    public generateNextBillingPeriod() {
        // TODO More info?
        this.dialogService
            .openConfirmation(
                "Are you sure you would like to generate the next billing period? This could take a up to a minute.",
                () => this.memberBillingService.generateNextBillingPeriod(),
            )
            .subscribe(() => {
                this.fetchBillingPeriods$.next();
            });
    }

    public autoAdvanceBillingPeriods() {
        this.dialogService
            .openConfirmation(
                "By default this action runs every 6 hours, but you can force it to run now. Are you sure you want to continue?",
                () => this.memberBillingService.autoAdvanceBillingPeriods(),
            )
            .subscribe(() => {
                this.fetchBillingPeriods$.next();
            });
    }

    public hasChartData(chartOptions?: AgCartesianChartOptions) {
        return isArrayWithData(chartOptions?.data);
    }

    // This whole method is quite complicated, perhaps it should be moved to the back end
    private calculateNextBillingPeriod(
        billingPeriodConfigs: MemberBillingPeriodConfig[],
        billingPeriods: MemberBillingPeriod[],
    ) {
        const latestBillingPeriod: MemberBillingPeriod | undefined = billingPeriods[0];

        // If we've been disabled a long time, the next one we generate will be in the future
        let candidateDate = latestBillingPeriod?.due;
        if (!candidateDate || candidateDate < new Date()) {
            candidateDate = new Date();
        }

        const candidateMonth = candidateDate.getMonth() + 1; // Yes, JS month is 0 based
        const candidateYear = candidateDate.getFullYear();
        const latestDueDate = latestBillingPeriod?.due;
        const latestDueYear = latestDueDate?.getFullYear();
        const latestDueMonth = latestDueDate ? latestDueDate.getMonth() + 1 : undefined;
        const orderedConfigs = billingPeriodConfigs.sort(
            (a, b) => a.autoGenerateMonth - b.autoGenerateMonth,
        );
        const nextConfigLaterThisYear = orderedConfigs.find((c) => {
            // If we are still in the month of a generated billing period, don't say we'll
            // generate it again!
            return latestDueYear === candidateYear && latestDueMonth === c.endOfPeriodMonth
                ? c.autoGenerateMonth > candidateMonth
                : c.autoGenerateMonth >= candidateMonth;
        });

        if (nextConfigLaterThisYear) {
            return {
                name: `${candidateYear} - ${nextConfigLaterThisYear.name}`,
                generationDate: new Date(
                    candidateYear,
                    nextConfigLaterThisYear.autoGenerateMonth - 1,
                    1,
                ),
            };
        } else if (orderedConfigs[0]) {
            const firstConfigNextYear = orderedConfigs[0];
            return {
                name: `${candidateYear + 1} - ${firstConfigNextYear.name}`,
                generationDate: new Date(
                    candidateYear + 1,
                    firstConfigNextYear.autoGenerateMonth - 1,
                    1,
                ),
            };
        } else {
            // TODO Log
            return undefined;
        }
    }

    private buildChartOptions(data: BillingPeriodStats[]): AgCartesianChartOptions {
        const incomeField: keyof BillingPeriodStats = "incomeDollars";
        const numConcessionField: keyof BillingPeriodStats = "numConcession";

        return {
            theme: orchestrateAgChartTheme,
            data,
            series: [
                ...paymentScheduleCategories.map<AgBarSeriesOptions>((c) => ({
                    type: "bar",
                    stacked: true,
                    xKey: "name",
                    yKey: c.statProperty,
                    yName: c.label,
                    fill: c.fill.toRgbColour().toCssString(),
                    stroke: c.stroke.toRgbColour().toCssString(),
                })),
                {
                    type: "line",
                    xKey: "name",
                    yKey: numConcessionField,
                    yName: "Concession invoices",
                    stroke: "#4258c9",
                    marker: {
                        shape: "triangle",
                        size: 16,
                        fill: "#4258c9",
                    },
                    lineDash: [6, 3],
                },
                {
                    type: "line",
                    xKey: "name",
                    yKey: incomeField,
                    yName: "Total income",
                    stroke: "#7442a3",
                    marker: {
                        fill: "#7442a3",
                        size: 16,
                    },
                },
            ],
            axes: [
                {
                    type: "category",
                    position: "bottom",
                },
                {
                    type: "number",
                    position: "left",
                    title: {
                        text: "Number of invoices",
                        enabled: true,
                    },
                    keys: paymentScheduleCategories
                        .map((e) => e.statProperty)
                        .concat([numConcessionField]),
                    min: 0,
                    max: this.calculateAxisMax(
                        data.map((d) => d.paidEarly + d.paidOnTime + d.paidLate + d.unpaid),
                        10,
                    ),
                },
                {
                    type: "number",
                    position: "right",
                    title: {
                        text: "Total income",
                        enabled: true,
                    },
                    label: {
                        formatter: (params) => `$${Number(params.value)}`,
                    },
                    keys: [incomeField],
                    min: 0,
                    max: this.calculateAxisMax(
                        data.map((d) => d.incomeDollars),
                        100,
                    ),
                    tick: {
                        width: 0,
                    },
                },
            ],
            legend: {
                position: "bottom",
            },
        };
    }

    private calculateAxisMax(values: number[], multiple: number) {
        const max = Math.max(...values);
        const factor = Math.ceil(max / multiple);
        return Math.max(1, factor) * multiple;
    }
}
