import { Component } from "@angular/core";
import { MatSidenav } from "@angular/material/sidenav";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthenticationService } from "app/auth/authentication.service";
import { FeaturePermission } from "app/auth/feature-permission";
import { User } from "app/auth/user";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { ResponsiveService } from "app/common-ux/responsive.service";
import { AppConfig, Environment } from "app/common/app-config";
import { buildHelpUrlTree, helpOutletName } from "app/common/routing-utils";
import { filterForDefinedValue } from "app/common/rxjs-utilities";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { TenantDto } from "app/tenant/module/tenant-dto";
import { TenantService } from "app/tenant/module/tenant.service";
import { Observable, combineLatest } from "rxjs";
import { filter, first, map, switchMap } from "rxjs/operators";
import { HelpBrokerService } from "../../../help/help-broker.service";

interface NavbarPage {
    displayName: string;
    icon: string;
    targetRoute: string;
    routerLinkActiveOptions?: { exact: boolean };
    requiredPermission: FeaturePermission | FeaturePermission[];
}

// TODO Angular material utility class?
const mobileToolbarHeight = 56;
const desktopToolbarHeight = 64;

@Component({
    selector: "app-shell",
    templateUrl: "./shell.component.html",
    styleUrls: ["./shell.component.scss"],
    providers: [
        HelpBrokerService,
    ],
    standalone: false,
})
export class ShellComponent {
    public user$: Observable<User>;
    public tenant$: Observable<TenantDto>;
    public navbarPages$: Observable<NavbarPage[]>;
    public nonProdEnvironment$: Observable<Environment | undefined>;
    public canAccessMultipleGroups$: Observable<boolean>;
    public isMobile$ = this.responsiveService.isMobile$;
    public isXl$ = this.responsiveService.isXl$;
    public toolbarHeightPx$: Observable<number>;
    public isMember$: Observable<boolean>;
    public accountIcon$: Observable<string>;
    private currentHelpCommands: unknown[] = ["view"];

    public readonly allNavbarPages: NavbarPage[];

    constructor(
        private responsiveService: ResponsiveService,
        private router: Router,
        private authenticationService: AuthenticationService,
        authorisationService: TenantAuthorisationService,
        tenantService: TenantService,
        private dialogService: DialogService,
        appConfig: AppConfig,
        private route: ActivatedRoute,
        public helpBrokerService: HelpBrokerService,
    ) {
        this.user$ = authenticationService.user$.pipe(filterForDefinedValue());
        this.tenant$ = tenantService.currentTenant$.pipe(filterForDefinedValue());
        this.isMember$ = combineLatest([
            this.user$,
            this.tenant$,
        ]).pipe(
            map(
                ([user, tenant]) =>
                    !!user.tenantAndRoles.find((t) => t.tenant.tenantId === tenant.tenantId),
            ),
        );
        this.accountIcon$ = this.isMember$.pipe(
            map((isMember) => (isMember ? "account_circle" : "no_accounts")),
        );

        this.allNavbarPages = this.generateAllNavbarPages();
        this.navbarPages$ = authorisationService.tenantRoles$.pipe(
            map((roles) =>
                this.allNavbarPages.filter((page) => {
                    const permissions = Array.isArray(page.requiredPermission)
                        ? page.requiredPermission
                        : [page.requiredPermission];
                    return permissions.some((p) => roles.hasPermission(p));
                }),
            ),
        );

        this.nonProdEnvironment$ = appConfig.environment$.pipe(
            map((e) => (e === Environment.Production ? undefined : e)),
        );
        this.canAccessMultipleGroups$ = this.user$.pipe(
            map((u) => u.isGlobalAdmin || u.getAccessibleTenants().length > 1),
        );
        this.toolbarHeightPx$ = this.isMobile$.pipe(
            map((isMobile) => (isMobile ? mobileToolbarHeight : desktopToolbarHeight)),
        );
    }

    public saveHelpRoute() {
        let helpRoute =
            this.route.snapshot.children.find((r) => r.outlet === helpOutletName) ?? null;
        if (!helpRoute) {
            return;
        }

        this.currentHelpCommands = [];
        while (helpRoute) {
            const url = helpRoute.url[0];
            if (url) {
                this.currentHelpCommands.push(url.path);
                this.currentHelpCommands.push(url.parameters);
            }

            helpRoute = helpRoute.firstChild;
        }
    }

    public async showAppropriateHelp(helpSidenav: MatSidenav) {
        const helpCommands = helpSidenav.opened ? null : this.currentHelpCommands;
        const urlTree = buildHelpUrlTree(this.route.snapshot, helpCommands);
        await this.router.navigateByUrl(urlTree);
    }

    public toggleSidenavIfAppropriate(sidenav: MatSidenav) {
        // Use getter so references after startup so we get the correct initial state
        this.responsiveService.isMobile$
            .pipe(
                first(),
                filter((isMobile) => isMobile),
                switchMap(() => sidenav.toggle()),
            )
            .subscribe();
    }

    public logout(event: MouseEvent) {
        event.preventDefault();

        this.dialogService
            .openConfirmation("Are you sure you would like to log out?")
            .pipe(
                switchMap(() => this.authenticationService.logout()),
                switchMap(() => this.router.navigateByUrl("/login")),
            )
            .subscribe();
    }

    private generateAllNavbarPages(): NavbarPage[] {
        // TODO deduplicate this from the route information
        return [
            {
                displayName: "Member Dashboard",
                icon: "dashboard",
                targetRoute: "members/dashboard",
                requiredPermission: FeaturePermission.MemberRead,
            },
            {
                displayName: "Assets",
                icon: "library_books",
                targetRoute: "assets",
                requiredPermission: FeaturePermission.AssetRead,
            },
            {
                displayName: "Music Library",
                icon: "library_music",
                targetRoute: "music-library",
                requiredPermission: FeaturePermission.ScoreRead,
            },
            {
                displayName: "Members",
                icon: "face",
                targetRoute: "members",
                requiredPermission: FeaturePermission.MemberWrite,
                // Hack: don't want to show this as active when going to the member dashboard
                // Should be able to remove it when we replace that page with a dedicated member
                // page. Will we still want to see all members and their performance history on one page?
                routerLinkActiveOptions: { exact: true },
            },
            {
                displayName: "Member Billing",
                icon: "monetization_on",
                targetRoute: "member-billing",
                requiredPermission: FeaturePermission.MemberBillingRead,
            },
            {
                displayName: "Ensembles",
                icon: "groups",
                targetRoute: "ensembles",
                requiredPermission: FeaturePermission.EnsembleRead,
            },
            {
                displayName: "Concerts",
                icon: "event",
                targetRoute: "concerts",
                requiredPermission: FeaturePermission.ConcertRead,
            },
            {
                displayName: "Mailing Lists",
                icon: "email",
                targetRoute: "mailing-lists",
                requiredPermission: FeaturePermission.MailingListRead,
            },
            {
                displayName: "Contacts",
                icon: "contacts",
                targetRoute: "contacts",
                requiredPermission: FeaturePermission.ContactsRead,
            },
            {
                displayName: "Users & Security",
                icon: "security",
                targetRoute: "users",
                requiredPermission: FeaturePermission.PlatformSecurityAccess,
            },
            {
                displayName: "Settings",
                icon: "settings",
                targetRoute: "settings",
                requiredPermission: [
                    FeaturePermission.SettingsWrite,
                    FeaturePermission.MemberBillingWrite,
                ],
            },
        ];
    }
}
