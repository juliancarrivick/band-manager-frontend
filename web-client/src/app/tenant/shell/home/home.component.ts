import { Component } from "@angular/core";
import {
    AgBarSeriesOptions,
    AgCartesianChartOptions,
    AgPolarChartOptions,
} from "ag-charts-community";
import { orchestrateAgChartTheme } from "app/common-ux/chart-theme";
import { pastelPalette } from "app/common-ux/palette";
import { cacheLatest, filterForDefinedValue } from "app/common/rxjs-utilities";
import {
    paidEarlyCategory,
    paidLateCategory,
    paidOnTimeCategory,
    unpaidCategory,
} from "app/tenant/feature/member-billing/common-member-billing-ux";
import { BillingPeriodShape } from "app/tenant/feature/member-billing/member-invoice-state-badge/member-invoice-state-badge.component";
import { selectedMemberParamName } from "app/tenant/feature/member/summary-page/summary-page.component";
import { TenantService } from "app/tenant/module/tenant.service";
import { Observable, map, tap } from "rxjs";
import {
    InvoiceSummaryDto,
    LandingService,
    LandingSummaryDto,
    PerformanceHistoryDto,
} from "./landing.service";

interface BillingPeriodProportion {
    count: number;
    tooltip: string;
    colour: string;
}

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"],
    providers: [
        LandingService,
    ],
    standalone: false,
})
export class HomeComponent {
    public logo$: Observable<{ src: string; alt: string } | undefined>;
    public summary$: Observable<LandingSummaryDto>;
    public billingPeriodProportions: Record<string, BillingPeriodProportion[]> = {};
    public performanceSummaryChartOptions$: Observable<AgCartesianChartOptions | undefined>;
    public billingList$: Observable<
        { invoice: InvoiceSummaryDto; billingPeriod: BillingPeriodShape }[]
    >;
    public membershipSnapshotChartOptions$: Observable<AgPolarChartOptions | undefined>;
    public mailingListUsageChartOptions$: Observable<AgCartesianChartOptions | undefined>;
    public memberDashboardCommands: unknown[] = [];

    public constructor(tenantService: TenantService, landingService: LandingService) {
        this.logo$ = tenantService.currentTenant$.pipe(
            filterForDefinedValue(),
            map((t) => ({
                src: t.logo ?? "/assets/group_placeholder.svg",
                alt: `${t.name} Logo`,
            })),
        );
        this.summary$ = landingService.getLandingSummary().pipe(
            tap((s) => {
                for (const billingPeriod of s.tenant.latestBillingPeriods) {
                    this.billingPeriodProportions[billingPeriod.name] = [
                        {
                            count: billingPeriod.paidEarly,
                            tooltip: `${billingPeriod.paidEarly} paid early`,
                            colour: paidEarlyCategory.fill.toCssString(),
                        },
                        {
                            count: billingPeriod.paidOnTime,
                            tooltip: `${billingPeriod.paidOnTime} paid on time`,
                            colour: paidOnTimeCategory.fill.toCssString(),
                        },
                        {
                            count: billingPeriod.paidLate,
                            tooltip: `${billingPeriod.paidLate} paid late`,
                            colour: paidLateCategory.fill.toCssString(),
                        },
                        {
                            count: billingPeriod.unpaid,
                            tooltip: `${billingPeriod.unpaid} unpaid`,
                            colour: unpaidCategory.fill.toCssString(),
                        },
                    ];
                }
            }),
            tap(
                (s) =>
                    (this.memberDashboardCommands = [
                        "..",
                        "members",
                        "dashboard",
                        {
                            [selectedMemberParamName]: s.personal?.memberId,
                        },
                    ]),
            ),
            cacheLatest(),
        );
        this.billingList$ = this.summary$.pipe(
            map((s) => {
                if (!s.personal) {
                    return [];
                }

                const invoices = [...s.personal.unpaidInvoices];
                if (s.personal.lastInvoice) {
                    invoices.push(s.personal.lastInvoice);
                }

                return invoices;
            }),
            map((invoices) =>
                invoices.map((i) => ({
                    invoice: i,
                    billingPeriod: {
                        earlyBirdDue: i.earlyBirdDue,
                        due: i.due,
                        state: i.billingPeriodState,
                    },
                })),
            ),
        );
        this.performanceSummaryChartOptions$ = this.summary$.pipe(
            map((d) => this.buildPerformanceSummaryChartOptions(d)),
        );
        this.membershipSnapshotChartOptions$ = this.summary$.pipe(
            map((d) => this.buildMembershipSnapshotChartOptions(d)),
        );
        this.mailingListUsageChartOptions$ = this.summary$.pipe(
            map((d) => this.buildMailingListUsageChartOptions(d)),
        );
    }

    public buildPerformanceSummaryChartOptions(
        dto: LandingSummaryDto,
    ): AgCartesianChartOptions | undefined {
        const rawData = dto.personal?.performanceHistory;
        if (!rawData || rawData.length === 0) {
            return undefined;
        }

        const { chartData, ensembleList } = this.prepareChartData(rawData);
        return {
            theme: orchestrateAgChartTheme,
            data: chartData,
            series: ensembleList.map<AgBarSeriesOptions>((e) => ({
                type: "bar",
                stacked: true,
                xKey: "year",
                yKey: e,
                yName: e,
            })),
            axes: [
                {
                    type: "category",
                    position: "bottom",
                    label: {
                        rotation: -90,
                    },
                },
                {
                    type: "number",
                    position: "left",
                    min: 0,
                },
            ],
            legend: {
                enabled: false,
            },
        };
    }

    private prepareChartData(rawData: PerformanceHistoryDto[]) {
        const ensembleSet = new Set<string>(rawData.map((d) => d.ensembleName));
        const ensembleList = Array.from(ensembleSet).sort((a, b) => a.localeCompare(b));

        const defaultEnsemblePerformanceCount: Record<string, number> = {};
        for (const e of ensembleList) {
            defaultEnsemblePerformanceCount[e] = 0;
        }

        const chartData: { year: number; [ensemble: string]: number }[] = [];
        for (const rawRow of rawData) {
            let matchingRow = chartData.find((d) => d.year === rawRow.year);
            if (!matchingRow) {
                matchingRow = { year: rawRow.year, ...defaultEnsemblePerformanceCount };
                chartData.push(matchingRow);
            }
            matchingRow[rawRow.ensembleName] = rawRow.performanceCount;
        }

        // Fill empty years with zeros
        if (chartData.length >= 2) {
            chartData.sort((a, b) => a.year - b.year);
            for (let i = 0; i < chartData.length - 1; i++) {
                const currYear = chartData[i]!.year;
                const nextYear = chartData[i + 1]!.year;
                if (nextYear - currYear > 1) {
                    // I know, I know, modifying the array as we're iterating over the length
                    // But it keeps this code tight
                    chartData.splice(i + 1, 0, {
                        year: currYear + 1,
                        ...defaultEnsemblePerformanceCount,
                    });
                }
            }
        }

        return { chartData, ensembleList };
    }

    private buildMembershipSnapshotChartOptions(
        dto: LandingSummaryDto,
    ): AgPolarChartOptions | undefined {
        const snapshot = dto.tenant.membershipSnapshot;
        if (!snapshot) {
            return undefined;
        }

        const memberCountColours = [
            pastelPalette.lightGreen,
            pastelPalette.lightBlue,
            pastelPalette.lightOrange,
            pastelPalette.lightRed,
        ];

        return {
            theme: orchestrateAgChartTheme,
            data: [
                {
                    label: "Joined",
                    value: snapshot.joinedCount,
                },
                {
                    label: "Stayed",
                    value: snapshot.stayedCount,
                },
                {
                    label: "Left",
                    value: snapshot.leftCount,
                },
                {
                    label: "Joined & left",
                    value: snapshot.joinedAndLeftCount,
                },
            ],
            padding: {
                bottom: 35,
            },
            series: [
                {
                    type: "pie",
                    angleKey: "value",
                    calloutLabelKey: "label",
                    fills: memberCountColours.map((e) => e.toRgbColour().toCssString()),
                    strokes: memberCountColours.map((e) =>
                        e.addLightness(-10).toRgbColour().toCssString(),
                    ),
                },
            ],
            legend: {
                position: "bottom",
            },
        };
    }

    private buildMailingListUsageChartOptions(
        dto: LandingSummaryDto,
    ): AgCartesianChartOptions | undefined {
        if (dto.tenant.mailingListUsage.length === 0) {
            return undefined;
        }

        return {
            theme: orchestrateAgChartTheme,
            // Multiple series to get different colours per mailing list
            series: dto.tenant.mailingListUsage.map((u) => ({
                type: "bar",
                data: [u],
                xKey: "name",
                yKey: "usageCount",
                yName: u.name,
                tooltip: {
                    renderer: () => ({ heading: undefined }),
                },
            })),
            axes: [
                {
                    type: "number",
                    position: "left",
                    min: 0,
                },
                {
                    type: "category",
                    position: "bottom",
                    label: {
                        enabled: false,
                    },
                },
            ],
        };
    }
}
