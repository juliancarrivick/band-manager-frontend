import { saneDate } from "app/common/utils";

export interface ReleaseInfo {
    tag: string;
    milestone?: number;
    date: Date;
    notableChanges: string[];
}

export interface MajorReleaseInfo extends ReleaseInfo {
    patches?: ReleaseInfo[];
}

export const releaseHistory: readonly MajorReleaseInfo[] = [
    {
        tag: "v2025.01",
        date: new Date(),
        notableChanges: [],
    },
    {
        tag: "v2024.12",
        milestone: 35,
        date: saneDate(2024, 12, 22),
        notableChanges: [
            "Improve readability of mailing list usage chart",
            "Show total number of mailing lists that can be created",
            "Update dependencies, notably to Angular 18",
        ],
        patches: [
            {
                tag: "v2024.12.1",
                date: saneDate(2024, 12, 23),
                notableChanges: [
                    "Fix errors when attempting to save data",
                ],
            },
            {
                tag: "v2024.12.2",
                date: saneDate(2024, 12, 27),
                notableChanges: [
                    "Fix another error when attempting to save data",
                ],
            },
            {
                tag: "v2024.12.3",
                date: saneDate(2025, 1, 19),
                notableChanges: [
                    "Fix an error when payments are made",
                ],
            },
        ],
    },
    {
        tag: "v2024.03",
        milestone: 33,
        date: saneDate(2024, 3, 16),
        notableChanges: [
            "Mailing lists are now out of Beta!",
            "Recipients can now unsubscribe from mailing lists",
            "Mailing list summary information has been added to the home page",
            "Mailing list help is now available",
            "Update to .NET 8 & Angular 17",
        ],
    },
    {
        tag: "v2024.02",
        milestone: 32,
        date: saneDate(2024, 2, 8),
        notableChanges: [
            "Introducing Mailing Lists Beta - a way to easily send emails to the people that are" +
                " important to your organisation",
            "Add new Committee Member role, which is a more powerful version of Editor",
            "Replies to invoice emails will now be directed to the member billing notification contact",
        ],
        patches: [
            {
                tag: "v2024.02.1",
                date: saneDate(2024, 3, 11),
                notableChanges: [
                    "Fix Committee members not being able to send members invitation emails",
                ],
            },
        ],
    },
    {
        tag: "v2023.11",
        date: saneDate(2023, 11, 5),
        notableChanges: [
            "Fix sorting of performances on Member dashboard page",
            "Update to Angular 16",
        ],
        patches: [
            {
                tag: "v2023.11.1",
                date: saneDate(2023, 11, 18),
                notableChanges: [
                    "Fix add concert dialog not using full width",
                    "Update Stripe SDK to support latest API version",
                ],
            },
            {
                tag: "v2023.11.2",
                date: saneDate(2023, 11, 20),
                notableChanges: [
                    "Revert Stripe SDK to version that failed event is sending",
                ],
            },
            {
                tag: "v2023.11.3",
                date: saneDate(2023, 11, 20),
                notableChanges: [
                    "Update Stripe SDK to match API version of future events",
                ],
            },
        ],
    },
    {
        tag: "v2023.06",
        milestone: 31,
        date: saneDate(2023, 6, 4),
        notableChanges: [
            "Introducing contacts - a place to store people that your group keeps in contact with",
            "Behind-the-scenes changes to improve reliability and minimise down-time",
        ],
        patches: [
            {
                tag: "v2023.06.1",
                date: saneDate(2023, 6, 6),
                notableChanges: ["Improve user experience when loading Orchestrate"],
            },
        ],
    },
    {
        tag: "v2023.05",
        milestone: 29,
        date: saneDate(2023, 5, 7),
        notableChanges: [
            "Add dialog to list performances for a piece of music",
            "Allow reciept to be sent to member while marking the invoice as paid",
            "Require captcha when registering to ensure user is not a bot",
        ],
    },
    {
        tag: "v2023.02",
        milestone: 28,
        date: saneDate(2023, 2, 12),
        notableChanges: [
            "Orchestrate subscription can now be managed from Settings",
            "Minor visual refresh with update to Angular 15",
        ],
    },
    {
        tag: "v2023.01",
        milestone: 27,
        date: saneDate(2023, 1, 7),
        notableChanges: [
            "Orchestrate registration can now be performed anonymously",
            "Updated home page highlighting interesting data in your group",
            "Add help centre that can be summoned from any page",
        ],
    },
    {
        tag: "v2022.11",
        milestone: 26,
        date: saneDate(2022, 11, 13),
        notableChanges: [
            "Fix some errors when adding members & deleting concerts",
            "Fix some minor layout issues on member invoices & ensemble performances",
            "Prevent billing periods from being generated when member billing is disabled",
            "Add help email, Facebook & GitLab links to about page",
            "Update to .NET 7, Angular 14",
        ],
    },
    {
        tag: "v2022.08",
        milestone: 25,
        date: saneDate(2022, 8, 12),
        notableChanges: [
            "You can now upload files to be stored along with Music, Assets and Concerts",
            "Enable text in tables to be selected",
        ],
    },
    {
        tag: "v2022.05",
        milestone: 24,
        date: saneDate(2022, 5, 13),
        notableChanges: [
            "Orchestrate now adapts much better to mobile devices",
            "Add table view to member billing periods and allow the contents to be exported",
            'Change email providers to <a href="https://postmarkapp.com/" target="_blank">Postmark</a> to prevent emails being caught as spam',
            'Update to <a href="https://docs.microsoft.com/en-us/dotnet/core/whats-new/dotnet-6" target="_blank">.NET 6</a>',
        ],
    },
    {
        tag: "v2022.04",
        milestone: 22,
        date: saneDate(2022, 4, 22),
        notableChanges: [
            "Orchestrate is no longer limited to a single group - it can now be used by multiple groups simultaneously",
            "Users & Security page has been updated to make permission management much clearer",
            "Tweak colour theme",
        ],
    },
    {
        tag: "v2022.02",
        milestone: 23,
        date: saneDate(2022, 2, 6),
        notableChanges: [
            'Orchestrate is now accessible via <a href="https://app.orchestrate.community" target="_blank">https://app.orchestrate.community</a>',
            "Add Orchestrate and Maestro Software branding",
            "Move What's New to About page and add Terms of Use",
        ],
        patches: [
            {
                tag: "v2022.02.1",
                date: saneDate(2022, 2, 6),
                notableChanges: [
                    "Update website favicon to Orchestrate icon",
                ],
            },
            {
                tag: "v2022.02.2",
                date: saneDate(2022, 2, 6),
                notableChanges: [
                    "Fix redirect to app.orchestrate.community",
                ],
            },
            {
                tag: "v2022.02.3",
                date: saneDate(2022, 3, 15),
                notableChanges: [
                    "Fix downloading of invoices",
                ],
            },
        ],
    },
    {
        tag: "v2022.01",
        milestone: 21,
        date: saneDate(2021, 1, 15),
        notableChanges: [
            "Refresh and improve the ensemble user experience, also exposing historical statistics",
            "Refresh and improve the concert user experience, also exposing key statistics",
            "Update frameworks and dependencies (Angular 13, ag-grid 26)",
        ],
    },
    {
        tag: "v2021.12",
        milestone: 18,
        date: saneDate(2021, 12, 18),
        notableChanges: [
            "Member page user experience improvements",
            "Update frameworks (Angular 12, .NET 5)",
        ],
    },
    {
        tag: "v2021.09",
        milestone: 17,
        date: saneDate(2021, 8, 28),
        notableChanges: [
            'Add "What\'s new" page',
            'Switch to "year.month.patch" versioning',
            "Additional billing notification emails for treasurer",
            "Music library improvements",
        ],
        patches: [
            {
                tag: "v2021.09.1",
                milestone: 19,
                date: saneDate(2021, 8, 29),
                notableChanges: [
                    "Fix error when saving new performance",
                ],
            },
            {
                tag: "v2021.09.2",
                milestone: 20,
                date: saneDate(2021, 9, 9),
                notableChanges: [
                    "Allow invoices to be edited when marking them as paid",
                ],
            },
        ],
    },
    {
        tag: "v0.19",
        milestone: 13,
        date: saneDate(2021, 7, 19),
        notableChanges: [
            "Add automated member billing",
            "Clearly indicate test environments in the UI",
            "After logging in, redirect to the originally requested page",
        ],
        patches: [
            {
                tag: "v0.19.1",
                milestone: 14,
                date: saneDate(2021, 8, 2),
                notableChanges: [
                    "Minor fix to prevent an erroneous error",
                ],
            },
            {
                tag: "v0.19.2",
                milestone: 15,
                date: saneDate(2021, 8, 12),
                notableChanges: [
                    "Don't automatically send manually generated invoices after the billing period has been sent out",
                    "Slight tweak to display of closed billing periods",
                ],
            },
            {
                tag: "v0.19.3",
                milestone: 16,
                date: saneDate(2021, 8, 20),
                notableChanges: [
                    "Fix error when attempting to create new members",
                    "Add due date to generated invoice PDFs",
                ],
            },
        ],
    },
    {
        tag: "v0.18",
        date: saneDate(2021, 4, 24),
        notableChanges: [
            "Give the UI a facelift, use Angular Material instead of Bootstrap",
            "Replace DevExpress on Asset page",
            "Setup BreezeJS/Breeze.NET support",
        ],
    },
    {
        tag: "v0.17",
        date: saneDate(2020, 11, 27),
        notableChanges: [
            "Migrate backend to ASP.NET Core",
        ],
        patches: [
            {
                tag: "v0.17.1",
                date: saneDate(2020, 12, 2),
                notableChanges: [
                    "Fix test users being created on a login attempt",
                ],
            },
            {
                tag: "v0.17.2",
                date: saneDate(2020, 12, 2),
                notableChanges: [
                    "Infrastructure updates",
                ],
            },
            {
                tag: "v0.17.3",
                date: saneDate(2020, 12, 4),
                notableChanges: [
                    "Fix redirect loop when resetting password",
                ],
            },
            {
                tag: "v0.17.4",
                date: saneDate(2020, 12, 4),
                notableChanges: [
                    "Fix email sending",
                ],
            },
        ],
    },
    {
        tag: "v0.16",
        date: saneDate(2020, 5, 23),
        notableChanges: [
            'Add "In Library" field to scores',
        ],
    },
    {
        tag: "v0.15",
        date: saneDate(2020, 1, 12),
        notableChanges: [
            "Handle expired reset password links",
        ],
    },
    {
        tag: "v0.14",
        date: saneDate(2019, 9, 1),
        notableChanges: [
            "Complete migration to Angular",
        ],
        patches: [
            {
                tag: "v0.14.1",
                date: saneDate(2019, 11, 24),
                notableChanges: [
                    "Bug fixes",
                ],
            },
        ],
    },
    {
        tag: "v0.13",
        date: saneDate(2019, 3, 31),
        notableChanges: [
            "Add password reset",
        ],
        patches: [
            {
                tag: "v0.13.1",
                date: saneDate(2019, 4, 6),
                notableChanges: [
                    "Fix bug where member instruments would be cleared on saving other data",
                    "Fix editor for member notes",
                    "Prevent creating multiple members with the same email address",
                ],
            },
        ],
    },
    {
        tag: "v0.12",
        date: saneDate(2019, 3, 4),
        notableChanges: [
            "Behind the scenes work to update to Angular",
        ],
    },
    {
        tag: "v0.11",
        date: saneDate(2018, 12, 12),
        notableChanges: [
            "Update Score and Member pages to use DevExpress",
        ],
    },
    {
        tag: "v0.10",
        milestone: 11,
        date: saneDate(2018, 10, 27),
        notableChanges: [
            "Switch to DevExpress component library",
        ],
    },
    {
        tag: "v0.9",
        milestone: 9,
        date: saneDate(2018, 2, 3),
        notableChanges: [
            "Use custom CSV exporter to correctly display nested data",
            "Start migration to Angular 2+ & Typescript",
        ],
    },
    {
        tag: "v0.8",
        milestone: 8,
        date: saneDate(2017, 7, 11),
        notableChanges: [
            "Allow ensemble membership to be tracked",
            "Allow scores to be created directly from the performance page",
        ],
        patches: [
            {
                tag: "v0.8.1",
                milestone: 10,
                date: saneDate(2017, 9, 10),
                notableChanges: [
                    "Only show current ensembles on member page",
                ],
            },
        ],
    },
    {
        tag: "v0.7",
        date: saneDate(2017, 4, 2),
        milestone: 6,
        notableChanges: [
            "Add CSV exporting",
            "Add statistics to home page",
        ],
        patches: [
            {
                tag: "v0.7.1",
                date: saneDate(2017, 4, 9),
                notableChanges: [
                    "Fix selection of ensembles on member page",
                    "Fix hidden ensembles checkbox",
                ],
            },
        ],
    },
    {
        tag: "v0.6",
        milestone: 4,
        date: saneDate(2016, 11, 20),
        notableChanges: [
            "Add member summary page, which shows which concerts each member has been involved in",
            "Add performance counts to score page",
            "Allow assets to be loaned to members",
            "Add role based access controls, and allow this to be managed by admins",
        ],
        patches: [
            {
                tag: "v0.6.1",
                date: saneDate(2017, 3, 25),
                milestone: 7,
                notableChanges: [
                    "Minor fixes",
                ],
            },
        ],
    },
    {
        tag: "v0.5",
        milestone: 3,
        date: saneDate(2016, 11, 9),
        notableChanges: [
            "Add concert and performance tracking",
        ],
    },
    {
        tag: "v0.4",
        milestone: 2,
        date: saneDate(2016, 10, 2),
        notableChanges: [
            "Add score management",
            "Display the number of members on the member page",
        ],
    },
    {
        tag: "v0.3",
        milestone: 1,
        date: saneDate(2016, 9, 11),
        notableChanges: [
            "Add ensemble and ensemble membership management",
            "Small tweaks to improve usability",
        ],
    },
    {
        tag: "v0.2",
        date: saneDate(2016, 5, 6),
        notableChanges: [
            "Implement basic member management",
            "Add quantity to assets",
        ],
    },
    {
        tag: "v0.1",
        date: saneDate(2015, 12, 30),
        notableChanges: [
            "First release!",
            "Allow user login and management of assets",
        ],
    },
];
