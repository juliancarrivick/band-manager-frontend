import { ChangeDetectionStrategy, Component } from "@angular/core";
import { AppConfig, Environment } from "app/common/app-config";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { MajorReleaseInfo, releaseHistory } from "./releases";

interface ReleaseData {
    info: MajorReleaseInfo;
    badge?: {
        class: string;
        icon: string;
        text: string;
    };
}

@Component({
    selector: "app-whats-new",
    templateUrl: "./whats-new.component.html",
    styleUrls: ["./whats-new.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: false,
})
export class WhatsNewComponent {
    public releases$: Observable<ReleaseData[]>;

    public constructor(config: AppConfig) {
        this.releases$ = config.environment$.pipe(
            map((environment) => {
                const releases: ReleaseData[] = releaseHistory.map((r) => ({ info: r }));

                // We know we've got more than 2 in this array
                releases[0]!.badge = {
                    class: "development",
                    icon: "construction",
                    text: "In development",
                };
                releases[1]!.badge = {
                    class: "current",
                    icon: "new_releases",
                    text: "Current release",
                };

                if (environment === Environment.Production) {
                    // Remove the current development version
                    releases.splice(0, 1);
                }

                return releases;
            }),
        );
    }
}
