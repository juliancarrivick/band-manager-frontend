import { Component } from "@angular/core";

@Component({
    templateUrl: "./about-page.component.html",
    styleUrls: ["./about-page.component.scss"],
    standalone: false,
})
export class AboutPageComponent {}
