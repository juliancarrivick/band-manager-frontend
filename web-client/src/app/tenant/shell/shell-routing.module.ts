import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AppRoutes } from "app/common/app-routing-types";
import { helpOutletName } from "app/common/routing-utils";
import { helpPages } from "app/help/route/help-list";
import { HomeComponent } from "app/tenant/shell/home/home.component";
import { restrictedToTenantPermissionGuard } from "../module/restricted-to-tenant-permission.guard";
import { ShellComponent } from "./shell/shell.component";
import { unsavedChangesGuard } from "./unsaved-changes-guard";

const shellRoutes: AppRoutes = [
    {
        path: "",
        component: ShellComponent,
        data: {
            isShellRoute: true,
        },
        canActivateChild: [
            restrictedToTenantPermissionGuard,
            unsavedChangesGuard,
        ],
        children: [
            {
                outlet: helpOutletName,
                path: "view",
                loadChildren: () =>
                    import("app/help/route/help.module").then((m) => m.AppHelpModule),
            },
            {
                path: "home",
                component: HomeComponent,
                data: {
                    help: [helpPages.getStarted],
                },
            },
            {
                path: "about",
                loadChildren: () => import("./about/about.module").then((m) => m.AppAboutModule),
            },
            {
                path: "assets",
                loadChildren: () =>
                    import("app/tenant/feature/asset/asset.module").then((m) => m.AppAssetModule),
            },
            {
                path: "members",
                loadChildren: () =>
                    import("app/tenant/feature/member/member.module").then(
                        (m) => m.AppMemberModule,
                    ),
            },
            {
                path: "member-billing",
                loadChildren: () =>
                    import("app/tenant/feature/member-billing/member-billing.module").then(
                        (m) => m.AppMemberBillingModule,
                    ),
            },
            {
                path: "music-library",
                loadChildren: () =>
                    import("app/tenant/feature/score/score.module").then((m) => m.AppScoreModule),
            },
            {
                path: "ensembles",
                loadChildren: () =>
                    import("app/tenant/feature/ensemble/ensemble.module").then(
                        (m) => m.AppEnsembleModule,
                    ),
            },
            {
                path: "concerts",
                loadChildren: () =>
                    import("app/tenant/feature/concert/concert.module").then(
                        (m) => m.AppConcertModule,
                    ),
            },
            {
                path: "mailing-lists",
                loadChildren: () =>
                    import("app/tenant/feature/mailing-lists/mailing-lists-routes").then(
                        (m) => m.mailingListsRoutes,
                    ),
            },
            {
                path: "contacts",
                loadChildren: () =>
                    import("app/tenant/feature/contacts/contacts.module").then(
                        (m) => m.AppContactsModule,
                    ),
            },
            {
                path: "users",
                loadChildren: () =>
                    import("app/tenant/feature/security/security.module").then(
                        (m) => m.AppSecurityModule,
                    ),
            },
            {
                path: "settings",
                loadChildren: () =>
                    import("app/tenant/feature/settings/settings.module").then(
                        (m) => m.AppSettingsModule,
                    ),
            },
        ],
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(shellRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppShellRoutingModule {}
