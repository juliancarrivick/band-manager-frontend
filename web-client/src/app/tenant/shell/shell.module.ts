import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { AgChartsModule } from "ag-charts-angular";
import { AppAuthModule } from "app/auth/auth.module";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppIconTextPairModule } from "app/common-ux/icon-text-pair/icon-text-pair.module";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AppProportionBarModule } from "app/common-ux/proportion-bar/proportion-bar.component";
import { PrimaryRouterLinkDirective } from "app/common-ux/router/primary-router-link.directive";
import { UserDropdownComponent } from "app/common-ux/user-dropdown/user-dropdown.component";
import { AppStorageModule } from "app/storage/storage.module";
import { BillingPeriodStateBadgeComponent } from "../feature/member-billing/billing-period-state-badge/billing-period-state-badge.component";
import { MemberInvoiceStateBadgeComponent } from "../feature/member-billing/member-invoice-state-badge/member-invoice-state-badge.component";
import { HomeComponent } from "./home/home.component";
import { AppShellRoutingModule } from "./shell-routing.module";
import { ShellComponent } from "./shell/shell.component";
import { UnsavedChangesDialogComponent } from "./unsaved-changes-dialog/unsaved-changes-dialog.component";

@NgModule({
    imports: [
        CommonModule,

        MatToolbarModule,
        MatSidenavModule,
        MatMenuModule,
        MatIconModule,
        MatListModule,
        MatTooltipModule,
        MatCardModule,

        AppButtonModule,
        AppAuthModule,
        AppShellRoutingModule,
        AppDialogModule,
        AppPageWrapperModule,
        AppBadgeModule,
        AppLoadingModule,
        AppIconTextPairModule,
        AppProportionBarModule,
        AppStorageModule,

        AgChartsModule,

        BillingPeriodStateBadgeComponent,
        MemberInvoiceStateBadgeComponent,
        PrimaryRouterLinkDirective,
        UserDropdownComponent,
    ],
    declarations: [
        HomeComponent,
        ShellComponent,
        UnsavedChangesDialogComponent,
    ],
    providers: [],
})
export class AppShellModule {}
