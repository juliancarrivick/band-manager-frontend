import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { TenantDto } from "app/tenant/module/tenant-dto";
import { switchMap } from "rxjs";

export interface RegisterTenantDto {
    hcaptchaResponse: string;
    registeringUser: {
        email: string;
        firstName: string;
        lastName: string;
    };
    tenant: {
        name: string;
        abbreviation: string;
        timeZone: string;
        website: string | null;
        dateFounded: Date | null;
        logo: string | null;
    };
}

export interface RegistrationResultDto {
    postRegisterAction: "Redirect" | "CheckEmail";
    tenant: TenantDto;
}

@Injectable()
export class GroupRegistrationService {
    public constructor(
        private httpClient: HttpClient,
        private appConfig: AppConfig,
    ) {}

    public register(dto: RegisterTenantDto) {
        return this.appConfig
            .serverEndpoint(`tenant/register`)
            .pipe(switchMap((url) => this.httpClient.post<RegistrationResultDto>(url, dto)));
    }
}
