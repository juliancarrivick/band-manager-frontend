export class TimeZone {
    public readonly name: string;
    public readonly label: string;
    public readonly offsetMinutes: number;
    public readonly gmtOffset: string;

    public constructor(name: string, label?: string) {
        this.name = name;
        this.label = label ?? name.split("/").reverse().join(", ");
        this.offsetMinutes = this.calculateOffsetMinutes();
        this.gmtOffset = this.calculateGmtOffset(this.offsetMinutes);
    }

    private calculateOffsetMinutes() {
        const now = new Date();
        const utcDate = new Date(now.toLocaleString("en-US", { timeZone: "UTC" }));
        const tzDate = new Date(now.toLocaleString("en-US", { timeZone: this.name }));
        const difference = tzDate.getTime() - utcDate.getTime();
        const differenceMinutes = difference / (60 * 1000);
        return differenceMinutes;
    }

    private calculateGmtOffset(offsetMinutes: number) {
        const hourOffset = String(Math.abs(Math.floor(offsetMinutes / 60))).padStart(2, "0");
        const minuteOffset = String(offsetMinutes % 60).padStart(2, "0");
        const formattedOffset = `GMT${offsetMinutes < 0 ? "-" : "+"}${hourOffset}:${minuteOffset}`;
        return formattedOffset;
    }
}

export function buildTimeZones() {
    // TODO Generate the list of timezones & offsets on the server
    // Adapted from https://stackoverflow.com/a/68593283/17403538
    const allTimeZones: string[] = (Intl as any).supportedValuesOf("timeZone");

    // TODO Can't dynamically generate labels :(
    const list = allTimeZones.map((tz) => new TimeZone(tz, tz));

    return list;
}

export const enabledTimeZones: TimeZone[] = [
    new TimeZone("Australia/Perth"),
    new TimeZone("Australia/Darwin"),
    new TimeZone("Australia/Brisbane"),
    new TimeZone("Australia/Adelaide"),
    new TimeZone("Australia/Hobart"),
    new TimeZone("Australia/Melbourne"),
    new TimeZone("Australia/Sydney"),
    new TimeZone("Pacific/Auckland", "New Zealand"),
].sort((a, b) => {
    const offsetDifference = a.offsetMinutes - b.offsetMinutes;
    return offsetDifference !== 0 ? offsetDifference : a.label.localeCompare(b.label);
});
