import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatStepper } from "@angular/material/stepper";
import { ResponsiveService } from "app/common-ux/responsive.service";
import { AppConfig, Environment } from "app/common/app-config";
import { BasicServerError, getErrorMessage } from "app/common/error";
import { catchErrorOfType } from "app/common/rxjs-utilities";
import { EMPTY, Observable, catchError, map, tap } from "rxjs";
import {
    ValidationFailure,
    ValidationResultError,
    setValidationResultsOnFormGroup,
} from "../../common/validation-result";
import {
    GroupRegistrationService,
    RegisterTenantDto,
    RegistrationResultDto,
} from "./group-registration.service";
import { enabledTimeZones } from "./timezones";

@Component({
    selector: "app-register-page",
    templateUrl: "./register-page.component.html",
    styleUrls: ["./register-page.component.scss"],
    providers: [
        GroupRegistrationService,
    ],
    standalone: false,
})
export class RegisterPageComponent {
    public userForm = new FormGroup({
        email: new FormControl("", {
            nonNullable: true,
            validators: [
                Validators.required,
                Validators.email,
            ],
        }),
        firstName: new FormControl("", {
            nonNullable: true,
            validators: [
                Validators.required,
                Validators.minLength(1),
            ],
        }),
        lastName: new FormControl("", {
            nonNullable: true,
            validators: [
                Validators.required,
                Validators.minLength(1),
            ],
        }),
    });
    public tenantForm = new FormGroup({
        name: new FormControl("", {
            nonNullable: true,
            validators: [Validators.required],
        }),
        abbreviation: new FormControl("", {
            nonNullable: true,
            validators: [Validators.required],
        }),
        timeZone: new FormControl(Intl.DateTimeFormat().resolvedOptions().timeZone, {
            nonNullable: true,
            validators: [Validators.required],
        }),
        dateFounded: new FormControl<Date | null>(null),
        website: new FormControl<string | null>(null),
        logo: new FormControl<string | null>(null),
    });

    public stepperOrientation$: Observable<"horizontal" | "vertical">;
    public isDevelopment$: Observable<boolean>;
    public registrationResponse?: RegistrationResultDto;
    public registrationValidation?: ValidationFailure<RegisterTenantDto>;
    public timeZones = enabledTimeZones;
    public logo?: string;
    public hcaptchaResponse?: string;
    public registrationError?: { text: string; getInContact: boolean };
    public readonly now = new Date();

    public constructor(
        private registrationService: GroupRegistrationService,
        responsiveService: ResponsiveService,
        appConfig: AppConfig,
    ) {
        this.stepperOrientation$ = responsiveService.isMobile$.pipe(
            map((isMobile) => (isMobile ? "vertical" : "horizontal")),
        );
        this.isDevelopment$ = appConfig.environment$.pipe(
            map((e) => e === Environment.Development),
            tap((isDev) => (this.hcaptchaResponse = isDev ? "placeholder" : undefined)),
        );
    }

    public register = (stepper?: MatStepper) => {
        this.registrationError = undefined;

        if (!this.hcaptchaResponse) {
            this.registrationError = {
                text: "Captcha needs to be filled out",
                getInContact: false,
            };
            return EMPTY;
        }

        return this.registrationService
            .register({
                hcaptchaResponse: this.hcaptchaResponse,
                registeringUser: this.userForm.getRawValue(),
                tenant: this.tenantForm.getRawValue(),
            })
            .pipe(
                tap((result) => {
                    this.registrationResponse = result;
                    stepper?.next();
                }),
                catchErrorOfType(
                    ValidationResultError,
                    (error: ValidationResultError<RegisterTenantDto>) => {
                        this.registrationValidation = error.validationResults;
                        setValidationResultsOnFormGroup(
                            error.validationResults.tenant,
                            this.tenantForm,
                        );
                        setValidationResultsOnFormGroup(
                            error.validationResults.registeringUser,
                            this.userForm,
                        );

                        if (this.userForm.invalid) {
                            stepper?.previous();
                        }

                        return EMPTY;
                    },
                ),
                catchError((e) => {
                    if (
                        e instanceof BasicServerError &&
                        e.errors.find((e) => e.code === "CAPTCHA_FAIL")
                    ) {
                        this.registrationError = {
                            text: "Unable to verify you are human (sorry!)",
                            getInContact: true,
                        };
                    } else {
                        this.registrationError = {
                            text: getErrorMessage(e),
                            getInContact: false,
                        };
                    }
                    return EMPTY;
                }),
            );
    };

    public setAbbreviationFromName() {
        if (!this.tenantForm.controls.abbreviation.pristine) {
            return;
        }

        const name = this.tenantForm.controls.name.value;
        const abbreviation = name.trim().toLocaleLowerCase().replaceAll(" ", "-");
        this.tenantForm.controls.abbreviation.setValue(abbreviation);
    }

    public async onFileSelected(event: Event) {
        const input = event.target as HTMLInputElement | null;
        if (!input) {
            return;
        }

        try {
            if (!input.files || input.files.length !== 1) {
                return;
            }

            const file = input.files[0]!;
            if (file.size > 2 * 1024 * 1024) {
                this.tenantForm.controls.logo.setErrors({
                    maxSize: "Logo cannot exceed 2MB, please compress or resize",
                });
                input.value = "";
                return;
            }

            const newLogo = await this.getFileAsDataUrl(file);

            this.logo = newLogo;
            this.tenantForm.controls.logo.setValue(this.getBase64TextFromDataUrl(newLogo) ?? null);
        } catch (e) {
            input.value = "";
            this.logo = undefined;
            this.tenantForm.controls.logo.reset();
        }
    }

    private getFileAsDataUrl(file: File) {
        return new Promise<string>((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = (e) => {
                if (reader.result) {
                    resolve(reader.result as string);
                } else {
                    reject(new Error("No data in file"));
                }
            };
            reader.onerror = (e) => {
                reject(new Error("Error while reading"));
            };
            reader.onabort = (e) => {
                reject(new Error("Reading aborted"));
            };

            reader.readAsDataURL(file);
        });
    }

    private getBase64TextFromDataUrl(dataUrl: string) {
        const partitionedDataUrl = dataUrl.split(";base64,");
        return partitionedDataUrl[1];
    }
}
