import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { AppMobileCollapsibleContentModule } from "app/common-ux/mobile-collapsible-content/mobile-collapsible-content.module";
import { GetStartedComponent } from "./get-started.component";
import { NotFoundPageComponent } from "./not-found-page.component";
import { PageWrapperComponent } from "./page-wrapper.component";
import { TwoColumnLayoutComponent } from "./two-column-layout.component";

@NgModule({
    imports: [
        CommonModule,

        MatToolbarModule,
        MatIconModule,
        MatCardModule,

        AppMobileCollapsibleContentModule,
    ],
    exports: [
        MatIconModule,

        AppMobileCollapsibleContentModule,

        PageWrapperComponent,
        TwoColumnLayoutComponent,
        NotFoundPageComponent,
        GetStartedComponent,
    ],
    declarations: [
        PageWrapperComponent,
        TwoColumnLayoutComponent,
        NotFoundPageComponent,
        GetStartedComponent,
    ],
})
export class AppPageWrapperModule {}
