import { Component } from "@angular/core";

@Component({
    selector: "app-get-started",
    template: `
        <div class="get-started-container">
            <mat-card>
                <img
                    mat-card-image
                    src="assets/get_started.png"
                    alt="piano keyboard"
                />
                <mat-card-content>
                    <ng-content select="[get-started-content]"></ng-content>
                </mat-card-content>
                <mat-card-actions>
                    <ng-content select="[get-started-actions]"></ng-content>
                </mat-card-actions>
            </mat-card>
        </div>
    `,
    styles: [
        `
            .get-started-container {
                display: flex;
                justify-content: space-around;
            }

            img {
                margin-top: -8px;
                margin-left: -2px;
                margin-right: -2px;
            }

            mat-card {
                max-width: 600px;
                overflow: hidden;
            }

            mat-card-content {
                padding-top: 1rem;
                padding-bottom: 1rem;
            }

            mat-card-actions:empty {
                display: none;
            }
        `,
    ],
    standalone: false,
})
export class GetStartedComponent {}
