import { MatDialogRef } from "@angular/material/dialog";
import { EntityTracker } from "app/dal/entity-tracker";
import { switchMap } from "rxjs";
import { BaseDialog } from "./base-dialog";

export abstract class BaseBreezeDialog<TInput, TResolve = TInput> extends BaseDialog<
    TInput,
    TResolve
> {
    protected abstract entityTracker: EntityTracker;

    public constructor(dialogRef: MatDialogRef<BaseBreezeDialog<TInput, TResolve>>) {
        super(dialogRef);

        this.dialogClose$.pipe(switchMap(() => this.entityTracker.cancelChanges())).subscribe();
    }
}
