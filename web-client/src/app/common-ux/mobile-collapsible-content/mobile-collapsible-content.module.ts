import { A11yModule } from "@angular/cdk/a11y";
import { OverlayModule } from "@angular/cdk/overlay";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MobileCollapsibleContentComponent } from "./mobile-collapsible-content.component";
import { MobileCollpsibleFilterComponent } from "./mobile-collapsible-filter.component";

@NgModule({
    imports: [
        CommonModule,

        OverlayModule,
        A11yModule,

        MatButtonModule,
        MatIconModule,
        MatTooltipModule,
        MatFormFieldModule,
        MatInputModule,
    ],
    exports: [
        MobileCollapsibleContentComponent,
        MobileCollpsibleFilterComponent,
    ],
    declarations: [
        MobileCollapsibleContentComponent,
        MobileCollpsibleFilterComponent,
    ],
})
export class AppMobileCollapsibleContentModule {}
