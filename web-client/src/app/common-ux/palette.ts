import { Colour, HslColour } from "./colours";

interface Palette {
    lightRed: Colour;
    darkRed: Colour;
    lightOrange: Colour;
    darkOrange: Colour;
    yellow: Colour;
    lightBlue: Colour;
    darkBlue: Colour;
    lightGreen: Colour;
    darkGreen: Colour;
    lightPurple: Colour;
    darkPurple: Colour;
}

export const pastelPalette = buildPalette({
    lightRed: new HslColour(2, 97, 74),
    darkRed: new HslColour(6, 57, 55),
    lightOrange: new HslColour(29, 98, 80),
    darkOrange: new HslColour(29, 86, 65),
    yellow: new HslColour(40, 89, 72),
    lightBlue: new HslColour(203, 94, 82),
    darkBlue: new HslColour(193, 55, 50),
    lightGreen: new HslColour(124, 38, 78),
    darkGreen: new HslColour(77, 39, 65),
    lightPurple: new HslColour(260, 45, 78),
    darkPurple: new HslColour(280, 51, 46),
});

function buildPalette<T extends Palette>(palette: T) {
    return palette;
}
