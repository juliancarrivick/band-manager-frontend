import { DatePipe } from "@angular/common";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "appDateTime",
    standalone: false,
})
export class DateTimePipe extends DatePipe implements PipeTransform {
    public transform(value: any, timezone?: string, locale?: string): any {
        if (!value) {
            return null;
        }

        return `${super.transform(value, "mediumDate", timezone, locale) ?? ""} at ${
            super.transform(value, "shortTime", timezone, locale) ?? ""
        }`;
    }
}
