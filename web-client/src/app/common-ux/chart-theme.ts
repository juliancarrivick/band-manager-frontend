import { AgChartTheme } from "ag-charts-community";

const materialFontFamily = 'Roboto, "Helvetica Neue", sans-serif';
const materialAxisTitleOptions = {
    fontFamily: materialFontFamily,
    fontWeight: 500,
    fontSize: 16,
} as const;
const materialAxisLabelOptions = {
    fontFamily: materialFontFamily,
    fontSize: 14,
} as const;

export const orchestrateAgChartTheme: AgChartTheme = {
    baseTheme: "ag-default",
    overrides: {
        common: {
            title: {
                fontFamily: materialFontFamily,
                fontWeight: 500,
                fontSize: 20,
            },
            subtitle: {
                fontFamily: materialFontFamily,
                fontSize: 16,
            },
            legend: {
                item: {
                    label: materialAxisLabelOptions,
                },
            },
        },
        bar: {
            axes: {
                category: {
                    title: materialAxisTitleOptions,
                    label: materialAxisLabelOptions,
                },
                number: {
                    title: materialAxisTitleOptions,
                    label: materialAxisLabelOptions,
                },
            },
        },
        pie: {
            series: {
                calloutLabel: materialAxisLabelOptions,
            },
        },
    },
};
