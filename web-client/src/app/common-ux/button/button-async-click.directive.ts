import {
    Directive,
    EventEmitter,
    HostListener,
    Input,
    Optional,
    Output,
    Renderer2,
    ViewContainerRef,
} from "@angular/core";
import { MatButton, MatFabButton, MatIconButton } from "@angular/material/button";
import { getErrorMessage } from "app/common/error";
import { from, ObservableInput } from "rxjs";
import { DialogService } from "../dialog/dialog.service";
import { MatButtonLoadingDirective } from "./button-loading.directive";

@Directive({
    selector: `button[mat-button][asyncClick], button[mat-raised-button][asyncClick], button[mat-icon-button][asyncClick],
               button[mat-fab][asyncClick], button[mat-mini-fab][asyncClick], button[mat-stroked-button][asyncClick],
               button[mat-flat-button][asyncClick]`,
    standalone: false,
})
export class MatButtonAsyncClickDirective<T = unknown> extends MatButtonLoadingDirective {
    @Input() public asyncClick?: (arg?: T) => ObservableInput<unknown>;
    @Input() public asyncClickArg?: T;
    @Output() public isLoading = new EventEmitter<boolean>();

    public constructor(
        @Optional() matButton: MatButton | null,
        @Optional() matIconButton: MatIconButton | null,
        @Optional() matFabButton: MatFabButton | null,
        private dialogService: DialogService,
        viewContainerRef: ViewContainerRef,
        renderer: Renderer2,
    ) {
        super(matButton, matIconButton, matFabButton, viewContainerRef, renderer);
    }

    public get matButtonInstance() {
        return this.matButton;
    }

    @HostListener("click")
    public onClick() {
        if (!this.asyncClick) {
            return;
        }

        this.setLoading(true);

        const subscription = from(this.asyncClick(this.asyncClickArg)).subscribe({
            error: (e) => {
                const message = getErrorMessage(e);
                this.dialogService.danger(message);
                this.setLoading(false);
                subscription.unsubscribe();
            },
            complete: () => this.setLoading(false),
        });
    }

    private setLoading(isLoading: boolean) {
        const previousValue = this.loading;
        this.loading = isLoading;

        if (previousValue === this.loading) {
            return;
        }

        // Bit of a hack, but it works
        this.ngOnChanges({
            loading: {
                currentValue: this.loading,
                previousValue,
                firstChange: false,
                isFirstChange: () => false,
            },
        });
        this.isLoading.emit(isLoading);
    }
}
