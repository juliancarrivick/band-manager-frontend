import { AfterContentInit, Component, ContentChildren, OnDestroy, QueryList } from "@angular/core";
import { MatButton } from "@angular/material/button";
import { forkJoin, Subscription } from "rxjs";
import { startWith, switchMap, tap } from "rxjs/operators";
import { MatButtonAsyncClickDirective } from "./button-async-click.directive";

@Component({
    selector: "app-async-button-group",
    template: `<ng-content></ng-content>`,
    standalone: false,
})
export class AsyncButtonGroupComponent implements AfterContentInit, OnDestroy {
    @ContentChildren(MatButton) public buttonsInGroup?: QueryList<MatButton>;
    @ContentChildren(MatButtonAsyncClickDirective)
    public asyncClickButtons?: QueryList<MatButtonAsyncClickDirective>;

    private initialButtonDisabledStates = new Map<MatButton, boolean>();
    private currentlyClickedAsyncButton?: MatButtonAsyncClickDirective;
    private subscription?: Subscription;

    public ngAfterContentInit() {
        if (!this.buttonsInGroup || !this.asyncClickButtons) {
            throw new Error("ContentChildren should be defined");
        }

        this.subscription = this.asyncClickButtons.changes
            .pipe(
                startWith(this.asyncClickButtons),
                switchMap((asyncButtons: QueryList<MatButtonAsyncClickDirective>) => {
                    return forkJoin(
                        asyncButtons.map((button) => this.handleAsyncClickIsLoading(button)),
                    );
                }),
            )
            .subscribe();
    }

    public ngOnDestroy() {
        this.subscription?.unsubscribe();
    }

    private handleAsyncClickIsLoading(asyncClickButton: MatButtonAsyncClickDirective) {
        return asyncClickButton.isLoading.pipe(
            tap((isLoading) => {
                if (isLoading) {
                    // Store existing state to restore later
                    this.buttonsInGroup?.forEach((button) => {
                        this.initialButtonDisabledStates.set(button, button.disabled);
                    });

                    this.currentlyClickedAsyncButton = asyncClickButton;
                    this.buttonsInGroup
                        ?.filter((button) => button !== asyncClickButton.matButtonInstance)
                        .forEach((button) => (button.disabled = true));
                } else {
                    this.buttonsInGroup
                        ?.filter(
                            (button) =>
                                button !== this.currentlyClickedAsyncButton?.matButtonInstance,
                        )
                        .forEach(
                            (button) =>
                                (button.disabled = !!this.initialButtonDisabledStates.get(button)),
                        );
                    this.currentlyClickedAsyncButton = undefined;
                }
            }),
        );
    }
}
