import {
    ComponentRef,
    Directive,
    EventEmitter,
    Injector,
    Input,
    OnChanges,
    Output,
    Type,
    ViewContainerRef,
} from "@angular/core";

// Ripped off NgComponentOutlet except with create and destroy events
// At some point might add support for inputs and outputs
@Directive({
    selector: "[appRenderComponent]",
    standalone: false,
})
export class AppRenderComponentDirective<T> implements OnChanges {
    @Input() appRenderComponent?: Type<T>;
    @Input() appRenderComponentInjector?: Injector;
    @Input() appRenderComponentContent?: any[][];

    @Output() created = new EventEmitter<T>();
    @Output() destroying = new EventEmitter<T>();

    private _componentRef?: ComponentRef<T>;

    constructor(private _viewContainerRef: ViewContainerRef) {}

    ngOnChanges(): void {
        this._destroyComponent();

        if (this.appRenderComponent) {
            const elInjector = this.appRenderComponentInjector ?? this._viewContainerRef.injector;

            this._componentRef = this._viewContainerRef.createComponent(this.appRenderComponent, {
                index: this._viewContainerRef.length,
                injector: elInjector,
                projectableNodes: this.appRenderComponentContent,
            });
            this.created.emit(this._componentRef.instance);
        }
    }

    private _destroyComponent(): void {
        if (this._componentRef) {
            this.destroying.emit(this._componentRef.instance);
            this._viewContainerRef.clear();
            this._componentRef = undefined;
        }
    }
}
