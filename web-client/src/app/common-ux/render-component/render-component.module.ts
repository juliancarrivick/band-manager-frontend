import { NgModule } from "@angular/core";
import { AppRenderComponentDirective } from "./render-component.directive";

@NgModule({
    exports: [
        AppRenderComponentDirective,
    ],
    declarations: [
        AppRenderComponentDirective,
    ],
})
export class AppRenderComponentModule {}
