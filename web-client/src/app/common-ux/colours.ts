export interface Colour {
    toCssString(): string;
}

export class RgbColour implements Colour {
    #red = 0;
    #green = 0;
    #blue = 0;

    public constructor(red: number, green: number, blue: number) {
        this.#red = this.sanitizeComponent(red);
        this.#green = this.sanitizeComponent(green);
        this.#blue = this.sanitizeComponent(blue);
    }

    public get red() {
        return this.#red;
    }
    public get green() {
        return this.#green;
    }
    public get blue() {
        return this.#blue;
    }

    public toCssString() {
        return `rgb(${this.red}, ${this.green}, ${this.blue})`;
    }

    private sanitizeComponent(componentColour: number) {
        if (componentColour < 0) {
            return 0;
        } else if (componentColour > 255) {
            return 255;
        } else {
            return componentColour;
        }
    }
}

export class HslColour implements Colour {
    #hue = 0;
    #saturation = 0;
    #lightness = 0;

    public constructor(hue: number, saturation: number, lightness: number) {
        if (hue < 0) {
            this.#hue = 0;
        } else if (hue > 360) {
            this.#hue = 360;
        } else {
            this.#hue = hue;
        }

        if (saturation < 0) {
            this.#saturation = 0;
        } else if (saturation > 100) {
            this.#saturation = 100;
        } else {
            this.#saturation = saturation;
        }

        if (lightness < 0) {
            this.#lightness = 0;
        } else if (lightness > 100) {
            this.#lightness = 100;
        } else {
            this.#lightness = lightness;
        }
    }

    public get hue() {
        return this.#hue;
    }
    public get saturation() {
        return this.#saturation;
    }
    public get lightness() {
        return this.#lightness;
    }

    public addSaturation(saturation: number) {
        return new HslColour(this.hue, this.saturation + saturation, this.lightness);
    }

    public addLightness(lightness: number) {
        return new HslColour(this.hue, this.saturation, this.lightness + lightness);
    }

    // Adapted from https://stackoverflow.com/a/9493060
    public toRgbColour() {
        const normalisedHue = this.hue / 360;
        const normalisedSaturation = this.saturation / 100;
        const normalisedLightness = this.lightness / 100;

        let r = 0;
        let g = 0;
        let b = 0;

        if (normalisedSaturation === 0) {
            r = g = b = normalisedLightness; // achromatic
        } else {
            const q =
                normalisedLightness < 0.5
                    ? normalisedLightness * (1 + normalisedSaturation)
                    : normalisedLightness +
                      normalisedSaturation -
                      normalisedLightness * normalisedSaturation;
            const p = 2 * normalisedLightness - q;

            r = hue2rgb(p, q, normalisedHue + 1 / 3);
            g = hue2rgb(p, q, normalisedHue);
            b = hue2rgb(p, q, normalisedHue - 1 / 3);
        }

        return new RgbColour(Math.round(r * 255), Math.round(g * 255), Math.round(b * 255));
    }

    public toCssString() {
        return `hsl(${this.hue}, ${this.saturation}%, ${this.lightness}%)`;
    }
}

function hue2rgb(p: number, q: number, t: number) {
    if (t < 0) {
        t += 1;
    }
    if (t > 1) {
        t -= 1;
    }
    if (t < 1 / 6) {
        return p + (q - p) * 6 * t;
    }
    if (t < 1 / 2) {
        return q;
    }
    if (t < 2 / 3) {
        return p + (q - p) * (2 / 3 - t) * 6;
    }
    return p;
}
