import { NgModule } from "@angular/core";
import { AppIconTextPairModule } from "../icon-text-pair/icon-text-pair.module";
import { BadgeComponent } from "./badge.component";

@NgModule({
    imports: [
        AppIconTextPairModule,
    ],
    exports: [
        AppIconTextPairModule,
        BadgeComponent,
    ],
    declarations: [
        BadgeComponent,
    ],
})
export class AppBadgeModule {}
