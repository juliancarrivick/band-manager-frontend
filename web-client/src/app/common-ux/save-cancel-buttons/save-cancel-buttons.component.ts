import { Component, ElementRef, EventEmitter, Input, Optional, Output } from "@angular/core";
import { UntypedFormGroup } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { voidFunction } from "app/common/utils";
import { BreezeService } from "app/dal/breeze.service";
import { EntityTracker } from "app/dal/entity-tracker";
import { EntityManager } from "breeze-client";
import { Observable, ObservableInput, of, throwError } from "rxjs";
import { catchError, finalize, switchMap, tap } from "rxjs/operators";
import { invalidControlsHaveBeenTouched } from "../form/form-utilities";

@Component({
    selector: "app-save-cancel-buttons,app-save-revert-buttons",
    template: `
        <app-async-button-group>
            <button
                [disabled]="saveDisabled()"
                mat-raised-button
                [mobileFriendly]="mobileFriendlyButtons"
                color="primary"
                [asyncClick]="saveChanges"
            >
                <app-icon-text-pair
                    icon="save"
                    [text]="saveText"
                    [isCollapsible]="mobileFriendlyButtons"
                ></app-icon-text-pair>
            </button>
            <button
                mat-button
                [mobileFriendly]="mobileFriendlyButtons"
                [disabled]="!alwaysAllowCancel && !hasChanges()"
                [asyncClick]="cancelChanges"
            >
                <app-icon-text-pair
                    [icon]="cancelButton.icon"
                    [text]="cancelButton.text"
                    [isCollapsible]="mobileFriendlyButtons"
                ></app-icon-text-pair>
            </button>
        </app-async-button-group>
    `,
    standalone: false,
})
export class SaveCancelButtonsComponent {
    @Input() public form?: UntypedFormGroup;
    @Input() public entityTracker?: EntityTracker;
    @Input() public postSaveAction?: () => ObservableInput<unknown>;
    @Output() public save = new EventEmitter<void>();
    // eslint-disable-next-line @angular-eslint/no-output-native
    @Output() public cancel = new EventEmitter<void>();
    // eslint-disable-next-line @angular-eslint/no-output-native
    @Output() public error = new EventEmitter<Error | undefined>();

    @Input() public saveText = "Save";

    public alwaysAllowCancel: boolean;
    public cancelButton: { text: string; icon: string };
    public mobileFriendlyButtons: boolean;

    private entityManager?: EntityManager;

    public constructor(
        elementRef: ElementRef<HTMLElement>,
        private breezeService: BreezeService,
        @Optional() private dialog: MatDialogRef<any> | null,
    ) {
        this.alwaysAllowCancel = elementRef.nativeElement.tagName === "APP-SAVE-CANCEL-BUTTONS";
        this.cancelButton = this.alwaysAllowCancel
            ? { text: "Cancel", icon: "cancel" }
            : { text: "Revert changes", icon: "undo" };

        // TODO bit of a hack, really breezeService should expose an observable for the
        // methods used below
        breezeService.entityManager$.subscribe((e) => {
            this.entityManager = e;
        });

        this.mobileFriendlyButtons = !dialog;
    }

    public saveDisabled() {
        if (!this.form) {
            return !this.hasChanges() || this.hasValidationErrors();
        } else if (this.form.invalid && !invalidControlsHaveBeenTouched(this.form)) {
            return false;
        } else {
            return (
                this.form.invalid ||
                this.form.pending ||
                !this.hasChanges() ||
                this.hasValidationErrors()
            );
        }
    }

    public hasChanges() {
        return this.entityTracker
            ? this.entityTracker.hasChanges()
            : !!this.entityManager?.hasChanges();
    }

    public hasValidationErrors() {
        return this.entityTracker
            ? this.entityTracker.hasValidationErrors()
            : !!this.entityManager?.getChanges().some((e) => e.entityAspect.hasValidationErrors);
    }

    public saveChanges = () => {
        this.error.emit();
        const dialogCloseRestorer = this.dialogCloseStateRestorer();

        if (this.form && !invalidControlsHaveBeenTouched(this.form)) {
            this.form.markAllAsTouched();

            if (this.form.invalid) {
                dialogCloseRestorer();
                return of(undefined);
            }
        }

        const result$ = this.entityTracker
            ? this.entityTracker.saveChanges()
            : this.breezeService.saveChanges();
        return result$.pipe(
            switchMap(() => this.postSaveAction?.() ?? of(undefined)),
            tap(() => this.save.emit()),
            catchError(this.handleError),
            finalize(dialogCloseRestorer),
        );
    };

    public cancelChanges = () => {
        this.error.emit();
        const dialogCloseRestorer = this.dialogCloseStateRestorer();

        let result$: Observable<void>;
        if (this.hasChanges()) {
            result$ = this.entityTracker
                ? this.entityTracker.cancelChanges()
                : this.breezeService.cancelChanges();
        } else {
            result$ = of(undefined);
        }

        return result$.pipe(
            tap(() => this.cancel.emit()),
            catchError(this.handleError),
            finalize(dialogCloseRestorer),
        );
    };

    private handleError = (e: any) => {
        if (e instanceof Error) {
            this.error.emit(e);
            return of(undefined);
        } else {
            return throwError(() => new Error(e));
        }
    };

    /** Prevents the current dialog from closing and returns a function which restores the previous behaviour */
    private dialogCloseStateRestorer() {
        if (!this.dialog) {
            return voidFunction;
        }

        const initialDisableCloseState = this.dialog.disableClose;
        this.dialog.disableClose = true;

        return () => {
            this.dialog!.disableClose = initialDisableCloseState;
        };
    }
}
