import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Optional,
    Output,
} from "@angular/core";
import { AbstractControl, FormGroup } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { voidFunction } from "app/common/utils";
import {
    ValidationResultError,
    setValidationResultsOnFormGroup,
} from "app/common/validation-result";
import { ObservableInput, from, of, throwError } from "rxjs";
import { catchError, finalize, tap } from "rxjs/operators";
import { invalidControlsHaveBeenTouched } from "../form/form-utilities";

@Component({
    selector: "app-save-cancel-form-buttons,app-save-revert-form-buttons",
    template: `
        <app-async-button-group>
            <button
                [disabled]="saveDisabled()"
                mat-raised-button
                [mobileFriendly]="mobileFriendlyButtons"
                color="primary"
                [asyncClick]="saveChanges"
            >
                <app-icon-text-pair
                    icon="save"
                    [text]="saveText"
                    [isCollapsible]="mobileFriendlyButtons"
                ></app-icon-text-pair>
            </button>
            <button
                mat-button
                [mobileFriendly]="mobileFriendlyButtons"
                [disabled]="!alwaysAllowCancel && form!.pristine"
                (click)="cancelChanges()"
            >
                <app-icon-text-pair
                    [icon]="cancelButton.icon"
                    [text]="cancelButton.text"
                    [isCollapsible]="mobileFriendlyButtons"
                ></app-icon-text-pair>
            </button>
        </app-async-button-group>
    `,
    standalone: false,
})
export class SaveCancelFormButtonsComponent<T extends { [K in keyof T]: AbstractControl<any, any> }>
    implements OnInit
{
    @Input() public form?: FormGroup<T>;
    @Input() public doSave?: (form: FormGroup<T>) => ObservableInput<unknown>;
    @Output() public save = new EventEmitter<void>();
    // eslint-disable-next-line @angular-eslint/no-output-native
    @Output() public cancel = new EventEmitter<void>();
    // eslint-disable-next-line @angular-eslint/no-output-native
    @Output() public error = new EventEmitter<Error | undefined>();

    @Input() public saveText = "Save";

    public alwaysAllowCancel: boolean;
    public cancelButton: { text: string; icon: string };
    public mobileFriendlyButtons: boolean;

    public constructor(
        elementRef: ElementRef<HTMLElement>,
        @Optional() private dialog: MatDialogRef<any> | null,
    ) {
        this.alwaysAllowCancel =
            elementRef.nativeElement.tagName === "APP-SAVE-CANCEL-FORM-BUTTONS";
        this.cancelButton = this.alwaysAllowCancel
            ? { text: "Cancel", icon: "cancel" }
            : { text: "Revert changes", icon: "undo" };

        this.mobileFriendlyButtons = !dialog;
    }

    public ngOnInit() {
        if (!this.form) {
            throw new Error("Form must be provided");
        }
        if (!this.doSave) {
            throw new Error("doSave must be provided");
        }
    }

    public saveDisabled() {
        if (this.form?.invalid && !invalidControlsHaveBeenTouched(this.form)) {
            return false;
        }

        return this.form!.pristine || this.form!.invalid || this.form!.pending;
    }

    public saveChanges = () => {
        this.error.emit();
        const dialogCloseRestorer = this.dialogCloseStateRestorer();

        if (this.form && !invalidControlsHaveBeenTouched(this.form)) {
            this.form.markAllAsTouched();

            if (this.form.invalid) {
                dialogCloseRestorer();
                return of(undefined);
            }
        }

        this.form!.disable();
        return from(this.doSave!(this.form!)).pipe(
            // form enable in tap & catchError so we are sure that the form is enabled prior
            // to any errors being set on the form (otherwise they won't be displayed)
            tap(() => {
                this.form!.enable();
                this.save.emit();
            }),
            catchError((e) => {
                this.form!.enable();
                return this.handleError(e);
            }),
            finalize(() => {
                dialogCloseRestorer();
            }),
        );
    };

    public cancelChanges = () => {
        this.error.emit();
        this.form?.reset();
        this.cancel.emit();
    };

    private handleError = (e: any) => {
        if (e instanceof ValidationResultError) {
            setValidationResultsOnFormGroup(e.validationResults as any, this.form as any);
            if (!invalidControlsHaveBeenTouched(this.form!)) {
                this.form?.markAllAsTouched();
            }
            return of(undefined);
        } else if (e instanceof Error) {
            this.error.emit(e);
            return of(undefined);
        } else {
            return throwError(() => e as unknown);
        }
    };

    /** Prevents the current dialog from closing and returns a function which restores the previous behaviour */
    private dialogCloseStateRestorer() {
        if (!this.dialog) {
            return voidFunction;
        }

        const initialDisableCloseState = this.dialog.disableClose;
        this.dialog.disableClose = true;

        return () => {
            this.dialog!.disableClose = initialDisableCloseState;
        };
    }
}
