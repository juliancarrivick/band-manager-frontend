import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { MatChipListboxChange, MatChipListbox } from "@angular/material/chips";

export interface Chip<T> {
    text: string;
    icon: string;
    tooltip?: string;
    value: T;
}

@Component({
    selector: "app-chip-selection",
    template: `
        <mat-chip-listbox
            [selectable]="true"
            [multiple]="multiple"
            (change)="emitSelectionChange($event)"
        >
            <mat-chip-option
                *ngFor="let chip of chips"
                [selected]="selections.has(chip.value)"
                (click)="blurElement($event)"
                [matTooltip]="chip.tooltip ?? ''"
                [value]="chip.value"
            >
                <div class="chip-container">
                    <mat-icon>{{ chip.icon }}</mat-icon>
                    {{ chip.text }}
                </div>
            </mat-chip-option>
        </mat-chip-listbox>
    `,
    styles: [
        `
            mat-chip-option {
                font-size: 16px;
                cursor: pointer;
            }

            .chip-container {
                display: inline-flex;
                align-items: center;
                padding-top: 0.25rem;
            }

            mat-icon {
                margin-right: 0.25rem;
            }
        `,
    ],
    standalone: false,
})
export class ChipSelectionComponent<T> implements OnChanges {
    @Input() public chips: Chip<T>[] = [];
    @Input() public multiple = false;
    @Input() public selection: T[] = [];
    @Output() public selectionChange = new EventEmitter<T[]>();

    public selections = new Set<T>();

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.chips) {
            this.selections = new Set<T>();
        }
        if (changes.selection) {
            for (const s of this.selection) {
                this.selections.add(s);
            }
        }
    }

    public emitSelectionChange(event: MatChipListboxChange) {
        const selections = this.getSelections(event.source).map((chip) => chip.value as T);

        // Spurious duplicate events occur, filter these out
        if (
            this.selections.size === selections.length &&
            selections.every((s) => this.selections.has(s))
        ) {
            return;
        }

        this.selections = new Set<T>(selections);
        this.selectionChange.emit(selections);
    }

    private getSelections(listbox: MatChipListbox) {
        const selected = listbox.selected;
        if (Array.isArray(selected)) {
            return selected;
        } else if (typeof selected !== "undefined") {
            return [selected];
        } else {
            return [];
        }
    }

    public blurElement(event: MouseEvent) {
        if (!(event.target instanceof HTMLElement)) {
            return;
        }

        // Make a best effort to blur, since this is liable to change version-to-version of Angular
        let focussedElement = event.target;
        for (let i = 0; i < 5; i++) {
            if (focussedElement === document.activeElement) {
                focussedElement.blur();
                break;
            }

            if (focussedElement.parentElement === null) {
                break;
            }

            focussedElement = focussedElement.parentElement;
        }
    }
}
