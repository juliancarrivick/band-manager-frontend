import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatChipsModule } from "@angular/material/chips";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ChipSelectionComponent } from "./chip-selection.component";

@NgModule({
    imports: [
        CommonModule,
        MatChipsModule,
        MatIconModule,
        MatTooltipModule,
    ],
    exports: [
        ChipSelectionComponent,
    ],
    declarations: [
        ChipSelectionComponent,
    ],
})
export class AppChipSelectionModule {}
