import { Directive, HostBinding, HostListener, Input, OnDestroy } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router, UrlTree } from "@angular/router";
import { buildPrimaryUrlTree, helpOutletName } from "app/common/routing-utils";
import { combineLatest, filter, map, ReplaySubject, startWith, Subscription } from "rxjs";

@Directive({
    standalone: true,
    selector: "[appPrimaryRouterLink]",
})
export class PrimaryRouterLinkDirective implements OnDestroy {
    @Input("appPrimaryRouterLink") public set primaryCommands(commands: unknown[]) {
        this.primaryCommands$.next(commands);
    }
    private primaryCommands$ = new ReplaySubject<unknown[]>(1);

    @HostBinding("href") public href?: string;
    private urlTree?: UrlTree;

    private subscription: Subscription;

    public constructor(
        private router: Router,
        route: ActivatedRoute,
    ) {
        const otherNavigations = this.router.events.pipe(
            filter((e): e is NavigationEnd => e instanceof NavigationEnd),
            map(() => route),
            startWith(route),
            filter((r) => r.outlet !== helpOutletName),
        );
        this.subscription = combineLatest([
            otherNavigations,
            this.primaryCommands$,
        ]).subscribe(([r, commands]) => {
            this.urlTree = buildPrimaryUrlTree(r.snapshot, commands);
            this.href = this.router.serializeUrl(this.urlTree);
        });
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    @HostListener("click", ["$event"])
    public async navigate(event: Event) {
        if (!this.urlTree) {
            return;
        }

        event.preventDefault();
        await this.router.navigateByUrl(this.urlTree);
    }
}
