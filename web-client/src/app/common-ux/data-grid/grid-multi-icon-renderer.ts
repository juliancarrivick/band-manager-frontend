import { ICellRendererComp, ICellRendererParams } from "ag-grid-community";

export interface IconDetails {
    icon: string;
    tooltip: string;
}

export abstract class GridMultiIconRenderer<T> implements ICellRendererComp {
    private gui: HTMLDivElement;

    public constructor() {
        this.gui = document.createElement("div");
    }

    public init(params: ICellRendererParams): void {
        this.updateData(params.data);
    }

    public refresh(params: ICellRendererParams): boolean {
        this.updateData(params.data);
        return true;
    }

    public getGui() {
        return this.gui;
    }

    private updateData(data: T) {
        this.gui.innerHTML = "";

        const icons = this.getIcons(data).map((i) => this.buildIcon(i));
        this.gui.append(...icons);
    }

    private buildIcon(icon: IconDetails) {
        const span = document.createElement("span");
        span.classList.add("material-icons");
        span.style.verticalAlign = "bottom";
        span.style.lineHeight = "inherit";
        span.style.opacity = "0.6";
        span.style.cursor = "help";

        span.innerText = icon.icon;
        span.title = icon.tooltip;

        return span;
    }

    protected abstract getIcons(data: T): IconDetails[];
}
