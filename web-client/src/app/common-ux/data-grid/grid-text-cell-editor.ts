import { Component, ElementRef, ViewChild } from "@angular/core";
import { ICellEditorAngularComp } from "ag-grid-angular";
import { ICellEditorParams } from "ag-grid-community";

@Component({
    template: `
        <mat-form-field subscriptSizing="dynamic">
            <mat-label *ngIf="columnName">{{ columnName }}</mat-label>
            <input
                #input
                matInput
                type="text"
                [(ngModel)]="value"
            />
        </mat-form-field>
    `,
    styles: [
        `
            :host {
                width: 100%;
            }

            mat-form-field {
                width: 100%;
            }
        `,
    ],
    standalone: false,
})
export class GridTextCellEditorComponent implements ICellEditorAngularComp {
    @ViewChild("input") public inputElement?: ElementRef<HTMLInputElement>;
    public columnName?: string;
    public value: string | null | undefined = "";

    public agInit(params: ICellEditorParams<any, string>) {
        this.columnName = params.api.getDisplayNameForColumn(params.column, null);
        this.value = params.value;
    }

    public getValue() {
        return this.value;
    }

    public focusIn(): void {
        this.inputElement?.nativeElement.focus();
    }
}
