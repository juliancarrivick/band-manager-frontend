import { NgZone } from "@angular/core";
import { ColDef, GridApi, ICellRendererComp, ICellRendererParams } from "ag-grid-community";
import { ObservableInput, from } from "rxjs";
import { DataGridComponent } from "./data-grid.component";
import { columnExportColDef } from "./grid-export-options";

export interface ColumnAction<T> {
    icon: string | ((data: T) => string);
    tooltip: string | ((data: T) => string);
    customiseElement?: (button: HTMLButtonElement, data: T) => void;
    onClick: (data: T) => ObservableInput<unknown> | void;
}

export function buildEditColumn<T>(
    dataGrid: DataGridComponent<T>,
    additionalActions: ColumnAction<T>[] = [],
): ColDef<T> {
    const numDefaultActions = (dataGrid.canEditRow ? 1 : 0) + (dataGrid.canDeleteRow ? 1 : 0);
    const numActions = numDefaultActions + additionalActions.length;
    let effectiveActionsForWidth = numActions;
    if (effectiveActionsForWidth < 2) {
        effectiveActionsForWidth = 2;
    }
    dataGrid.agGrid?.gridReady.subscribe((e) => {
        e.api.setColumnsVisible(["actionsColumn"], numActions > 0);
    });

    return {
        colId: "actionsColumn",
        headerName: "Actions",
        pinned: "right",
        filter: false,
        sortable: false,
        resizable: false,
        cellRenderer: GridEditColumnRenderer,
        cellStyle: {
            "padding-left": "4px",
            "padding-right": "4px",
            "text-overflow": "clip",
        },
        width: effectiveActionsForWidth * 48 + 8,
        cellRendererParams: {
            dataGrid,
            actions: additionalActions,
        } as EditColumnRendererParams<T>,
        ...columnExportColDef({ skipColumn: true }),
    };
}

interface EditColumnRendererParams<T> {
    dataGrid: DataGridComponent<T>;
    actions: ColumnAction<T>[];
}

class GridEditColumnRenderer<T> implements ICellRendererComp {
    private gui!: HTMLDivElement;
    private data?: T;

    public init(params: ICellRendererParams<T> & EditColumnRendererParams<T>) {
        this.data = params.data;
        const dataGrid: DataGridComponent<T> = params.dataGrid;

        this.gui = document.createElement("div");
        this.gui.style.position = "absolute";
        this.gui.style.top = "-6px";

        const actions: ColumnAction<T>[] = params.actions;
        for (const action of actions) {
            const button = this.createButton(action, dataGrid.ngZone, params.api);
            this.gui.append(button);
        }

        // Can't put this in the ngOnInit above as canEditRow and canDeleteRow won't be initialised yet
        if (dataGrid.canEditRow) {
            const button = this.createButton(
                {
                    icon: "edit",
                    tooltip: "Edit",
                    onClick: (d) => dataGrid.editRow(d),
                },
                dataGrid.ngZone,
            );
            this.gui.append(button);
        }
        if (dataGrid.canDeleteRow) {
            const button = this.createButton(
                {
                    icon: "delete",
                    tooltip: "Delete",
                    onClick: (d) => dataGrid.deleteRow(d),
                },
                dataGrid.ngZone,
            );
            this.gui.append(button);
        }
    }

    private createButton(action: ColumnAction<T>, ngZone: NgZone, api?: GridApi<T>) {
        const button = document.createElement("button");
        button.classList.add("mdc-icon-button", "mat-mdc-icon-button");
        button.title = this.valueOrDefault(action.tooltip);
        button.setAttribute("aria-label", button.title);

        const icon = document.createElement("span");
        icon.classList.add("material-icons");
        icon.style.verticalAlign = "middle";
        icon.innerText = this.valueOrDefault(action.icon);
        button.append(icon);

        button.addEventListener("click", (event: MouseEvent) => {
            if (this.data) {
                // Prevent grid selection from being toggled when we click a button
                event.stopPropagation();

                const ret = ngZone.run(() => action.onClick(this.data!));
                if (api && ret) {
                    from(ret).subscribe(() => {
                        api.applyTransaction({ update: [this.data!] });
                    });
                }
            }
        });

        if (action.customiseElement && this.data) {
            action.customiseElement(button, this.data);
        }

        return button;
    }

    private valueOrDefault(value: string | ((data: T) => string), defaultValue = "") {
        if (typeof value === "string") {
            return value;
        } else if (typeof value === "function") {
            return typeof this.data === "undefined" ? defaultValue : value(this.data);
        } else {
            return defaultValue;
        }
    }

    public refresh(params: ICellRendererParams) {
        // Return true, to do nothing, when some other row has changed
        // If this row has changed, refresh the buttons
        return params.data !== this.data;
    }

    public getGui(): HTMLElement {
        return this.gui;
    }
}
