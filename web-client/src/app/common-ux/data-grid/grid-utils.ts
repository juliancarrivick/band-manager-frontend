import {
    AgEvent,
    AgEventListener,
    AgEventTypeParams,
    AgPublicEventType,
    GridApi,
} from "ag-grid-community";
import { Observable } from "rxjs";

export function fromGridEvent<TEventType extends AgPublicEventType, TData = any>(
    agGridObject: { api: GridApi<TData> },
    eventName: TEventType,
) {
    return new Observable<AgEventTypeParams<TData>[TEventType]>((subscriber) => {
        const listener: AgEventListener<TData, any, TEventType> = (e) => subscriber.next(e);
        agGridObject.api.addEventListener<TEventType>(eventName, listener);
        return () => agGridObject.api.removeEventListener(eventName, listener);
    });
}

export function fromCustomGridEvent<T extends AgEvent>(
    agGridObject: { api: GridApi },
    eventName: string,
) {
    const forcedEventType = eventName as AgPublicEventType;
    return new Observable<T>((subscriber) => {
        const listener = (e: T) => subscriber.next(e);
        const forcedListener = listener as unknown as AgEventListener<any, any, AgPublicEventType>;
        agGridObject.api.addEventListener(forcedEventType, forcedListener);
        return () => agGridObject.api.removeEventListener(forcedEventType, forcedListener);
    });
}
