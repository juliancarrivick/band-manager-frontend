import { ICellRendererComp, ICellRendererParams } from "ag-grid-community";

export class GridEmailLinkRenderer<T> implements ICellRendererComp<T> {
    private gui: HTMLAnchorElement;

    public constructor() {
        this.gui = document.createElement("a");
        this.gui.classList.add("stealth-link");
    }

    public getGui() {
        return this.gui;
    }

    public init(params: ICellRendererParams<T, string>) {
        this.updateAnchor(params.value);
    }

    public refresh(params: ICellRendererParams<T, string>): boolean {
        this.updateAnchor(params.value);
        return true;
    }

    private updateAnchor(email: unknown) {
        if (typeof email === "string") {
            this.gui.href = `mailto:${email}`;
            this.gui.innerHTML = email;
        } else {
            this.gui.href = "";
            this.gui.innerHTML = "";
        }
    }
}
