import { Directive, EventEmitter, Input, OnChanges, OnDestroy, Output } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { Subscription } from "rxjs";

@Directive({
    selector: "ag-grid-angular[appGridSingleSelection]",
    standalone: false,
})
export class GridSingleSelectionDirective<T> implements OnChanges, OnDestroy {
    @Input("appGridSingleSelection") public selected?: T;
    // eslint-disable-next-line @angular-eslint/no-output-rename
    @Output("appGridSingleSelectionChange") public selectedChange = new EventEmitter<
        T | undefined
    >();

    private subscription: Subscription;
    private isReady = false;

    public constructor(private agGrid: AgGridAngular) {
        agGrid.rowSelection = {
            mode: "singleRow",
            enableClickSelection: true,
        };

        agGrid.firstDataRendered.subscribe(() => {
            this.isReady = true;
            this.selectRow();
        });
        this.subscription = agGrid.selectionChanged.subscribe(() => {
            const selection = agGrid.api.getSelectedRows();
            this.selectedChange.emit(selection[0]);
        });
    }

    public ngOnChanges(): void {
        if (!this.isReady) {
            return;
        }

        this.selectRow();
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private selectRow() {
        if (this.selected) {
            this.agGrid.api.forEachNode((n) => {
                n.setSelected(n.data === this.selected);
            });
        } else {
            this.agGrid.api.deselectAll();
        }
    }
}
