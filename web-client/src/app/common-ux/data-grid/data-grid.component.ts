import { formatDate } from "@angular/common";
import {
    AfterViewInit,
    Component,
    ContentChild,
    EventEmitter,
    Input,
    NgZone,
    OnDestroy,
    Optional,
    Output,
    TemplateRef,
    ViewChild,
} from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import {
    AllCommunityModule,
    Column,
    IRowNode,
    ModuleRegistry,
    provideGlobalGridOptions,
} from "ag-grid-community";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { PageWrapperComponent } from "app/common-ux/page-wrapper/page-wrapper.component";
import { BehaviorSubject, ObservableInput, from, of } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { getColumnExportOptions } from "./grid-export-options";

import "ag-grid-community/styles/ag-grid.min.css";
import "ag-grid-community/styles/ag-theme-material.min.css";

ModuleRegistry.registerModules([AllCommunityModule]);
provideGlobalGridOptions({
    theme: "legacy",
});

@Component({
    selector: "app-data-grid",
    templateUrl: "./data-grid.component.html",
    styleUrls: ["./data-grid.component.scss"],
    standalone: false,
})
export class DataGridComponent<T> implements OnDestroy, AfterViewInit {
    @Input() public titleText?: string;
    @Input() public allowAdding: boolean | null = false;
    // eslint-disable-next-line @angular-eslint/no-input-rename
    @Input("addRow") public doAddRow?: () => ObservableInput<T>;
    @Input() public allowEditing: boolean | null = false;
    // eslint-disable-next-line @angular-eslint/no-input-rename
    @Input("editRow") public doEditRow?: (row: T) => ObservableInput<T>;
    @Input() public allowDeleting: boolean | null = false;
    // eslint-disable-next-line @angular-eslint/no-input-rename
    @Input("deleteRow") public doDeleteRow?: (row: T) => ObservableInput<void>;
    @Input() public displayActionsInToolbar = false;
    @Input() public showFilter = true;
    @Input() public collapseFilter = false;
    @Input() public allowExportWithFilename?: string;
    @Output() public selected = new EventEmitter<T>();

    @Input() public additionalGridActions?: TemplateRef<unknown>;

    @ContentChild(AgGridAngular) agGrid?: AgGridAngular<T>;
    @ViewChild("gridActions", { read: TemplateRef })
    public gridActionsTemplate?: TemplateRef<unknown>;

    public agGridContext = this;
    public filterValue$ = new BehaviorSubject<string | undefined>(undefined);
    public filterValue?: string;
    public isLoading$ = of(true);

    public constructor(
        public ngZone: NgZone,
        private dialogService: DialogService,
        @Optional() private pageWrapperComponent?: PageWrapperComponent,
    ) {
        this.filterValue$.pipe(debounceTime(200)).subscribe((filterValue) => {
            this.filterValue = filterValue;
            this.agGrid?.api.onFilterChanged();
        });
    }

    public ngOnDestroy() {
        this.filterValue$.complete();
        this.pageWrapperComponent?.removePageActionsTemplate();
    }

    public ngAfterViewInit() {
        if (this.displayActionsInToolbar && this.gridActionsTemplate) {
            this.pageWrapperComponent?.setPageActionsTemplate(this.gridActionsTemplate);
        }
    }

    public isExternalFilterPresent = () => {
        return !!this.filterValue;
    };

    public applyFilter(filterText: string) {
        this.filterValue$.next(filterText.toLowerCase());
    }

    public filterRow = (node: IRowNode<T>) => {
        if (!this.filterValue) {
            return true;
        }

        const columns = this.agGrid?.api.getColumns() ?? [];
        const values = columns.map((c) => String(this.getColumnFormattedValue(c, node)));
        const serialisedRow = values.join("\t").toLocaleLowerCase();
        return serialisedRow.includes(this.filterValue);
    };

    private getColumnFormattedValue(c: Column, node: IRowNode<T>) {
        if (!this.agGrid) {
            return "";
        }

        let value = this.agGrid.api.getCellValue({
            colKey: c,
            rowNode: node,
        });

        const formatter = c.getColDef().valueFormatter;
        if (typeof formatter === "function") {
            value = formatter({
                node,
                data: node.data,
                colDef: c.getColDef(),
                column: c,
                api: this.agGrid.api,
                context: this.agGrid.context,
                value,
            });
        }

        return String(value);
    }

    public get canAddRow() {
        return !!this.allowAdding && !!this.doAddRow;
    }

    public addRow() {
        from(this.doAddRow!()).subscribe((row) => {
            this.agGrid?.api.applyTransaction({ add: [row] });
        });
    }

    public get canEditRow() {
        return !!this.allowEditing && !!this.doEditRow;
    }

    public editRow(row: T) {
        from(this.doEditRow!(row)).subscribe(() => {
            this.agGrid?.api.applyTransaction({ update: [row] });
        });
    }

    public get canDeleteRow() {
        return !!this.allowDeleting && !!this.doDeleteRow;
    }

    public deleteRow(row: T) {
        // TODO Could catch error from deletion
        this.dialogService
            .openConfirmation("Are you sure you want to delete this record?", () =>
                this.doDeleteRow!(row),
            )
            .subscribe(() => {
                this.agGrid?.api.applyTransaction({ remove: [row] });
            });
    }

    public exportGrid(name?: string) {
        if (!this.agGrid) {
            return;
        }

        const columnsToExport = this.agGrid.api
            .getColumns()
            ?.filter((c) => !getColumnExportOptions(c.getColDef())?.skipColumn)
            .map((c: Column) => c.getColId());
        if (!columnsToExport) {
            // TODO Log
            return;
        }

        this.agGrid.api.exportDataAsCsv({
            fileName: name ?? this.allowExportWithFilename,
            columnKeys: columnsToExport,
            processHeaderCallback: (params) => {
                const exportOptions = getColumnExportOptions(params.column.getColDef());
                if (exportOptions?.processHeaderCallback) {
                    return exportOptions.processHeaderCallback(params);
                } else {
                    return params.api.getDisplayNameForColumn(params.column, null);
                }
            },
            processCellCallback: (params) => {
                const exportOptions = getColumnExportOptions(params.column.getColDef());
                if (exportOptions?.preprocessCellValue) {
                    return exportOptions.preprocessCellValue(params);
                } else if (params.value instanceof Date) {
                    // Format like this so excel picks up the data type correctly
                    return formatDate(params.value, "yyyy-MM-dd", navigator.language);
                } else {
                    return typeof params.value === "undefined" || params.value === null
                        ? ""
                        : String(params.value);
                }
            },
        });
    }
}
