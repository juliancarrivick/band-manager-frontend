import { FormGroup } from "@angular/forms";

export function allControlsTouched(form: FormGroup) {
    const controls = Object.values(form.controls);
    return controls.every((c) => c.touched);
}

export function invalidControlsHaveBeenTouched(form: FormGroup) {
    const controls = Object.values(form.controls);
    return controls.filter((c) => c.invalid).every((c) => c.touched);
}

export function modifiedValues(form: FormGroup) {
    const values: Record<string, unknown> = {};

    for (const [name, control] of Object.entries(form.controls)) {
        if (control.dirty) {
            values[name] = control.value;
        }
    }

    return values;
}
