import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    Output,
    SimpleChanges,
    TemplateRef,
} from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import { BehaviorSubject, Observable, combineLatest, map, startWith } from "rxjs";

@Component({
    selector: "app-tag-box",
    templateUrl: "./tag-box.component.html",
    styles: [
        `
            mat-form-field {
                width: 100%;
            }
        `,
    ],
    standalone: false,
})
export class TagBoxComponent<T> implements OnChanges {
    @Input() public label?: string;
    @Input() public hint?: string;
    @Input() public selectedValues: T[] | null = [];
    @Input() public allValues: T[] | null = [];
    @Input() public isEqual: (a: T, b: T) => boolean = (a, b) => a === b;
    @Input() public formatValue: (value: T) => string = (value) => String(value);
    @Input() public valueTypeCheck: (value: any) => value is T = (value): value is T => !!value;
    @Input() public control = TagBoxComponent.buildFormControl<T>();

    @Output() public valueAdded = new EventEmitter<T>();
    @Output() public valueRemoved = new EventEmitter<T>();

    @Input() public chipContentTemplate?: TemplateRef<{ $implicit: T }>;

    protected valueControl = new FormControl<string | T>("");
    private allValues$ = new BehaviorSubject<T[]>([]);
    private selectedValues$ = new BehaviorSubject<T[]>([]);
    protected availableValues$: Observable<T[]>;

    public constructor() {
        this.availableValues$ = combineLatest([
            this.allValues$,
            this.selectedValues$,
            this.valueControl.valueChanges.pipe(startWith("")),
        ]).pipe(
            map(([allValues, selectedValues, inputText]) => {
                const lowerInputText =
                    typeof inputText === "string" ? inputText.toLocaleLowerCase() : "";
                let availableValues = allValues.filter((a) =>
                    selectedValues.every((b) => !this.isEqual(a, b)),
                );
                availableValues = availableValues.filter((t) => {
                    return this.formatValue(t).toLocaleLowerCase().includes(lowerInputText);
                });
                return availableValues;
            }),
        );
    }

    public static buildFormControl<T>(values: T[] = []) {
        return new FormControl<T[]>(values, { nonNullable: true });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.allValues) {
            this.allValues$.next(this.allValues ?? []);
        }

        if (changes.selectedValues) {
            this.control.setValue(this.selectedValues ?? []);
            this.selectedValues$.next(this.control.value);
        }

        if (changes.control) {
            this.selectedValues$.next(this.control.value);
        }
    }

    public removeValue(value: T) {
        this.control.setValue(this.control.value.filter((v) => !this.isEqual(v, value)));
        this.selectedValues$.next(this.control.value);
        this.valueControl.setValue("");
        this.valueRemoved.emit(value);
    }

    public addValue(event: MatAutocompleteSelectedEvent) {
        const value = event.option.value;
        if (this.valueTypeCheck(value)) {
            this.control.setValue([...this.control.value, value]);
            this.selectedValues$.next(this.control.value);
            this.valueControl.setValue("");
            this.valueAdded.emit(value);
        }
    }
}
