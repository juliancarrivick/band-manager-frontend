import { Directive, ElementRef, Input, OnDestroy, OnInit } from "@angular/core";
import { FormGroup, FormGroupDirective } from "@angular/forms";
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";
import { BreezeFormControl } from "./breeze-form-control";

@Directive({
    selector: "mat-error[appBreezeFormControlError]",
    standalone: false,
})
export class BreezeFormControlErrorDirective implements OnInit, OnDestroy {
    // eslint-disable-next-line @angular-eslint/no-input-rename
    @Input("appBreezeFormError") control?: BreezeFormControl<any, any>;
    private subscription?: Subscription;

    public constructor(private elementRef: ElementRef<HTMLElement>) {}

    public ngOnInit() {
        if (!this.control) {
            throw new Error("Must be defined");
        }

        this.subscription = this.control.statusChanges
            .pipe(startWith(this.control.status))
            .subscribe(() => {
                if (this.control?.invalid && this.control.errors) {
                    this.elementRef.nativeElement.innerText = Object.values(this.control.errors)
                        .filter((e) => typeof e === "string")
                        .join(", ");
                } else {
                    this.elementRef.nativeElement.innerText = "";
                }
            });
    }

    public ngOnDestroy() {
        this.subscription?.unsubscribe();
    }
}

@Directive({
    selector: "mat-error[appBreezeFormControlNameError]",
    standalone: false,
})
export class BreezeFormControlNameErrorDirective implements OnInit, OnDestroy {
    @Input("appBreezeFormControlNameError") controlName?: string;
    private subscription?: Subscription;

    public constructor(
        private elementRef: ElementRef<HTMLElement>,
        private formGroup: FormGroupDirective,
    ) {}

    public ngOnInit() {
        if (!this.controlName || !(this.formGroup.form instanceof FormGroup)) {
            throw new Error("Must be defined & be a FormGroup");
        }

        const control = this.formGroup.form.get(this.controlName);
        if (!control || !(control instanceof BreezeFormControl)) {
            throw new Error("Control not found or is not a BreezeFormControl");
        }

        this.subscription = control.statusChanges.pipe(startWith(control.status)).subscribe(() => {
            if (control.invalid && control.errors) {
                this.elementRef.nativeElement.innerText = Object.values(control.errors)
                    .filter((e) => typeof e === "string")
                    .join(", ");
            } else {
                this.elementRef.nativeElement.innerText = "";
            }
        });
    }

    public ngOnDestroy() {
        this.subscription?.unsubscribe();
    }
}
