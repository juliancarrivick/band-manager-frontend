import { Directive, HostBinding, HostListener, Input, OnChanges } from "@angular/core";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { StorageService } from "./storage.service";

@Directive({
    selector: "img[appBlobImage]",
    standalone: false,
})
export class BlobImageDirective implements OnChanges {
    @Input("appBlobImage") public blobId?: string;

    private _objectUrl?: string;
    @HostBinding("src") public safeObjectUrl?: SafeUrl;

    public constructor(
        private storageService: StorageService,
        private sanitizer: DomSanitizer,
    ) {}

    private get objectUrl() {
        return this._objectUrl;
    }
    private set objectUrl(value: string | undefined) {
        this._objectUrl = value;
        this.safeObjectUrl = value ? this.sanitizer.bypassSecurityTrustUrl(value) : undefined;
    }

    public ngOnChanges(): void {
        if (this.blobId) {
            this.storageService.getBlobContent(this.blobId).subscribe((blob) => {
                this.objectUrl = URL.createObjectURL(blob);
            });
        } else {
            this.objectUrl = undefined;
        }
    }

    @HostListener("load")
    public revokeObjectUrl() {
        if (!this.objectUrl) {
            return;
        }

        URL.revokeObjectURL(this.objectUrl);
    }
}
