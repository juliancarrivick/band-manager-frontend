import { ColumnAction } from "app/common-ux/data-grid/grid-edit-column";
import { map, Observable, tap } from "rxjs";
import { EntityAttachmentMetadata } from "./entity-attachment-metadata";

export function buildGridAttachmentActions<TEntityWithMetadata>(options: {
    source: Observable<TEntityWithMetadata[] | undefined>;
    getAttachmentCount: (entityWithMetadata: TEntityWithMetadata) => number;
    setAttachmentCount: (entityWithMetdata: TEntityWithMetadata, count: number) => void;
    allowEditing: () => boolean;
    openAttachmentDialog: (
        entityWithMetdata: TEntityWithMetadata,
    ) => Observable<EntityAttachmentMetadata[]>;
}) {
    return options.source.pipe(
        map((entities) => {
            if (!entities) {
                return [];
            }

            const numWithAttachments = entities
                .map(options.getAttachmentCount)
                .filter((e) => e > 0).length;
            if (!options.allowEditing() && numWithAttachments === 0) {
                return [];
            }

            const action: ColumnAction<TEntityWithMetadata> = {
                icon: "attach_file",
                tooltip: "0 attachments",
                onClick: (entityWithMetadata) =>
                    options
                        .openAttachmentDialog(entityWithMetadata)
                        .pipe(
                            tap((attachments) =>
                                options.setAttachmentCount(entityWithMetadata, attachments.length),
                            ),
                        ),
                customiseElement: (button, entityWithMetadata) => {
                    const attachmentCount = options.getAttachmentCount(entityWithMetadata);
                    if (attachmentCount === 0) {
                        if (!options.allowEditing()) {
                            button.style.display = "none";
                        }

                        return;
                    }

                    button.title = `${attachmentCount} attachment`;
                    if (attachmentCount > 1) {
                        button.title += "s";
                    }

                    const badge = document.createElement("span");
                    badge.classList.add("attachment-badge");
                    badge.innerText = attachmentCount.toString();
                    badge.setAttribute("aria-describedby", badge.title);
                    button.append(badge);
                },
            };
            return [action];
        }),
    );
}
