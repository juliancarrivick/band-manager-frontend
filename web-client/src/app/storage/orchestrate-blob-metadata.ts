export interface OrchestrateBlobMetadata<TAccess = object> {
    id: string;
    name: string;
    mimeType: string;
    modified: Date;
    sizeBytes: number;
    accessStrategy: {
        name: string;
        accessData: TAccess;
    };
}
