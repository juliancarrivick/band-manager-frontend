import { HttpClient, HttpEventType } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { formatBytes } from "app/common/utils";
import { map, startWith, switchMap, throwError } from "rxjs";
import { EntityAttachmentSensitivity } from "./entity-attachment-metadata";
import { EntityAttachmentService } from "./entity-attachment.service";
import { OrchestrateBlobMetadata } from "./orchestrate-blob-metadata";

export interface UploadBlobDto<TAccess = object | undefined, TLink = object | undefined> {
    storageStrategyName: string;
    accessData: TAccess;
    linkData: TLink;
}

export enum UploadStatus {
    Pending,
    InProgress,
    Done,
}

export interface UploadProgress {
    status: Exclude<UploadStatus, UploadStatus.Done>;
    progress?: number;
}

export interface UploadResult<TAccess = object | undefined> {
    status: UploadStatus.Done;
    progress: 100;
    result: OrchestrateBlobMetadata<TAccess>;
}

@Injectable()
export class StorageService {
    public constructor(
        private httpClient: HttpClient,
        private appConfig: AppConfig,
    ) {}

    public uploadBlob<TAccess = object | undefined, TLink = object | undefined>(
        dto: UploadBlobDto<TAccess, TLink>,
        file: File,
    ) {
        if (file.size > this.appConfig.maxUploadSizeBytes) {
            const formattedMaxSize = formatBytes(this.appConfig.maxUploadSizeBytes, 0);
            return throwError(() => new Error(`File cannot be larger than ${formattedMaxSize}`));
        }

        return this.appConfig.serverEndpoint(`blob`).pipe(
            switchMap((endpoint) => {
                const formData = new FormData();
                formData.append(`file`, file, file.name);

                for (const entry of Object.entries(dto)) {
                    const name = entry[0];
                    const data = typeof entry[1] === "object" ? JSON.stringify(entry[1]) : entry[1];
                    formData.append(name, data ?? null);
                }

                return this.httpClient.post<[OrchestrateBlobMetadata<TAccess>]>(
                    endpoint,
                    formData,
                    {
                        reportProgress: true,
                        observe: "events",
                    },
                );
            }),
            map((event) => {
                if (event.type === HttpEventType.UploadProgress) {
                    if (!event.total || event.total === event.loaded) {
                        return { status: UploadStatus.InProgress } as UploadProgress;
                    } else {
                        return {
                            status: UploadStatus.InProgress,
                            progress: event.total
                                ? Math.round((100 * event.loaded) / event.total)
                                : undefined,
                        } as UploadProgress;
                    }
                } else if (event.type === HttpEventType.Response) {
                    return {
                        status: UploadStatus.Done,
                        progress: 100,
                        result: event.body![0],
                    } as UploadResult<TAccess>;
                } else {
                    return { status: UploadStatus.Pending } as UploadProgress;
                }
            }),
            startWith({ status: UploadStatus.Pending } as UploadProgress),
        );
    }

    public listBlobsLinkedBy<TAccess = object | undefined>(
        storageStrategyName: string,
        linkData?: object,
    ) {
        return this.appConfig.serverEndpoint(`blob`).pipe(
            switchMap((endpoint) =>
                this.httpClient.get<OrchestrateBlobMetadata<TAccess>[]>(endpoint, {
                    params: {
                        storageStrategyName,
                        linkData: JSON.stringify(linkData),
                    },
                }),
            ),
        );
    }

    public getBlobContent(blobId: string) {
        return this.appConfig.serverEndpoint(`blob/${blobId}/file`).pipe(
            switchMap((endpoint) =>
                this.httpClient.get(endpoint, {
                    responseType: "blob",
                }),
            ),
        );
    }

    public updateAccessData(blobId: string, accessData?: object) {
        return this.appConfig
            .serverEndpoint(`blob/${blobId}/access-data`)
            .pipe(switchMap((endpoint) => this.httpClient.put(endpoint, accessData)));
    }

    public delete(blobId: string) {
        return this.appConfig
            .serverEndpoint(`blob/${blobId}`)
            .pipe(switchMap((endpoint) => this.httpClient.delete(endpoint)));
    }

    public buildEntityAttachmentService(
        storageStrategyName: string,
        entityId: number,
        defaultSensitivity: EntityAttachmentSensitivity,
    ) {
        return new EntityAttachmentService(this, storageStrategyName, entityId, defaultSensitivity);
    }
}
