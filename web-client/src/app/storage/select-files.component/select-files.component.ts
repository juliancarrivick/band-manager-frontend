import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    HostBinding,
    HostListener,
    Input,
    OnDestroy,
    Output,
    ViewChild,
} from "@angular/core";
import { Subscription, fromEvent, merge } from "rxjs";

@Component({
    selector: "app-select-files",
    template: `
        Click or drag files here
        <input
            #fileInput
            type="file"
            multiple
            [accept]="accept"
            hidden
        />
    `,
    styles: [
        `
            :host {
                display: flex;
                align-items: center;
                justify-content: center;
                width: 100%;
                height: 100%;
                border: 2px dashed var(--gray-400);
                cursor: pointer;
                padding: 1rem 2rem;
            }

            :host.files-hovering {
                background: var(--gray-100);
                border-color: var(--accent-500);
            }
        `,
    ],
    standalone: false,
})
export class SelectFilesComponent implements AfterViewInit, OnDestroy {
    @Input() accept?: string;
    @Output() public fileChange = new EventEmitter<File[]>();
    @ViewChild("fileInput") public fileInput!: ElementRef<HTMLInputElement>;
    @HostBinding("class.files-hovering") public filesHovering = false;

    private subscription: Subscription;

    public constructor() {
        this.subscription = merge(
            fromEvent<DragEvent>(window, "dragover"),
            fromEvent<DragEvent>(window, "drop"),
        ).subscribe((event) => {
            event.preventDefault();

            // This would be a race condition if we have multiple file uploads on the same page
            // Deal with that when we hit it :) Maybe we just disable globally?
            if (event.dataTransfer) {
                if (this.filesHovering) {
                    event.dataTransfer.dropEffect = "copy";
                } else {
                    event.dataTransfer.dropEffect = "none";
                }
            }
        });
    }

    public ngAfterViewInit(): void {
        // Massive delays randomly on files when we add this via a (change) handler in the HTML
        // Manually doing it here seems to solve the issue.
        this.fileInput.nativeElement.addEventListener("change", this.onFileSelected);
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
        this.fileInput.nativeElement.removeEventListener("change", this.onFileSelected);
    }

    public onFileSelected = (event: Event) => {
        const input = event.target as HTMLInputElement;
        if (!input.files || input.files.length === 0) {
            return;
        }

        const files = Array.from(input.files);
        this.fileChange.emit(files);
        input.value = "";
    };

    @HostListener("click")
    public onClick() {
        this.fileInput.nativeElement.click();
    }

    @HostListener("dragenter")
    @HostListener("pointerenter")
    public onEnter() {
        this.filesHovering = true;
    }

    @HostListener("dragleave")
    @HostListener("pointerleave")
    public onLeave() {
        this.filesHovering = false;
    }

    @HostListener("drop", ["$event"])
    public onDrop(event: DragEvent) {
        event.preventDefault();

        if (!event.dataTransfer || event.dataTransfer.files.length === 0) {
            return;
        }
        const files = Array.from(event.dataTransfer.files);
        this.fileChange.emit(files);
    }
}
