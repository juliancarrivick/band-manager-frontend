import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
} from "@angular/core";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { getErrorMessage } from "app/common/error";
import { StorageService, UploadBlobDto, UploadStatus } from "../storage.service";

@Component({
    selector: "app-select-image",
    templateUrl: "./select-image.component.html",
    styleUrls: ["./select-image.component.scss"],
    standalone: false,
})
export class SelectImageComponent implements OnInit, OnChanges {
    @Input() public blobId?: string | null;
    @Output() public blobIdChange = new EventEmitter<string | null>();

    @Input() public uploadDto!: UploadBlobDto;

    private _objectUrl?: string;
    public safeObjectUrl?: SafeUrl;
    public uploadInProgress = false;
    public uploadProgress?: number;
    public error?: string;

    public constructor(
        private storageService: StorageService,
        private sanitizer: DomSanitizer,
    ) {}

    private get objectUrl() {
        return this._objectUrl;
    }
    private set objectUrl(value: string | undefined) {
        this._objectUrl = value;
        this.safeObjectUrl = value ? this.sanitizer.bypassSecurityTrustUrl(value) : undefined;
    }

    public ngOnInit(): void {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        if (!this.uploadDto) {
            throw new Error("UploadDto must be defined");
        }
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.blobId) {
            this.setImageFromBlobId();
        }
    }

    public updateSelectedImage(input: HTMLInputElement) {
        this.error = undefined;

        if (!input.files || input.files.length !== 1) {
            this.setImageFromBlobId();
            return;
        }

        const selectedFile = input.files[0]!;
        this.objectUrl = URL.createObjectURL(selectedFile);

        this.uploadInProgress = true;
        this.storageService.uploadBlob(this.uploadDto, selectedFile).subscribe({
            next: (event) => {
                this.uploadProgress = event.progress;
                if (event.status === UploadStatus.Done) {
                    this.blobId = event.result.id;
                    this.blobIdChange.emit(this.blobId);
                    this.setImageFromBlobId();
                }
            },
            error: (error) => {
                this.error = getErrorMessage(error, "Unknown error uploading, please try again");
                this.uploadInProgress = false;
            },
            complete: () => {
                this.uploadInProgress = false;
            },
        });
    }

    private setImageFromBlobId() {
        if (this.blobId) {
            this.storageService.getBlobContent(this.blobId).subscribe((blob) => {
                this.objectUrl = URL.createObjectURL(blob);
            });
        } else {
            this.objectUrl = undefined;
        }
    }

    public deleteImage() {
        if (!this.blobId) {
            return;
        }

        this.blobId = undefined;
        this.objectUrl = undefined;
        this.blobIdChange.emit(undefined);
    }

    public revokeObjectUrl() {
        if (!this.objectUrl) {
            return;
        }

        URL.revokeObjectURL(this.objectUrl);
    }
}
