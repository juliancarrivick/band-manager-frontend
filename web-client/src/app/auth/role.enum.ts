export enum RoleName {
    Admin = "Administrator",
    BillingAdmin = "BillingAdministrator",
    CommitteeMember = "CommitteeMember",
    Editor = "Editor",
    Viewer = "Viewer",
}
