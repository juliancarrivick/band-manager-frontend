import { Directive, HostBinding, HostListener, Input, OnChanges } from "@angular/core";
import { Router, UrlTree } from "@angular/router";

@Directive({
    standalone: true,
    selector: "[appUrlTreeRouterLink]",
})
export class UrlTreeRouterLinkDirective implements OnChanges {
    @Input("appUrlTreeRouterLink") public urlTree?: UrlTree;

    @HostBinding("href") public href: string | null = null;

    public constructor(private router: Router) {}

    public ngOnChanges(): void {
        this.href = this.urlTree ? this.router.serializeUrl(this.urlTree) : null;
    }

    @HostListener("click", ["$event"])
    public async onClick(event: Event) {
        if (this.urlTree) {
            event.preventDefault();
            await this.router.navigateByUrl(this.urlTree);
        }
    }
}
