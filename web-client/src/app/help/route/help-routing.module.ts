import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AppRoutes } from "app/common/app-routing-types";
import { GetStartedHelpComponent } from "./get-started-help/get-started-help.component";
import { HelpContainerComponent } from "./help-container/help-container.component";

export const routes: AppRoutes = [
    {
        path: "",
        component: HelpContainerComponent,
        children: [
            {
                path: "get-started",
                component: GetStartedHelpComponent,
            },
            {
                path: "ensembles",
                loadComponent: () =>
                    import(
                        "../../tenant/feature/ensemble/ensemble-help/ensemble-help.component"
                    ).then((m) => m.EnsembleHelpComponent),
            },
            {
                path: "members",
                loadComponent: () =>
                    import("app/tenant/feature/member/member-help/member-help.component").then(
                        (m) => m.MemberHelpComponent,
                    ),
            },
            {
                path: "music-library",
                loadComponent: () =>
                    import(
                        "app/tenant/feature/score/music-library-help/music-library-help.component"
                    ).then((m) => m.MusicLibraryHelpComponent),
            },
            {
                path: "concerts",
                loadComponent: () =>
                    import("app/tenant/feature/concert/concert-help/concert-help.component").then(
                        (m) => m.ConcertHelpComponent,
                    ),
            },
            {
                path: "member-billing",
                loadComponent: () =>
                    import(
                        "app/tenant/feature/member-billing/member-billing-help/member-billing-help.component"
                    ).then((m) => m.MemberBillingHelpComponent),
            },
            {
                path: "assets",
                loadComponent: () =>
                    import("app/tenant/feature/asset/asset-help/asset-help.component").then(
                        (m) => m.AssetHelpComponent,
                    ),
            },
            {
                path: "mailing-lists",
                loadComponent: () =>
                    import(
                        "app/tenant/feature/mailing-lists/mailing-list-help/mailing-list-help.component"
                    ).then((m) => m.MailingListHelpComponent),
            },
            {
                path: "contacts",
                loadComponent: () =>
                    import("app/tenant/feature/contacts/contact-help/contact-help.component").then(
                        (m) => m.ContactHelpComponent,
                    ),
            },
            {
                path: "settings",
                loadComponent: () =>
                    import(
                        "app/tenant/feature/settings/settings-help/settings-help.component"
                    ).then((m) => m.SettingsHelpComponent),
            },
        ],
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppHelpRoutingModule {}
