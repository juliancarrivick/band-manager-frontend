import { STEPPER_GLOBAL_OPTIONS, StepperSelectionEvent } from "@angular/cdk/stepper";
import { Component, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { buildHelpUrlTree, buildPrimaryAndHelpUrlTree } from "app/common/routing-utils";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { first, map, Observable, startWith, switchMap } from "rxjs";
import { allSteps, GetStartedStep, GetStartedStepData } from "./steps";

const stepParam = "step";

const stepCompletionKey = "getStartedCompletion";
type GetStartedStepString = keyof typeof GetStartedStep;
type StepCompletion = Partial<Record<GetStartedStepString, boolean>>;

@Component({
    selector: "app-get-started-help",
    templateUrl: "./get-started-help.component.html",
    styleUrls: ["./get-started-help.component.scss"],
    providers: [
        {
            provide: STEPPER_GLOBAL_OPTIONS,
            useValue: { displayDefaultIndicatorType: false },
        },
    ],
    standalone: false,
})
export class GetStartedHelpComponent implements OnDestroy {
    public steps = allSteps;
    public isFirstRun$: Observable<boolean>;
    public step$: Observable<GetStartedStep>;
    public stepCompletion: StepCompletion;

    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        private tenantAuthorisation: TenantAuthorisationService,
    ) {
        this.isFirstRun$ = route.paramMap.pipe(
            map((params) => params.get("isFirstRun") === "true"),
        );
        this.step$ = route.paramMap.pipe(
            startWith(route.snapshot.paramMap),
            map((params) => params.get(stepParam)),
            map((stepKey) =>
                Object.values(GetStartedStep).includes(stepKey ?? "")
                    ? GetStartedStep[stepKey as GetStartedStepString]
                    : GetStartedStep.Welcome,
            ),
        );
        this.stepCompletion = this.getCompletionFromLocalStorage();
    }

    public async stepChange(event: StepperSelectionEvent) {
        const previousStep = GetStartedStep[event.previouslySelectedIndex];
        if (previousStep) {
            this.stepCompletion[previousStep as GetStartedStepString] = true;
        }

        const step = GetStartedStep[event.selectedIndex];
        if (step) {
            await this.router.navigate(
                [this.mergeExistingAndNewRouteParams({ [stepParam]: step })],
                { relativeTo: this.route, replaceUrl: true },
            );
        }
    }

    public ngOnDestroy(): void {
        localStorage.setItem(stepCompletionKey, JSON.stringify(this.stepCompletion));
    }

    public isStepCompleted(step: GetStartedStep) {
        const stepString = GetStartedStep[step] as GetStartedStepString;
        return this.stepCompletion[stepString];
    }

    public navigateToStepCallToAction(data: GetStartedStepData) {
        const stepString = GetStartedStep[data.step] as GetStartedStepString;
        this.stepCompletion[stepString] = true;

        const stepIndex = allSteps.indexOf(data);
        const nextStep =
            0 <= stepIndex && stepIndex < allSteps.length - 1
                ? allSteps[stepIndex + 1]!.step
                : GetStartedStep.Done;
        const helpCommands = [
            ...data.callToActionHelpRouteCommands,
            { continueTour: this.getThisUrlForStep(nextStep) },
        ];

        this.tenantAuthorisation
            .hasPermission(data.readPermission)
            .pipe(
                first(),
                map((hasReadPermission) => {
                    if (hasReadPermission) {
                        return buildPrimaryAndHelpUrlTree(
                            this.route.snapshot,
                            data.callToActionPrimaryRouteCommands,
                            helpCommands,
                        );
                    } else {
                        return buildHelpUrlTree(this.route.snapshot, helpCommands);
                    }
                }),
                switchMap((urlTree) => this.router.navigateByUrl(urlTree)),
            )
            .subscribe();
    }

    private mergeExistingAndNewRouteParams(newParams = {}) {
        return {
            ...this.route.snapshot.params,
            ...newParams,
        };
    }

    public async closeHelp() {
        const urlTree = buildHelpUrlTree(this.route.snapshot, null);
        await this.router.navigateByUrl(urlTree);
    }

    private getThisUrlForStep(step: GetStartedStep) {
        const stepString = GetStartedStep[step];
        const currParams = this.route.snapshot.params;
        const params = stepString ? { ...currParams, [stepParam]: stepString } : currParams;
        const urlTree = buildHelpUrlTree(this.route.snapshot, ["view", "get-started", params]);
        return this.router.serializeUrl(urlTree);
    }

    private getCompletionFromLocalStorage(): StepCompletion {
        try {
            const rawCompletion = localStorage.getItem(stepCompletionKey);
            if (!rawCompletion) {
                return {};
            }

            const localStorageCompletion = JSON.parse(rawCompletion);
            return typeof localStorageCompletion === "object" && localStorageCompletion !== null
                ? (localStorageCompletion as StepCompletion)
                : {};
        } catch {
            return {};
        }
    }
}
