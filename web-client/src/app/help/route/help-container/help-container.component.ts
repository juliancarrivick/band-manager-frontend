import { Component } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { AppRouteData } from "app/common/app-routing-types";
import { getCurrentPrimaryRouteLeaf } from "app/common/routing-utils";
import { filter, map, Observable, startWith, switchMap } from "rxjs";
import { allHelpPages } from "../help-list";
import { HelpPage } from "../help-page";

@Component({
    selector: "app-help-container",
    templateUrl: "./help-container.component.html",
    styleUrls: ["./help-container.component.scss"],
    standalone: false,
})
export class HelpContainerComponent {
    public allHelp = allHelpPages;
    public currentPageHelp$: Observable<HelpPage[]>;

    public constructor(router: Router, route: ActivatedRoute) {
        this.currentPageHelp$ = router.events.pipe(
            filter((e) => e instanceof NavigationEnd),
            map(() => route),
            startWith(route),
            map((r) => getCurrentPrimaryRouteLeaf(r)),
            switchMap((r) => r.data),
            map((routeData: AppRouteData) => {
                return Array.isArray(routeData.help) ? routeData.help : [];
            }),
        );
    }
}
