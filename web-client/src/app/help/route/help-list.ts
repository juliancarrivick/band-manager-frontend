import { HelpPage } from "./help-page";

export const helpPages = {
    helpCenter: {
        name: "Help Center",
        commands: ["view"],
    },
    getStarted: {
        name: "Get Started",
        commands: ["view", "get-started"],
    },
    ensembles: {
        name: "Ensembles",
        commands: ["view", "ensembles"],
    },
    members: {
        name: "Members",
        commands: ["view", "members"],
    },
    musicLibrary: {
        name: "Music Library",
        commands: ["view", "music-library"],
    },
    concerts: {
        name: "Concerts",
        commands: ["view", "concerts"],
    },
    memberBilling: {
        name: "Member Billing",
        commands: ["view", "member-billing"],
    },
    assets: {
        name: "Assets",
        commands: ["view", "assets"],
    },
    mailingLists: {
        name: "Mailing Lists",
        commands: ["view", "mailing-lists"],
    },
    contacts: {
        name: "Contacts",
        commands: ["view", "contacts"],
    },
    settings: {
        name: "Settings",
        commands: ["view", "settings"],
    },
};

export const allHelpPages: readonly HelpPage[] = Object.values(helpPages)
    .filter((h) => h !== helpPages.helpCenter)
    .sort((a, b) => a.name.localeCompare(b.name));
