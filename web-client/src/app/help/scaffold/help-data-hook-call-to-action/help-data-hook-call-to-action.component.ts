import { Component, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ResponsiveService } from "app/common-ux/responsive.service";
import {
    buildHelpUrlTree,
    buildPrimaryAndHelpUrlTree,
    buildPrimaryUrlTree,
} from "app/common/routing-utils";
import { HelpDataHooks } from "app/help/help-broker.service";
import { first, map, switchMap } from "rxjs";

@Component({
    selector: "app-help-data-hook-call-to-action",
    templateUrl: "./help-data-hook-call-to-action.component.html",
    styleUrls: ["./help-data-hook-call-to-action.component.scss"],
    standalone: false,
})
export class HelpDataHookCallToActionComponent {
    @Input() public dataHooks?: HelpDataHooks | null;
    @Input() public callToActionText = "Add";

    public constructor(
        private router: Router,
        private route: ActivatedRoute,
        private responsiveService: ResponsiveService,
    ) {}

    public addData(dataHooks: HelpDataHooks) {
        this.responsiveService.isXl$
            .pipe(
                first(),
                map((isXl) => {
                    const addDataReturn = dataHooks.addData();

                    if (Array.isArray(addDataReturn)) {
                        return isXl
                            ? buildPrimaryUrlTree(this.route.snapshot, addDataReturn)
                            : buildPrimaryAndHelpUrlTree(this.route.snapshot, addDataReturn, null);
                    } else if (!isXl) {
                        return buildHelpUrlTree(this.route.snapshot, null);
                    }
                }),
                switchMap(async (urlTree) => {
                    if (urlTree) {
                        await this.router.navigateByUrl(urlTree);
                    }
                }),
            )
            .subscribe();
    }
}
