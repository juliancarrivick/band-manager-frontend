import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatDividerModule } from "@angular/material/divider";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { AppAuthModule } from "app/auth/auth.module";
import { HelpRouterLinkDirective } from "app/common-ux/router/help-router-link.directive";
import { UrlTreeRouterLinkDirective } from "../url-tree-router-link.directive";
import { HelpCenterPageComponent } from "./help-center-page/help-center-page.component";
import { HelpDataHookCallToActionComponent } from "./help-data-hook-call-to-action/help-data-hook-call-to-action.component";

@NgModule({
    imports: [
        CommonModule,

        MatIconModule,
        MatButtonModule,
        MatToolbarModule,
        MatDividerModule,

        AppAuthModule,

        HelpRouterLinkDirective,
        UrlTreeRouterLinkDirective,
    ],
    exports: [
        HelpCenterPageComponent,
        HelpDataHookCallToActionComponent,

        HelpRouterLinkDirective,
        UrlTreeRouterLinkDirective,
    ],
    declarations: [
        HelpCenterPageComponent,
        HelpDataHookCallToActionComponent,
    ],
})
export class AppHelpScaffoldModule {}
