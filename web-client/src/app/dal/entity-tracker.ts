import { EntityState } from "breeze-client";
import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";
import { BreezeDomainService } from "./breeze-domain.service";
import { BreezeEntity } from "./breeze-entity";
import { BreezeService } from "./breeze.service";

export class EntityTracker {
    private trackedEntities = new Set<BreezeEntity>();

    public constructor(private breezeService: BreezeService | BreezeDomainService) {}

    public get validEntities() {
        return this.trackedEntitiesAsArray().filter(
            (e) => !e.entityAspect.entityState.isDetached(),
        );
    }

    public add(...entities: BreezeEntity[]) {
        for (const e of entities) {
            this.trackedEntities.add(e);
        }
    }

    public hasChanges() {
        return this.trackedEntitiesAsArray().some(
            (e) => e.entityAspect.entityState !== EntityState.Unchanged,
        );
    }

    public hasValidationErrors() {
        return this.trackedEntitiesAsArray().some((e) => e.entityAspect.hasValidationErrors);
    }

    public saveChanges() {
        const save$: Observable<unknown> = this.breezeService.saveChanges(...this.validEntities);
        return save$.pipe(
            map(() => {
                // Force void return type
                this.trackedEntities = new Set<BreezeEntity>(this.validEntities);
            }),
        );
    }

    public cancelChanges() {
        // Cancelling changes on an added entity doesn't delete them
        const addedEntities = this.validEntities.filter((e) =>
            e.entityAspect.entityState.isAdded(),
        );
        for (const entity of addedEntities) {
            this.breezeService.delete(entity);
        }

        return this.breezeService
            .cancelChanges(...this.validEntities)
            .pipe(tap(() => (this.trackedEntities = new Set<BreezeEntity>(this.validEntities))));
    }

    private trackedEntitiesAsArray(): readonly BreezeEntity[] {
        return Array.from(this.trackedEntities);
    }
}
