#!/bin/bash

pushd api/

dotnet tool install --global dotnet-ef --version 9.0
export PATH="$PATH:/home/vscode/.dotnet/tools"
cat << \EOF >> ~/.bashrc
# Add .NET Core SDK tools
export PATH="$PATH:/home/vscode/.dotnet/tools"
EOF

dotnet restore orchestrate-api.sln

API_CONFIG=OrchestrateApi/appsettings.Development.json
if [[ ! -f $API_CONFIG ]]; then
    cp OrchestrateApi/appsettings.json $API_CONFIG
    sed -i 's/<Postgres URL>/postgres:\/\/postgres:postgres@localhost\/postgres/g' $API_CONFIG
fi

TEST_CONFIG=OrchestrateApi.Tests/appsettings.Testing.json
if [[ ! -f $TEST_CONFIG ]]; then
    cp OrchestrateApi.Tests/appsettings.json $TEST_CONFIG
    sed -i 's/user/postgres/g' $TEST_CONFIG
    sed -i 's/pass/postgres/g' $TEST_CONFIG
fi

pushd OrchestrateApi
dotnet ef database update
popd
popd

pushd web-client/

npm install
echo '{}' > src/app/dal/breeze-metadata.json

# Create real config and delete lines starting with comment
WEB_CONFIG=src/app-config.json
if [[ ! -f $WEB_CONFIG ]]; then
    cp src/app-config.template.json $WEB_CONFIG
    sed -i '/^\s*\/\//d' $WEB_CONFIG
fi

popd
